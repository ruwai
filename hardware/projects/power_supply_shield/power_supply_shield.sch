EESchema Schematic File Version 2
LIBS:power_supply_shield-rescue
LIBS:ruwai
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:power_supply_shield-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title "Ruwai ADC Main Shield"
Date "2017-09-17"
Rev "3"
Comp "Copyright Mertl Research GmbH 2014 "
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 "This documentation describes Open Hardware and is licensed under the CERN OHL v. 1.2."
$EndDescr
$Sheet
S 8200 3050 1150 1500
U 5480666D
F0 "Interface" 60
F1 "interface.sch" 60
F2 "+15V_PGA" I L 8200 3400 60 
F3 "-15V_PGA" I L 8200 3550 60 
F4 "5.1V" I L 8200 3250 60 
F5 "temp" I L 8200 3700 60 
$EndSheet
$Sheet
S 2400 3050 1150 1500
U 5480667B
F0 "Main supply" 60
F1 "main_supply.sch" 60
F2 "5.1V" O R 3550 3250 60 
F3 "temp" O R 3550 3450 60 
$EndSheet
$Sheet
S 5450 3050 1150 1500
U 54806694
F0 "PGA supply" 60
F1 "pga_supply.sch" 60
F2 "5.1V" I L 5450 3250 60 
F3 "+15V_PGA" O R 6600 3400 60 
F4 "-15V_PGA" O R 6600 3550 60 
$EndSheet
Wire Wire Line
	3550 3250 5450 3250
Wire Wire Line
	6600 3400 8200 3400
Wire Wire Line
	6600 3550 8200 3550
Wire Wire Line
	5100 3250 5100 2650
Wire Wire Line
	5100 2650 8000 2650
Wire Wire Line
	8000 2650 8000 3250
Wire Wire Line
	8000 3250 8200 3250
Connection ~ 5100 3250
Wire Wire Line
	3550 3450 4050 3450
Wire Wire Line
	4050 3450 4050 5050
Wire Wire Line
	4050 5050 8000 5050
Wire Wire Line
	8000 5050 8000 3700
Wire Wire Line
	8000 3700 8200 3700
$EndSCHEMATC
