EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title "Ruwai Power Supply Two"
Date "2020-02-25"
Rev "1"
Comp "Copyright Mertl Research GmbH, 2020"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 "This documentation describes Open Hardware and is licensed under the CERN OHL v. 1.2."
$EndDescr
$Sheet
S 7950 1500 1100 1400
U 5E4E4EE5
F0 "logic_level_converter" 50
F1 "logic_level_converter.sch" 50
F2 "3.3V" I L 7950 1650 50 
F3 "5V" I L 7950 1800 50 
F4 "BBB_TX" I L 7950 2000 50 
F5 "ARD_RX" O R 9050 2000 50 
F6 "BBB_RX" O L 7950 2150 50 
F7 "ARD_TX" I R 9050 2150 50 
$EndSheet
$Sheet
S 1750 1400 1000 1400
U 5E50399A
F0 "Power Supply 5V" 50
F1 "power_supply_5V.sch" 50
F2 "V+5.1" O R 2750 1500 50 
F3 "GND+5.1" U R 2750 1650 50 
$EndSheet
$Sheet
S 4850 1500 1250 1400
U 5E5091DB
F0 "Power Supply 15V" 50
F1 "power_supply_15V.sch" 50
F2 "+Vin" I L 4850 1600 50 
F3 "-Vin" I L 4850 1750 50 
F4 "GND_15V" U R 6100 1900 50 
F5 "+15V" O R 6100 1600 50 
F6 "-15V" O R 6100 1750 50 
$EndSheet
Wire Wire Line
	2750 1500 2900 1500
Text Label 2900 1500 0    50   ~ 0
V+5.1
Wire Wire Line
	4850 1600 4700 1600
Text Label 4700 1600 2    50   ~ 0
V+5.1
$Comp
L power:GND #PWR01
U 1 1 5E51A233
P 2900 1750
F 0 "#PWR01" H 2900 1500 50  0001 C CNN
F 1 "GND" H 2905 1577 50  0000 C CNN
F 2 "" H 2900 1750 50  0001 C CNN
F 3 "" H 2900 1750 50  0001 C CNN
	1    2900 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 1650 2900 1650
Wire Wire Line
	2900 1650 2900 1750
$Comp
L power:GND #PWR02
U 1 1 5E51A7C0
P 4700 1850
F 0 "#PWR02" H 4700 1600 50  0001 C CNN
F 1 "GND" H 4705 1677 50  0000 C CNN
F 2 "" H 4700 1850 50  0001 C CNN
F 3 "" H 4700 1850 50  0001 C CNN
	1    4700 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 1850 4700 1750
Wire Wire Line
	4700 1750 4850 1750
$Sheet
S 2100 3700 1250 1900
U 5E50533F
F0 "BBB Interface" 50
F1 "BBB_interface.sch" 50
F2 "VDD_5V" I L 2100 3850 50 
F3 "3.3V" O R 3350 3850 50 
F4 "UART4_RXD" I R 3350 4250 50 
F5 "UART4_TXD" O R 3350 4400 50 
F6 "SYS_5V" O R 3350 4000 50 
$EndSheet
Wire Wire Line
	2100 3850 1900 3850
Text Label 1900 3850 2    50   ~ 0
V+5.1
Wire Wire Line
	3350 3850 3600 3850
Text Label 3600 3850 0    50   ~ 0
V+3.3_BBB
Text Label 3600 4250 0    50   ~ 0
BBB_RX
Text Label 3600 4400 0    50   ~ 0
BBB_TX
Wire Wire Line
	7950 1650 7750 1650
Text Label 7750 1650 2    50   ~ 0
V+3.3_BBB
Wire Wire Line
	7950 2000 7750 2000
Wire Wire Line
	7950 2150 7750 2150
Text Label 7750 2000 2    50   ~ 0
BBB_TX
Text Label 7750 2150 2    50   ~ 0
BBB_RX
Wire Wire Line
	3350 4250 3600 4250
Wire Wire Line
	3350 4400 3600 4400
Wire Wire Line
	3350 4000 3600 4000
Text Label 3600 4000 0    50   ~ 0
V+5_BBB
Wire Wire Line
	7950 1800 7750 1800
Text Label 7750 1800 2    50   ~ 0
V+5_BBB
$Comp
L ruwai:70543-0009 P1
U 1 1 5E55D822
P 7700 4150
F 0 "P1" H 7778 4191 50  0000 L CNN
F 1 "70543-0009" H 7778 4100 50  0000 L CNN
F 2 "Ruwai:Molex_SL_70543-0009" H 8300 3600 60  0001 C CNN
F 3 "" H 7700 4700 60  0000 C CNN
F 4 "Molex" H 7700 3450 60  0001 L CNN "Manu"
F 5 "70553-0009" H 7700 3350 60  0001 L CNN "Manu#"
F 6 "1x10 pins; 2.54mm pitch" H 7700 3250 60  0001 L CNN "Package"
F 7 "Molex 70553-0009 plug connector 1x10 pins, header, straight" H 7700 3150 60  0001 L CNN "Desc"
	1    7700 4150
	1    0    0    -1  
$EndComp
$Comp
L ruwai:70543-0001 P2
U 1 1 5E55E3F7
P 7750 5450
F 0 "P2" H 7672 5508 60  0000 R CNN
F 1 "70543-0001" H 7672 5402 60  0000 R CNN
F 2 "Ruwai:Molex_SL_70543-0001" H 7750 4750 60  0001 L CNN
F 3 "" H 7750 5450 60  0000 C CNN
F 4 "Molex" H 7750 5150 60  0001 L CNN "Manu"
F 5 "70543-0001" H 7750 5050 60  0001 L CNN "Manu#"
F 6 "1x2 pins; 2.54mm pitch; straight" H 7750 4950 60  0001 L CNN "Package"
F 7 "Molex plug connector 70553-0001 1x2 pins, header, straight" H 7750 4850 60  0001 L CNN "Desc"
	1    7750 5450
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7500 3700 7350 3700
Text Label 7350 3700 2    50   ~ 0
V+5.1
$Comp
L power:GND #PWR03
U 1 1 5E575E98
P 7000 4700
F 0 "#PWR03" H 7000 4450 50  0001 C CNN
F 1 "GND" H 7005 4527 50  0000 C CNN
F 2 "" H 7000 4700 50  0001 C CNN
F 3 "" H 7000 4700 50  0001 C CNN
	1    7000 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 3900 7000 4100
Wire Wire Line
	7000 3900 7500 3900
Wire Wire Line
	7000 3800 7000 3900
Wire Wire Line
	7000 3800 7500 3800
Connection ~ 7000 3900
Wire Wire Line
	7500 4000 7350 4000
Wire Wire Line
	7500 4200 7350 4200
Wire Wire Line
	7500 4400 7350 4400
Text Label 7350 4000 2    50   ~ 0
V+5.1
Wire Wire Line
	7000 4100 7500 4100
Connection ~ 7000 4100
Wire Wire Line
	7000 4100 7000 4300
Text Label 7350 4200 2    50   ~ 0
V-15
Text Label 7350 4400 2    50   ~ 0
V+15
Wire Wire Line
	7000 4300 7500 4300
Connection ~ 7000 4300
Wire Wire Line
	7000 4300 7000 4700
Wire Wire Line
	7450 5400 7300 5400
Wire Wire Line
	7450 5500 7300 5500
Text Label 7300 5400 2    50   ~ 0
ARD_RX
Text Label 7300 5500 2    50   ~ 0
ARD_TX
Wire Wire Line
	9050 2000 9250 2000
Wire Wire Line
	9050 2150 9250 2150
Text Label 9250 2000 0    50   ~ 0
ARD_RX
Text Label 9250 2150 0    50   ~ 0
ARD_TX
Wire Notes Line
	6900 3600 7950 3600
Wire Notes Line
	7950 3950 6900 3950
Text Notes 8000 3700 0    50   ~ 0
Arduino power supply
Wire Notes Line
	6900 4450 7950 4450
Wire Notes Line
	7950 3600 7950 4450
Wire Notes Line
	6900 3600 6900 4450
Text Notes 8000 4050 0    50   ~ 0
Stack power supply
Wire Wire Line
	6100 1600 6350 1600
Wire Wire Line
	6100 1750 6350 1750
Wire Wire Line
	6100 1900 6350 1900
Text Label 6350 1600 0    50   ~ 0
V+15
Text Label 6350 1750 0    50   ~ 0
V-15
$Comp
L power:GND #PWR0101
U 1 1 5E5BF939
P 6350 2000
F 0 "#PWR0101" H 6350 1750 50  0001 C CNN
F 1 "GND" H 6355 1827 50  0000 C CNN
F 2 "" H 6350 2000 50  0001 C CNN
F 3 "" H 6350 2000 50  0001 C CNN
	1    6350 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 1900 6350 2000
NoConn ~ 7500 4500
NoConn ~ 7500 4600
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5E5E1331
P 1900 3850
F 0 "#FLG0101" H 1900 3925 50  0001 C CNN
F 1 "PWR_FLAG" H 1900 4023 50  0000 C CNN
F 2 "" H 1900 3850 50  0001 C CNN
F 3 "~" H 1900 3850 50  0001 C CNN
	1    1900 3850
	1    0    0    -1  
$EndComp
$EndSCHEMATC
