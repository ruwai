EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 5
Title "Ruwai Power Supply Two"
Date "2020-02-25"
Rev "1"
Comp "Mertl Research GmbH"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 "This documentation describes Open Hardware and is licensed under the CERN OHL v. 1.2."
$EndDescr
$Comp
L ruwai:SN74LVC1T45 U1
U 1 1 5E4E5715
P 5100 2000
F 0 "U1" H 5100 2487 60  0000 C CNN
F 1 "SN74LVC1T45" H 5100 2381 60  0000 C CNN
F 2 "Ruwai:SOT23-6" H 5500 1700 60  0001 C CNN
F 3 "" H 5100 1750 60  0000 C CNN
F 4 "Texas Instruments" H 5100 1300 60  0001 L CNN "Manu"
F 5 "SN74LVC1T45DBVT" H 5100 1200 60  0001 L CNN "Manu#"
F 6 "SOT23 6-pin" H 5100 1100 60  0001 L CNN "Package"
F 7 "Single-Bit Dual-Supply Bus Transceiver with Configurable Voltage Translation and 3-State Outputs" H 5100 1000 60  0001 L CNN "Desc"
	1    5100 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 1800 4350 1800
Wire Wire Line
	4450 1950 2900 1950
$Comp
L ruwai:Resistor R1
U 1 1 5E4E99C3
P 3900 2150
F 0 "R1" V 3715 2150 40  0000 C CNN
F 1 "3.9k" V 3791 2150 40  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3830 2150 30  0001 C CNN
F 3 "" V 3980 2150 30  0000 C CNN
F 4 "Yageo" H 3900 1650 60  0001 L CNN "Manu"
F 5 "RC0805FR-073K9L" H 3900 1550 60  0001 L CNN "Manu#"
F 6 "0805" H 3900 1450 60  0001 L CNN "Package"
F 7 "SMD Chip Resistor, Thick Film, 3.9 kohm, 150 V, 0805 [2012 Metric], 125 mW, ± 1%, RC Series" H 3900 1350 60  0001 L CNN "Desc"
	1    3900 2150
	0    1    1    0   
$EndComp
$Comp
L ruwai:Capacitor C2
U 1 1 5E4EA23E
P 4350 2550
F 0 "C2" H 4465 2588 40  0000 L CNN
F 1 "0.1u" H 4465 2512 40  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4388 2400 30  0001 C CNN
F 3 "" H 4350 2650 60  0000 C CNN
F 4 "Würth Elektronik" H 4400 2150 60  0001 L CNN "Manu"
F 5 "885012207072" H 4400 2050 60  0001 L CNN "Manu#"
F 6 "0805" H 4400 1950 60  0001 L CNN "Package"
F 7 "Multilayer Ceramic Capacitors MLCC - SMD/SMT WCAP-CSGP 0.1uF 0805 10% 25V MLCC " H 4350 2200 60  0001 L CNN "Desc"
	1    4350 2550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5E4EB49D
P 5100 2950
F 0 "#PWR04" H 5100 2700 50  0001 C CNN
F 1 "GND" H 5105 2777 50  0000 C CNN
F 2 "" H 5100 2950 50  0001 C CNN
F 3 "" H 5100 2950 50  0001 C CNN
	1    5100 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 2350 4350 1800
Connection ~ 4350 1800
Wire Wire Line
	4350 1800 3500 1800
Wire Wire Line
	4350 2750 4350 2850
Wire Wire Line
	4350 2850 5100 2850
Wire Wire Line
	5100 2850 5100 2950
Wire Wire Line
	5100 2550 5100 2850
Connection ~ 5100 2850
Wire Wire Line
	5100 2850 6000 2850
Wire Wire Line
	6000 2850 6000 2750
Wire Wire Line
	5750 1800 6000 1800
Wire Wire Line
	5750 1950 6500 1950
Wire Wire Line
	6000 2350 6000 1800
Connection ~ 6000 1800
Wire Wire Line
	6000 1800 6500 1800
Wire Wire Line
	3650 2150 3500 2150
Wire Wire Line
	3500 2150 3500 1800
Connection ~ 3500 1800
Wire Wire Line
	3500 1800 2900 1800
$Comp
L ruwai:SN74LVC1T45 U2
U 1 1 5E4F07C8
P 5100 4550
F 0 "U2" H 5100 5037 60  0000 C CNN
F 1 "SN74LVC1T45" H 5100 4931 60  0000 C CNN
F 2 "Ruwai:SOT23-6" H 5500 4250 60  0001 C CNN
F 3 "" H 5100 4300 60  0000 C CNN
F 4 "Texas Instruments" H 5100 3850 60  0001 L CNN "Manu"
F 5 "SN74LVC1T45DBVT" H 5100 3750 60  0001 L CNN "Manu#"
F 6 "SOT23 6-pin" H 5100 3650 60  0001 L CNN "Package"
F 7 "Single-Bit Dual-Supply Bus Transceiver with Configurable Voltage Translation and 3-State Outputs" H 5100 3550 60  0001 L CNN "Desc"
	1    5100 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 4500 3500 4500
$Comp
L power:GND #PWR05
U 1 1 5E4F07FF
P 5100 5500
F 0 "#PWR05" H 5100 5250 50  0001 C CNN
F 1 "GND" H 5105 5327 50  0000 C CNN
F 2 "" H 5100 5500 50  0001 C CNN
F 3 "" H 5100 5500 50  0001 C CNN
	1    5100 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 4900 4050 4350
Wire Wire Line
	4050 5300 4050 5400
Wire Wire Line
	4050 5400 4350 5400
Wire Wire Line
	5100 5400 5100 5500
Wire Wire Line
	5100 5100 5100 5400
Connection ~ 5100 5400
Wire Wire Line
	5100 5400 6000 5400
Wire Wire Line
	6000 5400 6000 5300
Wire Wire Line
	5750 4500 6500 4500
Wire Wire Line
	6000 4900 6000 4350
Wire Wire Line
	6000 4350 6500 4350
Wire Wire Line
	4450 2150 4150 2150
Wire Wire Line
	3500 4350 4050 4350
Wire Wire Line
	4450 4700 4350 4700
Wire Wire Line
	4350 4700 4350 5400
Connection ~ 4350 5400
Wire Wire Line
	4350 5400 5100 5400
Text HLabel 2900 1800 0    50   Input ~ 0
3.3V
Text HLabel 3500 4350 0    50   Input ~ 0
3.3V
Text HLabel 6500 1800 2    50   Input ~ 0
5V
Text HLabel 6500 4350 2    50   Input ~ 0
5V
Text Notes 4850 1400 0    50   ~ 0
A to B (3.3 to 5)
Text Notes 4750 4000 0    50   ~ 0
B to A (5 to 3.3)
Text HLabel 2900 1950 0    50   Input ~ 0
BBB_TX
Text HLabel 6500 1950 2    50   Output ~ 0
ARD_RX
Text HLabel 3500 4500 0    50   Output ~ 0
BBB_RX
Text HLabel 6500 4500 2    50   Input ~ 0
ARD_TX
Wire Wire Line
	5750 4350 6000 4350
Connection ~ 6000 4350
Wire Wire Line
	4450 4350 4050 4350
Connection ~ 4050 4350
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5E5D30EE
P 6500 1800
F 0 "#FLG0102" H 6500 1875 50  0001 C CNN
F 1 "PWR_FLAG" H 6500 1973 50  0000 C CNN
F 2 "" H 6500 1800 50  0001 C CNN
F 3 "~" H 6500 1800 50  0001 C CNN
	1    6500 1800
	1    0    0    -1  
$EndComp
$Comp
L ruwai:Capacitor C3
U 1 1 5E5220EA
P 6000 2550
F 0 "C3" H 6115 2588 40  0000 L CNN
F 1 "0.1u" H 6115 2512 40  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 6038 2400 30  0001 C CNN
F 3 "" H 6000 2650 60  0000 C CNN
F 4 "Würth Elektronik" H 6050 2150 60  0001 L CNN "Manu"
F 5 "885012207072" H 6050 2050 60  0001 L CNN "Manu#"
F 6 "0805" H 6050 1950 60  0001 L CNN "Package"
F 7 "Multilayer Ceramic Capacitors MLCC - SMD/SMT WCAP-CSGP 0.1uF 0805 10% 25V MLCC " H 6000 2200 60  0001 L CNN "Desc"
	1    6000 2550
	1    0    0    -1  
$EndComp
$Comp
L ruwai:Capacitor C1
U 1 1 5E522B9C
P 4050 5100
F 0 "C1" H 4165 5138 40  0000 L CNN
F 1 "0.1u" H 4165 5062 40  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4088 4950 30  0001 C CNN
F 3 "" H 4050 5200 60  0000 C CNN
F 4 "Würth Elektronik" H 4100 4700 60  0001 L CNN "Manu"
F 5 "885012207072" H 4100 4600 60  0001 L CNN "Manu#"
F 6 "0805" H 4100 4500 60  0001 L CNN "Package"
F 7 "Multilayer Ceramic Capacitors MLCC - SMD/SMT WCAP-CSGP 0.1uF 0805 10% 25V MLCC " H 4050 4750 60  0001 L CNN "Desc"
	1    4050 5100
	1    0    0    -1  
$EndComp
$Comp
L ruwai:Capacitor C4
U 1 1 5E5230C4
P 6000 5100
F 0 "C4" H 6115 5138 40  0000 L CNN
F 1 "0.1u" H 6115 5062 40  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 6038 4950 30  0001 C CNN
F 3 "" H 6000 5200 60  0000 C CNN
F 4 "Würth Elektronik" H 6050 4700 60  0001 L CNN "Manu"
F 5 "885012207072" H 6050 4600 60  0001 L CNN "Manu#"
F 6 "0805" H 6050 4500 60  0001 L CNN "Package"
F 7 "Multilayer Ceramic Capacitors MLCC - SMD/SMT WCAP-CSGP 0.1uF 0805 10% 25V MLCC " H 6000 4750 60  0001 L CNN "Desc"
	1    6000 5100
	1    0    0    -1  
$EndComp
$EndSCHEMATC
