EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 5
Title "Ruwai Power Supply Two"
Date "2020-02-25"
Rev "1"
Comp "Mertl Research GmbH"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 "This documentation describes Open Hardware and is licensed under the CERN OHL v. 1.2."
$EndDescr
$Comp
L ruwai:inductor L2
U 1 1 5E507158
P 6450 2550
F 0 "L2" H 6450 2719 40  0000 C CNN
F 1 "22u" H 6450 2643 40  0000 C CNN
F 2 "Ruwai:5848" H 6350 2550 60  0001 C CNN
F 3 "" H 6350 2550 60  0000 C CNN
F 4 "Würth Elektronik" H 6450 2350 60  0001 L BNN "Manu"
F 5 "744774122" H 6450 2250 60  0001 L BNN "Manu#"
F 6 "5848" H 6450 2150 60  0001 L BNN "Package"
F 7 "Fixed Inductors WE-PD2 5848 22uH 1.28A .18Ohm " H 6450 2050 60  0001 L BNN "Desc"
	1    6450 2550
	1    0    0    -1  
$EndComp
$Comp
L ruwai:Capacitor C6
U 1 1 5E50899C
P 6050 2850
F 0 "C6" H 6165 2888 40  0000 L CNN
F 1 "2.2u" H 6165 2812 40  0000 L CNN
F 2 "Capacitor_SMD:C_1812_4532Metric" H 6088 2700 30  0001 C CNN
F 3 "" H 6050 2950 60  0000 C CNN
F 4 "Würth Elektronik" H 6100 2450 60  0001 L CNN "Manu"
F 5 " 885012210032 " H 6100 2350 60  0001 L CNN "Manu#"
F 6 "1812" H 6100 2250 60  0001 L CNN "Package"
F 7 "Multilayer Ceramic Capacitors MLCC - SMD/SMT WCAP-CSGP 2.2uF 1812 10% 50V MLCC " H 6050 2500 60  0001 L CNN "Desc"
	1    6050 2850
	1    0    0    -1  
$EndComp
$Comp
L ruwai:Varistance F1
U 1 1 5E50D7C8
P 2400 2500
F 0 "F1" H 2400 2702 40  0000 C CNN
F 1 "1812L110/33MR" H 2400 2626 40  0000 C CNN
F 2 "Ruwai:PF1812L" H 2450 2450 60  0001 C CNN
F 3 "" V 2400 2500 60  0000 C CNN
F 4 "Littlefuse" H 2400 2300 60  0001 L BNN "Manu"
F 5 "1812L110/33MR" H 2400 2200 60  0001 L BNN "Manu#"
F 6 "PF1812L" H 2400 2100 60  0001 L BNN "Package"
F 7 "Littelfuse 1.1A Surface Mount Resettable Fuse, 33 V dc" H 2400 2000 60  0001 L BNN "Desc"
	1    2400 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 2500 6850 2500
Wire Wire Line
	6250 2500 6050 2500
Wire Wire Line
	6850 2650 6850 2500
Connection ~ 6850 2500
Wire Wire Line
	6850 2500 6650 2500
Wire Wire Line
	6050 2650 6050 2500
Wire Wire Line
	7650 3200 6850 3200
Wire Wire Line
	6050 3050 6050 3200
Wire Wire Line
	6850 3050 6850 3200
Connection ~ 6850 3200
Wire Wire Line
	6850 3200 6050 3200
Wire Wire Line
	4100 2650 4100 2500
Wire Wire Line
	4100 3050 4100 3200
$Comp
L ruwai:FQP47P06 Q1
U 1 1 5E4F13A7
P 3050 2550
F 0 "Q1" H 3050 2842 50  0000 C CNN
F 1 "FQP47P06" H 3050 2751 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 3150 1450 50  0001 L CIN
F 3 "" V 3050 2500 50  0001 L CNN
F 4 "Fairchild Semiconductor" H 3150 2000 50  0001 L CNN "Manu"
F 5 "FQP47P06" H 3150 1900 50  0001 L CNN "Manu#"
F 6 "TO-220B03" H 3150 1800 50  0001 L CNN "Package"
F 7 "P-Channel MOSFET, -60 V, -47 A, 26 mOhm" H 3150 1700 50  0001 L CNN "Desc"
	1    3050 2550
	1    0    0    -1  
$EndComp
$Comp
L ruwai:Resistor R2
U 1 1 5E4F45C3
P 3050 3200
F 0 "R2" H 2979 3162 40  0000 R CNN
F 1 "100k" H 2979 3238 40  0000 R CNN
F 2 "Ruwai:Resistor_SMD0805_ReflowWave" V 2980 3200 30  0001 C CNN
F 3 "" V 3130 3200 30  0000 C CNN
F 4 "Yageo" H 3050 2700 60  0001 L CNN "Manu"
F 5 "RC0805FR-07100KL" H 3050 2600 60  0001 L CNN "Manu#"
F 6 "0805" H 3050 2500 60  0001 L CNN "Package"
F 7 "SMD Chip Resistor, Thick Film, 100 kohm, 150 V, 0805 [2012 Metric], 125 mW, ± 1%, RC Series" H 3050 2400 60  0001 L CNN "Desc"
	1    3050 3200
	-1   0    0    1   
$EndComp
$Comp
L Device:D_Zener_Small_ALT D1
U 1 1 5E4F5C74
P 3400 2700
F 0 "D1" V 3354 2768 50  0000 L CNN
F 1 "1N5240BTR" V 3445 2768 50  0000 L CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" V 3400 2700 50  0001 C CNN
F 3 "" V 3400 2700 50  0001 C CNN
F 4 "ON Semiconductor" V 3400 2700 50  0001 C CNN "Manu"
F 5 "1N5240BTR" V 3400 2700 50  0001 C CNN "Manu#"
F 6 "ON Semiconductor Zenerdiode 10V / 500 mW, DO-35 2-Pin" V 3400 2700 50  0001 C CNN "Desc"
F 7 "DO-35" V 3400 2700 50  0001 C CNN "Package"
	1    3400 2700
	0    1    1    0   
$EndComp
Wire Wire Line
	3250 2500 3400 2500
Wire Wire Line
	3400 2600 3400 2500
Connection ~ 3400 2500
Wire Wire Line
	3050 2800 3050 2900
Wire Wire Line
	3400 2800 3400 2900
Wire Wire Line
	3400 2900 3050 2900
Connection ~ 3050 2900
Wire Wire Line
	3050 2900 3050 2950
Wire Wire Line
	3050 3450 3050 3650
Wire Wire Line
	3050 3650 3950 3650
Wire Wire Line
	3950 3650 3950 3200
Connection ~ 3050 3650
$Comp
L ruwai:Capacitor_pol. C5
U 1 1 5E50ABDF
P 4100 2850
F 0 "C5" H 4215 2888 40  0000 L CNN
F 1 "22u" H 4215 2812 40  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P3.50mm" H 4138 2700 30  0001 C CNN
F 3 "" H 4100 2950 60  0000 C CNN
F 4 "Würth Elektronik" H 4110 2560 60  0001 L CNN "Manu"
F 5 "860010674014" H 4110 2490 60  0001 L CNN "Manu#"
F 6 "D8.0-P3.5" H 4110 2410 60  0001 L CNN "Package"
F 7 "Würth WCAP-ATG8, THT Alu Kondensator, Elko radial 100μF ±20%, 50Vdc, Ø 8mm " H 4110 2320 60  0001 L CNN "Desc"
	1    4100 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 2500 4100 2500
Wire Wire Line
	3950 3200 4100 3200
Wire Wire Line
	4100 2500 4500 2500
Wire Wire Line
	4500 2500 4500 2750
Wire Wire Line
	4500 2750 4750 2750
Connection ~ 4100 2500
Wire Wire Line
	5350 2750 5600 2750
Wire Wire Line
	5600 2750 5600 2500
Wire Wire Line
	5600 2500 6050 2500
Connection ~ 6050 2500
Wire Wire Line
	4100 3200 4500 3200
Wire Wire Line
	4500 3200 4500 2950
Wire Wire Line
	4500 2950 4750 2950
Connection ~ 4100 3200
Wire Wire Line
	5350 2950 5600 2950
Wire Wire Line
	5600 2950 5600 3200
Wire Wire Line
	5600 3200 6050 3200
Connection ~ 6050 3200
Wire Wire Line
	2600 2500 2850 2500
$Comp
L ruwai:TRI_10-1211 U3
U 1 1 5E4EF6F7
P 8450 2850
F 0 "U3" H 8450 3387 60  0000 C CNN
F 1 "TRI_10-1211" H 8450 3281 60  0000 C CNN
F 2 "Ruwai:TRI_10_DIP_24" H 8500 2200 60  0001 C CNN
F 3 "" H 8600 2300 60  0001 C CNN
F 4 "Traco Power" H 8500 2150 60  0001 L CNN "Manu"
F 5 "TRI 10-1211" H 8500 2050 60  0001 L CNN "Manu#"
F 6 "DIP-24" H 8500 1950 60  0001 L CNN "Package"
F 7 "TRI 10-1211 DC/DC Converter, 9 - 18 VDC input, 5.1 VDC output, 10 Watt" H 8500 1850 60  0001 L CNN "Desc"
	1    8450 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 2500 7650 2600
Wire Wire Line
	7650 2600 8000 2600
Wire Wire Line
	7650 3200 7650 3100
Wire Wire Line
	7650 3100 8000 3100
Wire Wire Line
	7650 3100 7650 2950
Wire Wire Line
	7650 2950 8000 2950
Connection ~ 7650 3100
$Comp
L ruwai:7448014501 L1
U 1 1 5E4F32A2
P 5050 2850
F 0 "L1" H 5050 3197 60  0000 C CNN
F 1 "7448014501" H 5050 3091 60  0000 C CNN
F 2 "Ruwai:7448014501" H 5050 2100 60  0001 C CNN
F 3 "" H 5150 2200 60  0001 C CNN
F 4 "Würth Elektronik" H 5050 2050 60  0001 L CNN "Manu"
F 5 "7448014501" H 5050 1950 60  0001 L CNN "Manu#"
F 6 "10 mm x 4.5 mm THT" H 5050 1850 60  0001 L CNN "Package"
F 7 "WE-CMBNC Common Mode Power Line Choke Nanocrystalline, 0.4 mH, 22 mOhm, 4.5 A" H 5050 1750 60  0001 L CNN "Desc"
	1    5050 2850
	1    0    0    -1  
$EndComp
Wire Notes Line
	5900 2200 5900 3350
Wire Notes Line
	5900 3350 7150 3350
Wire Notes Line
	7150 3350 7150 2200
Wire Notes Line
	7150 2200 5900 2200
Text Notes 5900 2150 0    50   ~ 0
Low Pass Pi-Filter (fc = 32.353 kHz).
Wire Notes Line
	4000 3300 5750 3300
Wire Notes Line
	5750 3300 5750 2350
Wire Notes Line
	5750 2350 4000 2350
Wire Notes Line
	4000 2350 4000 3300
Text Notes 4000 2300 0    50   ~ 0
EMI Filter according to Traco application note.
Wire Notes Line
	2750 2150 2750 3800
Wire Notes Line
	2750 3800 3900 3800
Wire Notes Line
	3900 3800 3900 2150
Wire Notes Line
	3900 2150 2750 2150
Text Notes 2750 2100 0    50   ~ 0
Reverse polarity protection.
$Comp
L ruwai:Capacitor C8
U 1 1 5E4FE5B9
P 9200 2850
F 0 "C8" H 9315 2888 40  0000 L CNN
F 1 "100n" H 9315 2812 40  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9238 2700 30  0001 C CNN
F 3 "" H 9200 2950 60  0000 C CNN
F 4 "Würth Elektronik" H 9250 2450 60  0001 L CNN "Manu"
F 5 "885012207079" H 9250 2350 60  0001 L CNN "Manu#"
F 6 "0805" H 9250 2250 60  0001 L CNN "Package"
F 7 "Multilayer Ceramic Capacitors MLCC - SMD/SMT WCAP-CSGP 2.2uF 0805 10% 25V MLCC " H 9200 2500 60  0001 L CNN "Desc"
	1    9200 2850
	1    0    0    -1  
$EndComp
$Comp
L ruwai:ferrite_bead L3
U 1 1 5E4FF224
P 9500 2600
F 0 "L3" H 9500 2785 40  0000 C CNN
F 1 "240@100Mhz" H 9500 2709 40  0000 C CNN
F 2 "Ruwai:Inductor_SMD0805_ReflowWave" H 9500 2530 30  0001 C CNN
F 3 "" H 9500 2680 30  0001 C CNN
F 4 "Würth Elektronik" H 9500 2350 60  0001 L CNN "Manu"
F 5 "742792038" H 9500 2250 60  0001 L CNN "Manu#"
F 6 "0805" H 9500 2150 60  0001 L CNN "Package"
F 7 "Custom Description" H 9500 2050 60  0001 L CNN "Desc"
	1    9500 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 2600 9200 2600
Wire Wire Line
	8900 3100 9200 3100
Wire Wire Line
	9700 2600 9750 2600
Wire Wire Line
	9200 2650 9200 2600
Connection ~ 9200 2600
Wire Wire Line
	9200 2600 9300 2600
Wire Wire Line
	9200 3050 9200 3100
Connection ~ 9200 3100
Wire Wire Line
	9200 3100 9750 3100
Wire Wire Line
	9750 2650 9750 2600
Connection ~ 9750 2600
Wire Wire Line
	9750 3050 9750 3100
Connection ~ 9750 3100
Wire Wire Line
	9750 2600 10250 2600
Wire Wire Line
	9750 3100 10250 3100
Text HLabel 10250 2600 2    50   Output ~ 0
V+5.1
Text HLabel 10250 3100 2    50   UnSpc ~ 0
GND+5.1
$Comp
L ruwai:70543-0001 P3
U 1 1 5E52E6D9
P 1400 3050
F 0 "P3" H 1533 3347 60  0000 C CNN
F 1 "70543-0001" H 1533 3241 60  0000 C CNN
F 2 "Ruwai:Molex_SL_70543-0001" H 1400 2350 60  0001 L CNN
F 3 "" H 1400 3050 60  0000 C CNN
F 4 "Molex" H 1400 2750 60  0001 L CNN "Manu"
F 5 "70543-0001" H 1400 2650 60  0001 L CNN "Manu#"
F 6 "1x2 pins; 2.54mm pitch; straight" H 1400 2550 60  0001 L CNN "Package"
F 7 "Custom Description" H 1400 2450 60  0001 L CNN "Desc"
	1    1400 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 3000 1850 3000
Wire Wire Line
	1850 3000 1850 2500
Wire Wire Line
	1850 2500 2200 2500
Wire Wire Line
	1700 3100 1850 3100
Wire Wire Line
	1850 3100 1850 3650
Wire Wire Line
	1850 3650 3050 3650
$Comp
L ruwai:Capacitor C7
U 1 1 5E584824
P 6850 2850
F 0 "C7" H 6965 2888 40  0000 L CNN
F 1 "2.2u" H 6965 2812 40  0000 L CNN
F 2 "Capacitor_SMD:C_1812_4532Metric" H 6888 2700 30  0001 C CNN
F 3 "" H 6850 2950 60  0000 C CNN
F 4 "Würth Elektronik" H 6900 2450 60  0001 L CNN "Manu"
F 5 " 885012210032 " H 6900 2350 60  0001 L CNN "Manu#"
F 6 "1812" H 6900 2250 60  0001 L CNN "Package"
F 7 "Multilayer Ceramic Capacitors MLCC - SMD/SMT WCAP-CSGP 2.2uF 1812 10% 50V MLCC " H 6850 2500 60  0001 L CNN "Desc"
	1    6850 2850
	1    0    0    -1  
$EndComp
Text Notes 9400 2400 0    50   ~ 0
Nennstrom Ir1 oder Ir2?
$Comp
L power:PWR_FLAG #FLG0105
U 1 1 5E5D93E0
P 6850 2500
F 0 "#FLG0105" H 6850 2575 50  0001 C CNN
F 1 "PWR_FLAG" H 6850 2673 50  0000 C CNN
F 2 "" H 6850 2500 50  0001 C CNN
F 3 "~" H 6850 2500 50  0001 C CNN
	1    6850 2500
	1    0    0    -1  
$EndComp
$Comp
L ruwai:Capacitor C9
U 1 1 5E51C890
P 9750 2850
F 0 "C9" H 9865 2888 40  0000 L CNN
F 1 "100n" H 9865 2812 40  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9788 2700 30  0001 C CNN
F 3 "" H 9750 2950 60  0000 C CNN
F 4 "Würth Elektronik" H 9800 2450 60  0001 L CNN "Manu"
F 5 "885012207079" H 9800 2350 60  0001 L CNN "Manu#"
F 6 "0805" H 9800 2250 60  0001 L CNN "Package"
F 7 "Multilayer Ceramic Capacitors MLCC - SMD/SMT WCAP-CSGP 2.2uF 0805 10% 25V MLCC " H 9750 2500 60  0001 L CNN "Desc"
	1    9750 2850
	1    0    0    -1  
$EndComp
Text Label 1850 3000 0    50   ~ 0
V+12V
Text Label 1850 3100 0    50   ~ 0
GND+12V
$EndSCHEMATC
