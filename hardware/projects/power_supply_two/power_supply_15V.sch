EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title "Ruwai Power Supply Two"
Date "2020-02-25"
Rev "1"
Comp "Mertl Research GmbH"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 "This documentation describes Open Hardware and is licensed under the CERN OHL v. 1.2."
$EndDescr
$Comp
L ruwai:TMR_1 U4
U 1 1 5E50DF66
P 4900 2650
F 0 "U4" H 4900 3137 60  0000 C CNN
F 1 "TMR_1" H 4900 3031 60  0000 C CNN
F 2 "Ruwai:Traco_TMR_1" H 4910 1950 60  0001 L CNN
F 3 "" H 4950 2750 60  0000 C CNN
F 4 "Traco" H 4900 2350 60  0001 L CNN "Manu"
F 5 "TMR 1-0523" H 4900 2250 60  0001 L CNN "Manu#"
F 6 "TMR_1" H 4900 2150 60  0001 L CNN "Package"
F 7 "Custom Description" H 4900 2050 60  0001 L CNN "Desc"
	1    4900 2650
	1    0    0    -1  
$EndComp
$Comp
L ruwai:inductor L?
U 1 1 5E5148CB
P 3800 2400
AR Path="/5E50399A/5E5148CB" Ref="L?"  Part="1" 
AR Path="/5E5091DB/5E5148CB" Ref="L4"  Part="1" 
F 0 "L4" H 3800 2569 40  0000 C CNN
F 1 "4.7u" H 3800 2493 40  0000 C CNN
F 2 "Ruwai:5848" H 3700 2400 60  0001 C CNN
F 3 "" H 3700 2400 60  0000 C CNN
F 4 "Würth Elektronik" H 3800 2200 60  0001 L BNN "Manu"
F 5 "744774047" H 3800 2100 60  0001 L BNN "Manu#"
F 6 "5848" H 3800 2000 60  0001 L BNN "Package"
F 7 "Fixed Inductors WE-PD2 5848 4.7uH 3A .071Ohm " H 3800 1900 60  0001 L BNN "Desc"
	1    3800 2400
	1    0    0    -1  
$EndComp
$Comp
L ruwai:Capacitor C?
U 1 1 5E5148D5
P 3450 2650
AR Path="/5E50399A/5E5148D5" Ref="C?"  Part="1" 
AR Path="/5E5091DB/5E5148D5" Ref="C10"  Part="1" 
F 0 "C10" H 3565 2688 40  0000 L CNN
F 1 "4.7u" H 3565 2612 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD1206_ReflowWave" H 3488 2500 30  0001 C CNN
F 3 "" H 3450 2750 60  0000 C CNN
F 4 "Würth Elektronik" H 3500 2250 60  0001 L CNN "Manu"
F 5 "885012208094" H 3500 2150 60  0001 L CNN "Manu#"
F 6 "1206" H 3500 2050 60  0001 L CNN "Package"
F 7 "Multilayer Ceramic Capacitors MLCC - SMD/SMT WCAP-CSGP 4.7uF 1206 10% 50V MLCC " H 3450 2300 60  0001 L CNN "Desc"
	1    3450 2650
	1    0    0    -1  
$EndComp
Text Notes 3150 2150 0    50   ~ 0
Low Pass LC-Filter (fc = 33.86 kHz).
Wire Wire Line
	4350 2500 4150 2500
Wire Wire Line
	4150 2500 4150 2350
Wire Wire Line
	4150 2350 4000 2350
Wire Wire Line
	3600 2350 3450 2350
Wire Wire Line
	3450 2450 3450 2350
Connection ~ 3450 2350
Wire Wire Line
	3450 2350 3200 2350
Wire Wire Line
	4350 2800 4150 2800
Wire Wire Line
	4150 2800 4150 3000
Wire Wire Line
	4150 3000 3450 3000
Wire Wire Line
	3450 2850 3450 3000
Connection ~ 3450 3000
Wire Wire Line
	3450 3000 3200 3000
Wire Wire Line
	5450 2650 5950 2650
$Comp
L ruwai:inductor L?
U 1 1 5E517DCF
P 6450 2050
AR Path="/5E50399A/5E517DCF" Ref="L?"  Part="1" 
AR Path="/5E5091DB/5E517DCF" Ref="L5"  Part="1" 
F 0 "L5" H 6450 2219 40  0000 C CNN
F 1 "22u" H 6450 2143 40  0000 C CNN
F 2 "Ruwai:5848" H 6350 2050 60  0001 C CNN
F 3 "" H 6350 2050 60  0000 C CNN
F 4 "Würth Elektronik" H 6450 1850 60  0001 L BNN "Manu"
F 5 "744774122" H 6450 1750 60  0001 L BNN "Manu#"
F 6 "5848" H 6450 1650 60  0001 L BNN "Package"
F 7 "Fixed Inductors WE-PD2 5848 22uH 1.28A .18Ohm " H 6450 1550 60  0001 L BNN "Desc"
	1    6450 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 2500 5600 2500
Wire Wire Line
	5600 2500 5600 2000
Wire Wire Line
	5600 2000 5950 2000
Wire Wire Line
	7050 2100 7050 2000
Connection ~ 7050 2000
Wire Wire Line
	7050 2000 7700 2000
Wire Wire Line
	7050 2500 7050 2650
Connection ~ 7050 2650
Wire Wire Line
	7050 2650 7700 2650
Wire Wire Line
	7050 2800 7050 2650
Wire Wire Line
	7050 3300 7700 3300
Connection ~ 7050 3300
Wire Wire Line
	7050 3200 7050 3300
Wire Wire Line
	5600 3300 5950 3300
Wire Wire Line
	5600 3300 5600 2800
Wire Wire Line
	5600 2800 5450 2800
Text Notes 5600 1750 0    50   ~ 0
Low Pass LC Pi Filter (fc = 32.352 kHz).
Wire Wire Line
	6650 2000 7050 2000
Wire Wire Line
	6750 3300 7050 3300
$Comp
L ruwai:Capacitor C?
U 1 1 5E50160D
P 5950 2300
AR Path="/5E50399A/5E50160D" Ref="C?"  Part="1" 
AR Path="/5E5091DB/5E50160D" Ref="C11"  Part="1" 
F 0 "C11" H 6065 2338 40  0000 L CNN
F 1 "2.2u" H 6065 2262 40  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5988 2150 30  0001 C CNN
F 3 "" H 5950 2400 60  0000 C CNN
F 4 "Würth Elektronik" H 6000 1900 60  0001 L CNN "Manu"
F 5 "885012207079" H 6000 1800 60  0001 L CNN "Manu#"
F 6 "0805" H 6000 1700 60  0001 L CNN "Package"
F 7 "Multilayer Ceramic Capacitors MLCC - SMD/SMT WCAP-CSGP 2.2uF 0805 10% 25V MLCC" H 5950 1950 60  0001 L CNN "Desc"
	1    5950 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 2100 5950 2000
Connection ~ 5950 2000
Wire Wire Line
	5950 2000 6250 2000
Wire Wire Line
	5950 2500 5950 2650
Connection ~ 5950 2650
Wire Wire Line
	5950 2650 7050 2650
Wire Wire Line
	5950 3200 5950 3300
Connection ~ 5950 3300
Wire Wire Line
	5950 3300 6350 3300
Wire Wire Line
	5950 2800 5950 2650
Text Notes 5650 3550 0    50   ~ 0
Low Pass LC Pi Filter (fc = 32.352 kHz).
Text HLabel 3200 2350 0    50   Input ~ 0
+Vin
Text HLabel 3200 3000 0    50   Input ~ 0
-Vin
Text HLabel 7700 2650 2    50   UnSpc ~ 0
GND_15V
Text HLabel 7700 2000 2    50   Output ~ 0
+15V
Text HLabel 7700 3300 2    50   Output ~ 0
-15V
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 5E5D3E78
P 4000 2350
F 0 "#FLG0103" H 4000 2425 50  0001 C CNN
F 1 "PWR_FLAG" H 4000 2523 50  0000 C CNN
F 2 "" H 4000 2350 50  0001 C CNN
F 3 "~" H 4000 2350 50  0001 C CNN
	1    4000 2350
	1    0    0    -1  
$EndComp
Connection ~ 4000 2350
$Comp
L ruwai:Capacitor C?
U 1 1 5E51DA30
P 7050 2300
AR Path="/5E50399A/5E51DA30" Ref="C?"  Part="1" 
AR Path="/5E5091DB/5E51DA30" Ref="C13"  Part="1" 
F 0 "C13" H 7165 2338 40  0000 L CNN
F 1 "2.2u" H 7165 2262 40  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7088 2150 30  0001 C CNN
F 3 "" H 7050 2400 60  0000 C CNN
F 4 "Würth Elektronik" H 7100 1900 60  0001 L CNN "Manu"
F 5 "885012207079" H 7100 1800 60  0001 L CNN "Manu#"
F 6 "0805" H 7100 1700 60  0001 L CNN "Package"
F 7 "Multilayer Ceramic Capacitors MLCC - SMD/SMT WCAP-CSGP 2.2uF 0805 10% 25V MLCC" H 7050 1950 60  0001 L CNN "Desc"
	1    7050 2300
	1    0    0    -1  
$EndComp
$Comp
L ruwai:Capacitor C?
U 1 1 5E51DDC7
P 5950 3000
AR Path="/5E50399A/5E51DDC7" Ref="C?"  Part="1" 
AR Path="/5E5091DB/5E51DDC7" Ref="C12"  Part="1" 
F 0 "C12" H 6065 3038 40  0000 L CNN
F 1 "2.2u" H 6065 2962 40  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5988 2850 30  0001 C CNN
F 3 "" H 5950 3100 60  0000 C CNN
F 4 "Würth Elektronik" H 6000 2600 60  0001 L CNN "Manu"
F 5 "885012207079" H 6000 2500 60  0001 L CNN "Manu#"
F 6 "0805" H 6000 2400 60  0001 L CNN "Package"
F 7 "Multilayer Ceramic Capacitors MLCC - SMD/SMT WCAP-CSGP 2.2uF 0805 10% 25V MLCC" H 5950 2650 60  0001 L CNN "Desc"
	1    5950 3000
	1    0    0    -1  
$EndComp
$Comp
L ruwai:Capacitor C?
U 1 1 5E51E082
P 7050 3000
AR Path="/5E50399A/5E51E082" Ref="C?"  Part="1" 
AR Path="/5E5091DB/5E51E082" Ref="C14"  Part="1" 
F 0 "C14" H 7165 3038 40  0000 L CNN
F 1 "2.2u" H 7165 2962 40  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7088 2850 30  0001 C CNN
F 3 "" H 7050 3100 60  0000 C CNN
F 4 "Würth Elektronik" H 7100 2600 60  0001 L CNN "Manu"
F 5 "885012207079" H 7100 2500 60  0001 L CNN "Manu#"
F 6 "0805" H 7100 2400 60  0001 L CNN "Package"
F 7 "Multilayer Ceramic Capacitors MLCC - SMD/SMT WCAP-CSGP 2.2uF 0805 10% 25V MLCC" H 7050 2650 60  0001 L CNN "Desc"
	1    7050 3000
	1    0    0    -1  
$EndComp
$Comp
L ruwai:inductor L?
U 1 1 5E51F89A
P 6550 3250
AR Path="/5E50399A/5E51F89A" Ref="L?"  Part="1" 
AR Path="/5E5091DB/5E51F89A" Ref="L6"  Part="1" 
F 0 "L6" H 6550 3419 40  0000 C CNN
F 1 "22u" H 6550 3343 40  0000 C CNN
F 2 "Ruwai:5848" H 6450 3250 60  0001 C CNN
F 3 "" H 6450 3250 60  0000 C CNN
F 4 "Würth Elektronik" H 6550 3050 60  0001 L BNN "Manu"
F 5 "744774122" H 6550 2950 60  0001 L BNN "Manu#"
F 6 "5848" H 6550 2850 60  0001 L BNN "Package"
F 7 "Fixed Inductors WE-PD2 5848 22uH 1.28A .18Ohm " H 6550 2750 60  0001 L BNN "Desc"
	1    6550 3250
	1    0    0    1   
$EndComp
$EndSCHEMATC
