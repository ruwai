EESchema Schematic File Version 2
LIBS:ruwai_gps_timing_shield-rescue
LIBS:ruwai
LIBS:74xgxx
LIBS:74xx
LIBS:ac-dc
LIBS:actel
LIBS:adc-dac
LIBS:analog_switches
LIBS:atmel
LIBS:audio
LIBS:brooktre
LIBS:cmos4000
LIBS:cmos_ieee
LIBS:conn
LIBS:contrib
LIBS:cypress
LIBS:dc-dc
LIBS:device
LIBS:digital-audio
LIBS:display
LIBS:dsp
LIBS:elec-unifil
LIBS:ftdi
LIBS:gennum
LIBS:hc11
LIBS:intel
LIBS:interface
LIBS:ir
LIBS:linear
LIBS:memory
LIBS:microchip
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers
LIBS:motor_drivers
LIBS:motorola
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:opto
LIBS:philips
LIBS:power
LIBS:powerint
LIBS:pspice
LIBS:references
LIBS:regul
LIBS:relays
LIBS:rfcom
LIBS:sensors
LIBS:silabs
LIBS:siliconi
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:texas
LIBS:transf
LIBS:transistors
LIBS:ttl_ieee
LIBS:valves
LIBS:video
LIBS:xilinx
LIBS:ruwai_gps_timing_shield-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 6
Title "Ruwai GPS Timing Shield"
Date "2017-09-12"
Rev "3"
Comp "Copyright 2014 Mertl Research GmbH"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 "This documentation describes Open Hardware and is licensed under the CERN OHL v. 1.2."
$EndDescr
$Comp
L ublox_lea6T U7
U 1 1 53A83C58
P 6550 2250
F 0 "U7" H 6000 1400 60  0000 C CNN
F 1 "ublox_lea6T" H 6200 3150 60  0000 C CNN
F 2 "Ruwai:UBLOX_LEA6T" H 7000 1400 60  0001 C CNN
F 3 "" H 6650 1950 60  0000 C CNN
F 4 "u-Blox" H 3650 3400 60  0001 C CNN "Manu"
F 5 "LEA-6T-0" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "LEA-6T" H 3650 3600 60  0001 C CNN "Package"
F 7 "GPS module" H 3650 4500 60  0001 C CNN "Desc"
	1    6550 2250
	1    0    0    -1  
$EndComp
$Comp
L 24AA32A_(8_pins) U8
U 1 1 53A83D1F
P 2900 1450
F 0 "U8" H 2600 1150 60  0000 C CNN
F 1 "24AA32A_(8_pins)" H 2950 1750 60  0000 C CNN
F 2 "Ruwai:SOIC-8_N" H 3300 1150 60  0001 C CNN
F 3 "" H 2900 1450 60  0000 C CNN
F 4 "Microchip" H 3650 3400 60  0001 C CNN "Manu"
F 5 "24AA32A-I/SN" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "SOIC 8 150mil" H 3650 3600 60  0001 C CNN "Package"
F 7 "Custom Description" H 3650 4500 60  0001 C CNN "Desc"
	1    2900 1450
	1    0    0    -1  
$EndComp
$Comp
L INDUCTOR_SMALL L1
U 1 1 53A841E4
P 5200 1500
F 0 "L1" H 5450 1550 50  0000 C CNN
F 1 "220" H 5200 1450 50  0001 C CNN
F 2 "Ruwai:Inductor_SMD0805_HandSoldering" H 5200 1500 60  0001 C CNN
F 3 "" H 5200 1500 60  0000 C CNN
F 4 "Murata" H 3650 3400 60  0001 C CNN "Manu"
F 5 "BLM21PG221SN1D" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 3600 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    5200 1500
	1    0    0    -1  
$EndComp
$Comp
L INDUCTOR_SMALL L2
U 1 1 53A8429E
P 5200 1600
F 0 "L2" H 5450 1650 50  0000 C CNN
F 1 "220" H 5200 1550 50  0001 C CNN
F 2 "Ruwai:Inductor_SMD0805_HandSoldering" H 5200 1600 60  0001 C CNN
F 3 "" H 5200 1600 60  0000 C CNN
F 4 "Murata" H 3650 3400 60  0001 C CNN "Manu"
F 5 "BLM21PG221SN1D" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 3600 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    5200 1600
	1    0    0    -1  
$EndComp
$Comp
L INDUCTOR_SMALL L3
U 1 1 53A842B6
P 5200 1700
F 0 "L3" H 5450 1750 50  0000 C CNN
F 1 "220" H 5200 1650 50  0001 C CNN
F 2 "Ruwai:Inductor_SMD0805_HandSoldering" H 5200 1700 60  0001 C CNN
F 3 "" H 5200 1700 60  0000 C CNN
F 4 "Murata" H 3650 3400 60  0001 C CNN "Manu"
F 5 "BLM21PG221SN1D" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 3600 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    5200 1700
	1    0    0    -1  
$EndComp
$Comp
L INDUCTOR_SMALL L4
U 1 1 53A842CF
P 5200 1800
F 0 "L4" H 5450 1850 50  0000 C CNN
F 1 "220" H 5200 1750 50  0000 C CNN
F 2 "Ruwai:Inductor_SMD0805_HandSoldering" H 5200 1800 60  0001 C CNN
F 3 "" H 5200 1800 60  0000 C CNN
F 4 "Murata" H 3650 3400 60  0001 C CNN "Manu"
F 5 "BLM21PG221SN1D" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 3600 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    5200 1800
	1    0    0    -1  
$EndComp
Text HLabel 4750 1700 0    60   Output ~ 0
TX_GPS
Text HLabel 4750 1800 0    60   Input ~ 0
RX_GPS
$Comp
L INDUCTOR_SMALL L5
U 1 1 53A84C87
P 1750 2000
F 0 "L5" H 2000 2050 50  0000 C CNN
F 1 "220" H 1750 1950 50  0000 C CNN
F 2 "Ruwai:Inductor_SMD0805_HandSoldering" H 1750 2000 60  0001 C CNN
F 3 "" H 1750 2000 60  0000 C CNN
F 4 "Murata" H 3650 3400 60  0001 C CNN "Manu"
F 5 "BLM21PG221SN1D" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 3600 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    1750 2000
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-ruwai_gps_timing_shield #PWR06
U 1 1 53A84E2A
P 1700 1750
F 0 "#PWR06" H 1700 1750 30  0001 C CNN
F 1 "GND" H 1700 1680 30  0001 C CNN
F 2 "" H 1700 1750 60  0000 C CNN
F 3 "" H 1700 1750 60  0000 C CNN
	1    1700 1750
	1    0    0    -1  
$EndComp
$Comp
L Resistor R15
U 1 1 53A84EED
P 1850 1350
F 0 "R15" V 1930 1350 40  0000 C CNN
F 1 "0" V 1857 1351 40  0000 C CNN
F 2 "Ruwai:Resistor_SMD0102_HandSoldering" V 1780 1350 30  0001 C CNN
F 3 "" H 1850 1350 30  0000 C CNN
F 4 "Yageo" H 3650 3400 60  0001 C CNN "Manu"
F 5 "RC0805JR-070RL" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 3600 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    1850 1350
	1    0    0    -1  
$EndComp
Text HLabel 1300 2000 0    60   Input ~ 0
GPS3.3V
$Comp
L Capacitor C2
U 1 1 53A8512D
P 2150 1800
AR Path="/53A8512D" Ref="C2"  Part="1" 
AR Path="/53A17A14/53A8512D" Ref="C2"  Part="1" 
F 0 "C2" H 2150 1900 40  0000 L CNN
F 1 "1uF" H 2156 1715 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD0805_HandSoldering" H 2188 1650 30  0001 C CNN
F 3 "" H 2150 1800 60  0000 C CNN
F 4 "Murata" H 3650 3400 60  0001 C CNN "Manu"
F 5 "GRM21BR71H105KA12L" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 3600 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    2150 1800
	1    0    0    -1  
$EndComp
$Comp
L Resistor R13
U 1 1 53A8564F
P 4950 2350
F 0 "R13" V 5030 2350 40  0000 C CNN
F 1 "10" V 4957 2351 40  0000 C CNN
F 2 "Ruwai:Resistor_SMD0805_HandSoldering" V 4880 2350 30  0001 C CNN
F 3 "" H 4950 2350 30  0000 C CNN
F 4 "Yageo" H 3650 3400 60  0001 C CNN "Manu"
F 5 "RC0805FR-0710RL" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 3600 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    4950 2350
	0    1    1    0   
$EndComp
$Comp
L Resistor R14
U 1 1 53A85723
P 4950 2550
F 0 "R14" V 5030 2550 40  0000 C CNN
F 1 "470" V 4957 2551 40  0000 C CNN
F 2 "Ruwai:Resistor_SMD0805_HandSoldering" V 4880 2550 30  0001 C CNN
F 3 "" H 4950 2550 30  0000 C CNN
F 4 "Yageo" H 3650 3400 60  0001 C CNN "Manu"
F 5 "RC0805FR-07470RL" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 3600 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    4950 2550
	0    1    1    0   
$EndComp
Text Notes 5100 2650 0    60   ~ 0
DNP
Text HLabel 4400 2350 0    60   Output ~ 0
FREQ_GPS
Text HLabel 4400 2550 0    60   Input ~ 0
RST_GPS
$Comp
L INDUCTOR_SMALL L7
U 1 1 53A859A8
P 4150 2750
F 0 "L7" H 4400 2800 50  0000 C CNN
F 1 "220" H 4150 2700 50  0000 C CNN
F 2 "Ruwai:Inductor_SMD0805_HandSoldering" H 4150 2750 60  0001 C CNN
F 3 "" H 4150 2750 60  0000 C CNN
F 4 "Murata" H 3650 3400 60  0001 C CNN "Manu"
F 5 "BLM21PG221SN1D" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 3600 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    4150 2750
	1    0    0    -1  
$EndComp
$Comp
L Capacitor C3
U 1 1 53A85A8B
P 4700 3200
AR Path="/53A85A8B" Ref="C3"  Part="1" 
AR Path="/53A17A14/53A85A8B" Ref="C3"  Part="1" 
F 0 "C3" H 4700 3300 40  0000 L CNN
F 1 "1nF" H 4706 3115 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD0805_HandSoldering" H 4738 3050 30  0001 C CNN
F 3 "" H 4700 3200 60  0000 C CNN
F 4 "Kemet" H 3650 3400 60  0001 C CNN "Manu"
F 5 "C0805C102J5GACTU" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 3600 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    4700 3200
	1    0    0    -1  
$EndComp
$Comp
L testpoint TP1
U 1 1 53A85B5C
P 5300 3150
F 0 "TP1" H 5200 3000 60  0000 C CNN
F 1 "P_GND_GPS" H 5200 3300 60  0000 C CNN
F 2 "Ruwai:testpoint_1" H 5300 3150 60  0001 C CNN
F 3 "" H 5300 3150 60  0000 C CNN
F 4 "RS" H 3650 3400 60  0001 C CNN "Manu"
F 5 "200-203" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "1mm" H 3650 3600 60  0001 C CNN "Package"
F 7 "Custom Description" H 3650 4500 60  0001 C CNN "Desc"
	1    5300 3150
	1    0    0    -1  
$EndComp
NoConn ~ 5650 2200
$Comp
L GND-RESCUE-ruwai_gps_timing_shield #PWR07
U 1 1 53A86109
P 5500 3550
F 0 "#PWR07" H 5500 3550 30  0001 C CNN
F 1 "GND" H 5500 3480 30  0001 C CNN
F 2 "" H 5500 3550 60  0000 C CNN
F 3 "" H 5500 3550 60  0000 C CNN
	1    5500 3550
	1    0    0    -1  
$EndComp
Text HLabel 3300 2750 0    60   Input ~ 0
VBAT
$Comp
L GND-RESCUE-ruwai_gps_timing_shield #PWR08
U 1 1 53A876CB
P 4700 3550
F 0 "#PWR08" H 4700 3550 30  0001 C CNN
F 1 "GND" H 4700 3480 30  0001 C CNN
F 2 "" H 4700 3550 60  0000 C CNN
F 3 "" H 4700 3550 60  0000 C CNN
	1    4700 3550
	1    0    0    -1  
$EndComp
$Comp
L INDUCTOR_SMALL L8
U 1 1 53A87B8D
P 7950 1500
F 0 "L8" H 8200 1550 50  0000 C CNN
F 1 "220" H 7950 1450 50  0000 C CNN
F 2 "Ruwai:Inductor_SMD0805_HandSoldering" H 7950 1500 60  0001 C CNN
F 3 "" H 7950 1500 60  0000 C CNN
F 4 "Murata" H 3650 3400 60  0001 C CNN "Manu"
F 5 "BLM21PG221SN1D" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 3600 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    7950 1500
	1    0    0    -1  
$EndComp
Text HLabel 8700 1500 2    60   Output ~ 0
PPS_GPS
NoConn ~ 7450 1600
Text HLabel 8700 1700 2    60   BiDi ~ 0
USB_DP
Text HLabel 8700 1800 2    60   BiDi ~ 0
USB_DM
Text HLabel 10550 1900 2    60   Input ~ 0
VUSB
$Comp
L Resistor R16
U 1 1 53A89138
P 9150 2250
F 0 "R16" V 9230 2250 40  0000 C CNN
F 1 "10k" V 9157 2251 40  0000 C CNN
F 2 "Ruwai:Resistor_SMD0102_HandSoldering" V 9080 2250 30  0001 C CNN
F 3 "" H 9150 2250 30  0000 C CNN
F 4 "Yageo" H 3650 3400 60  0001 C CNN "Manu"
F 5 "RC0805FR-0710KL" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 3600 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    9150 2250
	1    0    0    -1  
$EndComp
$Comp
L Capacitor C4
U 1 1 53A892C2
P 9450 2250
AR Path="/53A892C2" Ref="C4"  Part="1" 
AR Path="/53A17A14/53A892C2" Ref="C4"  Part="1" 
F 0 "C4" H 9450 2350 40  0000 L CNN
F 1 "1nF" H 9456 2165 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD0805_HandSoldering" H 9488 2100 30  0001 C CNN
F 3 "" H 9450 2250 60  0000 C CNN
F 4 "Kemet" H 3650 3400 60  0001 C CNN "Manu"
F 5 "C0805C102J5GACTU" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 3600 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    9450 2250
	1    0    0    -1  
$EndComp
$Comp
L INDUCTOR_SMALL L9
U 1 1 53A89388
P 9950 1900
F 0 "L9" H 10200 1950 50  0000 C CNN
F 1 "220" H 9950 1850 50  0000 C CNN
F 2 "Ruwai:Inductor_SMD0805_HandSoldering" H 9950 1900 60  0001 C CNN
F 3 "" H 9950 1900 60  0000 C CNN
F 4 "Murata" H 3650 3400 60  0001 C CNN "Manu"
F 5 "BLM21PG221SN1D" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 3600 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    9950 1900
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-ruwai_gps_timing_shield #PWR09
U 1 1 53A895B6
P 9150 2650
F 0 "#PWR09" H 9150 2650 30  0001 C CNN
F 1 "GND" H 9150 2580 30  0001 C CNN
F 2 "" H 9150 2650 60  0000 C CNN
F 3 "" H 9150 2650 60  0000 C CNN
	1    9150 2650
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-ruwai_gps_timing_shield #PWR010
U 1 1 53A895D3
P 9450 2650
F 0 "#PWR010" H 9450 2650 30  0001 C CNN
F 1 "GND" H 9450 2580 30  0001 C CNN
F 2 "" H 9450 2650 60  0000 C CNN
F 3 "" H 9450 2650 60  0000 C CNN
	1    9450 2650
	1    0    0    -1  
$EndComp
NoConn ~ 7450 2450
NoConn ~ 7450 2650
Text Label 7850 2550 0    60   ~ 0
V_ANT
Text Label 7850 2850 0    60   ~ 0
RF_IN
$Comp
L RFGND #PWR011
U 1 1 53A897BE
P 7550 3550
F 0 "#PWR011" H 7550 3550 40  0001 C CNN
F 1 "RFGND" H 7550 3480 40  0000 C CNN
F 2 "" H 7550 3550 60  0000 C CNN
F 3 "" H 7550 3550 60  0000 C CNN
	1    7550 3550
	1    0    0    -1  
$EndComp
$Comp
L SF1575L U12
U 1 1 53A89C8F
P 3350 4650
F 0 "U12" H 3100 4400 60  0000 C CNN
F 1 "SAW1.575GHz" H 3350 4900 60  0000 C CNN
F 2 "Ruwai:S25" H 3600 4400 60  0001 C CNN
F 3 "" H 3350 4650 60  0000 C CNN
F 4 "Epcos" H 2850 3400 60  0001 C CNN "Manu"
F 5 "B39162B3520U410" H 2850 3500 60  0001 C CNN "Manu#"
F 6 "DCC6C" H 2850 3600 60  0001 C CNN "Package"
F 7 "GPS- & Galileo Filter unbalanced / unbalanced" H 2850 4500 60  0001 C CNN "Desc"
	1    3350 4650
	1    0    0    -1  
$EndComp
$Comp
L SF1575L U9
U 1 1 53A89D04
P 8950 4850
F 0 "U9" H 8700 4600 60  0000 C CNN
F 1 "SAW1.575GHz" H 8950 5100 60  0000 C CNN
F 2 "Ruwai:S25" H 9200 4600 60  0001 C CNN
F 3 "" H 8950 4850 60  0000 C CNN
F 4 "Epcos" H 4700 3400 60  0001 C CNN "Manu"
F 5 "B39162B3520U410" H 4700 3500 60  0001 C CNN "Manu#"
F 6 "DCC6C" H 4700 3600 60  0001 C CNN "Package"
F 7 "GPS- & Galileo Filter unbalanced / unbalanced" H 4700 4500 60  0001 C CNN "Desc"
	1    8950 4850
	1    0    0    -1  
$EndComp
$Comp
L MAX2641 U13
U 1 1 53A89D54
P 6450 4750
F 0 "U13" H 6200 4500 60  0000 C CNN
F 1 "MAX2641" H 6350 5000 60  0000 C CNN
F 2 "Ruwai:SOT23-6" H 6800 4500 60  0001 C CNN
F 3 "" H 6450 4750 60  0000 C CNN
F 4 "Maxim Integrated" H 4150 3400 60  0001 C CNN "Manu"
F 5 "MAX2641EUT+T" H 4150 3500 60  0001 C CNN "Manu#"
F 6 "SOT23 6-pin" H 4150 3600 60  0001 C CNN "Package"
F 7 "300MHz to 2500MHz SiGe Ultra-Low-Noise Amplifier" H 4150 4500 60  0001 C CNN "Desc"
	1    6450 4750
	1    0    0    -1  
$EndComp
$Comp
L Capacitor C8
U 1 1 53A89E6D
P 7550 4850
AR Path="/53A89E6D" Ref="C8"  Part="1" 
AR Path="/53A17A14/53A89E6D" Ref="C8"  Part="1" 
F 0 "C8" H 7550 4950 40  0000 L CNN
F 1 "100pF" H 7556 4765 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD0603_reflow" H 7588 4700 30  0001 C CNN
F 3 "" H 7550 4850 60  0000 C CNN
F 4 "Murata" H 4300 3400 60  0001 C CNN "Manu"
F 5 "GRM1885C1H101JA01D" H 4300 3500 60  0001 C CNN "Manu#"
F 6 "0603" H 4300 3600 60  0001 C CNN "Package"
F 7 " " H 4300 4500 60  0001 C CNN "Desc"
	1    7550 4850
	0    1    1    0   
$EndComp
$Comp
L RFGND #PWR012
U 1 1 53A8A24A
P 8250 5600
F 0 "#PWR012" H 8250 5600 40  0001 C CNN
F 1 "RFGND" H 8250 5530 40  0000 C CNN
F 2 "" H 8250 5600 60  0000 C CNN
F 3 "" H 8250 5600 60  0000 C CNN
	1    8250 5600
	1    0    0    -1  
$EndComp
Text Label 2350 2300 0    60   ~ 0
V_GPS
$Comp
L RFGND #PWR013
U 1 1 53A8BD54
P 4100 5250
F 0 "#PWR013" H 4100 5250 40  0001 C CNN
F 1 "RFGND" H 4100 5180 40  0000 C CNN
F 2 "" H 4100 5250 60  0000 C CNN
F 3 "" H 4100 5250 60  0000 C CNN
	1    4100 5250
	1    0    0    -1  
$EndComp
Text Label 10400 4850 0    60   ~ 0
RF_IN
$Comp
L RFGND #PWR014
U 1 1 53A8CA84
P 9750 5200
F 0 "#PWR014" H 9750 5200 40  0001 C CNN
F 1 "RFGND" H 9750 5130 40  0000 C CNN
F 2 "" H 9750 5200 60  0000 C CNN
F 3 "" H 9750 5200 60  0000 C CNN
	1    9750 5200
	1    0    0    -1  
$EndComp
$Comp
L RFGND #PWR015
U 1 1 53A8D111
P 2550 5250
F 0 "#PWR015" H 2550 5250 40  0001 C CNN
F 1 "RFGND" H 2550 5180 40  0000 C CNN
F 2 "" H 2550 5250 60  0000 C CNN
F 3 "" H 2550 5250 60  0000 C CNN
	1    2550 5250
	1    0    0    -1  
$EndComp
$Comp
L Capacitor C9
U 1 1 53A8D237
P 2150 4650
AR Path="/53A8D237" Ref="C9"  Part="1" 
AR Path="/53A17A14/53A8D237" Ref="C9"  Part="1" 
F 0 "C9" H 2150 4750 40  0000 L CNN
F 1 "100pF" H 2156 4565 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD0603_reflow" H 2188 4500 30  0001 C CNN
F 3 "" H 2150 4650 60  0000 C CNN
F 4 "Murata" H 2850 3400 60  0001 C CNN "Manu"
F 5 "GRM1885C1H101JA01D" H 2850 3500 60  0001 C CNN "Manu#"
F 6 "0603" H 2850 3600 60  0001 C CNN "Package"
F 7 " " H 2850 4500 60  0001 C CNN "Desc"
	1    2150 4650
	0    1    1    0   
$EndComp
$Comp
L INDUCTOR_SMALL L12
U 1 1 53A8D480
P 1650 5050
F 0 "L12" H 1900 5100 50  0000 C CNN
F 1 "27nH" H 1650 5000 50  0000 C CNN
F 2 "Ruwai:Inductor_SMD0603_reflow" H 1650 5050 60  0001 C CNN
F 3 "" H 1650 5050 60  0000 C CNN
F 4 "Würth Elektronik" H 2850 3400 60  0001 C CNN "Manu"
F 5 "744786127A" H 2850 3500 60  0001 C CNN "Manu#"
F 6 "0603" H 2850 3600 60  0001 C CNN "Package"
F 7 " " H 2850 4500 60  0001 C CNN "Desc"
	1    1650 5050
	0    -1   -1   0   
$EndComp
$Comp
L Capacitor C10
U 1 1 53A8D5E6
P 2450 6150
AR Path="/53A8D5E6" Ref="C10"  Part="1" 
AR Path="/53A17A14/53A8D5E6" Ref="C10"  Part="1" 
F 0 "C10" H 2450 6250 40  0000 L CNN
F 1 "33pF" H 2456 6065 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD0805_HandSoldering" H 2488 6000 30  0001 C CNN
F 3 "" H 2450 6150 60  0000 C CNN
F 4 "AVX" H 3650 3400 60  0001 C CNN "Manu"
F 5 "08051A330JAT2A" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 3600 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    2450 6150
	-1   0    0    1   
$EndComp
$Comp
L RFGND #PWR016
U 1 1 53A8D688
P 2450 6600
F 0 "#PWR016" H 2450 6600 40  0001 C CNN
F 1 "RFGND" H 2450 6530 40  0000 C CNN
F 2 "" H 2450 6600 60  0000 C CNN
F 3 "" H 2450 6600 60  0000 C CNN
	1    2450 6600
	1    0    0    -1  
$EndComp
$Comp
L RFGND #PWR017
U 1 1 53A8D6AC
P 1300 6600
F 0 "#PWR017" H 1300 6600 40  0001 C CNN
F 1 "RFGND" H 1300 6530 40  0000 C CNN
F 2 "" H 1300 6600 60  0000 C CNN
F 3 "" H 1300 6600 60  0000 C CNN
	1    1300 6600
	1    0    0    -1  
$EndComp
$Comp
L testpoint TP2
U 1 1 53A8D917
P 1850 6450
F 0 "TP2" H 1750 6300 60  0000 C CNN
F 1 "P_RFGND" H 1750 6600 60  0000 C CNN
F 2 "Ruwai:testpoint_1" H 1850 6450 60  0001 C CNN
F 3 "" H 1850 6450 60  0000 C CNN
F 4 "RS" H 3650 3400 60  0001 C CNN "Manu"
F 5 "200-203" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "1mm" H 3650 3600 60  0001 C CNN "Package"
F 7 "Custom Description" H 3650 4500 60  0001 C CNN "Desc"
	1    1850 6450
	0    -1   -1   0   
$EndComp
$Comp
L MMBT5401 U14
U 1 1 53A8DD36
P 4150 6550
F 0 "U14" H 3950 6300 60  0000 C CNN
F 1 "MMBT5401" H 4150 6800 60  0000 C CNN
F 2 "Ruwai:SOT23-6" H 4500 6300 60  0001 C CNN
F 3 "" H 4200 6550 60  0000 C CNN
F 4 "Diodes Zetex" H 3650 3400 60  0001 C CNN "Manu"
F 5 "DMMT5401-7-F" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "SOT23 6-pin" H 3650 3600 60  0001 C CNN "Package"
F 7 "PNP Transistor Bipolar" H 3650 4500 60  0001 C CNN "Desc"
	1    4150 6550
	1    0    0    -1  
$EndComp
$Comp
L RFGND #PWR018
U 1 1 53A8DDB6
P 3350 7350
F 0 "#PWR018" H 3350 7350 40  0001 C CNN
F 1 "RFGND" H 3350 7280 40  0000 C CNN
F 2 "" H 3350 7350 60  0000 C CNN
F 3 "" H 3350 7350 60  0000 C CNN
	1    3350 7350
	1    0    0    -1  
$EndComp
$Comp
L RFGND #PWR019
U 1 1 53A8DDDC
P 4900 7400
F 0 "#PWR019" H 4900 7400 40  0001 C CNN
F 1 "RFGND" H 4900 7330 40  0000 C CNN
F 2 "" H 4900 7400 60  0000 C CNN
F 3 "" H 4900 7400 60  0000 C CNN
	1    4900 7400
	1    0    0    -1  
$EndComp
$Comp
L Resistor R24
U 1 1 53A8DE1D
P 3350 7000
F 0 "R24" V 3430 7000 40  0000 C CNN
F 1 "2.2k" V 3357 7001 40  0000 C CNN
F 2 "Ruwai:Resistor_SMD0102_HandSoldering" V 3280 7000 30  0001 C CNN
F 3 "" H 3350 7000 30  0000 C CNN
F 4 "Yageo" H 3650 3400 60  0001 C CNN "Manu"
F 5 "RC0805FR-072K2L" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 3600 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    3350 7000
	1    0    0    -1  
$EndComp
$Comp
L Capacitor C11
U 1 1 53A8DFB8
P 4900 7000
AR Path="/53A8DFB8" Ref="C11"  Part="1" 
AR Path="/53A17A14/53A8DFB8" Ref="C11"  Part="1" 
F 0 "C11" H 4900 7100 40  0000 L CNN
F 1 "0.1uF" H 4906 6915 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD0805_HandSoldering" H 4938 6850 30  0001 C CNN
F 3 "" H 4900 7000 60  0000 C CNN
F 4 "Murata" H 3650 3400 60  0001 C CNN "Manu"
F 5 "GCM21BR71H104KA37L" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 3600 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    4900 7000
	-1   0    0    1   
$EndComp
$Comp
L Resistor R23
U 1 1 53A8E3FF
P 5250 6450
F 0 "R23" V 5330 6450 40  0000 C CNN
F 1 "220" V 5257 6451 40  0000 C CNN
F 2 "Ruwai:Resistor_SMD0102_HandSoldering" V 5180 6450 30  0001 C CNN
F 3 "" H 5250 6450 30  0000 C CNN
F 4 "Yageo" H 3650 3400 60  0001 C CNN "Manu"
F 5 "RC0805FR-07220RL" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 3600 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    5250 6450
	0    1    1    0   
$EndComp
$Comp
L Resistor R22
U 1 1 53A8E493
P 5250 6100
F 0 "R22" V 5330 6100 40  0000 C CNN
F 1 "10" V 5257 6101 40  0000 C CNN
F 2 "Ruwai:Resistor_SMD0805_HandSoldering" V 5180 6100 30  0001 C CNN
F 3 "" H 5250 6100 30  0000 C CNN
F 4 "Yageo" H 3650 3400 60  0001 C CNN "Manu"
F 5 "RC0805FR-0710RL" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 3600 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    5250 6100
	0    1    1    0   
$EndComp
$Comp
L INDUCTOR_SMALL L10
U 1 1 53A8E4F3
P 5250 5750
F 0 "L10" H 5250 5850 50  0000 C CNN
F 1 "27nH" H 5250 5700 50  0000 C CNN
F 2 "Ruwai:Inductor_SMD0805_HandSoldering" H 5250 5750 60  0001 C CNN
F 3 "" H 5250 5750 60  0000 C CNN
F 4 "Murata" H 3650 3400 60  0001 C CNN "Manu"
F 5 "LQW2BAS27NJ00L" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 3600 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    5250 5750
	1    0    0    -1  
$EndComp
$Comp
L INDUCTOR_SMALL L11
U 1 1 53A8E833
P 7300 6100
F 0 "L11" H 7550 6150 50  0000 C CNN
F 1 "27nH" H 7300 6050 50  0000 C CNN
F 2 "Ruwai:Inductor_SMD0805_HandSoldering" H 7300 6100 60  0001 C CNN
F 3 "" H 7300 6100 60  0000 C CNN
F 4 "Murata" H 3650 3400 60  0001 C CNN "Manu"
F 5 "LQW2BAS27NJ00L" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 3600 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    7300 6100
	1    0    0    -1  
$EndComp
$Comp
L RFGND #PWR020
U 1 1 53A8E9D8
P 6550 6800
F 0 "#PWR020" H 6550 6800 40  0001 C CNN
F 1 "RFGND" H 6550 6730 40  0000 C CNN
F 2 "" H 6550 6800 60  0000 C CNN
F 3 "" H 6550 6800 60  0000 C CNN
	1    6550 6800
	1    0    0    -1  
$EndComp
$Comp
L Capacitor C7
U 1 1 53A8EA04
P 6550 6400
AR Path="/53A8EA04" Ref="C7"  Part="1" 
AR Path="/53A17A14/53A8EA04" Ref="C7"  Part="1" 
F 0 "C7" H 6550 6500 40  0000 L CNN
F 1 "1uF" H 6556 6315 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD0805_HandSoldering" H 6588 6250 30  0001 C CNN
F 3 "" H 6550 6400 60  0000 C CNN
F 4 "Murata" H 3650 3400 60  0001 C CNN "Manu"
F 5 "GRM21BR71H105KA12L" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 3600 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    6550 6400
	-1   0    0    1   
$EndComp
$Comp
L Resistor R21
U 1 1 53A8EDEE
P 7300 5750
F 0 "R21" V 7380 5750 40  0000 C CNN
F 1 "1.8k" V 7307 5751 40  0000 C CNN
F 2 "Ruwai:Resistor_SMD0102_HandSoldering" V 7230 5750 30  0001 C CNN
F 3 "" H 7300 5750 30  0000 C CNN
F 4 "Yageo" H 3650 3400 60  0001 C CNN "Manu"
F 5 "RC0805FR-071K8L" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 3600 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    7300 5750
	0    1    1    0   
$EndComp
Text Label 8000 6100 0    60   ~ 0
V_GPS
Text Label 9350 5750 0    60   ~ 0
V_ANT
Text Notes 6050 900  2    60   ~ 0
GPS module
Text Notes 6400 4200 2    60   ~ 0
RF amplifier and filter
$Comp
L Capacitor C5
U 1 1 53A8F911
P 7650 4400
AR Path="/53A8F911" Ref="C5"  Part="1" 
AR Path="/53A17A14/53A8F911" Ref="C5"  Part="1" 
F 0 "C5" H 7650 4500 40  0000 L CNN
F 1 "1nF" H 7656 4315 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD0805_HandSoldering" H 7688 4250 30  0001 C CNN
F 3 "" H 7650 4400 60  0000 C CNN
F 4 "Kemet" H 4400 3400 60  0001 C CNN "Manu"
F 5 "C0805C102J5GACTU" H 4400 3500 60  0001 C CNN "Manu#"
F 6 "0805" H 4400 3600 60  0001 C CNN "Package"
F 7 " " H 4400 4500 60  0001 C CNN "Desc"
	1    7650 4400
	0    -1   -1   0   
$EndComp
$Comp
L RFGND #PWR021
U 1 1 53A8FA87
P 8000 4500
F 0 "#PWR021" H 8000 4500 40  0001 C CNN
F 1 "RFGND" H 8000 4430 40  0000 C CNN
F 2 "" H 8000 4500 60  0000 C CNN
F 3 "" H 8000 4500 60  0000 C CNN
	1    8000 4500
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG022
U 1 1 53AB5F80
P 2150 2500
F 0 "#FLG022" H 2150 2595 30  0001 C CNN
F 1 "PWR_FLAG" H 2150 2680 30  0000 C CNN
F 2 "" H 2150 2500 60  0000 C CNN
F 3 "" H 2150 2500 60  0000 C CNN
	1    2150 2500
	-1   0    0    1   
$EndComp
$Comp
L PWR_FLAG #FLG023
U 1 1 53AB6F4D
P 6850 6250
F 0 "#FLG023" H 6850 6345 30  0001 C CNN
F 1 "PWR_FLAG" H 6850 6430 30  0000 C CNN
F 2 "" H 6850 6250 60  0000 C CNN
F 3 "" H 6850 6250 60  0000 C CNN
	1    6850 6250
	-1   0    0    1   
$EndComp
$Comp
L PWR_FLAG #FLG024
U 1 1 53AB825D
P 7800 3450
F 0 "#FLG024" H 7800 3545 30  0001 C CNN
F 1 "PWR_FLAG" H 7800 3630 30  0000 C CNN
F 2 "" H 7800 3450 60  0000 C CNN
F 3 "" H 7800 3450 60  0000 C CNN
	1    7800 3450
	-1   0    0    1   
$EndComp
$Comp
L PWR_FLAG #FLG025
U 1 1 53ABB828
P 9650 1800
F 0 "#FLG025" H 9650 1895 30  0001 C CNN
F 1 "PWR_FLAG" H 9650 1980 30  0000 C CNN
F 2 "" H 9650 1800 60  0000 C CNN
F 3 "" H 9650 1800 60  0000 C CNN
	1    9650 1800
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG026
U 1 1 53ABBF57
P 4450 2850
F 0 "#FLG026" H 4450 2945 30  0001 C CNN
F 1 "PWR_FLAG" H 4450 3030 30  0000 C CNN
F 2 "" H 4450 2850 60  0000 C CNN
F 3 "" H 4450 2850 60  0000 C CNN
	1    4450 2850
	-1   0    0    1   
$EndComp
$Comp
L CONN_UFL CON3
U 1 1 53A855A9
P 1300 4650
F 0 "CON3" H 1225 4850 60  0000 C CNN
F 1 "CONN_UFL" H 1400 4775 60  0000 C CNN
F 2 "Ruwai:coaxial_u.fl-r-smt-1" H 1325 4450 50  0001 C CNN
F 3 "" H 1325 4650 60  0000 C CNN
F 4 "Hirose" H 3650 3400 60  0001 C CNN "Manu"
F 5 "U.FL-R-SMT-1(10)" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "SMT" H 3650 3600 60  0001 C CNN "Package"
F 7 "Custom Description" H 3650 4500 60  0001 C CNN "Desc"
	1    1300 4650
	-1   0    0    -1  
$EndComp
$Comp
L Capacitor C6
U 1 1 53A989A2
P 8850 6050
AR Path="/53A989A2" Ref="C6"  Part="1" 
AR Path="/53A17A14/53A989A2" Ref="C6"  Part="1" 
F 0 "C6" H 8850 6150 40  0000 L CNN
F 1 "1nF" H 8856 5965 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD0805_HandSoldering" H 8888 5900 30  0001 C CNN
F 3 "" H 8850 6050 60  0000 C CNN
F 4 "Kemet" H 3650 3400 60  0001 C CNN "Manu"
F 5 "C0805C102J5GACTU" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 3600 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    8850 6050
	-1   0    0    1   
$EndComp
$Comp
L RFGND #PWR027
U 1 1 53A98AD2
P 8850 6400
F 0 "#PWR027" H 8850 6400 40  0001 C CNN
F 1 "RFGND" H 8850 6330 40  0000 C CNN
F 2 "" H 8850 6400 60  0000 C CNN
F 3 "" H 8850 6400 60  0000 C CNN
	1    8850 6400
	1    0    0    -1  
$EndComp
$Comp
L Capacitor C1
U 1 1 53A9926E
P 3800 2150
AR Path="/53A9926E" Ref="C1"  Part="1" 
AR Path="/53A17A14/53A9926E" Ref="C1"  Part="1" 
F 0 "C1" H 3800 2250 40  0000 L CNN
F 1 "1nF" H 3806 2065 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD0805_HandSoldering" H 3838 2000 30  0001 C CNN
F 3 "" H 3800 2150 60  0000 C CNN
F 4 "Kemet" H 3650 3400 60  0001 C CNN "Manu"
F 5 "C0805C102J5GACTU" H 3650 3500 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 3600 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    3800 2150
	0    1    1    0   
$EndComp
$Comp
L GND-RESCUE-ruwai_gps_timing_shield #PWR028
U 1 1 53A99489
P 3200 2250
F 0 "#PWR028" H 3200 2250 30  0001 C CNN
F 1 "GND" H 3200 2180 30  0001 C CNN
F 2 "" H 3200 2250 60  0000 C CNN
F 3 "" H 3200 2250 60  0000 C CNN
	1    3200 2250
	1    0    0    -1  
$EndComp
$Comp
L inductor L6
U 1 1 548AF347
P 5450 4650
F 0 "L6" H 5450 4700 40  0000 C BNN
F 1 "5.6nH" H 5450 4600 40  0000 C CNN
F 2 "Ruwai:Inductor_SMD0603_reflow" H 5350 4650 60  0001 C CNN
F 3 "" H 5350 4650 60  0000 C CNN
F 4 "Würth Elektronik" H 5450 4450 60  0001 L BNN "Manu"
F 5 "744786056A" H 5450 4350 60  0001 L BNN "Manu#"
F 6 "0603" H 5450 4250 60  0001 L BNN "Package"
F 7 "Custom description." H 5450 4150 60  0001 L BNN "Desc"
	1    5450 4650
	1    0    0    -1  
$EndComp
$Comp
L Capacitor C32
U 1 1 548AF73A
P 4900 4950
AR Path="/548AF73A" Ref="C32"  Part="1" 
AR Path="/53A17A14/548AF73A" Ref="C32"  Part="1" 
F 0 "C32" H 4900 5050 40  0000 L CNN
F 1 "1pF" H 4906 4865 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD0603_reflow" H 4938 4800 30  0001 C CNN
F 3 "" H 4900 4950 60  0000 C CNN
F 4 "Vishay" H 1650 3950 60  0001 C CNN "Manu"
F 5 "VJ0603D1R0BXPAJ" H 1650 4050 60  0001 C CNN "Manu#"
F 6 "0603" H 1650 4150 60  0001 C CNN "Package"
F 7 " " H 1650 5050 60  0001 C CNN "Desc"
	1    4900 4950
	1    0    0    -1  
$EndComp
$Comp
L RFGND #PWR029
U 1 1 548AFDF1
P 5700 5250
F 0 "#PWR029" H 5700 5250 40  0001 C CNN
F 1 "RFGND" H 5700 5180 40  0000 C CNN
F 2 "" H 5700 5250 60  0000 C CNN
F 3 "" H 5700 5250 60  0000 C CNN
	1    5700 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 1500 5650 1500
Wire Wire Line
	5450 1600 5650 1600
Wire Wire Line
	5450 1700 5650 1700
Wire Wire Line
	4950 1700 4750 1700
Wire Wire Line
	4950 1800 4750 1800
Wire Wire Line
	4200 1500 4950 1500
Wire Wire Line
	4200 1500 4100 1600
Wire Wire Line
	4200 1600 4950 1600
Wire Wire Line
	4200 1600 4100 1500
Wire Wire Line
	4100 1500 3550 1500
Wire Wire Line
	4100 1600 3550 1600
Wire Wire Line
	2000 2000 5650 2000
Wire Wire Line
	3550 1300 3800 1300
Wire Wire Line
	3800 1300 3800 2000
Connection ~ 3800 2000
Wire Wire Line
	2150 1500 2250 1500
Wire Wire Line
	2150 1400 2250 1400
Connection ~ 2150 1500
Wire Wire Line
	2250 1300 2150 1300
Connection ~ 2150 1400
Wire Wire Line
	3550 1400 3700 1400
Wire Wire Line
	3700 1400 3700 1050
Wire Wire Line
	3700 1050 1850 1050
Connection ~ 2150 1300
Wire Wire Line
	1850 1050 1850 1100
Wire Wire Line
	2150 1050 2150 1500
Connection ~ 2150 1050
Wire Wire Line
	1500 2000 1300 2000
Wire Wire Line
	1700 1600 2250 1600
Connection ~ 2150 1600
Wire Wire Line
	1700 1600 1700 1750
Connection ~ 1850 1600
Wire Wire Line
	5650 2450 5400 2450
Wire Wire Line
	5400 2450 5400 2350
Wire Wire Line
	5400 2350 5200 2350
Wire Wire Line
	5650 2550 5200 2550
Wire Wire Line
	4700 2350 4400 2350
Wire Wire Line
	4700 2550 4400 2550
Wire Wire Line
	5650 2650 5400 2650
Wire Wire Line
	5400 2650 5400 2750
Wire Wire Line
	5400 2750 4400 2750
Wire Wire Line
	4700 2750 4700 3000
Connection ~ 4700 2750
Wire Wire Line
	5650 2100 5500 2100
Wire Wire Line
	5500 2100 5500 3550
Wire Wire Line
	5650 2850 5500 2850
Connection ~ 5500 2850
Wire Wire Line
	5650 2950 5500 2950
Connection ~ 5500 2950
Wire Wire Line
	5400 3150 5500 3150
Connection ~ 5500 3150
Wire Wire Line
	3900 2750 3300 2750
Wire Wire Line
	4700 3400 4700 3550
Wire Wire Line
	7450 1500 7700 1500
Wire Wire Line
	8200 1500 8700 1500
Wire Wire Line
	7450 1700 8700 1700
Wire Wire Line
	7450 1800 8700 1800
Wire Wire Line
	7450 1900 9700 1900
Wire Wire Line
	10200 1900 10550 1900
Wire Wire Line
	9450 2050 9450 1900
Connection ~ 9450 1900
Wire Wire Line
	9150 2000 9150 1900
Connection ~ 9150 1900
Wire Wire Line
	9450 2450 9450 2650
Wire Wire Line
	9150 2500 9150 2650
Wire Wire Line
	7450 2550 7850 2550
Wire Wire Line
	7450 2850 7850 2850
Wire Wire Line
	7450 2750 7550 2750
Wire Wire Line
	7550 2750 7550 3550
Wire Wire Line
	7450 2950 7550 2950
Connection ~ 7550 2950
Wire Wire Line
	7050 4850 7350 4850
Wire Wire Line
	7750 4850 8350 4850
Wire Wire Line
	7050 4750 8350 4750
Wire Wire Line
	8250 4750 8250 5600
Wire Wire Line
	8250 4950 8350 4950
Connection ~ 8250 4750
Connection ~ 8250 4950
Wire Wire Line
	2150 2000 2150 2500
Wire Wire Line
	2150 2300 2350 2300
Connection ~ 2150 2000
Wire Wire Line
	9550 4850 10400 4850
Wire Wire Line
	9550 4750 9750 4750
Wire Wire Line
	9750 4750 9750 5200
Wire Wire Line
	9550 4950 9750 4950
Connection ~ 9750 4950
Wire Wire Line
	2750 4550 2550 4550
Wire Wire Line
	2550 4550 2550 5250
Wire Wire Line
	2750 4750 2550 4750
Connection ~ 2550 4750
Wire Wire Line
	2350 4650 2750 4650
Wire Wire Line
	1450 4650 1950 4650
Wire Wire Line
	1650 4650 1650 4800
Wire Wire Line
	2450 5750 2450 5950
Wire Wire Line
	2450 6350 2450 6600
Wire Wire Line
	1300 6350 2450 6350
Wire Wire Line
	1300 4800 1300 6600
Connection ~ 1850 6350
Connection ~ 1300 6350
Connection ~ 1650 4650
Wire Wire Line
	4900 7200 4900 7400
Wire Wire Line
	3350 7250 3350 7350
Wire Wire Line
	3350 6450 3350 6750
Wire Wire Line
	3350 6650 3600 6650
Wire Wire Line
	4700 6650 4900 6650
Wire Wire Line
	4900 5750 4900 6800
Wire Wire Line
	3600 6550 3450 6550
Wire Wire Line
	3450 6550 3450 6100
Wire Wire Line
	3450 6100 5000 6100
Wire Wire Line
	4800 6100 4800 6550
Wire Wire Line
	4800 6550 4700 6550
Wire Wire Line
	4700 6450 5000 6450
Connection ~ 4800 6100
Wire Wire Line
	5500 6450 5700 6450
Wire Wire Line
	5500 6100 7050 6100
Wire Wire Line
	7550 6100 8000 6100
Wire Wire Line
	6550 6800 6550 6600
Wire Wire Line
	6550 5400 6550 6200
Connection ~ 6550 6100
Wire Wire Line
	1650 5750 5000 5750
Connection ~ 2450 5750
Wire Wire Line
	5500 5750 7050 5750
Wire Wire Line
	7550 5750 9350 5750
Wire Wire Line
	7300 4400 7300 5400
Wire Wire Line
	7300 4650 7050 4650
Connection ~ 7300 4650
Wire Wire Line
	7850 4400 8000 4400
Wire Wire Line
	8000 4400 8000 4500
Wire Wire Line
	7300 4400 7450 4400
Wire Wire Line
	5700 6450 5700 6100
Connection ~ 5700 6100
Wire Wire Line
	3600 6450 3350 6450
Connection ~ 3350 6650
Connection ~ 2150 2300
Wire Wire Line
	6850 6250 6850 6100
Connection ~ 6850 6100
Wire Wire Line
	7800 3450 7800 3350
Wire Wire Line
	7800 3350 7550 3350
Connection ~ 7550 3350
Wire Wire Line
	5450 1800 5650 1800
Wire Wire Line
	9650 1800 9650 1900
Connection ~ 9650 1900
Wire Wire Line
	4450 2850 4450 2750
Connection ~ 4450 2750
Connection ~ 4900 5750
Connection ~ 4900 6650
Wire Wire Line
	8850 5850 8850 5750
Connection ~ 8850 5750
Wire Wire Line
	8850 6250 8850 6400
Wire Wire Line
	4000 2150 4300 2150
Wire Wire Line
	4300 2150 4300 2000
Connection ~ 4300 2000
Wire Wire Line
	3600 2150 3200 2150
Wire Wire Line
	3200 2150 3200 2250
Wire Wire Line
	4900 4750 4900 4650
Wire Wire Line
	3950 4650 5250 4650
Wire Wire Line
	5650 4650 5850 4650
Wire Wire Line
	3950 4550 4100 4550
Wire Wire Line
	4100 4550 4100 5250
Wire Wire Line
	3950 4750 4100 4750
Connection ~ 4100 4750
Connection ~ 4900 4650
Wire Wire Line
	5850 4750 5700 4750
Wire Wire Line
	5700 4750 5700 5250
Wire Wire Line
	5850 4850 5700 4850
Connection ~ 5700 4850
$Comp
L RFGND #PWR030
U 1 1 548B01D5
P 4900 5250
F 0 "#PWR030" H 4900 5250 40  0001 C CNN
F 1 "RFGND" H 4900 5180 40  0000 C CNN
F 2 "" H 4900 5250 60  0000 C CNN
F 3 "" H 4900 5250 60  0000 C CNN
	1    4900 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 5150 4900 5250
Wire Wire Line
	1650 5300 1650 5750
Wire Wire Line
	7300 5400 6550 5400
$Comp
L inductor L17
U 1 1 548AF34A
P 7950 5150
F 0 "L17" H 7950 5200 40  0000 C BNN
F 1 "6.8nH" H 7950 5100 40  0000 C CNN
F 2 "Ruwai:Inductor_SMD0603_reflow" H 7850 5150 60  0001 C CNN
F 3 "" H 7850 5150 60  0000 C CNN
F 4 "Würth Elektronik" H 7950 4950 60  0001 L BNN "Manu"
F 5 "744786068A" H 7950 4850 60  0001 L BNN "Manu#"
F 6 "0603" H 7950 4750 60  0001 L BNN "Package"
F 7 "Custom description." H 7950 4650 60  0001 L BNN "Desc"
	1    7950 5150
	0    1    1    0   
$EndComp
Wire Wire Line
	7950 4950 7950 4850
Connection ~ 7950 4850
Wire Wire Line
	7950 5350 7950 5450
Wire Wire Line
	7950 5450 8250 5450
Connection ~ 8250 5450
$EndSCHEMATC
