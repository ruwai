EESchema Schematic File Version 2
LIBS:ruwai_gps_timing_shield-rescue
LIBS:ruwai
LIBS:74xgxx
LIBS:74xx
LIBS:ac-dc
LIBS:actel
LIBS:adc-dac
LIBS:analog_switches
LIBS:atmel
LIBS:audio
LIBS:brooktre
LIBS:cmos4000
LIBS:cmos_ieee
LIBS:conn
LIBS:contrib
LIBS:cypress
LIBS:dc-dc
LIBS:device
LIBS:digital-audio
LIBS:display
LIBS:dsp
LIBS:elec-unifil
LIBS:ftdi
LIBS:gennum
LIBS:hc11
LIBS:intel
LIBS:interface
LIBS:ir
LIBS:linear
LIBS:memory
LIBS:microchip
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers
LIBS:motor_drivers
LIBS:motorola
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:opto
LIBS:philips
LIBS:power
LIBS:powerint
LIBS:pspice
LIBS:references
LIBS:regul
LIBS:relays
LIBS:rfcom
LIBS:sensors
LIBS:silabs
LIBS:siliconi
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:texas
LIBS:transf
LIBS:transistors
LIBS:ttl_ieee
LIBS:valves
LIBS:video
LIBS:xilinx
LIBS:ruwai_gps_timing_shield-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 6
Title "Ruwai GPS Timing Shield"
Date "2017-09-12"
Rev "3"
Comp "Copyright 2014 Mertl Research GmbH"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 "This documentation describes Open Hardware and is licensed under the CERN OHL v. 1.2."
$EndDescr
$Sheet
S 1700 3800 1200 2000
U 53A179DC
F0 "arduino interface" 60
F1 "arduino_interface.sch" 60
F2 "SCL_ARD" B R 2900 4300 60 
F3 "SDA_ARD" I R 2900 4200 60 
F4 "RXD2_ARD" I R 2900 4050 60 
F5 "TXD2_ARD" O R 2900 3950 60 
F6 "SQW_ARD" I R 2900 5550 60 
F7 "PPS_ARD" I R 2900 5050 60 
F8 "FREQ_ARD" I R 2900 5150 60 
F9 "5V_CONN" O L 1700 3950 60 
F10 "FREQ_GPS_3" I R 2900 5350 60 
F11 "USB_VBUS" O R 2900 4650 60 
F12 "USB_DM" B R 2900 4750 60 
F13 "USB_DP" B R 2900 4850 60 
F14 "~EN_USB_ARD~" O R 2900 4550 60 
F15 "DQT_ARD" B R 2900 5650 60 
F16 "RST_GPS_ARD" O R 2900 5250 60 
$EndSheet
$Sheet
S 8950 1400 850  1350
U 53A17A14
F0 "GPS" 60
F1 "gps.sch" 60
F2 "TX_GPS" O L 8950 1750 60 
F3 "RX_GPS" I L 8950 1850 60 
F4 "GPS3.3V" I L 8950 1500 60 
F5 "FREQ_GPS" O L 8950 2000 60 
F6 "RST_GPS" I L 8950 2600 60 
F7 "VBAT" I L 8950 1600 60 
F8 "PPS_GPS" O L 8950 2100 60 
F9 "USB_DP" B L 8950 2250 60 
F10 "USB_DM" B L 8950 2350 60 
F11 "VUSB" I L 8950 2450 60 
$EndSheet
$Sheet
S 1650 1350 1250 1350
U 53A17A2E
F0 "power supply" 60
F1 "power_supply.sch" 60
F2 "5V_CONN" I L 1650 1500 60 
F3 "RTC3.3V" O R 2900 1950 60 
F4 "3.3V" O R 2900 1650 60 
F5 "GPS3.3V" O R 2900 1800 60 
F6 "5V" O R 2900 1500 60 
F7 "~EN_USB_ARD" I L 1650 1800 60 
F8 "VUSB" O R 2900 2250 60 
F9 "VBAT" O R 2900 2100 60 
F10 "USB_VBUS" I L 1650 1650 60 
$EndSheet
$Sheet
S 5200 1350 1300 1400
U 53A18860
F0 "level_shifting" 60
F1 "level_shifting.sch" 60
F2 "RX_GPS" O L 5200 1750 60 
F3 "3.3V" I L 5200 1550 60 
F4 "5V" I L 5200 1450 60 
F5 "TXD2_ARD" I R 6500 1750 60 
F6 "TX_GPS" I L 5200 1850 60 
F7 "RXD2_ARD" O R 6500 1850 60 
F8 "SQW_RTC" I L 5200 2450 60 
F9 "SQW_ARD" O R 6500 2450 60 
F10 "PPS_GPS" I L 5200 1950 60 
F11 "FREQ_GPS" I L 5200 2050 60 
F12 "PPS_ARD" O R 6500 1950 60 
F13 "FREQ_ARD" O R 6500 2050 60 
F14 "RST_GPS" O L 5200 2250 60 
F15 "RST_GPS_ARD" I R 6500 2250 60 
F16 "FREQ_GPS_3" O R 6500 2150 60 
$EndSheet
Text Label 1450 1500 2    60   ~ 0
5V_CONN
Text Label 1450 1650 2    60   ~ 0
USB_VBUS
Text Label 1450 1800 2    60   ~ 0
~EN_USB_ARD
Text Label 1450 3950 2    60   ~ 0
5V_CONN
Text Label 3150 4550 0    60   ~ 0
~EN_USB_ARD
Text Label 3150 4650 0    60   ~ 0
USB_VBUS
Text Label 3200 1500 0    60   ~ 0
5V
Text Label 3200 1650 0    60   ~ 0
3V
Text Label 4950 1450 2    60   ~ 0
5V
Text Label 4950 1550 2    60   ~ 0
3V
Text Label 6750 1750 0    60   ~ 0
TXD2_ARD
Text Label 6750 1850 0    60   ~ 0
RXD2_ARD
Text Label 6750 1950 0    60   ~ 0
PPS_ARD
Text Label 6750 2050 0    60   ~ 0
FREQ_ARD
Text Label 6750 2150 0    60   ~ 0
FREQ_GPS_3
Text Label 6750 2250 0    60   ~ 0
RST_GPS_ARD
Text Label 4950 1750 2    60   ~ 0
RX_GPS
Text Label 4950 1850 2    60   ~ 0
TX_GPS
Text Label 4950 1950 2    60   ~ 0
PPS_GPS
Text Label 4950 2050 2    60   ~ 0
FREQ_GPS
Text Label 3150 3950 0    60   ~ 0
TXD2_ARD
Text Label 3150 4050 0    60   ~ 0
RXD2_ARD
Text Label 3150 5050 0    60   ~ 0
PPS_ARD
Text Label 3150 5150 0    60   ~ 0
FREQ_ARD
Text Label 3150 5250 0    60   ~ 0
RST_GPS_ARD
Text Label 3150 5350 0    60   ~ 0
FREQ_GPS_3
Text Label 3150 5550 0    60   ~ 0
SQW_ARD
Wire Wire Line
	1650 1500 1450 1500
Wire Wire Line
	1650 1650 1450 1650
Wire Wire Line
	1650 1800 1450 1800
Wire Wire Line
	1700 3950 1450 3950
Wire Wire Line
	2900 3950 3150 3950
Wire Wire Line
	2900 4050 3150 4050
Wire Wire Line
	2900 4200 3150 4200
Wire Wire Line
	2900 4300 3150 4300
Wire Wire Line
	2900 4550 3150 4550
Wire Wire Line
	2900 4650 3150 4650
Wire Wire Line
	2900 4750 3150 4750
Wire Wire Line
	2900 4850 3150 4850
Wire Wire Line
	2900 5050 3150 5050
Wire Wire Line
	2900 5150 3150 5150
Wire Wire Line
	2900 5550 3150 5550
Wire Wire Line
	2900 5650 3150 5650
Wire Wire Line
	2900 5250 3150 5250
Wire Wire Line
	2900 5350 3150 5350
Wire Wire Line
	2900 1500 3200 1500
Wire Wire Line
	2900 1650 3200 1650
Wire Wire Line
	5200 1450 4950 1450
Wire Wire Line
	5200 1550 4950 1550
Wire Wire Line
	6500 1750 6750 1750
Wire Wire Line
	6500 1850 6750 1850
Wire Wire Line
	6500 1950 6750 1950
Wire Wire Line
	6500 2050 6750 2050
Wire Wire Line
	6500 2150 6750 2150
Wire Wire Line
	6500 2250 6750 2250
Wire Wire Line
	4950 1750 5200 1750
Wire Wire Line
	5200 1850 4950 1850
Wire Wire Line
	5200 1950 4950 1950
Wire Wire Line
	5200 2050 4950 2050
Wire Wire Line
	6500 2450 6750 2450
Text Label 6750 2450 0    60   ~ 0
SQW_ARD
Wire Wire Line
	5200 2250 4950 2250
Wire Wire Line
	5200 2450 4950 2450
Text Label 4950 2250 2    60   ~ 0
RST_GPS
Text Label 4950 2450 2    60   ~ 0
SQW_RTC
Wire Wire Line
	8950 4000 8650 4000
Wire Wire Line
	8950 4100 8650 4100
Wire Wire Line
	8950 4200 8650 4200
Wire Wire Line
	8950 4400 8650 4400
Wire Wire Line
	8950 4500 8650 4500
Wire Wire Line
	8950 4600 8650 4600
Wire Wire Line
	8950 4700 8650 4700
Wire Wire Line
	8950 4800 8650 4800
Text Label 8650 4000 2    60   ~ 0
5V
Text Label 8650 4100 2    60   ~ 0
RTC3.3V
Text Label 8650 4200 2    60   ~ 0
VBAT
Wire Wire Line
	2900 2100 3200 2100
Text Label 3200 2100 0    60   ~ 0
VBAT
Text Label 3150 5650 0    60   ~ 0
DQT_ARD
Text Label 8650 4400 2    60   ~ 0
DQT_ARD
Text Label 8650 4500 2    60   ~ 0
PPS_GPS
Text Label 8650 4600 2    60   ~ 0
SQW_RTC
Text Label 8650 4700 2    60   ~ 0
SCL_ARD
Text Label 8650 4800 2    60   ~ 0
SDA_ARD
Wire Wire Line
	8950 1500 8700 1500
Wire Wire Line
	8950 1600 8700 1600
Wire Wire Line
	8950 1750 8700 1750
Wire Wire Line
	8950 1850 8700 1850
Wire Wire Line
	8950 2000 8700 2000
Wire Wire Line
	8950 2100 8700 2100
Wire Wire Line
	8950 2250 8700 2250
Wire Wire Line
	8950 2350 8700 2350
Wire Wire Line
	8950 2450 8700 2450
Wire Wire Line
	8950 2600 8700 2600
Text Label 8700 1500 2    60   ~ 0
GPS3.3V
Text Label 8700 1600 2    60   ~ 0
VBAT
Text Label 8700 1750 2    60   ~ 0
TX_GPS
Text Label 8700 1850 2    60   ~ 0
RX_GPS
Text Label 8700 2000 2    60   ~ 0
FREQ_GPS
Text Label 8700 2100 2    60   ~ 0
PPS_GPS
Text Label 8700 2250 2    60   ~ 0
USB_DP
Text Label 8700 2350 2    60   ~ 0
USB_DM
Text Label 8700 2450 2    60   ~ 0
VUSB
Text Label 8700 2600 2    60   ~ 0
RST_GPS
Wire Wire Line
	2900 1800 3200 1800
Wire Wire Line
	2900 1950 3200 1950
Text Label 3200 1800 0    60   ~ 0
GPS3.3V
Text Label 3200 1950 0    60   ~ 0
RTC3.3V
Wire Wire Line
	2900 2250 3200 2250
Text Label 3200 2250 0    60   ~ 0
VUSB
Text Label 3150 4200 0    60   ~ 0
SDA_ARD
Text Label 3150 4300 0    60   ~ 0
SCL_ARD
Text Label 3150 4750 0    60   ~ 0
USB_DM
Text Label 3150 4850 0    60   ~ 0
USB_DP
$Sheet
S 8950 3800 1050 1300
U 53A17A3E
F0 "real time clock" 60
F1 "real_time_clock.sch" 60
F2 "5V" I L 8950 4000 60 
F3 "DQT_ARD" B L 8950 4400 60 
F4 "PPS_GPS" I L 8950 4500 60 
F5 "SQW_RTC" O L 8950 4600 60 
F6 "SCL_ARD" I L 8950 4700 60 
F7 "SDA_ARD" B L 8950 4800 60 
F8 "VBAT" I L 8950 4200 60 
F9 "RTC3.3V" I L 8950 4100 60 
$EndSheet
$EndSCHEMATC
