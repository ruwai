EESchema Schematic File Version 2
LIBS:ruwai_gps_timing_shield-rescue
LIBS:ruwai
LIBS:74xgxx
LIBS:74xx
LIBS:ac-dc
LIBS:actel
LIBS:adc-dac
LIBS:analog_switches
LIBS:atmel
LIBS:audio
LIBS:brooktre
LIBS:cmos4000
LIBS:cmos_ieee
LIBS:conn
LIBS:contrib
LIBS:cypress
LIBS:dc-dc
LIBS:device
LIBS:digital-audio
LIBS:display
LIBS:dsp
LIBS:elec-unifil
LIBS:ftdi
LIBS:gennum
LIBS:hc11
LIBS:intel
LIBS:interface
LIBS:ir
LIBS:linear
LIBS:memory
LIBS:microchip
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers
LIBS:motor_drivers
LIBS:motorola
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:opto
LIBS:philips
LIBS:power
LIBS:powerint
LIBS:pspice
LIBS:references
LIBS:regul
LIBS:relays
LIBS:rfcom
LIBS:sensors
LIBS:silabs
LIBS:siliconi
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:texas
LIBS:transf
LIBS:transistors
LIBS:ttl_ieee
LIBS:valves
LIBS:video
LIBS:xilinx
LIBS:ruwai_gps_timing_shield-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 6
Title "Ruwai GPS Timing Shield"
Date "2017-09-12"
Rev "3"
Comp "Copyright 2014 Mertl Research GmbH"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 "This documentation describes Open Hardware and is licensed under the CERN OHL v. 1.2."
$EndDescr
$Comp
L GND-RESCUE-ruwai_gps_timing_shield #PWR01
U 1 1 53A17C69
P 1050 3350
F 0 "#PWR01" H 1050 3350 30  0001 C CNN
F 1 "GND" H 1050 3280 30  0001 C CNN
F 2 "" H 1050 3350 60  0000 C CNN
F 3 "" H 1050 3350 60  0000 C CNN
	1    1050 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 2950 1050 2950
Wire Wire Line
	1050 2950 1050 3350
Wire Wire Line
	1700 3050 1050 3050
Connection ~ 1050 3050
NoConn ~ 1700 2450
NoConn ~ 1700 2550
NoConn ~ 1700 2650
NoConn ~ 1700 2750
NoConn ~ 1700 2850
NoConn ~ 1700 3150
NoConn ~ 1700 3550
NoConn ~ 1700 3650
NoConn ~ 1700 3750
NoConn ~ 1700 3850
NoConn ~ 1700 3950
NoConn ~ 1700 4050
NoConn ~ 1700 4150
NoConn ~ 1700 4250
NoConn ~ 1700 4450
NoConn ~ 1700 4550
NoConn ~ 1700 4650
NoConn ~ 1700 4750
NoConn ~ 1700 4850
NoConn ~ 1700 4950
NoConn ~ 1700 5050
NoConn ~ 1700 5150
$Comp
L GND-RESCUE-ruwai_gps_timing_shield #PWR02
U 1 1 53A17CC0
P 1050 5750
F 0 "#PWR02" H 1050 5750 30  0001 C CNN
F 1 "GND" H 1050 5680 30  0001 C CNN
F 2 "" H 1050 5750 60  0000 C CNN
F 3 "" H 1050 5750 60  0000 C CNN
	1    1050 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 5550 1050 5750
Wire Wire Line
	1700 5650 1050 5650
Wire Wire Line
	1700 5550 1050 5550
Connection ~ 1050 5650
Wire Wire Line
	1700 7350 1350 7350
Wire Wire Line
	4900 7250 5200 7250
Wire Wire Line
	4900 7050 5200 7050
Text HLabel 5200 7050 2    60   Output ~ 0
~EN_USB_ARD~
Text HLabel 5200 7250 2    60   BiDi ~ 0
DQT_ARD
NoConn ~ 1700 5850
NoConn ~ 1700 5950
NoConn ~ 1700 6050
NoConn ~ 1700 6150
NoConn ~ 1700 6250
NoConn ~ 1700 6350
NoConn ~ 1700 6450
NoConn ~ 1700 6550
NoConn ~ 1700 6650
NoConn ~ 1700 6750
NoConn ~ 1700 6850
NoConn ~ 1700 6950
NoConn ~ 1700 7050
NoConn ~ 1700 7250
NoConn ~ 4900 7350
NoConn ~ 4900 7150
NoConn ~ 4900 6950
NoConn ~ 4900 6750
NoConn ~ 4900 6650
NoConn ~ 4900 6550
NoConn ~ 4900 6450
NoConn ~ 4900 6350
NoConn ~ 4900 6250
NoConn ~ 4900 6150
NoConn ~ 4900 6050
NoConn ~ 4900 5950
NoConn ~ 4900 5850
NoConn ~ 4900 5650
NoConn ~ 4900 5550
Wire Wire Line
	4900 5150 5250 5150
Wire Wire Line
	4900 5050 5250 5050
NoConn ~ 4900 4950
NoConn ~ 4900 4850
Wire Wire Line
	4900 4750 5200 4750
Wire Wire Line
	4900 4650 5200 4650
NoConn ~ 4900 4550
NoConn ~ 4900 4450
NoConn ~ 4900 4250
NoConn ~ 4900 4150
Wire Wire Line
	4900 4050 5200 4050
Wire Wire Line
	4900 3950 5200 3950
NoConn ~ 4900 3850
NoConn ~ 4900 3750
NoConn ~ 4900 3650
NoConn ~ 4900 3550
NoConn ~ 4900 3350
NoConn ~ 4900 3250
NoConn ~ 4900 3150
NoConn ~ 4900 3050
NoConn ~ 4900 2950
NoConn ~ 4900 2850
NoConn ~ 4900 2650
NoConn ~ 4900 2550
NoConn ~ 4900 2450
$Comp
L INDUCTOR_SMALL L15
U 1 1 53A18204
P 5500 5050
F 0 "L15" H 5750 5100 40  0000 C CNN
F 1 "600" H 5500 5125 40  0000 C CNN
F 2 "Ruwai:Inductor_SMD0805_HandSoldering" H 5500 5050 60  0001 C CNN
F 3 "" H 5500 5050 60  0000 C CNN
F 4 "Murata" H 3650 4200 60  0001 C CNN "Manu"
F 5 "BLM21BD601SN1D" H 3650 4300 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 4400 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    5500 5050
	1    0    0    -1  
$EndComp
$Comp
L INDUCTOR_SMALL L16
U 1 1 53A18323
P 5500 5150
F 0 "L16" H 5750 5100 40  0000 C CNN
F 1 "600" H 5500 5100 40  0000 C CNN
F 2 "Ruwai:Inductor_SMD0805_HandSoldering" H 5500 5150 60  0001 C CNN
F 3 "" H 5500 5150 60  0000 C CNN
F 4 "Murata" H 3650 4200 60  0001 C CNN "Manu"
F 5 "BLM21BD601SN1D" H 3650 4300 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 4400 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    5500 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 5150 6150 5150
Text HLabel 6150 5150 2    60   BiDi ~ 0
SCL_ARD
Text HLabel 6150 5050 2    60   Input ~ 0
SDA_ARD
Text HLabel 5200 4750 2    60   Input ~ 0
RXD2_ARD
Text HLabel 5200 4650 2    60   Output ~ 0
TXD2_ARD
Text HLabel 5200 4050 2    60   Input ~ 0
SQW_ARD
Text HLabel 5200 3950 2    60   Input ~ 0
PPS_ARD
Text HLabel 1350 7350 0    60   Input ~ 0
FREQ_ARD
$Comp
L INDUCTOR_SMALL L18
U 1 1 53A18F15
P 5550 2150
F 0 "L18" H 5550 2100 40  0000 C CNN
F 1 "220" H 5550 2225 40  0000 C CNN
F 2 "Ruwai:Inductor_SMD0805_HandSoldering" H 5550 2150 60  0001 C CNN
F 3 "" H 5550 2150 60  0000 C CNN
F 4 "Murata" H 50  3000 60  0001 C CNN "Manu"
F 5 "BLM21PG221SN1D" H 50  3100 60  0001 C CNN "Manu#"
F 6 "0805" H 50  3200 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    5550 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 2150 5300 2150
Wire Wire Line
	5800 2150 6700 2150
Text HLabel 6700 2150 2    60   Output ~ 0
5V_CONN
$Comp
L GND-RESCUE-ruwai_gps_timing_shield #PWR03
U 1 1 53A18FEA
P 5050 2350
F 0 "#PWR03" H 5050 2350 30  0001 C CNN
F 1 "GND" H 5050 2280 30  0001 C CNN
F 2 "" H 5050 2350 60  0000 C CNN
F 3 "" H 5050 2350 60  0000 C CNN
	1    5050 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 2050 5050 2050
Text HLabel 5200 6850 2    60   Input ~ 0
FREQ_GPS_3
NoConn ~ 4900 1750
NoConn ~ 4900 1650
NoConn ~ 4900 1450
NoConn ~ 4900 1350
NoConn ~ 4900 1250
$Comp
L USB_MICRO CON1
U 1 1 53A192CC
P 7300 3650
F 0 "CON1" H 7250 3200 60  0000 C CNN
F 1 "USB_MICRO" H 7400 4000 60  0000 C CNN
F 2 "Ruwai:JAE-DX4R005J91-micro-usb" H 7800 3100 60  0001 C CNN
F 3 "" H 7300 3650 60  0000 C CNN
F 4 "JAE" H 3650 1550 60  0001 C CNN "Manu"
F 5 "DX4R005J91" H 3650 1650 60  0001 C CNN "Manu#"
F 6 "micro USB" H 3650 1750 60  0001 C CNN "Package"
F 7 "Micro USB jack" H 3650 4500 60  0001 C CNN "Desc"
	1    7300 3650
	-1   0    0    -1  
$EndComp
$Comp
L USBLC6-2 U21
U 1 1 53A1982A
P 8800 3650
F 0 "U21" H 8550 3400 60  0000 C CNN
F 1 "USBLC6-2" H 8750 3900 60  0000 C CNN
F 2 "Ruwai:SOT23-6" H 9150 3400 60  0001 C CNN
F 3 "" H 8800 3500 60  0000 C CNN
F 4 "STMicroelectronics" H 3650 1550 60  0001 C CNN "Manu"
F 5 "USBLC6-2SC6" H 3650 1650 60  0001 C CNN "Manu#"
F 6 "SOT23 6-pin" H 3650 1750 60  0001 C CNN "Package"
F 7 "Very low capacitance ESD protection" H 3650 4500 60  0001 C CNN "Desc"
	1    8800 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 3550 8200 3550
NoConn ~ 7750 3750
Wire Wire Line
	7750 3650 8200 3650
Wire Wire Line
	7750 3850 8000 3850
Wire Wire Line
	8000 3750 8000 4200
Wire Wire Line
	8000 3750 8200 3750
Wire Wire Line
	8000 3950 7750 3950
Connection ~ 8000 3850
$Comp
L GND-RESCUE-ruwai_gps_timing_shield #PWR04
U 1 1 53A19B7B
P 8000 4200
F 0 "#PWR04" H 8000 4200 30  0001 C CNN
F 1 "GND" H 8000 4130 30  0001 C CNN
F 2 "" H 8000 4200 60  0000 C CNN
F 3 "" H 8000 4200 60  0000 C CNN
	1    8000 4200
	1    0    0    -1  
$EndComp
Connection ~ 8000 3950
Wire Wire Line
	7750 3450 8200 3450
Wire Wire Line
	8200 3450 8200 3250
Wire Wire Line
	8200 3250 9550 3250
Wire Wire Line
	9550 3250 9550 3550
Wire Wire Line
	9400 3550 10400 3550
Connection ~ 9550 3550
Text HLabel 10400 3550 2    60   Output ~ 0
USB_VBUS
$Comp
L Resistor R42
U 1 1 53A19EE7
P 9800 3650
F 0 "R42" V 9880 3650 40  0000 C CNN
F 1 "22" V 9807 3651 40  0000 C CNN
F 2 "Ruwai:Resistor_SMD0102_HandSoldering" V 9730 3650 30  0001 C CNN
F 3 "" H 9800 3650 30  0000 C CNN
F 4 "Yageo" H 3650 1550 60  0001 C CNN "Manu"
F 5 "RC0805FR-0722RL" H 3650 1650 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 1750 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    9800 3650
	0    1    1    0   
$EndComp
$Comp
L Resistor R43
U 1 1 53A19FA5
P 9800 3800
F 0 "R43" V 9880 3800 40  0000 C CNN
F 1 "22" V 9807 3801 40  0000 C CNN
F 2 "Ruwai:Resistor_SMD0102_HandSoldering" V 9730 3800 30  0001 C CNN
F 3 "" H 9800 3800 30  0000 C CNN
F 4 "Yageo" H 3650 1550 60  0001 C CNN "Manu"
F 5 "RC0805FR-0722RL" H 3650 1650 60  0001 C CNN "Manu#"
F 6 "0805" H 3650 1750 60  0001 C CNN "Package"
F 7 " " H 3650 4500 60  0001 C CNN "Desc"
	1    9800 3800
	0    1    1    0   
$EndComp
Wire Wire Line
	9400 3650 9550 3650
Wire Wire Line
	10050 3650 10400 3650
Wire Wire Line
	9400 3750 9450 3750
Wire Wire Line
	9450 3750 9450 3800
Wire Wire Line
	9450 3800 9550 3800
Wire Wire Line
	10050 3800 10400 3800
Text HLabel 10400 3650 2    60   BiDi ~ 0
USB_DM
Text HLabel 10400 3800 2    60   BiDi ~ 0
USB_DP
Text Notes 9400 3100 2    60   ~ 0
GPS micro-USB connector
Wire Wire Line
	1700 7150 1300 7150
Text HLabel 1300 7150 0    60   Output ~ 0
RST_GPS_ARD
Wire Wire Line
	6150 5050 5750 5050
$Comp
L PWR_FLAG #FLG05
U 1 1 53AABF9B
P 6350 2000
F 0 "#FLG05" H 6350 2095 30  0001 C CNN
F 1 "PWR_FLAG" H 6350 2180 30  0000 C CNN
F 2 "" H 6350 2000 60  0000 C CNN
F 3 "" H 6350 2000 60  0000 C CNN
	1    6350 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 2000 6350 2150
Connection ~ 6350 2150
$Comp
L RUWAI_ARDUINO_SHIELD-RESCUE-ruwai_gps_timing_shield SHIELD1
U 1 1 545E59C5
P 3300 4850
F 0 "SHIELD1" H 2200 2200 60  0000 C CNN
F 1 "RUWAI_ARDUINO_SHIELD" H 2550 8650 60  0000 C CNN
F 2 "Ruwai:RUWAI_ARDUINO_SHIELD" H 3900 2200 60  0001 C CNN
F 3 "" H 3300 4850 60  0000 C CNN
F 4 "Manufacturer" V 3480 4950 60  0001 C CNN "Manu"
F 5 "Manufacturer part number" V 3580 5050 60  0001 C CNN "Manu#"
F 6 "The package of the part." V 3680 5150 60  0001 C CNN "Package"
F 7 "Custom Description" H 3650 4500 60  0001 C CNN "Desc"
	1    3300 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 6850 5200 6850
Wire Wire Line
	5050 1850 5050 2350
Wire Wire Line
	4900 1850 5050 1850
Connection ~ 5050 2050
NoConn ~ 4900 1950
NoConn ~ 4900 1550
NoConn ~ 4900 2750
$EndSCHEMATC
