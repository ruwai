EESchema Schematic File Version 2
LIBS:adc_main_shield-rescue
LIBS:ruwai
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:adc_main_shield-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 6
Title "Ruwai ADC Main Shield"
Date "2017-09-12"
Rev "3"
Comp "Copyright Mertl Research GmbH 2014 "
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 "This documentation describes Open Hardware and is licensed under the CERN OHL v. 1.2."
$EndDescr
$Comp
L ADS1274 U17
U 1 1 5469B717
P 7100 3600
F 0 "U17" H 7100 3500 50  0000 C CNN
F 1 "ADS1274" H 7100 3700 50  0000 C CNN
F 2 "Ruwai:PQFP64_05_TP" H 8000 2600 50  0001 L CNN
F 3 "" H 8350 2600 50  0001 C CNN
F 4 "Texas Instruments" H 8000 2500 60  0001 L CNN "Manu"
F 5 "ADS1274IPAPT" H 8000 2400 60  0001 L CNN "Manu#"
F 6 "HTQFP (PAP) 64pins" H 8000 2300 60  0001 L CNN "Package"
F 7 "Custom Description" H 7100 3250 60  0001 L CNN "Desc"
	1    7100 3600
	1    0    0    -1  
$EndComp
Text HLabel 5250 3150 0    60   Input ~ 0
AOUT1_N
Text HLabel 5250 3050 0    60   Input ~ 0
AOUT1_P
Text HLabel 5250 2950 0    60   Input ~ 0
AOUT2_N
Text HLabel 5250 2850 0    60   Input ~ 0
AOUT2_P
Text HLabel 6350 2050 1    60   Input ~ 0
AOUT3_N
Text HLabel 6450 2050 1    60   Input ~ 0
AOUT3_P
Text HLabel 6550 2050 1    60   Input ~ 0
AOUT4_N
Text HLabel 6650 2050 1    60   Input ~ 0
AOUT4_P
$Comp
L GND-RESCUE-adc_main_shield #PWR025
U 1 1 5469CEEF
P 5100 3550
F 0 "#PWR025" H 5100 3550 30  0001 C CNN
F 1 "GND" H 5100 3480 30  0001 C CNN
F 2 "" H 5100 3550 60  0000 C CNN
F 3 "" H 5100 3550 60  0000 C CNN
	1    5100 3550
	1    0    0    -1  
$EndComp
Text HLabel 5250 3750 0    60   Input ~ 0
CLKDIV_3
Text HLabel 5250 3850 0    60   Input ~ 0
SYNC_3
Text HLabel 5250 3950 0    60   Input ~ 0
DIN_ADC
$Comp
L Resistor R44
U 1 1 5469D134
P 5350 4300
F 0 "R44" V 5430 4300 40  0000 C CNN
F 1 "10k" V 5357 4301 40  0000 C CNN
F 2 "Ruwai:Resistor_SMD0805_ReflowWave" V 5280 4300 30  0001 C CNN
F 3 "" V 5430 4300 30  0000 C CNN
F 4 "Yageo" H 5350 3800 60  0001 L CNN "Manu"
F 5 "RC0805FR-0710KL" H 5350 3700 60  0001 L CNN "Manu#"
F 6 "0805" H 5350 3600 60  0001 L CNN "Package"
F 7 "Custom Description" H 5350 3500 60  0001 L CNN "Desc"
	1    5350 4300
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-adc_main_shield #PWR026
U 1 1 5469D195
P 5350 4700
F 0 "#PWR026" H 5350 4700 30  0001 C CNN
F 1 "GND" H 5350 4630 30  0001 C CNN
F 2 "" H 5350 4700 60  0000 C CNN
F 3 "" H 5350 4700 60  0000 C CNN
	1    5350 4700
	1    0    0    -1  
$EndComp
NoConn ~ 6350 4850
NoConn ~ 6450 4850
NoConn ~ 6550 4850
Text HLabel 5850 5100 0    60   Output ~ 0
DOUT1_3
Text HLabel 5850 5350 0    60   Input ~ 0
DOUT_ADC_STACK
Text Notes 5250 5650 0    60   ~ 0
channel 1-4: R43 not inserted\nchannel 5-8: R42 not inserted
$Comp
L GND-RESCUE-adc_main_shield #PWR027
U 1 1 5469E23A
P 7050 5100
F 0 "#PWR027" H 7050 5100 30  0001 C CNN
F 1 "GND" H 7050 5030 30  0001 C CNN
F 2 "" H 7050 5100 60  0000 C CNN
F 3 "" H 7050 5100 60  0000 C CNN
	1    7050 5100
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-adc_main_shield #PWR028
U 1 1 5469E5F6
P 6100 6050
F 0 "#PWR028" H 6100 6050 30  0001 C CNN
F 1 "GND" H 6100 5980 30  0001 C CNN
F 2 "" H 6100 6050 60  0000 C CNN
F 3 "" H 6100 6050 60  0000 C CNN
	1    6100 6050
	1    0    0    -1  
$EndComp
Text HLabel 6100 6350 0    60   Input ~ 0
3.3V_P
$Comp
L GND-RESCUE-adc_main_shield #PWR029
U 1 1 5469ED3C
P 8000 6050
F 0 "#PWR029" H 8000 6050 30  0001 C CNN
F 1 "GND" H 8000 5980 30  0001 C CNN
F 2 "" H 8000 6050 60  0000 C CNN
F 3 "" H 8000 6050 60  0000 C CNN
	1    8000 6050
	-1   0    0    -1  
$EndComp
Text HLabel 8000 6350 2    60   Input ~ 0
1.8V_ADC
$Comp
L GND-RESCUE-adc_main_shield #PWR030
U 1 1 5469F1F0
P 4650 3850
F 0 "#PWR030" H 4650 3850 30  0001 C CNN
F 1 "GND" H 4650 3780 30  0001 C CNN
F 2 "" H 4650 3850 60  0000 C CNN
F 3 "" H 4650 3850 60  0000 C CNN
	1    4650 3850
	1    0    0    -1  
$EndComp
Text HLabel 1200 3400 0    60   Input ~ 0
5V_ADC
Text Label 2750 3400 0    60   ~ 0
5V_ADC_L
Text Label 4500 3250 2    60   ~ 0
5V_ADC_L
$Comp
L Resistor R39
U 1 1 546A28F4
P 8350 5300
F 0 "R39" V 8430 5300 40  0000 C CNN
F 1 "51" V 8357 5301 40  0000 C CNN
F 2 "Ruwai:Resistor_SMD0805_ReflowWave" V 8280 5300 30  0001 C CNN
F 3 "" V 8430 5300 30  0000 C CNN
F 4 "Multicomp" H 8350 4800 60  0001 L CNN "Manu"
F 5 "MCPWR05FTFW0510" H 8350 4700 60  0001 L CNN "Manu#"
F 6 "0805" H 8350 4600 60  0001 L CNN "Package"
F 7 "Custom Description" H 8350 4500 60  0001 L CNN "Desc"
	1    8350 5300
	0    1    1    0   
$EndComp
$Comp
L GND-RESCUE-adc_main_shield #PWR031
U 1 1 546A2BAB
P 7750 5100
F 0 "#PWR031" H 7750 5100 30  0001 C CNN
F 1 "GND" H 7750 5030 30  0001 C CNN
F 2 "" H 7750 5100 60  0000 C CNN
F 3 "" H 7750 5100 60  0000 C CNN
	1    7750 5100
	1    0    0    -1  
$EndComp
Text HLabel 8750 5700 2    60   Input ~ 0
GPS_CLK_3
Text HLabel 8750 5500 2    60   Input ~ 0
SCLK_3
Text HLabel 8750 5300 2    60   Output ~ 0
DRDY_3
Text HLabel 8750 5000 2    60   Input ~ 0
FORMAT0_3
$Comp
L GND-RESCUE-adc_main_shield #PWR032
U 1 1 546A3847
P 8950 4050
F 0 "#PWR032" H 8950 4050 30  0001 C CNN
F 1 "GND" H 8950 3980 30  0001 C CNN
F 2 "" H 8950 4050 60  0000 C CNN
F 3 "" H 8950 4050 60  0000 C CNN
	1    8950 4050
	1    0    0    -1  
$EndComp
Text Label 9400 3250 0    60   ~ 0
5V_ADC_L
$Comp
L GND-RESCUE-adc_main_shield #PWR033
U 1 1 546A40FE
P 9650 3450
F 0 "#PWR033" H 9650 3450 30  0001 C CNN
F 1 "GND" H 9650 3380 30  0001 C CNN
F 2 "" H 9650 3450 60  0000 C CNN
F 3 "" H 9650 3450 60  0000 C CNN
	1    9650 3450
	1    0    0    -1  
$EndComp
Text HLabel 8950 4250 2    60   Input ~ 0
MODE0_3
Text HLabel 8950 4350 2    60   Input ~ 0
MODE1_3
$Comp
L GND-RESCUE-adc_main_shield #PWR034
U 1 1 546A41DC
P 9350 2750
F 0 "#PWR034" H 9350 2750 30  0001 C CNN
F 1 "GND" H 9350 2680 30  0001 C CNN
F 2 "" H 9350 2750 60  0000 C CNN
F 3 "" H 9350 2750 60  0000 C CNN
	1    9350 2750
	1    0    0    -1  
$EndComp
Text Label 7450 1200 1    60   ~ 0
5V_ADC_L
$Comp
L GND-RESCUE-adc_main_shield #PWR035
U 1 1 546A4875
P 8050 2150
F 0 "#PWR035" H 8050 2150 30  0001 C CNN
F 1 "GND" H 8050 2080 30  0001 C CNN
F 2 "" H 8050 2150 60  0000 C CNN
F 3 "" H 8050 2150 60  0000 C CNN
	1    8050 2150
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-adc_main_shield #PWR036
U 1 1 546A4B29
P 6850 2050
F 0 "#PWR036" H 6850 2050 30  0001 C CNN
F 1 "GND" H 6850 1980 30  0001 C CNN
F 2 "" H 6850 2050 60  0000 C CNN
F 3 "" H 6850 2050 60  0000 C CNN
	1    6850 2050
	1    0    0    -1  
$EndComp
Text Label 6750 1200 1    60   ~ 0
5V_ADC_L
$Comp
L GND-RESCUE-adc_main_shield #PWR037
U 1 1 546A4F73
P 6100 1450
F 0 "#PWR037" H 6100 1450 30  0001 C CNN
F 1 "GND" H 6100 1380 30  0001 C CNN
F 2 "" H 6100 1450 60  0000 C CNN
F 3 "" H 6100 1450 60  0000 C CNN
	1    6100 1450
	1    0    0    -1  
$EndComp
$Comp
L Capacitor C70
U 1 1 546A52B5
P 8450 1800
F 0 "C70" H 8450 1900 40  0000 L CNN
F 1 "0.1uF" H 8456 1715 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD1206_ReflowWave" H 8488 1650 30  0001 C CNN
F 3 "" H 8450 1900 60  0000 C CNN
F 4 "Samsung Electro-Mechanics" H 8500 1400 60  0001 L CNN "Manu"
F 5 "CL31B104KBCNNNL" H 8500 1300 60  0001 L CNN "Manu#"
F 6 "1206" H 8500 1200 60  0001 L CNN "Package"
F 7 "Custom Description" H 8450 1450 60  0001 L CNN "Desc"
	1    8450 1800
	1    0    0    -1  
$EndComp
$Comp
L OPA333DBV U20
U 1 1 5469D17F
P 9200 1350
F 0 "U20" H 9400 1150 50  0000 C CNN
F 1 "OPA333DBV" H 9550 1550 50  0000 C CNN
F 2 "Ruwai:SOT23-5" H 9200 800 50  0001 L CNN
F 3 "" H 9300 1350 50  0001 C CNN
F 4 "Texas Instruments" H 9200 700 60  0001 L CNN "Manu"
F 5 "OPA333AIDBVT" H 9200 600 60  0001 L CNN "Manu#"
F 6 "SOT23 5-pin" H 9200 500 60  0001 L CNN "Package"
F 7 "1.8V, 17µA, microPower, Precision, Zero Drift CMOS Op Amp " H 9200 400 60  0001 L CNN "Desc"
	1    9200 1350
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-adc_main_shield #PWR038
U 1 1 5469D5F1
P 8450 2150
F 0 "#PWR038" H 8450 2150 30  0001 C CNN
F 1 "GND" H 8450 2080 30  0001 C CNN
F 2 "" H 8450 2150 60  0000 C CNN
F 3 "" H 8450 2150 60  0000 C CNN
	1    8450 2150
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-adc_main_shield #PWR039
U 1 1 5469D6FA
P 9300 2150
F 0 "#PWR039" H 9300 2150 30  0001 C CNN
F 1 "GND" H 9300 2080 30  0001 C CNN
F 2 "" H 9300 2150 60  0000 C CNN
F 3 "" H 9300 2150 60  0000 C CNN
	1    9300 2150
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-adc_main_shield #PWR040
U 1 1 5469D7D0
P 10000 2150
F 0 "#PWR040" H 10000 2150 30  0001 C CNN
F 1 "GND" H 10000 2080 30  0001 C CNN
F 2 "" H 10000 2150 60  0000 C CNN
F 3 "" H 10000 2150 60  0000 C CNN
	1    10000 2150
	1    0    0    -1  
$EndComp
$Comp
L Capacitor C54
U 1 1 5469DB07
P 10300 1650
F 0 "C54" H 10300 1750 40  0000 L CNN
F 1 "1uF" H 10306 1565 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD0805_ReflowWave" H 10338 1500 30  0001 C CNN
F 3 "" H 10300 1750 60  0000 C CNN
F 4 "Murata" H 10350 1250 60  0001 L CNN "Manu"
F 5 "GRM21BR71H105KA12L" H 10350 1150 60  0001 L CNN "Manu#"
F 6 "0805" H 10350 1050 60  0001 L CNN "Package"
F 7 "Custom Description" H 10300 1300 60  0001 L CNN "Desc"
	1    10300 1650
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-adc_main_shield #PWR041
U 1 1 5469DDBC
P 10300 2150
F 0 "#PWR041" H 10300 2150 30  0001 C CNN
F 1 "GND" H 10300 2080 30  0001 C CNN
F 2 "" H 10300 2150 60  0000 C CNN
F 3 "" H 10300 2150 60  0000 C CNN
	1    10300 2150
	1    0    0    -1  
$EndComp
$Comp
L inductor L37
U 1 1 5469DFD6
P 2400 3800
F 0 "L37" H 2400 3850 40  0000 C BNN
F 1 "33uH" H 2400 3750 40  0000 C CNN
F 2 "Ruwai:Inductor_SMD1210_reflow" H 2300 3800 60  0001 C CNN
F 3 "" H 2300 3800 60  0000 C CNN
F 4 "Murata" H 2400 3800 60  0001 C CNN "Manu"
F 5 "LQH32CN330K53L" H 2400 3800 60  0001 C CNN "Manu#"
F 6 "1210" H 2400 3800 60  0001 C CNN "Package"
F 7 "Custom description." H 2400 3800 60  0001 C CNN "Desc"
	1    2400 3800
	1    0    0    -1  
$EndComp
$Comp
L Capacitor C52
U 1 1 5469E021
P 2050 4100
F 0 "C52" H 2050 4200 40  0000 L CNN
F 1 "1uF" H 2056 4015 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD0805_ReflowWave" H 2088 3950 30  0001 C CNN
F 3 "" H 2050 4200 60  0000 C CNN
F 4 "Murata" H 2100 3700 60  0001 L CNN "Manu"
F 5 "GRM21BR71H105KA12L" H 2100 3600 60  0001 L CNN "Manu#"
F 6 "0805" H 2100 3500 60  0001 L CNN "Package"
F 7 "Custom Description" H 2050 3750 60  0001 L CNN "Desc"
	1    2050 4100
	1    0    0    -1  
$EndComp
$Comp
L Resistor R35
U 1 1 5469E0C5
P 1650 3800
F 0 "R35" V 1730 3800 40  0000 C CNN
F 1 "10" V 1657 3801 40  0000 C CNN
F 2 "Ruwai:Resistor_SMD0805_ReflowWave" V 1580 3800 30  0001 C CNN
F 3 "" V 1730 3800 30  0000 C CNN
F 4 "Yageo" H 1650 3300 60  0001 L CNN "Manu"
F 5 "RC0805FR-0710RL" H 1650 3200 60  0001 L CNN "Manu#"
F 6 "0805" H 1650 3100 60  0001 L CNN "Package"
F 7 "Custom Description" H 1650 3000 60  0001 L CNN "Desc"
	1    1650 3800
	0    1    1    0   
$EndComp
$Comp
L GND-RESCUE-adc_main_shield #PWR042
U 1 1 5469E403
P 2050 4450
F 0 "#PWR042" H 2050 4450 30  0001 C CNN
F 1 "GND" H 2050 4380 30  0001 C CNN
F 2 "" H 2050 4450 60  0000 C CNN
F 3 "" H 2050 4450 60  0000 C CNN
	1    2050 4450
	1    0    0    -1  
$EndComp
Text HLabel 1200 3800 0    60   Input ~ 0
3.3V_P
Text Label 2750 3800 0    60   ~ 0
3.3V_REF
Text Notes 8400 1200 1    60   ~ 0
DNP
Text Label 10600 800  0    60   ~ 0
3.3V_REF
Text HLabel 10600 1350 2    60   Output ~ 0
ADC_VCOM
$Comp
L REF5025 U18
U 1 1 546A0A73
P 3900 1650
F 0 "U18" H 3600 1350 50  0000 C CNN
F 1 "REF5025" H 3700 1950 50  0000 C CNN
F 2 "Ruwai:SOIC-8_N" H 4200 1300 50  0001 C CNN
F 3 "" H 3850 1650 50  0001 C CNN
F 4 "Texas Instruments" H 3900 1150 60  0001 L CNN "Manu"
F 5 "REF5025AID" H 3900 1050 60  0001 L CNN "Manu#"
F 6 "SOIC 8-pin 150mil" H 3900 950 60  0001 L CNN "Package"
F 7 "Low Noise, Very Low Drift, Precision Voltage Reference" H 3900 850 60  0001 L CNN "Desc"
	1    3900 1650
	1    0    0    -1  
$EndComp
$Comp
L Capacitor C51
U 1 1 546A0E95
P 3000 2100
F 0 "C51" H 3000 2200 40  0000 L CNN
F 1 "1uF" H 3006 2015 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD0805_ReflowWave" H 3038 1950 30  0001 C CNN
F 3 "" H 3000 2200 60  0000 C CNN
F 4 "Murata" H 3050 1700 60  0001 L CNN "Manu"
F 5 "GRM21BR71H105KA12L" H 3050 1600 60  0001 L CNN "Manu#"
F 6 "0805" H 3050 1500 60  0001 L CNN "Package"
F 7 "Custom Description" H 3000 1750 60  0001 L CNN "Desc"
	1    3000 2100
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-adc_main_shield #PWR043
U 1 1 546A0F32
P 3000 2450
F 0 "#PWR043" H 3000 2450 30  0001 C CNN
F 1 "GND" H 3000 2380 30  0001 C CNN
F 2 "" H 3000 2450 60  0000 C CNN
F 3 "" H 3000 2450 60  0000 C CNN
	1    3000 2450
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-adc_main_shield #PWR044
U 1 1 546A0F7B
P 3200 2450
F 0 "#PWR044" H 3200 2450 30  0001 C CNN
F 1 "GND" H 3200 2380 30  0001 C CNN
F 2 "" H 3200 2450 60  0000 C CNN
F 3 "" H 3200 2450 60  0000 C CNN
	1    3200 2450
	1    0    0    -1  
$EndComp
$Comp
L Capacitor C50
U 1 1 546A1997
P 4950 2100
F 0 "C50" H 4950 2200 40  0000 L CNN
F 1 "10uF" H 4956 2015 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD1206_ReflowWave" H 4988 1950 30  0001 C CNN
F 3 "" H 4950 2200 60  0000 C CNN
F 4 "Murata" H 5000 1700 60  0001 L CNN "Manu"
F 5 "GRM31CR71E106KA12L" H 5000 1600 60  0001 L CNN "Manu#"
F 6 "1206" H 5000 1500 60  0001 L CNN "Package"
F 7 "Custom Description" H 4950 1750 60  0001 L CNN "Desc"
	1    4950 2100
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-adc_main_shield #PWR045
U 1 1 546A1D47
P 4950 2450
F 0 "#PWR045" H 4950 2450 30  0001 C CNN
F 1 "GND" H 4950 2380 30  0001 C CNN
F 2 "" H 4950 2450 60  0000 C CNN
F 3 "" H 4950 2450 60  0000 C CNN
	1    4950 2450
	1    0    0    -1  
$EndComp
$Comp
L Capacitor C68
U 1 1 546A5A85
P 6250 900
F 0 "C68" H 6250 1000 40  0000 L CNN
F 1 "4.7uF" H 6256 815 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD1206_ReflowWave" H 6288 750 30  0001 C CNN
F 3 "" H 6250 1000 60  0000 C CNN
F 4 "Murata" H 6300 500 60  0001 L CNN "Manu"
F 5 "GRM31CR71H475KA12L" H 6300 400 60  0001 L CNN "Manu#"
F 6 "1206" H 6300 300 60  0001 L CNN "Package"
F 7 "Custom Description" H 6250 550 60  0001 L CNN "Desc"
	1    6250 900 
	-1   0    0    1   
$EndComp
$Comp
L GND-RESCUE-adc_main_shield #PWR046
U 1 1 546A5B66
P 6250 1150
F 0 "#PWR046" H 6250 1150 30  0001 C CNN
F 1 "GND" H 6250 1080 30  0001 C CNN
F 2 "" H 6250 1150 60  0000 C CNN
F 3 "" H 6250 1150 60  0000 C CNN
	1    6250 1150
	1    0    0    -1  
$EndComp
$Comp
L Resistor R37
U 1 1 546A62B2
P 1650 6400
F 0 "R37" V 1730 6400 40  0000 C CNN
F 1 "0" V 1657 6401 40  0000 C CNN
F 2 "Ruwai:Resistor_SMD0805_ReflowWave" V 1580 6400 30  0001 C CNN
F 3 "" V 1730 6400 30  0000 C CNN
F 4 "Yageo" H 1650 5900 60  0001 L CNN "Manu"
F 5 "RC0805JR-070RL" H 1650 5800 60  0001 L CNN "Manu#"
F 6 "0805" H 1650 5700 60  0001 L CNN "Package"
F 7 "Custom Description" H 1650 5600 60  0001 L CNN "Desc"
	1    1650 6400
	-1   0    0    1   
$EndComp
$Comp
L Capacitor C55
U 1 1 546A62EE
P 1200 6350
F 0 "C55" H 1200 6450 40  0000 L CNN
F 1 "0.1uF" H 1206 6265 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD0805_ReflowWave" H 1238 6200 30  0001 C CNN
F 3 "" H 1200 6450 60  0000 C CNN
F 4 "Murata" H 1250 5950 60  0001 L CNN "Manu"
F 5 "GCM21BR71H104KA37L" H 1250 5850 60  0001 L CNN "Manu#"
F 6 "0805" H 1250 5750 60  0001 L CNN "Package"
F 7 "Custom Description" H 1200 6000 60  0001 L CNN "Desc"
	1    1200 6350
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-adc_main_shield #PWR047
U 1 1 546A6427
P 2500 6850
F 0 "#PWR047" H 2500 6850 30  0001 C CNN
F 1 "GND" H 2500 6780 30  0001 C CNN
F 2 "" H 2500 6850 60  0000 C CNN
F 3 "" H 2500 6850 60  0000 C CNN
	1    2500 6850
	1    0    0    -1  
$EndComp
$Comp
L PCF8574A_DGV_PW U21
U 1 1 546A64CB
P 3250 6100
F 0 "U21" H 2900 5500 60  0000 C CNN
F 1 "PCF8574A_DGV_PW" H 3250 6700 60  0000 C CNN
F 2 "Ruwai:TSSOP-20_4.4x6.5mm_Pitch0.65mm" H 4450 5500 60  0001 C CNN
F 3 "" H 3250 6100 60  0000 C CNN
F 4 "Texas Instruments" H 3250 5350 60  0001 L CNN "Manu"
F 5 "PCF8574APWR" H 3250 5250 60  0001 L CNN "Manu#"
F 6 "TSSOP 20-pin" H 3250 5150 60  0001 L CNN "Package"
F 7 "Remote 8-Bit I/O Expander for I2C-Bus" H 3250 5050 60  0001 L CNN "Desc"
	1    3250 6100
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-adc_main_shield #PWR048
U 1 1 546A675A
P 3950 6850
F 0 "#PWR048" H 3950 6850 30  0001 C CNN
F 1 "GND" H 3950 6780 30  0001 C CNN
F 2 "" H 3950 6850 60  0000 C CNN
F 3 "" H 3950 6850 60  0000 C CNN
	1    3950 6850
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-adc_main_shield #PWR049
U 1 1 546A7817
P 1650 7500
F 0 "#PWR049" H 1650 7500 30  0001 C CNN
F 1 "GND" H 1650 7430 30  0001 C CNN
F 2 "" H 1650 7500 60  0000 C CNN
F 3 "" H 1650 7500 60  0000 C CNN
	1    1650 7500
	1    0    0    -1  
$EndComp
Text HLabel 1050 6050 0    60   Input ~ 0
3.3V_P
Text Notes 1850 7250 0    60   ~ 0
channel 1-4: R37 not inserted\nchannel 5-8: R36 not inserted
NoConn ~ 2650 5650
NoConn ~ 3850 6050
NoConn ~ 3850 5950
NoConn ~ 3850 5750
NoConn ~ 3850 5650
Text HLabel 2400 5750 0    60   Input ~ 0
SCL_3
Text HLabel 2400 5950 0    60   BiDi ~ 0
SDA_3
$Comp
L GND-RESCUE-adc_main_shield #PWR050
U 1 1 546AC1B0
P 1200 6650
F 0 "#PWR050" H 1200 6650 30  0001 C CNN
F 1 "GND" H 1200 6580 30  0001 C CNN
F 2 "" H 1200 6650 60  0000 C CNN
F 3 "" H 1200 6650 60  0000 C CNN
	1    1200 6650
	1    0    0    -1  
$EndComp
Text Label 8950 3450 0    60   ~ 0
PWDN1_3
Text Label 8950 3550 0    60   ~ 0
PWDN2_3
Text Label 8950 3650 0    60   ~ 0
PWDN3_3
Text Label 8950 3750 0    60   ~ 0
PWDN4_3
Text Label 2400 6550 2    60   ~ 0
PWDN1_3
Text Label 4100 6550 0    60   ~ 0
PWDN2_3
Text Label 4100 6450 0    60   ~ 0
PWDN3_3
Text Label 4100 6250 0    60   ~ 0
PWDN4_3
$Comp
L ferrite_bead L38
U 1 1 546B2A20
P 1650 3400
F 0 "L38" H 1650 3490 40  0000 C CNN
F 1 "220" H 1650 3310 40  0000 C CNN
F 2 "Ruwai:Inductor_SMD0805_ReflowWave" H 1650 3330 30  0001 C CNN
F 3 "" H 1650 3480 30  0000 C CNN
F 4 "Murata" H 1650 3150 60  0001 L CNN "Manu"
F 5 "BLM21PG221SN1D" H 1650 3050 60  0001 L CNN "Manu#"
F 6 "0805" H 1650 2950 60  0001 L CNN "Package"
F 7 "Custom Description" H 1650 2850 60  0001 L CNN "Desc"
	1    1650 3400
	1    0    0    -1  
$EndComp
$Comp
L ferrite_bead L39
U 1 1 546B31FD
P 6500 6350
F 0 "L39" H 6500 6440 40  0000 C CNN
F 1 "220" H 6500 6260 40  0000 C CNN
F 2 "Ruwai:Inductor_SMD0805_ReflowWave" H 6500 6280 30  0001 C CNN
F 3 "" H 6500 6430 30  0000 C CNN
F 4 "Murata" H 6500 6100 60  0001 L CNN "Manu"
F 5 "BLM21PG221SN1D" H 6500 6000 60  0001 L CNN "Manu#"
F 6 "0805" H 6500 5900 60  0001 L CNN "Package"
F 7 "Custom Description" H 6500 5800 60  0001 L CNN "Desc"
	1    6500 6350
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG051
U 1 1 546E5E08
P 2650 3300
F 0 "#FLG051" H 2650 3395 30  0001 C CNN
F 1 "PWR_FLAG" H 2650 3480 30  0000 C CNN
F 2 "" H 2650 3300 60  0000 C CNN
F 3 "" H 2650 3300 60  0000 C CNN
	1    2650 3300
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG052
U 1 1 546E5EAA
P 2650 3700
F 0 "#FLG052" H 2650 3795 30  0001 C CNN
F 1 "PWR_FLAG" H 2650 3880 30  0000 C CNN
F 2 "" H 2650 3700 60  0000 C CNN
F 3 "" H 2650 3700 60  0000 C CNN
	1    2650 3700
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG053
U 1 1 546E6A6C
P 7200 5500
F 0 "#FLG053" H 7200 5595 30  0001 C CNN
F 1 "PWR_FLAG" H 7200 5680 30  0000 C CNN
F 2 "" H 7200 5500 60  0000 C CNN
F 3 "" H 7200 5500 60  0000 C CNN
	1    7200 5500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5550 2850 5250 2850
Wire Wire Line
	5550 2950 5250 2950
Wire Wire Line
	5550 3050 5250 3050
Wire Wire Line
	5550 3150 5250 3150
Wire Wire Line
	6350 2350 6350 2050
Wire Wire Line
	6450 2350 6450 2050
Wire Wire Line
	6550 2350 6550 2050
Wire Wire Line
	6650 2350 6650 2050
Wire Wire Line
	5100 3550 5100 3450
Wire Wire Line
	5100 3450 5550 3450
Wire Wire Line
	5450 3350 5450 3650
Wire Wire Line
	5450 3350 5550 3350
Connection ~ 5450 3450
Wire Wire Line
	5450 3550 5550 3550
Wire Wire Line
	5450 3650 5550 3650
Connection ~ 5450 3550
Wire Wire Line
	5550 3750 5250 3750
Wire Wire Line
	5550 3850 5250 3850
Wire Wire Line
	5250 3950 5550 3950
Wire Wire Line
	5350 4050 5350 3950
Connection ~ 5350 3950
Wire Wire Line
	5350 4550 5350 4700
Wire Wire Line
	6650 4850 6650 5350
Wire Wire Line
	6650 5100 6550 5100
Wire Wire Line
	6650 5350 6550 5350
Connection ~ 6650 5100
Wire Wire Line
	6050 5100 5850 5100
Wire Wire Line
	6050 5350 5850 5350
Wire Notes Line
	6000 5450 5200 5450
Wire Notes Line
	5200 5450 5200 5700
Wire Notes Line
	5200 5700 6700 5700
Wire Notes Line
	6700 5700 6700 5450
Wire Notes Line
	6700 5450 6600 5450
Wire Notes Line
	6600 5450 6600 5000
Wire Notes Line
	6600 5000 6000 5000
Wire Notes Line
	6000 5000 6000 5450
Wire Wire Line
	6750 4850 6750 5000
Wire Wire Line
	6750 5000 7150 5000
Wire Wire Line
	7050 4850 7050 5100
Connection ~ 7050 5000
Wire Wire Line
	7150 5000 7150 4850
Wire Wire Line
	6850 4850 6850 6350
Wire Wire Line
	6850 6350 6700 6350
Wire Wire Line
	6700 6000 6850 6000
Connection ~ 6850 6000
Wire Wire Line
	6300 6000 6100 6000
Wire Wire Line
	6100 6000 6100 6050
Wire Wire Line
	6300 6350 6100 6350
Wire Wire Line
	6950 4850 6950 5250
Wire Wire Line
	6950 5250 6850 5250
Connection ~ 6850 5250
Wire Wire Line
	7250 4850 7250 6350
Wire Wire Line
	7250 6350 7400 6350
Wire Wire Line
	7400 6000 7250 6000
Connection ~ 7250 6000
Wire Wire Line
	7800 6000 8000 6000
Wire Wire Line
	8000 6000 8000 6050
Wire Wire Line
	7800 6350 8000 6350
Wire Wire Line
	1450 3400 1200 3400
Wire Wire Line
	1850 3400 2750 3400
Wire Wire Line
	4500 3250 5550 3250
Wire Wire Line
	4650 3250 4650 3350
Connection ~ 4650 3250
Wire Wire Line
	4650 3750 4650 3850
Wire Wire Line
	8100 5700 7350 5700
Wire Wire Line
	7350 5700 7350 4850
Wire Wire Line
	8100 5500 7450 5500
Wire Wire Line
	7450 5500 7450 4850
Wire Wire Line
	8100 5300 7550 5300
Wire Wire Line
	7550 5300 7550 4850
Wire Wire Line
	7750 4850 7750 5100
Wire Wire Line
	7650 4850 7650 5000
Wire Wire Line
	7650 5000 7750 5000
Connection ~ 7750 5000
Wire Wire Line
	7850 4850 7850 5000
Wire Wire Line
	7850 5000 8750 5000
Wire Wire Line
	8600 5300 8750 5300
Wire Wire Line
	8600 5500 8750 5500
Wire Wire Line
	8600 5700 8750 5700
Wire Wire Line
	8650 4350 8950 4350
Wire Wire Line
	8650 4250 8950 4250
Wire Wire Line
	8650 3850 8800 3850
Wire Wire Line
	8800 3850 8800 4150
Wire Wire Line
	8800 4150 8650 4150
Wire Wire Line
	8650 4050 8800 4050
Connection ~ 8800 4050
Wire Wire Line
	8650 3950 8950 3950
Connection ~ 8800 3950
Wire Wire Line
	8950 3950 8950 4050
Wire Wire Line
	8650 3750 8950 3750
Wire Wire Line
	8650 3650 8950 3650
Wire Wire Line
	8650 3550 8950 3550
Wire Wire Line
	8650 3450 8950 3450
Wire Wire Line
	8650 3250 9400 3250
Wire Wire Line
	9100 3150 9100 3250
Connection ~ 9100 3250
Wire Wire Line
	8650 3350 9650 3350
Wire Wire Line
	9650 3350 9650 3450
Wire Wire Line
	9100 2750 9100 2700
Wire Wire Line
	9100 2700 9350 2700
Wire Wire Line
	9350 2700 9350 2750
Wire Wire Line
	7450 1200 7450 2350
Wire Wire Line
	7550 2050 7450 2050
Connection ~ 7450 2050
Wire Wire Line
	7950 2050 8050 2050
Wire Wire Line
	8050 2050 8050 2150
Wire Wire Line
	6850 2350 6850 2200
Wire Wire Line
	6850 2200 7350 2200
Wire Wire Line
	7350 2200 7350 2350
Wire Wire Line
	6950 2000 6950 2350
Connection ~ 6950 2200
Wire Wire Line
	7050 2350 7050 2200
Connection ~ 7050 2200
Wire Wire Line
	6950 2000 6850 2000
Wire Wire Line
	6850 2000 6850 2050
Wire Wire Line
	6750 1200 6750 2350
Wire Wire Line
	6650 1350 6750 1350
Connection ~ 6750 1350
Wire Wire Line
	6250 1350 6100 1350
Wire Wire Line
	6100 1350 6100 1450
Wire Wire Line
	8700 1200 8600 1200
Wire Wire Line
	8600 1200 8600 650 
Wire Wire Line
	8450 650  10000 650 
Wire Wire Line
	10000 650  10000 1450
Wire Wire Line
	9900 1350 10600 1350
Wire Wire Line
	8450 650  8450 850 
Connection ~ 8600 650 
Wire Wire Line
	8450 1350 8450 1600
Wire Wire Line
	7250 1500 8700 1500
Connection ~ 8450 1500
Wire Wire Line
	7250 1500 7250 2350
Wire Wire Line
	8450 2000 8450 2150
Wire Wire Line
	9300 1700 9300 2150
Wire Wire Line
	10000 1850 10000 2150
Connection ~ 10000 1350
Wire Wire Line
	9300 1000 9300 800 
Wire Wire Line
	9300 800  10600 800 
Wire Wire Line
	10300 1450 10300 800 
Connection ~ 10300 800 
Wire Wire Line
	10300 1850 10300 2150
Wire Wire Line
	1900 3800 2200 3800
Wire Wire Line
	2050 3900 2050 3800
Connection ~ 2050 3800
Wire Wire Line
	2050 4300 2050 4450
Wire Wire Line
	2600 3800 2750 3800
Wire Wire Line
	1400 3800 1200 3800
Wire Wire Line
	3250 1800 3200 1800
Wire Wire Line
	3200 1800 3200 2450
Wire Wire Line
	2400 1600 3250 1600
Wire Wire Line
	3000 2300 3000 2450
Wire Wire Line
	4550 1800 4650 1800
Wire Wire Line
	4650 1800 4650 1900
Wire Wire Line
	4550 1700 5550 1700
Wire Wire Line
	4950 1900 4950 1700
Connection ~ 4950 1700
Wire Wire Line
	5250 1900 5250 1700
Connection ~ 5250 1700
Wire Wire Line
	4650 2300 4650 2400
Wire Wire Line
	4650 2400 5250 2400
Wire Wire Line
	4950 2300 4950 2450
Connection ~ 4950 2400
Wire Wire Line
	5250 2400 5250 2300
Wire Wire Line
	7150 650  7150 2350
Wire Wire Line
	5550 650  7150 650 
Wire Wire Line
	6250 700  6250 650 
Connection ~ 6250 650 
Wire Wire Line
	6250 1100 6250 1150
Wire Wire Line
	5550 1700 5550 650 
Wire Wire Line
	2650 6250 2500 6250
Wire Wire Line
	2500 6250 2500 6850
Wire Wire Line
	2650 6450 2500 6450
Connection ~ 2500 6450
Wire Wire Line
	3850 6150 3950 6150
Wire Wire Line
	3950 6150 3950 6850
Wire Wire Line
	2650 5750 2400 5750
Wire Wire Line
	2650 5950 2400 5950
Wire Wire Line
	2650 6550 2400 6550
Wire Wire Line
	3850 6550 4100 6550
Wire Wire Line
	3850 6450 4100 6450
Wire Wire Line
	3850 6250 4100 6250
Wire Wire Line
	1050 6050 2650 6050
Wire Wire Line
	1650 6150 1650 6050
Connection ~ 1650 6050
Wire Wire Line
	1650 6650 1650 6850
Wire Wire Line
	1650 6750 1850 6750
Wire Wire Line
	1850 6750 1850 6150
Wire Wire Line
	1850 6150 2650 6150
Connection ~ 1650 6750
Wire Wire Line
	1650 7350 1650 7500
Wire Wire Line
	1200 6150 1200 6050
Connection ~ 1200 6050
Wire Wire Line
	1200 6550 1200 6650
Wire Notes Line
	1500 6100 1500 7400
Wire Notes Line
	1500 7400 3350 7400
Wire Notes Line
	3350 7400 3350 7000
Wire Notes Line
	3350 7000 1750 7000
Wire Notes Line
	1750 7000 1750 6100
Wire Notes Line
	1750 6100 1500 6100
Wire Wire Line
	2650 3300 2650 3400
Connection ~ 2650 3400
Wire Wire Line
	2650 3700 2650 3800
Connection ~ 2650 3800
Wire Wire Line
	7200 5500 7250 5500
Connection ~ 7250 5500
$Comp
L PWR_FLAG #FLG054
U 1 1 546E6F99
P 6900 5750
F 0 "#FLG054" H 6900 5845 30  0001 C CNN
F 1 "PWR_FLAG" H 6900 5930 30  0000 C CNN
F 2 "" H 6900 5750 60  0000 C CNN
F 3 "" H 6900 5750 60  0000 C CNN
	1    6900 5750
	0    1    1    0   
$EndComp
Wire Wire Line
	6900 5750 6850 5750
Connection ~ 6850 5750
Connection ~ 3000 1600
Text Label 2400 1600 2    60   ~ 0
3.3V_REF
$Comp
L Capacitor C48
U 1 1 546DA469
P 4650 2100
F 0 "C48" H 4650 2200 40  0000 L CNN
F 1 "1uF" H 4656 2015 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD0805_ReflowWave" H 4688 1950 30  0001 C CNN
F 3 "" H 4650 2200 60  0000 C CNN
F 4 "Murata" H 4700 1700 60  0001 L CNN "Manu"
F 5 "GRM21BR71H105KA12L" H 4700 1600 60  0001 L CNN "Manu#"
F 6 "0805" H 4700 1500 60  0001 L CNN "Package"
F 7 "Custom Description" H 4650 1750 60  0001 L CNN "Desc"
	1    4650 2100
	1    0    0    -1  
$EndComp
$Comp
L Capacitor C47
U 1 1 546DA6B0
P 5250 2100
F 0 "C47" H 5250 2200 40  0000 L CNN
F 1 "10uF" H 5256 2015 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD1206_ReflowWave" H 5288 1950 30  0001 C CNN
F 3 "" H 5250 2200 60  0000 C CNN
F 4 "Murata" H 5300 1700 60  0001 L CNN "Manu"
F 5 "GRM31CR71E106KA12L" H 5300 1600 60  0001 L CNN "Manu#"
F 6 "1206" H 5300 1500 60  0001 L CNN "Package"
F 7 "Custom Description" H 5250 1750 60  0001 L CNN "Desc"
	1    5250 2100
	1    0    0    -1  
$EndComp
$Comp
L Capacitor C57
U 1 1 546DB0AB
P 6450 1350
F 0 "C57" H 6450 1450 40  0000 L CNN
F 1 "4.7uF" H 6456 1265 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD1206_ReflowWave" H 6488 1200 30  0001 C CNN
F 3 "" H 6450 1450 60  0000 C CNN
F 4 "Murata" H 6500 950 60  0001 L CNN "Manu"
F 5 "GRM31CR71H475KA12L" H 6500 850 60  0001 L CNN "Manu#"
F 6 "1206" H 6500 750 60  0001 L CNN "Package"
F 7 "Custom Description" H 6450 1000 60  0001 L CNN "Desc"
	1    6450 1350
	0    1    1    0   
$EndComp
$Comp
L Capacitor C59
U 1 1 546DB409
P 7750 2050
F 0 "C59" H 7750 2150 40  0000 L CNN
F 1 "4.7uF" H 7756 1965 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD1206_ReflowWave" H 7788 1900 30  0001 C CNN
F 3 "" H 7750 2150 60  0000 C CNN
F 4 "Murata" H 7800 1650 60  0001 L CNN "Manu"
F 5 "GRM31CR71H475KA12L" H 7800 1550 60  0001 L CNN "Manu#"
F 6 "1206" H 7800 1450 60  0001 L CNN "Package"
F 7 "Custom Description" H 7750 1700 60  0001 L CNN "Desc"
	1    7750 2050
	0    1    1    0   
$EndComp
$Comp
L Capacitor C56
U 1 1 546DB623
P 9100 2950
F 0 "C56" H 9100 3050 40  0000 L CNN
F 1 "4.7uF" H 9106 2865 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD1206_ReflowWave" H 9138 2800 30  0001 C CNN
F 3 "" H 9100 3050 60  0000 C CNN
F 4 "Murata" H 9150 2550 60  0001 L CNN "Manu"
F 5 "GRM31CR71H475KA12L" H 9150 2450 60  0001 L CNN "Manu#"
F 6 "1206" H 9150 2350 60  0001 L CNN "Package"
F 7 "Custom Description" H 9100 2600 60  0001 L CNN "Desc"
	1    9100 2950
	1    0    0    -1  
$EndComp
$Comp
L Capacitor C65
U 1 1 546DB8E5
P 7600 6000
F 0 "C65" H 7600 6100 40  0000 L CNN
F 1 "4.7uF" H 7606 5915 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD1206_ReflowWave" H 7638 5850 30  0001 C CNN
F 3 "" H 7600 6100 60  0000 C CNN
F 4 "Murata" H 7650 5600 60  0001 L CNN "Manu"
F 5 "GRM31CR71H475KA12L" H 7650 5500 60  0001 L CNN "Manu#"
F 6 "1206" H 7650 5400 60  0001 L CNN "Package"
F 7 "Custom Description" H 7600 5650 60  0001 L CNN "Desc"
	1    7600 6000
	0    -1   -1   0   
$EndComp
$Comp
L Capacitor C64
U 1 1 546DBDC0
P 6500 6000
F 0 "C64" H 6500 6100 40  0000 L CNN
F 1 "4.7uF" H 6506 5915 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD1206_ReflowWave" H 6538 5850 30  0001 C CNN
F 3 "" H 6500 6100 60  0000 C CNN
F 4 "Murata" H 6550 5600 60  0001 L CNN "Manu"
F 5 "GRM31CR71H475KA12L" H 6550 5500 60  0001 L CNN "Manu#"
F 6 "1206" H 6550 5400 60  0001 L CNN "Package"
F 7 "Custom Description" H 6500 5650 60  0001 L CNN "Desc"
	1    6500 6000
	0    -1   -1   0   
$EndComp
$Comp
L Capacitor C58
U 1 1 546DBFB6
P 4650 3550
F 0 "C58" H 4650 3650 40  0000 L CNN
F 1 "4.7uF" H 4656 3465 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD1206_ReflowWave" H 4688 3400 30  0001 C CNN
F 3 "" H 4650 3650 60  0000 C CNN
F 4 "Murata" H 4700 3150 60  0001 L CNN "Manu"
F 5 "GRM31CR71H475KA12L" H 4700 3050 60  0001 L CNN "Manu#"
F 6 "1206" H 4700 2950 60  0001 L CNN "Package"
F 7 "Custom Description" H 4650 3200 60  0001 L CNN "Desc"
	1    4650 3550
	1    0    0    -1  
$EndComp
$Comp
L Capacitor C71
U 1 1 546DC575
P 10000 1650
F 0 "C71" H 10000 1750 40  0000 L CNN
F 1 "4.7uF" H 10006 1565 40  0000 L CNN
F 2 "Ruwai:Capacitor_SMD1206_ReflowWave" H 10038 1500 30  0001 C CNN
F 3 "" H 10000 1750 60  0000 C CNN
F 4 "Murata" H 10050 1250 60  0001 L CNN "Manu"
F 5 "GRM31CR71H475KA12L" H 10050 1150 60  0001 L CNN "Manu#"
F 6 "1206" H 10050 1050 60  0001 L CNN "Package"
F 7 "Custom Description" H 10000 1300 60  0001 L CNN "Desc"
	1    10000 1650
	1    0    0    -1  
$EndComp
$Comp
L Resistor R47
U 1 1 546DC79D
P 8450 1100
F 0 "R47" V 8530 1100 40  0000 C CNN
F 1 "10M" V 8457 1101 40  0000 C CNN
F 2 "Ruwai:Resistor_SMD0805_ReflowWave" V 8380 1100 30  0001 C CNN
F 3 "" V 8530 1100 30  0000 C CNN
F 4 "Yageo" H 8450 600 60  0001 L CNN "Manu"
F 5 "RC0805FR-0710ML" H 8450 500 60  0001 L CNN "Manu#"
F 6 "0805" H 8450 400 60  0001 L CNN "Package"
F 7 "Custom Description" H 8450 300 60  0001 L CNN "Desc"
	1    8450 1100
	1    0    0    -1  
$EndComp
$Comp
L Resistor R40
U 1 1 546DD59B
P 8350 5500
F 0 "R40" V 8430 5500 40  0000 C CNN
F 1 "51" V 8357 5501 40  0000 C CNN
F 2 "Ruwai:Resistor_SMD0805_ReflowWave" V 8280 5500 30  0001 C CNN
F 3 "" V 8430 5500 30  0000 C CNN
F 4 "Multicomp" H 8350 5000 60  0001 L CNN "Manu"
F 5 "MCPWR05FTFW0510" H 8350 4900 60  0001 L CNN "Manu#"
F 6 "0805" H 8350 4800 60  0001 L CNN "Package"
F 7 "Custom Description" H 8350 4700 60  0001 L CNN "Desc"
	1    8350 5500
	0    1    1    0   
$EndComp
$Comp
L Resistor R41
U 1 1 546DD83F
P 8350 5700
F 0 "R41" V 8430 5700 40  0000 C CNN
F 1 "51" V 8357 5701 40  0000 C CNN
F 2 "Ruwai:Resistor_SMD0805_ReflowWave" V 8280 5700 30  0001 C CNN
F 3 "" V 8430 5700 30  0000 C CNN
F 4 "Multicomp" H 8350 5200 60  0001 L CNN "Manu"
F 5 "MCPWR05FTFW0510" H 8350 5100 60  0001 L CNN "Manu#"
F 6 "0805" H 8350 5000 60  0001 L CNN "Package"
F 7 "Custom Description" H 8350 4900 60  0001 L CNN "Desc"
	1    8350 5700
	0    1    1    0   
$EndComp
$Comp
L Resistor R42
U 1 1 546DDF17
P 6300 5100
F 0 "R42" V 6380 5100 40  0000 C CNN
F 1 "51" V 6307 5101 40  0000 C CNN
F 2 "Ruwai:Resistor_SMD0805_ReflowWave" V 6230 5100 30  0001 C CNN
F 3 "" V 6380 5100 30  0000 C CNN
F 4 "Multicomp" H 6300 4600 60  0001 L CNN "Manu"
F 5 "MCPWR05FTFW0510" H 6300 4500 60  0001 L CNN "Manu#"
F 6 "0805" H 6300 4400 60  0001 L CNN "Package"
F 7 "Custom Description" H 6300 4300 60  0001 L CNN "Desc"
	1    6300 5100
	0    1    1    0   
$EndComp
$Comp
L Resistor R43
U 1 1 546DE0D6
P 6300 5350
F 0 "R43" V 6380 5350 40  0000 C CNN
F 1 "51" V 6307 5351 40  0000 C CNN
F 2 "Ruwai:Resistor_SMD0805_ReflowWave" V 6230 5350 30  0001 C CNN
F 3 "" V 6380 5350 30  0000 C CNN
F 4 "Multicomp" H 6300 4850 60  0001 L CNN "Manu"
F 5 "MCPWR05FTFW0510" H 6300 4750 60  0001 L CNN "Manu#"
F 6 "0805" H 6300 4650 60  0001 L CNN "Package"
F 7 "Custom Description" H 6300 4550 60  0001 L CNN "Desc"
	1    6300 5350
	0    1    1    0   
$EndComp
$Comp
L Resistor R36
U 1 1 546DF045
P 1650 7100
F 0 "R36" V 1730 7100 40  0000 C CNN
F 1 "0" V 1657 7101 40  0000 C CNN
F 2 "Ruwai:Resistor_SMD0805_ReflowWave" V 1580 7100 30  0001 C CNN
F 3 "" V 1730 7100 30  0000 C CNN
F 4 "Yageo" H 1650 6600 60  0001 L CNN "Manu"
F 5 "RC0805JR-070RL" H 1650 6500 60  0001 L CNN "Manu#"
F 6 "0805" H 1650 6400 60  0001 L CNN "Package"
F 7 "Custom Description" H 1650 6300 60  0001 L CNN "Desc"
	1    1650 7100
	-1   0    0    1   
$EndComp
$Comp
L ferrite_bead L40
U 1 1 546E8512
P 7600 6350
F 0 "L40" H 7600 6440 40  0000 C CNN
F 1 "220" H 7600 6260 40  0000 C CNN
F 2 "Ruwai:Inductor_SMD0805_ReflowWave" H 7600 6280 30  0001 C CNN
F 3 "" H 7600 6430 30  0000 C CNN
F 4 "Murata" H 7600 6100 60  0001 L CNN "Manu"
F 5 "BLM21PG221SN1D" H 7600 6000 60  0001 L CNN "Manu#"
F 6 "0805" H 7600 5900 60  0001 L CNN "Package"
F 7 "Custom Description" H 7600 5800 60  0001 L CNN "Desc"
	1    7600 6350
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-adc_main_shield #PWR055
U 1 1 54721B69
P 6000 2350
F 0 "#PWR055" H 6000 2350 30  0001 C CNN
F 1 "GND" H 6000 2280 30  0001 C CNN
F 2 "" H 6000 2350 60  0000 C CNN
F 3 "" H 6000 2350 60  0000 C CNN
	1    6000 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 2350 6200 2200
Wire Wire Line
	6200 2200 6000 2200
Wire Wire Line
	6000 2200 6000 2350
Wire Wire Line
	3000 1600 3000 1900
NoConn ~ 3250 1700
Text Notes 8500 2550 0    60   ~ 0
The OPA333DBV added strange saw-tooth like noise\nto the ADC_VCOM signal. I dropped the IC and used a\n 0 Ohm resistor at R47. \nDNP: C70, C71, C54, U20.
Wire Notes Line
	11150 550  8300 550 
Wire Notes Line
	8300 550  8300 2600
Wire Notes Line
	8300 2600 11150 2600
Wire Notes Line
	11150 2600 11150 550 
$EndSCHEMATC
