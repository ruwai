EESchema Schematic File Version 2
LIBS:ruwai
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:adc_ai_pga-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 6
Title "Ruwai ADC analog interface with PGA"
Date "2017-09-15"
Rev "3"
Comp "copyright Mertl Research GmbH 2014"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 "This documentation describes Open Hardware and is licensed under the CERN OHL v. 1.2."
$EndDescr
Wire Wire Line
	6000 2800 7050 2800
Wire Wire Line
	6000 2700 7050 2700
Wire Wire Line
	6000 2600 7050 2600
Wire Wire Line
	6000 2500 7050 2500
Wire Wire Line
	6000 2000 7050 2000
Text HLabel 7050 2800 2    60   Output ~ 0
3.3V_P
Text HLabel 7050 2700 2    60   BiDi ~ 0
SDA_3
Text HLabel 7050 2600 2    60   Output ~ 0
SCL_3
Text HLabel 7050 2500 2    60   Output ~ 0
+15V_PGA
Text HLabel 7050 2300 2    60   Output ~ 0
-15V_PGA
Wire Wire Line
	6000 2200 7050 2200
$Comp
L GND #PWR061
U 1 1 54738A66
P 6300 3250
F 0 "#PWR061" H 6300 3250 30  0001 C CNN
F 1 "GND" H 6300 3180 30  0001 C CNN
F 2 "" H 6300 3250 60  0000 C CNN
F 3 "" H 6300 3250 60  0000 C CNN
	1    6300 3250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6000 2100 6300 2100
Wire Wire Line
	6300 2100 6300 3250
Text HLabel 7050 2200 2    60   Output ~ 0
ADC_VCOM
Text HLabel 7050 2000 2    60   Output ~ 0
5V_PGA
Wire Wire Line
	3800 2050 3550 2050
Wire Wire Line
	3800 2150 3550 2150
Wire Wire Line
	3800 2250 3550 2250
Wire Wire Line
	3800 2350 3550 2350
Wire Wire Line
	3800 2450 3550 2450
Wire Wire Line
	3800 2550 3550 2550
Wire Wire Line
	3800 2650 3550 2650
Wire Wire Line
	3800 2750 3550 2750
Text HLabel 3550 2150 0    60   Input ~ 0
CH1_P
Text HLabel 3550 2050 0    60   Input ~ 0
CH1_N
Text HLabel 3550 2350 0    60   Input ~ 0
CH2_P
Text HLabel 3550 2250 0    60   Input ~ 0
CH2_N
Text HLabel 3550 2550 0    60   Input ~ 0
CH3_P
Text HLabel 3550 2450 0    60   Input ~ 0
CH3_N
Text HLabel 3550 2750 0    60   Input ~ 0
CH4_P
Text HLabel 3550 2650 0    60   Input ~ 0
CH4_N
Wire Wire Line
	4550 3850 4550 4200
Wire Wire Line
	4650 3850 4650 4200
Wire Wire Line
	4750 3850 4750 4200
Wire Wire Line
	4850 3850 4850 4200
Wire Wire Line
	4950 3850 4950 4200
Wire Wire Line
	5050 3850 5050 4200
Wire Wire Line
	5150 3850 5150 4200
Wire Wire Line
	5250 3850 5250 4200
Text HLabel 4650 4200 3    60   Output ~ 0
AIN1_N
Text HLabel 4550 4200 3    60   Output ~ 0
AIN1_P
Text HLabel 4850 4200 3    60   Output ~ 0
AIN2_N
Text HLabel 4750 4200 3    60   Output ~ 0
AIN2_P
Text HLabel 5150 4200 3    60   Output ~ 0
AIN4_P
Text HLabel 5250 4200 3    60   Output ~ 0
AIN4_N
Text HLabel 4950 4200 3    60   Output ~ 0
AIN3_P
Text HLabel 5050 4200 3    60   Output ~ 0
AIN3_N
$Comp
L GND #PWR062
U 1 1 54739D1A
P 5600 4200
F 0 "#PWR062" H 5600 4200 30  0001 C CNN
F 1 "GND" H 5600 4130 30  0001 C CNN
F 2 "" H 5600 4200 60  0000 C CNN
F 3 "" H 5600 4200 60  0000 C CNN
	1    5600 4200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4450 3850 4450 4000
Wire Wire Line
	4450 4000 5600 4000
Wire Wire Line
	5350 3850 5350 4000
Connection ~ 5350 4000
$Comp
L PWR_FLAG #FLG063
U 1 1 5475BB83
P 6250 2000
F 0 "#FLG063" H 6250 2095 30  0001 C CNN
F 1 "PWR_FLAG" H 6250 2180 30  0000 C CNN
F 2 "" H 6250 2000 60  0000 C CNN
F 3 "" H 6250 2000 60  0000 C CNN
	1    6250 2000
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG064
U 1 1 5475BB95
P 6450 2200
F 0 "#FLG064" H 6450 2295 30  0001 C CNN
F 1 "PWR_FLAG" H 6450 2380 30  0000 C CNN
F 2 "" H 6450 2200 60  0000 C CNN
F 3 "" H 6450 2200 60  0000 C CNN
	1    6450 2200
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG065
U 1 1 5475BBA5
P 6550 2400
F 0 "#FLG065" H 6550 2495 30  0001 C CNN
F 1 "PWR_FLAG" H 6550 2580 30  0000 C CNN
F 2 "" H 6550 2400 60  0000 C CNN
F 3 "" H 6550 2400 60  0000 C CNN
	1    6550 2400
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG066
U 1 1 5475BBB8
P 6900 2500
F 0 "#FLG066" H 6900 2595 30  0001 C CNN
F 1 "PWR_FLAG" H 6900 2680 30  0000 C CNN
F 2 "" H 6900 2500 60  0000 C CNN
F 3 "" H 6900 2500 60  0000 C CNN
	1    6900 2500
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG067
U 1 1 5475BBCB
P 6900 2800
F 0 "#FLG067" H 6900 2895 30  0001 C CNN
F 1 "PWR_FLAG" H 6900 2980 30  0000 C CNN
F 2 "" H 6900 2800 60  0000 C CNN
F 3 "" H 6900 2800 60  0000 C CNN
	1    6900 2800
	-1   0    0    1   
$EndComp
Wire Wire Line
	6000 2400 6650 2400
Wire Wire Line
	6650 2400 6650 2300
Wire Wire Line
	6650 2300 7050 2300
Connection ~ 6250 2000
Connection ~ 6450 2200
Connection ~ 6550 2400
Connection ~ 6900 2500
Connection ~ 6900 2800
$Comp
L GND #PWR068
U 1 1 5476B4E2
P 6150 3250
F 0 "#PWR068" H 6150 3250 30  0001 C CNN
F 1 "GND" H 6150 3180 30  0001 C CNN
F 2 "" H 6150 3250 60  0000 C CNN
F 3 "" H 6150 3250 60  0000 C CNN
	1    6150 3250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6000 2300 6150 2300
Wire Wire Line
	6150 2300 6150 3250
Wire Wire Line
	5600 4000 5600 4200
$Comp
L GND #PWR069
U 1 1 547F3037
P 3550 3250
F 0 "#PWR069" H 3550 3250 30  0001 C CNN
F 1 "GND" H 3550 3180 30  0001 C CNN
F 2 "" H 3550 3250 60  0000 C CNN
F 3 "" H 3550 3250 60  0000 C CNN
	1    3550 3250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3800 3050 3550 3050
Wire Wire Line
	3550 2950 3550 3250
Wire Wire Line
	3800 2950 3550 2950
Connection ~ 3550 3050
$Comp
L ADC_analog_interface X1
U 1 1 571DFD91
P 4800 2650
F 0 "X1" H 5600 1650 60  0000 C CNN
F 1 "ADC_analog_interface" H 4600 3500 60  0000 C CNN
F 2 "Ruwai:RUWAI_ADC_ANALOG_INTERFACE" H 4800 1100 60  0001 L CNN
F 3 "" H 4900 3100 60  0000 C CNN
F 4 "Manufacturer" H 4800 950 60  0001 L CNN "Manu"
F 5 "Manufacturer part number" H 4800 850 60  0001 L CNN "Manu#"
F 6 "The package of the part." H 4800 750 60  0001 L CNN "Package"
F 7 "Custom Description" H 4800 650 60  0001 L CNN "Desc"
	1    4800 2650
	1    0    0    -1  
$EndComp
$EndSCHEMATC
