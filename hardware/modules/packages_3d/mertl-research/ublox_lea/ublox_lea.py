# Import the Blender-Python API module
import bpy
import math

# Define the u-Blox dimensions.
A = 22.4;
B = 17.0;
C = 2.4;
D = 2.55;
E = 1.1;
F = 3.8;
G = 2.85;
H = 0.82;
K = 0.8;
M = 1.0;
N = 0.5;


def make_material(name, diffuse, specular, alpha):
    mat = bpy.data.materials.new(name)
    mat.diffuse_color = diffuse
    mat.diffuse_shader = 'LAMBERT' 
    mat.diffuse_intensity = 1.0 
    mat.specular_color = specular
    mat.specular_shader = 'COOKTORR'
    mat.specular_intensity = 0.5
    mat.alpha = alpha
    mat.ambient = 1
    return mat

def set_material(obj, mat):
    me = obj.data
    me.materials.append(mat)


def create_pin(x, y):
    bpy.ops.mesh.primitive_cube_add()
    bpy.ops.transform.resize(value=(K/2, M/2, H/2+0.01))
    pin = bpy.context.object
    
    bpy.ops.mesh.primitive_cylinder_add(radius = N/2.)
    cyl = bpy.context.object
    cyl.name = 'pin'
    
    if(y<0):
        shift = M/2
    else: 
        shift = -M/2
    
    cyl.location = (0, -shift, 0)
    
    mod_bool = pin.modifiers.new('Bool', 'BOOLEAN')
    mod_bool.object = cyl  
    mod_bool.operation = 'DIFFERENCE'
    
    bpy.context.scene.objects.active = pin
    res = bpy.ops.object.modifier_apply(modifier = 'Bool')

    if(y<0):
        shift = M/2 - 0.01
    else: 
        shift = -M/2 + 0.01
        
    pin.location = (x, y + shift, 0)
    
    bpy.data.scenes['Scene'].objects.unlink(cyl)
    return pin
    


def create_base_plate():
    # Create the materials.
    pcb_green = make_material('pcb green', (0, 0.71, 0.04), (1,1,1), 1)
    pin_gold = make_material('pin gold', (1, 0.93, 0), (1,1,1), 1)
    
    bpy.ops.mesh.primitive_cube_add()
    cube = bpy.context.object
    cube.name = 'base_plate'
    set_material(cube, pcb_green)
    bpy.ops.transform.resize(value=(A/2, B/2, H/2))
    
    bpy.ops.mesh.primitive_cylinder_add(radius = N/2.)
    cyl = bpy.context.object
    cyl.name = 'pin'
        
    n_pins = 14
    for k in range(0, n_pins):
        bpy.ops.object.select_all(action = 'DESELECT')
        cyl.select = True
        # I couldn't figure out why, but when using the same formula as 
        # for the other pin group, the mesh got messed up.
        # I had to go from positive to negative for this pin group and 
        # from negative to positive for the other.
        if k > 7:
            cyl.location = (A/2 - D - F - ((k-1)*E), -B/2, 0)
        else:
            cyl.location = (A/2 - D - (k*E), -B/2, 0)
        
        mod_bool = cube.modifiers.new('Bool', 'BOOLEAN')
        mod_bool.object = cyl  
        mod_bool.operation = 'DIFFERENCE'
        
        
        bpy.context.scene.objects.active = cube
        res = bpy.ops.object.modifier_apply(modifier = 'Bool')
        
        pin = create_pin(cyl.location[0], cyl.location[1])
        set_material(pin, pin_gold)
        
    
    n_pins = 14
    for k in range(0, n_pins):
        bpy.ops.object.select_all(action = 'DESELECT')
        cyl.select = True
        if k > 5:
            cyl.location = (-A/2 + G + F + ((k-1)*E), B/2, 0)
        else:
            cyl.location = (-A/2 + G + (k*E), B/2, 0)
        
        mod_bool = cube.modifiers.new('Bool', 'BOOLEAN')
        mod_bool.object = cyl  
        mod_bool.operation = 'DIFFERENCE'
        
        
        bpy.context.scene.objects.active = cube
        res = bpy.ops.object.modifier_apply(modifier = 'Bool')
        
        pin = create_pin(cyl.location[0], cyl.location[1])
        set_material(pin, pin_gold)
        
    bpy.data.scenes['Scene'].objects.unlink(cyl)
    return cube
    
    
def create_housing():
    case_silver = make_material('pin gold', (0.65, 0.65, 0.65), (1,1,1), 1)
    
    bpy.ops.mesh.primitive_cube_add()
    bpy.ops.transform.resize(value=((A-1)/2, (B-1)/2, (C - H) / 2))
    case = bpy.context.object
    case.location = (0, 0, H)
    
    bpy.ops.mesh.primitive_cylinder_add(radius = 1)
    bpy.ops.transform.resize(value=(1, 1, (C - H)/4))
    cyl = bpy.context.object
    cyl.name = 'pin1_mark'
    cyl.location = (A/2 - 2, B/2 - 2, C/2 + 0.5)    
    mod_bool = case.modifiers.new('Bool', 'BOOLEAN')
    mod_bool.object = cyl  
    mod_bool.operation = 'DIFFERENCE'
    
    bpy.context.scene.objects.active = case
    res = bpy.ops.object.modifier_apply(modifier = 'Bool')
    
    set_material(case, case_silver)
    
    bpy.data.scenes['Scene'].objects.unlink(cyl)



# Delete all objects from the scene.
bpy.ops.object.select_all(action = 'SELECT')
bpy.ops.object.delete()

# Create the ublox lea6T package.
base_plate = create_base_plate()
create_housing()
bpy.ops.object.select_all(action = 'SELECT')
bpy.context.scene.objects.active = base_plate
bpy.ops.object.join()

# Rotate the object to fit it to the coordinate system used by wings3D.
# Positive Y-axis in Blender is the negative Z-axis in Wings3D
bpy.ops.transform.rotate(value = (math.pi/2, ), axis = (1, 0, 0))
# Shift the object to the zero position.
base_plate.location = (0, -0.41, 0)
