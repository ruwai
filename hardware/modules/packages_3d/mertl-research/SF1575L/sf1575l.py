# Import the Blender-Python API module
import bpy
import math

# Define the u-Blox dimensions.
L = 3
W = 3
H1 = 1.08
P_W = 0.6
P_L = 0.75
P_H = 0.6
H2 = P_H + 0.15




def make_material(name, diffuse, specular, alpha):
    mat = bpy.data.materials.new(name)
    mat.diffuse_color = diffuse
    mat.diffuse_shader = 'LAMBERT' 
    mat.diffuse_intensity = 1.0 
    mat.specular_color = specular
    mat.specular_shader = 'COOKTORR'
    mat.specular_intensity = 0.5
    mat.alpha = alpha
    mat.ambient = 1
    return mat

def set_material(obj, mat):
    me = obj.data
    me.materials.append(mat)


def create_pin_rect(x, y, z, L, W, H, R, hole_pos = 'center'):
    bpy.ops.mesh.primitive_cube_add()
    bpy.ops.transform.resize(value=(W/2, L/2, H/2+0.01))
    pin = bpy.context.object
    
    bpy.ops.mesh.primitive_cylinder_add(radius = R)
    cyl = bpy.context.object
    cyl.name = 'pin'
    
    if hole_pos == 'center':
        if(y<0):
            yshift = L/2.
            xshift = 0
        else: 
            yshift = -L/2.
            xshift = 0
    elif hole_pos == 'left':
        if(y<0):
            yshift = L/2.
            xshift = W/2.
        else: 
            yshift = -L/2.    
            xshift = W/2.  
    elif hole_pos == 'right':
        if(y<0):
            yshift = L/2.
            xshift = -W/2.
        else: 
            yshift = -L/2.    
            xshift = -W/2.    
    
    cyl.location = (-xshift, -yshift, 0)
    
    mod_bool = pin.modifiers.new('Bool', 'BOOLEAN')
    mod_bool.object = cyl  
    mod_bool.operation = 'DIFFERENCE'
    
    bpy.context.scene.objects.active = pin
    res = bpy.ops.object.modifier_apply(modifier = 'Bool')

    if(y<0):
        yshift = L/2. - 0.01
    else: 
        yshift = -L/2. + 0.01

    if(x<0):
        xshift = -0.01
    else: 
        xshift = 0.01
              
        
    pin.location = (x + xshift, y + yshift, z)
    
    bpy.data.scenes['Scene'].objects.unlink(cyl)
    return pin



def create_base_plate():
    # Create the materials.
    case_dark_gray = make_material('pcb green', (0.3, 0.3, 0.3), (1,1,1), 1)
    pin_gold = make_material('pin gold', (1, 0.93, 0), (1,1,1), 1)
    
    pin_r =P_W/4.
    
    bpy.ops.mesh.primitive_cube_add()
    cube = bpy.context.object
    cube.name = 'base_plate'
    set_material(cube, case_dark_gray)
    bpy.ops.transform.resize(value=(W/2, L/2, H2/2))
    
    bpy.ops.mesh.primitive_cylinder_add(radius = pin_r + 0.02)
    cyl = bpy.context.object
    cyl.name = 'pin'
    
    hole_locations = [(-W/2., L/2.), 
                     (0, L/2.), 
                     (W/2., L/2.), 
                     (-W/2., -L/2.), 
                     (0, -L/2.), 
                     (W/2., -L/2.),
                     (-W/2., 0),
                     (W/2., 0)]    
    
    pin_locations = [(-1.2, L/2., 'left'), 
                     (0, L/2., 'center'), 
                     (1.2, L/2., 'right'), 
                     (-1.2, -L/2., 'left'), 
                     (0, -L/2., 'center'), 
                     (1.2, -L/2., 'right')]
   
    for cur_hole_loc in hole_locations:
        bpy.ops.object.select_all(action = 'DESELECT')
        cyl.select = True
        cyl.location = (cur_hole_loc[0], cur_hole_loc[1], 0)

        
        mod_bool = cube.modifiers.new('Bool', 'BOOLEAN')
        mod_bool.object = cyl  
        mod_bool.operation = 'DIFFERENCE'

        bpy.context.scene.objects.active = cube
        res = bpy.ops.object.modifier_apply(modifier = 'Bool')   
        
    
    for cur_pin_loc in pin_locations:
        pin = create_pin_rect(cur_pin_loc[0], cur_pin_loc[1], - (H2 - P_H)/2, P_L, P_W, P_H, pin_r, hole_pos = cur_pin_loc[2])
        set_material(pin, pin_gold)
        
    bpy.data.scenes['Scene'].objects.unlink(cyl)
    return cube


def create_top_case():
    # Create the materials.
    case_light_gray = make_material('pcb green', (0.8, 0.8, 0.8), (1,1,1), 1)
    
    pin_r =P_W/4.
    
    bpy.ops.mesh.primitive_cube_add()
    cube = bpy.context.object
    cube.name = 'case'
    set_material(cube, case_light_gray)
    bpy.ops.transform.resize(value=(((W - 2*pin_r - 0.05)/2, (L - 2*pin_r - 0.05)/2, H1/2))) 
    cube.location = (0, 0, H1/2 - H2/2)
    
    return cube
    

#create_pin_rect(0, 0, 0, P_L, P_W, H2, P_W/4.)
#create_pin_rect(1, 0, 0, P_L, P_W, H2, P_W/4., hole_pos = 'right')
#create_pin_rect(-1, 0, 0, P_L, P_W, H2, P_W/4., hole_pos = 'left')
base_plate = create_base_plate()
top_case = create_top_case()

bpy.ops.object.select_all(action = 'SELECT')
bpy.context.scene.objects.active = base_plate
bpy.ops.object.join()

# Rotate the object to fit it to the coordinate system used by wings3D.
# Positive Y-axis in Blender is the negative Z-axis in Wings3D
bpy.ops.transform.rotate(value = (math.pi/2, ), axis = (1, 0, 0))
# Shift the object to the zero position.
base_plate.location = (0, -H2/2, 0)