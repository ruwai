/*--------------------------------------------------------------------------*/
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai_parser in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// ruwai is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*--------------------------------------------------------------------------*/

#include <iostream>
#include <vector>

#include <libmseed/libmseed.h>

void record_handler (char *record, int reclen, void *ptr);

int main(void)
{
    std::vector <std::int32_t> data_buffer;     // The vector holding the sample data.
    MSRecord *msr = NULL;                       // The miniseed record used for packing.

    // Initialize the MSRecord.
    msr = msr_init(NULL);
    strcpy(msr->network, "XX");
    strcpy(msr->station, "MR01");
    strcpy(msr->channel, "01");
    strcpy(msr->location, "00");
    msr->starttime = ms_time2hptime(2015, 301, 10, 53, 10, 0);
    msr->samprate = 100;
    msr->byteorder = 0;
    msr->encoding = 11;
    msr->dataquality = 'D';
    msr->sequence_number = 0;
    msr->reclen = 512;

    // Fill the data_buffer with values.
    for (int i = 0; i < 1000; i++)
    {
        data_buffer.push_back(i);
    }

    // Set the datasamples pointer of MSRecord.
    msr->datasamples = &data_buffer[0];
    msr->numsamples = data_buffer.size();
    msr->sampletype = 'i';

    // Write the MSRecord to a miniseed file.
    FILE *out_fid = NULL;
    int64_t packed_samples = 0;
    int64_t packed_records = 0;
    char isotime_string[28];
    out_fid = fopen("packed_data.msd", "wb");
    ms_hptime2isotimestr(msr->starttime, isotime_string, 1);
    std::cout << "start time: " << isotime_string << "\n";
    std::cout << "data_buffer size: " << data_buffer.size() << "\n\n";
    packed_records = msr_pack(msr, &record_handler, out_fid, &packed_samples, 0, 5);


    ms_hptime2isotimestr(msr->starttime, isotime_string, 1);
    std::cout << "start time: " << isotime_string << "\n";
    std::cout << "data_buffer size: " << data_buffer.size() << "\n\n";
    packed_records = msr_pack(msr, &record_handler, out_fid, &packed_samples, 0, 5);

    ms_hptime2isotimestr(msr->starttime, isotime_string, 1);
    std::cout << "start time: " << isotime_string << "\n";
    std::cout << "data_buffer size: " << data_buffer.size() << "\n\n";

    fclose(out_fid);

    // Unlink the datasamples and free the MSRecord structure.
    msr->datasamples = NULL;
    msr_free(&msr);
}


/***************************************************************************
 * record_handler:
 * Saves passed records to the output file.
 ***************************************************************************/
void
record_handler (char *record, int reclen, void *ptr)
{
  if ( fwrite(record, reclen, 1, (FILE*)ptr) != 1 )
    {
      ms_log (2, "Cannot write to output file\n");
    }
}  

