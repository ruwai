/*--------------------------------------------------------------------------*/
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai_parser in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// ruwai is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*--------------------------------------------------------------------------*/

#include "serial_parser.h"
#include <signal.h>


void
serial_watchdog(int signum)
{
    if (signum == SIGALRM)
    {
        syslog(LOG_ERR, "[ERROR][serial_watchdog] Timeout in record mode. No serial data received for some time.");
        std::exit(EXIT_FAILURE);
    }
}


SerialParser::SerialParser(int serial_port, Recorder* recorder)
{
    this->serial_port = serial_port;
    this->recorder = recorder;

    synced = false;
    seq_num = 1;
    last_ack_msg_id = -1;
    last_ack_msg_class = -1;

    control_mode = true;
    record_mode = false;
    parser_size = sizeof(rw_header_t);
}


bool
SerialParser::set_control_mode(void)
{
    bool confirmed;
    confirmed = set_mode(0);
    control_mode = true;
    record_mode = false;
    parser_size = sizeof(rw_header_t);
    signal(SIGALRM, SIG_DFL);
    return confirmed;
}


bool
SerialParser::set_record_mode(void)
{
    bool confirmed;
    struct termios port_options; // struct to hold the port settings
    
    confirmed = set_mode(1);
    control_mode = false;
    record_mode = true;
    parser_size = 1000;

    //close(serial_port);
    
    tcgetattr(serial_port, &port_options);
    port_options.c_cc[VMIN] = 200;     // Minimum number of bytes read.
    port_options.c_cc[VTIME] = 0;   // timeout in deci-seconds
    
    tcsetattr(serial_port, TCSANOW, &port_options);
    tcflush(serial_port, TCIOFLUSH);
    //fcntl(serial_port, F_SETFL, 0);

    signal(SIGALRM, serial_watchdog);
    alarm(5);
    
    return confirmed;
}


bool
SerialParser::request_version(void)
{
    rw_soh_request_t msg;
    msg.msg_id = RW_SOH_ID_VERSION;

    send_ruwai_message(RW_CLASS_SOH, RW_SOH_ID_REQUEST, &msg, sizeof(msg));

    return true;
}


bool
SerialParser::is_synced(void)
{
    return synced;
}

void
SerialParser::start_handshake(void)
{
    syslog(LOG_NOTICE, "Starting the handshake.");
    send_ack_sync(1, 0, seq_num, 0); 
}

void
SerialParser::sync(std::vector<uint8_t> payload)
{
    syslog(LOG_DEBUG, "in sync");
    rw_ack_sync_t paket;
    assert(payload.size() == sizeof paket);
    std::copy(payload.begin(), payload.end(), reinterpret_cast<uint8_t*>(&paket));
    syslog(LOG_DEBUG, "sync: %d, ack: %d, seq_num: %d, ack_num: %d", paket.sync, paket.ack, paket.seq_num, paket.ack_num);
    if ((paket.sync == 1) && (paket.ack == 1))
    {
        // Handle the sync request.
        syslog(LOG_NOTICE, "Got a handshake response from the Arduino stack.");
        //send_ack_sync(0, 1, seq_num++, payload.rw_ack_sync.seq_num + 1); 
        send_ack_sync(0, 1, seq_num, 0); 
        synced = true;
    }
}


void
SerialParser::send_ack_sync(uint8_t sync, uint8_t ack, uint8_t seq_num, uint8_t ack_num)
{
    rw_ack_sync_t msg;
    msg.sync = sync;
    msg.ack = ack;
    msg.seq_num = seq_num;
    msg.ack_num = ack_num;

    send_ruwai_message(RW_CLASS_ACK, RW_ACK_ID_SYNC, &msg, sizeof(msg));
}


void 
SerialParser::send_ruwai_message(uint8_t msg_class, uint8_t msg_id, void* msg, uint8_t length)
{
    rw_header_t header;
    uint8_t ck0 = 0, ck1 = 0;
    header.sync_char1 = RUWAI_SYNC_CHAR1;
    header.sync_char2 = RUWAI_SYNC_CHAR2;
    header.msg_class = msg_class;
    header.msg_id = msg_id;
    header.payload_length = length;

    // Update the checksum using the relevant header part.
    update_fletcher16_checksum((uint8_t *)&header.msg_class, sizeof(header) - 2, ck0, ck1);

    // Update the checksum using the payload.
    update_fletcher16_checksum((uint8_t *)msg, length, ck0, ck1);

    // Send the paket to the serial port.
    write(serial_port, (const uint8_t *)&header, sizeof(header));
    write(serial_port, (const uint8_t *)msg, length);
    write(serial_port, &ck0, 1);
    write(serial_port, &ck1, 1);
}

bool
SerialParser::wait_for_cfg_ack(uint8_t msg_class, uint8_t msg_id)
{
    float timeout = 5;         // The timeout [s].
    auto start = std::chrono::high_resolution_clock::now();

    float diff_sec = 0;

    while ((last_ack_msg_class != msg_class) && (last_ack_msg_id != msg_id))
    {
        //std::cout << "last_ack_msg_id: " << last_ack_msg_id << std::endl;
        //std::cout << "msg_id: " << msg_id << std::endl;
        auto end = std::chrono::high_resolution_clock::now();		
        auto diff = end - start;
        diff_sec = (float)std::chrono::duration_cast<std::chrono::seconds>(diff).count();

        if (diff_sec > timeout)
        {
            last_ack_msg_class = -1;
            last_ack_msg_id = -1;
            return false;
        }
        usleep(100000);
    }
    last_ack_msg_class = -1;
    last_ack_msg_id = -1;
    return true;
}

bool
SerialParser::set_sps(uint16_t sps)
{
    bool confirmed = false;
    rw_cfg_sps_t msg;
    msg.sps = sps;
    send_ruwai_message(RW_CLASS_CFG, RW_CFG_ID_SPS, &msg, sizeof(msg));
    confirmed = wait_for_cfg_ack(RW_CLASS_CFG, RW_CFG_ID_SPS);
    return confirmed;
}


bool
SerialParser::set_gps(bool usb_active)
{
    bool confirmed = false;
    rw_cfg_gps_t msg;

    if (usb_active)
        msg.ports.usb = 1;

    send_ruwai_message(RW_CLASS_CFG, RW_CFG_ID_GPS, &msg, sizeof(msg));
    confirmed = wait_for_cfg_ack(RW_CLASS_CFG, RW_CFG_ID_GPS);
    return confirmed;
}


bool 
SerialParser::set_channels(bool c1_active, bool c2_active, bool c3_active, bool c4_active, pga_gain_t c1_gain, pga_gain_t c2_gain, pga_gain_t c3_gain, pga_gain_t c4_gain)
{
    bool confirmed = false;
    rw_cfg_channels_t msg;

    msg.active.reserved = 0;

    if (c1_active)
        msg.active.channel1 = 1;
    else
        msg.active.channel1 = 0;

    if (c2_active)
        msg.active.channel2 = 1;
    else
        msg.active.channel2 = 0;

    if (c3_active)
        msg.active.channel3 = 1;
    else
        msg.active.channel3 = 0;

    if (c4_active)
        msg.active.channel4 = 1;
    else
        msg.active.channel4 = 0;

    msg.gain_channel1 = c1_gain;
    msg.gain_channel2 = c2_gain;
    msg.gain_channel3 = c3_gain;
    msg.gain_channel4 = c4_gain;

    send_ruwai_message(RW_CLASS_CFG, RW_CFG_ID_CHANNELS, &msg, sizeof(msg));
    confirmed = wait_for_cfg_ack(RW_CLASS_CFG, RW_CFG_ID_CHANNELS);
    return confirmed;
}


bool
SerialParser::set_mode(uint8_t mode)
{
    bool confirmed = false;
    rw_cfg_mode_t msg;
    msg.mode = mode;

    send_ruwai_message(RW_CLASS_CFG, RW_CFG_ID_MODE, &msg, sizeof(msg));
    confirmed = wait_for_cfg_ack(RW_CLASS_CFG, RW_CFG_ID_MODE);
    return confirmed;
}



void
SerialParser::read_and_parse(void)
{
    int bytes_read;
    int count = 0;
    const int buffer_size = 256;
    float timeout_limit = 1;      // The maximum timeout in seconds.
    bool timeout = false;
    int timeout_cnt = 0;
    float read_diff = 0;
    //const int parser_size = 10;
    std::uint8_t data_buffer[buffer_size];

    for(int j = 0; j < buffer_size; j++)
    {
	data_buffer[j] = 0;
    }

    // Flush the buffer one more time.
    tcflush(serial_port, TCIOFLUSH);

    auto last_read = std::chrono::high_resolution_clock::now();
    while(!timeout)
    {
	    //ioctl(serial_port, FIONREAD, &bytes_available);
	    //syslog(LOG_NOTICE, "[read_and_parse] before read");
	    bytes_read = read(serial_port, data_buffer, buffer_size);
	    //syslog(LOG_NOTICE, "[read_and_parse] after read");
	    //syslog(LOG_NOTICE, "[read_and_parse] bytes_read: %d.", bytes_read);
	    //std::cout << "Bytes read: " << bytes_read << "\n";

	    /*
	    std::cout << "serial buffer size: " << dec << serial_buffer.size() << std::endl;
	    for(int j = 0; j < serial_buffer.size(); j++)
	    {
		std::cout << hex << int(serial_buffer[j]) << dec << " ";
	    }
	    std::cout << std::endl;
	    */

	    if (bytes_read > 0)
	    {
                last_read = std::chrono::high_resolution_clock::now();

		for(int j = 0; j < bytes_read; j++)
		{
		    serial_buffer.push_back(data_buffer[j]);
		    //std::stringstream stream;
		    //stream << std::hex << int(data_buffer[j]);
		    //std::string result( stream.str() );
		    //syslog(LOG_DEBUG, "[read_and_parse] byte: %s", result.c_str());
		    //std::cout << std::hex << int(data_buffer[j]) << std::dec << " ";
		}
		//std::cout << std::endl;
	    }

	    /*
	    std::cout << "serial buffer size: " << serial_buffer.size() << std::endl;
	    for(int j = 0; j < serial_buffer.size(); j++)
	    {
		std::cout << hex << int(serial_buffer[j]) << dec << " ";
	    }
	    std::cout << std::endl;
	    */
	    //syslog(LOG_DEBUG, "[read_and_parse] parser_size: %d; serial_buffer.size(): %d.", parser_size, serial_buffer.size());
	    if (serial_buffer.size() > parser_size)
	    {
		syslog(LOG_DEBUG, "[read_and_parse] parser_size: %d; serial_buffer.size(): %d.", parser_size, (int)serial_buffer.size());
	    	//auto start = std::chrono::high_resolution_clock::now();
	    	parse_serial_buffer();
	    	//auto end = std::chrono::high_resolution_clock::now();		
	    	//auto diff = end - start;

	    	//std::cout.precision(10);
	    	//std::cout << "Parsed the pakets in " << std::chrono::duration_cast<std::chrono::nanoseconds>(diff).count() << " nanoseconds.\n";
	    	//std::cout.precision(3);
	    	syslog(LOG_DEBUG, "[read_and_parse] after parsing ---- parser_size: %d; serial_buffer.size(): %d.", parser_size, (int)serial_buffer.size());
			uint32_t max_message_size = 256;
			if (serial_buffer.size() > max_message_size)
			{
				serial_buffer.erase(serial_buffer.begin(), serial_buffer.end() - sizeof(rw_header_t));
				syslog(LOG_DEBUG, "[read_and_parse] cropping the buffer ---- parser_size: %d; serial_buffer.size(): %d.", parser_size, (int)serial_buffer.size());
	    	}

		count++;
	    }

            // Check for timeout.
	    /*
	    if (record_mode)
	    {
	        if (timeout_cnt > 10)
	        {
		     auto cur_time = std::chrono::high_resolution_clock::now();
		     auto diff = cur_time - last_read;
		     read_diff = (float)std::chrono::duration_cast<std::chrono::seconds>(diff).count();
		     //syslog(LOG_NOTICE, "read_diff: %f.", read_diff);
		     if (read_diff > timeout_limit)
		     {
		         syslog(LOG_ERR, "[read_and_parse] Timeout in record mode. No data received for %f seconds.", read_diff);
		         timeout = true;
		     }
		     timeout_cnt = 0;
	        }
	        timeout_cnt++;
           }
	   */
    }
}

void
SerialParser::parse_serial_buffer(void)
{
    if (serial_buffer.size() == 0) {
        //std::cout << "Serial buffer is empty. Skip parsing." << std::endl;
        return;
    }

    std::uint8_t msg_class;
    std::uint8_t msg_id;
    std::uint16_t payload_length;

    std::vector<std::uint8_t> sync_sequ;
    sync_sequ.push_back(RUWAI_SYNC_CHAR1);
    sync_sequ.push_back(RUWAI_SYNC_CHAR2);

    std::vector<std::uint8_t>::iterator sync_pos = serial_buffer.begin();
    std::vector<std::uint8_t>::iterator last_sync_pos = sync_pos;
    std::vector<std::uint8_t>::iterator payload_pos = sync_pos;

    while (sync_pos != serial_buffer.end()) {
        sync_pos = std::search(sync_pos, serial_buffer.end(), sync_sequ.begin(), sync_sequ.end());
        if (sync_pos != serial_buffer.end()) {
            //std::cout << "SYNC CHAR found: " << std::hex << static_cast<int>(*sync_pos) << ' ' << static_cast<int>(*(sync_pos + 1)) << std::dec << std::endl;
            //std::cout << "iterator diff: " << serial_buffer.end() - sync_pos << std::endl;
            if (serial_buffer.end() - sync_pos <= 6) {
                // The remaining bytes don't represent a complete paket.
                sync_pos = serial_buffer.end();
            }
            else {
                msg_class = *(sync_pos + 2);
                msg_id = *(sync_pos + 3);
                payload_length = (*(sync_pos + 5) << 8) | *(sync_pos + 4);
                //std::cout << "msg_class: " << static_cast<int>(msg_class) << std::endl;
                //std::cout << "msg_id: " << static_cast<int>(msg_id) << std::endl;
                //std::cout << "payload length: " << payload_length << std::endl;
                payload_pos = sync_pos + 6;
                if (distance(payload_pos, serial_buffer.end()) < (payload_length + 2)) {
                    // Not enough bytes read for this paket.
                    //std::cout << "not enough bytes for this paket in the buffer" << std::endl;
                    break; 
                }

                std::vector<uint8_t> payload;
                for (int i = 0; i < payload_length; i++)
                {
                    //payload.bytes[i] = *(payload_pos + i);
                    payload.push_back(*(payload_pos + i));
                }

                if (record_mode && (msg_class == RW_CLASS_SMP)) {

                    if (msg_id == RW_SMP_ID_24BIT)
                    {
		        parse_smp_24bit(payload);
                        alarm(1);
                    }
                    else if (msg_id == RW_SMP_ID_24BIT_TSP)
                    {
                        parse_smp_24bit_tsp(payload);
                        alarm(1);
                    }
                }
                else if (msg_class == RW_CLASS_ACK) {
                    if (msg_id == RW_ACK_ID_SYNC)
                    {
		        sync(payload);
                    }
                    else if (msg_id == RW_ACK_ID_CFG)
                    {
                        rw_ack_cfg_t paket;
                        assert(payload.size() == sizeof paket);
                        std::copy(payload.begin(), payload.end(), reinterpret_cast<uint8_t*>(&paket));
                        syslog(LOG_DEBUG, "Received ACK_ID_CFG for msg_class ###: %d; msg_id: %d.\n", paket.msg_class, paket.msg_id);
                        last_ack_msg_class = paket.msg_class;
                        last_ack_msg_id = paket.msg_id;
                    }
                }
                else if (msg_class == RW_CLASS_SOH) {
                    syslog(LOG_INFO, "Received a RW_CLASS_SOH message.");
                    if (msg_id == RW_SOH_ID_STATUS)
                    {
                        rw_soh_status_t paket;
                        std::copy(payload.begin(), payload.end(), reinterpret_cast<uint8_t*>(&paket));
                        syslog(LOG_NOTICE, "[as_status][type: %d] %s", paket.type, paket.status);
                    }
                    else if (msg_id == RW_SOH_ID_GPSPOS)
                    {
                        rw_soh_gpspos_t paket;
                        std::copy(payload.begin(), payload.end(), reinterpret_cast<uint8_t*>(&paket));
                        syslog(LOG_NOTICE, "[soh][gps_pos] %f, %f, %f", paket.lon * 1e-7, paket.lat * 1e-7, (float)paket.height / 1000);
                    }
                    else if (msg_id == RW_SOH_ID_ENVDATA)
                    {
                        rw_soh_envdata_t paket;
                        std::copy(payload.begin(), payload.end(), reinterpret_cast<uint8_t*>(&paket));
                        syslog(LOG_NOTICE, "[soh][temperature_gts] %f", (float) paket.temp_gts / 100);
                    }
                }
                sync_pos = payload_pos + payload_length + 2;
                last_sync_pos = sync_pos;
            }
        }
        else {
            //std::cout << "SYNC_CHAR not found." << std::endl;
            /*
            for (int i = 0; i < serial_buffer.size(); i++) {
                std::cout << hex << static_cast<int>(serial_buffer[i]) << " ";
            }
            std::cout << "\n\n";
            */
        }
    }


    //std::cout << "distance: " << distance(serial_buffer.begin(), last_sync_pos) << std::endl;
    if (std::distance(serial_buffer.begin(), last_sync_pos) > 0) {
        serial_buffer.erase(serial_buffer.begin(), last_sync_pos);
        //std::cout << "buffer size after erase: " << serial_buffer.size() << std::endl;
        /* 
        for (int i = 0; i < serial_buffer.size(); i++) {
            std::cout << hex << static_cast<int>(serial_buffer[i]) << " ";
        }
        std::cout << "\n\n";
        */
    }
}


bool 
SerialParser::parse_smp_24bit(std::vector<uint8_t> payload) {
    rw_smp_24bit_t paket;
    assert(payload.size() == sizeof paket);
    std::copy(payload.begin(), payload.end(), reinterpret_cast<uint8_t*>(&paket));
    recorder->add_sample(paket);
    return true;
}


bool 
SerialParser::parse_smp_24bit_tsp(std::vector<uint8_t> payload) {
    rw_smp_24bit_tsp_t paket;
    assert(payload.size() == sizeof paket);
    std::copy(payload.begin(), payload.end(), reinterpret_cast<uint8_t*>(&paket));
    recorder->add_timestamp_sample(paket);
    return true;
}

