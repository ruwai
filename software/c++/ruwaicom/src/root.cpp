/*--------------------------------------------------------------------------*/
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai_parser in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// ruwai is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*--------------------------------------------------------------------------*/

#include "root.h"
#include "log_temperature.h"

Root::Root(std::string config_file)
{
    boost::property_tree::ptree pt;
    double c1_gain, c2_gain, c3_gain, c4_gain;
    int sps_raw;

    display = new LCDDisplay(66, 67, 45, 23, 47, 27);
    display->init();
    display->clear();
    display->home();
    display->print("Hello, Ruwai", "is speaking.");

    try {
        boost::property_tree::ini_parser::read_ini(config_file, pt);

        // ---------------------------------------------------------
        // Parse the arduino stack section.
        dev_name = pt.get<std::string>("arduino_stack.serial_port");
        n_channels = pt.get<unsigned int>("arduino_stack.n_channels");

        channel1_active = pt.get<bool>("arduino_stack.channel1_active");
        channel2_active = pt.get<bool>("arduino_stack.channel2_active");
        channel3_active = pt.get<bool>("arduino_stack.channel3_active");
        channel4_active = pt.get<bool>("arduino_stack.channel4_active");

        if (channel1_active)
        {
            record_channels.push_back(1);
        }

        if (channel2_active)
        {
            record_channels.push_back(2);
        }

        if (channel3_active)
        {
            record_channels.push_back(3);
        }

        if (channel4_active)
        {
            record_channels.push_back(4);
        }

        c1_gain = pt.get<double>("arduino_stack.channel1_pga");
        channel1_gain = translate_pga_gain(c1_gain);
        if (channel1_gain == PGA_GAIN_UNKNOWN)
        {
            syslog(LOG_ERR, "[ERROR][root] Unknown gain setting for channel 1: %f.", c1_gain);
            std::exit(EXIT_FAILURE);
        }

        c2_gain = pt.get<double>("arduino_stack.channel2_pga");
        channel2_gain = translate_pga_gain(c2_gain);
        if (channel2_gain == PGA_GAIN_UNKNOWN)
        {
            syslog(LOG_ERR, "[ERROR][root] Unknown gain setting for channel 4: %f.", c2_gain);
            std::exit(EXIT_FAILURE);
        }

        c3_gain = pt.get<double>("arduino_stack.channel3_pga");
        channel3_gain = translate_pga_gain(c3_gain);
        if (channel3_gain == PGA_GAIN_UNKNOWN)
        {
            syslog(LOG_ERR, "[ERROR][root] Unknown gain setting for channel 4: %f.", c3_gain);
            std::exit(EXIT_FAILURE);
        }

        c4_gain = pt.get<double>("arduino_stack.channel4_pga");
        channel4_gain = translate_pga_gain(c4_gain);
        if (channel4_gain == PGA_GAIN_UNKNOWN)
        {
            syslog(LOG_ERR, "[ERROR][root] Unknown gain setting for channel 4: %f.", c4_gain);
            std::exit(EXIT_FAILURE);
        }

        sps_raw = pt.get<uint16_t>("arduino_stack.sps");
        sps = translate_adc_sps(sps_raw);
        if (sps == ADC_SPS_UNKNOWN)
        {
            syslog(LOG_ERR, "[ERROR][root] Not supported sps setting: %d.", sps_raw);
            std::exit(EXIT_FAILURE);
        }


        // ------------------------------------------------------------------------
        // Parse the record section.
        storage_mode = pt.get<std::string>("record.storage_mode");
        tmp_dir = pt.get<std::string>("record.tmp_dir");
        ruwai_serial_file = pt.get<std::string>("record.ruwai_serial_file");
        file_length = pt.get<unsigned int>("record.file_length");
        //record_channels = to_array<unsigned int>(pt.get<std::string>("record.record_channels"));


        // ------------------------------------------------------------------------
        // Parse the gps section.
        gps_usb_active = pt.get<bool>("gps.usb_active");

        
        // ------------------------------------------------------------------------
        // Depending on the selected storage mode, parse the sd or hdd section.
        if (storage_mode == "sd")
        {
            sd_dev_label = pt.get<std::string>("sd.dev_label");
            sd_dev_1 = pt.get<std::string>("sd.dev_1");
            sd_dev_2 = pt.get<std::string>("sd.dev_2");
            sd_mnt_point = pt.get<std::string>("sd.mnt_point");
        } else if (storage_mode == "hdd")
        {
            hdd_mseed_dir = pt.get<std::string>("hdd.mseed_dir");
        }

        // ------------------------------------------------------------------------
        rts_start_server = pt.get<bool>("rt_server.start_server");
        rts_port = pt.get<unsigned int>("rt_server.port");
        rts_max_clients = pt.get<unsigned int>("rt_server.max_clients");
    }
    catch (const boost::property_tree::ptree_bad_path& e) {
        syslog(LOG_ERR, "[ERROR][root] Couldn't read the config file %s. Error message: %s.", config_file.c_str(), e.what());
        throw;
    }


    try {
        //const std::string serial_filename = "/etc/ruwai_serial";
        std::string line;
        std::ifstream myfile(ruwai_serial_file);
        if (myfile.is_open())
        {
            std::getline(myfile, line);
            boost::trim_left(line);
            boost::trim_right(line);
            serial_number = line;
            syslog(LOG_NOTICE, "[root] Ruwai serial number: %s.", serial_number.c_str());
            myfile.close();
        }
        else 
        {
            syslog(LOG_ERR, "[ERROR][root] Unable to open the file %s to read the serial number.", ruwai_serial_file.c_str());
            std::exit(EXIT_FAILURE);
        }
    }
    catch (...) {
        syslog(LOG_ERR, "[ERROR][root] Couldn't read the serial number from %s. Unknown error.", ruwai_serial_file.c_str());
        throw;
    }


    std::string rec_channels_string = "";
    for (unsigned int k = 0; k < record_channels.size(); k++)
    {
        rec_channels_string += std::to_string(record_channels[k]);

        if (k < record_channels.size() - 1)
        {
            rec_channels_string += ",";
        }
    }

    syslog(LOG_NOTICE, "------- read configuration --------");
    syslog(LOG_NOTICE, "arduino_stack");
    syslog(LOG_NOTICE, "serial_port: %s", dev_name.c_str());
    syslog(LOG_NOTICE, "n_channels: %d", n_channels);
    syslog(LOG_NOTICE, "channel1_active: %d", channel1_active);
    syslog(LOG_NOTICE, "channel2_active: %d", channel2_active);
    syslog(LOG_NOTICE, "channel3_active: %d", channel3_active);
    syslog(LOG_NOTICE, "channel4_active: %d", channel4_active);
    syslog(LOG_NOTICE, "channel1_gain: %.3f (%d)", c1_gain, channel1_gain);
    syslog(LOG_NOTICE, "channel2_gain: %.3f (%d)", c2_gain, channel2_gain);
    syslog(LOG_NOTICE, "channel3_gain: %.3f (%d)", c3_gain, channel3_gain);
    syslog(LOG_NOTICE, "channel4_gain: %.3f (%d)", c4_gain, channel4_gain);
    syslog(LOG_NOTICE, "sps: %d (%d)", sps_raw, sps);
    syslog(LOG_NOTICE, "record");
    syslog(LOG_NOTICE, "storage_mode: %s", storage_mode.c_str());
    syslog(LOG_NOTICE, "tmp_dir: %s", tmp_dir.c_str());
    syslog(LOG_NOTICE, "record_channels: %s", rec_channels_string.c_str()); 
    syslog(LOG_NOTICE, "file_length: %d", file_length);
    if (storage_mode == "sd")
    {
        syslog(LOG_NOTICE, "sd");
        syslog(LOG_NOTICE, "dev_label: %s", sd_dev_label.c_str());
        syslog(LOG_NOTICE, "dev_1: %s", sd_dev_1.c_str());
        syslog(LOG_NOTICE, "dev_2: %s", sd_dev_2.c_str());
        syslog(LOG_NOTICE, "mnt_point: %s", sd_mnt_point.c_str());
    } else if (storage_mode == "hdd")
    {
        syslog(LOG_NOTICE, "hdd");
        syslog(LOG_NOTICE, "mseed_dir: %s", hdd_mseed_dir.c_str());
    }
    syslog(LOG_NOTICE, "gps");
    syslog(LOG_NOTICE, "usb_active: %d", gps_usb_active);
    syslog(LOG_NOTICE, "rt_server");
    syslog(LOG_NOTICE, "start_server: %d", rts_start_server);
    syslog(LOG_NOTICE, "port: %d", rts_port);
    syslog(LOG_NOTICE, "max_clients: %d", rts_max_clients);
    syslog(LOG_NOTICE, "-----------------------------------");

    // Create the temporary data directory.
    if (!boost::filesystem::exists(tmp_dir))
    {
        if (!boost::filesystem::create_directory(tmp_dir))
        {
            syslog(LOG_ERR, "[ERROR][root] Can't create the tmp_dir: %s. This is mandatory. Exiting.", tmp_dir.c_str());
            std::exit(EXIT_FAILURE);
        }
    }

    std::string mseed_dir;
    if (storage_mode == "sd")
        mseed_dir = sd_mnt_point;
    else if (storage_mode == "hdd")
        mseed_dir = hdd_mseed_dir;
    recorder = new Recorder(serial_number, n_channels, record_channels, sps, mseed_dir, tmp_dir, file_length);
    rt_server = new RealTimeServer(rts_port, rts_max_clients);
}


bool
Root::ready(void)
{
    bool is_ready = true;

    // Check the output directory structure. If it doesn't exist, it's created.
    if(!recorder->check_output_dir_structure())
    {
        syslog(LOG_ERR, "[ERROR][root] The output directory structure couldn't be created. Can't start the recorder.");
        is_ready = false;
    }

    if (storage_mode == "sd" && !is_sd_mounted())
    {
        is_ready = false;
    }

    return is_ready;
}

bool
Root::is_sd_dev_available(void)
{
    bool dev_available = false;
    struct stat sb;

    if (stat(sd_dev_label.c_str(), &sb) != -1)
    {
        switch (sb.st_mode & S_IFMT) {
            case S_IFBLK:
                syslog(LOG_NOTICE, "[root] SD card block device found at %s.", sd_dev_label.c_str());
                dev_available = true;
                break;
            default:
                syslog(LOG_ERR, "[ERROR][root] SD card device file %s is not a block device.", sd_dev_label.c_str());
                break;
        }        
    }
    else
    {
        syslog(LOG_WARNING, "[WARNING][root] SD card block device file %s not found.", sd_dev_label.c_str());
    }

    return dev_available;
}

bool
Root::is_sd_mounted(void)
{
    FILE *mtab = NULL;
    struct mntent *part = NULL;
    bool is_mounted = false;

    if ((mtab = setmntent("/etc/mtab", "r")) != NULL)
    {
        while ((part = getmntent(mtab)) != NULL)
        {
            if((part->mnt_fsname != NULL) && ((strcmp(part->mnt_fsname, sd_dev_1.c_str())) == 0 || (strcmp(part->mnt_fsname, sd_dev_2.c_str())) == 0) && (strcmp(part->mnt_dir, sd_mnt_point.c_str()) == 0) )
            {
                is_mounted = true;
            }
        }
    }
    endmntent(mtab);
    if (is_mounted)
    {
        syslog(LOG_NOTICE, "[root] The SD card is correctly mounted at %s.", sd_mnt_point.c_str());
    }
    else
    {
        syslog(LOG_WARNING, "[WARNING][root] The SD card is NOT mounted. Couldn't find an entry for %s or %s mounted at %s in /etc/mtab.", sd_dev_1.c_str(), sd_dev_2.c_str(), sd_mnt_point.c_str());
    }
    return is_mounted;
}


bool
Root::mount_sd(void)
{
    bool mount_success = false;
    int ret_val = -1;

    // TODO: Use the mount function instead of calling mount using the system
    // command. Using the mount function didn't work because of missing
    // permissions when running as a normal user. Also the mtab entry is not
    // written automatically. I think I would have to add an entry to mtab.
    //ret_val = mount(sd_dev_label.c_str(), sd_mnt_point.c_str(), "ext4", MS_NOSUID | MS_NOEXEC | MS_NODEV, NULL);
    
    std::string cmd = "mount " + sd_mnt_point;
    ret_val = system(cmd.c_str());

    if (ret_val == 0)
    {
        syslog(LOG_NOTICE, "[root] Successfully mounted the sd card on %s.", sd_mnt_point.c_str());
        mount_success = true;
    }
    else
    {
        syslog(LOG_ERR, "[ERROR][root] Troubles when mounting the SD card: %s.", strerror(errno)); 
    }
    return mount_success;
}


void
Root::run(void)
{
    bool port_open = false;
    bool config_confirmed = false;
    int max_try = 10;

    display->print("Starting...", "");

    // Start the temperature logging thread.
    syslog(LOG_NOTICE, "[root] Starting the temperature logging thread.");
    std::thread t_temp_log(log_temperature);
    t_temp_log.detach();

    // Check the output directory structure. If it doesn't exist, it's created.
    if(!recorder->check_output_dir_structure())
    {
        syslog(LOG_ERR, "[ERROR][root] The output directory structure couldn't be created. Can't start the recorder.");
    }
    else
    {
        port_open = open_port();

        if (port_open)
        {
            bool success;
            // Test the chrono measurement.
            /*
            syslog(LOG_DEBUG, "Testing the chrono measurement with usleep(1000).");
            auto start = std::chrono::high_resolution_clock::now();
            usleep(1000);
            auto end = std::chrono::high_resolution_clock::now();		
            auto diff = end - start;
            syslog(LOG_DEBUG, "Measured time: %d nanoseconds", (int)std::chrono::duration_cast<std::chrono::nanoseconds>(diff).count());
            */

            // Start the real time server if activated.
            if (rts_start_server)
            {
                syslog(LOG_NOTICE, "[root] Starting the realtime server thread.");
                std::thread t_rts([&] (RealTimeServer* rt_server) {rt_server->start_listen();}, rt_server);
                t_rts.detach();
            }

            configure_port();
            parser = new SerialParser(serial_port, recorder);
            std::thread t_read_and_parse([&] (SerialParser* parser) {parser->read_and_parse();}, parser);

            // Wait for handshaking.
            display->print("Handshaking...", "");
            syslog(LOG_NOTICE, "[root] Handshaking with Arduino stack....");
            max_try = 10;
            for (int k = 0; k < max_try; k++)
            {
                syslog(LOG_NOTICE, "[root] Waiting for handshake with Arduino stack...");
                parser->start_handshake();
                usleep(1000000);
                if (parser->is_synced())
                {
                    break;
                }
            }
            if (!parser->is_synced())
            {
                syslog(LOG_ERR, "[ERROR][root] ...tried handshake for %d times. Got no answer. Giving up.", max_try);
                std::exit(EXIT_FAILURE);
            }
            else
            {
                syslog(LOG_NOTICE, "[root] ...handshake was successful.");
            }


            display->print("Configuring...", "");
            syslog(LOG_NOTICE, "[root] Setting the Arduino stack to control mode.");
            success = parser->set_control_mode();
            if (success)
            {
                syslog(LOG_NOTICE, "[root] Confirmed control mode configuration by the Arduino stack.");
            }
            else
            {
                syslog(LOG_ERR, "[ERROR][root] No confirmation of the control mode configuration.");
            }

            // Configure the Arduino stack.
            // TODO: Restart the configuration if no confirmation was received.
            syslog(LOG_NOTICE, "[root] Configuring the Arduino stack....");
            max_try = 10;
            for (int k = 0; k < max_try; k++)
            {
                config_confirmed = true;
                syslog(LOG_NOTICE, "[root] Setting the sps to %d.", sps);
                success = parser->set_sps((uint16_t)sps);
                if (success)
                {
                    syslog(LOG_NOTICE, "[root] Confirmed sps configuration by the Arduino stack.");
                }
                else
                {
                    syslog(LOG_ERR, "[ERROR][root] No confirmation of the sps configuration. Start again.");
                    config_confirmed = false;
                    continue;
                }

                syslog(LOG_NOTICE, "[root] Activating the channels.");
                success = parser->set_channels(channel1_active, channel2_active, channel3_active, channel4_active, channel1_gain, channel2_gain, channel3_gain, channel4_gain);
                if (success)
                {
                    syslog(LOG_NOTICE, "[root] Confirmed channel configuration by the Arduino stack.");
                }
                else
                {
                    syslog(LOG_ERR, "[ERROR][root] No confirmation of the channel configuration. Start again.");
                    config_confirmed = false;
                    continue;
                }
                
                syslog(LOG_NOTICE, "[root] Sending the GPS configuration.");
                success = parser->set_gps(gps_usb_active);
                if (success)
                {
                    syslog(LOG_NOTICE, "[root] Confirmed gps configuration by the Arduino stack.");
                }
                else
                {
                    syslog(LOG_ERR, "[ERROR][root] No confirmation of the gps configuration. Start again.");
                    config_confirmed = false;
                    continue;
                }


                if (config_confirmed)
                {
                    break;
                }
            }

            if (!config_confirmed)
            {
                syslog(LOG_ERR, "[ERROR][root] Couldn't configure the Arduino Stack. Tried it %d times.", max_try);
                std::exit(EXIT_FAILURE);
            }

            syslog(LOG_NOTICE, "[root] Stack configured successfully.");

            syslog(LOG_NOTICE, "[root] Set to record mode.");
            success = parser->set_record_mode();
            if (success)
            {
                syslog(LOG_NOTICE, "[root] Confirmed record mode configuration by the Arduino stack.");
                display->print("Recording...", "");
            }
            else
            {
                syslog(LOG_ERR, "[ERROR][root] No confirmation of the record mode configuration.");
            }
            
            //std::thread t_lcd_status([&] (Root* this) {this->lcd_status();}, this);
            std::thread t_lcd_status(&Root::lcd_status, this);

            t_read_and_parse.join();

            // Close the serial port.
            close_port();
        }
    }

}

bool
Root::open_port(void)
{
    serial_port = open(dev_name.c_str(), O_RDWR | O_NOCTTY );
    if(serial_port == -1)
    {
        // Couldn't open the serial port.
        syslog(LOG_ERR, "[ERROR][root] Unable to open the Arduino stack serial port %s", dev_name.c_str()); 
        return false;
    }
    else
    {
        //fcntl(serial_port, F_SETFL, 0);
        syslog(LOG_NOTICE, "[root] Serial port %s to Arduino stack opened.", dev_name.c_str()); 
        return true;
    }
}

void
Root::close_port(void)
{
    close(serial_port);
}


void
Root::configure_port(void)
{
    struct termios port_options; // struct to hold the port settings

    // Fetch the current port settings
    tcgetattr(serial_port, &port_options);

    // Flush the port's buffers (in and out) before we start using it
    tcflush(serial_port, TCIOFLUSH);

    // Input flags - Turn off input processing
    //
    // convert break to null byte, no CR to NL translation,
    // no NL to CR translation, don't mark parity errors or breaks
    // no input parity check, don't strip high bit off,
    // no XON/XOFF software flow control
    //
    // port_options.c_iflag &= ~(IGNBRK | BRKINT | ICRNL |
    //                 INLCR | PARMRK | INPCK | ISTRIP | IXON);
    port_options.c_iflag = 0;

    // Output flags - Turn off output processing
    //
    // no CR to NL translation, no NL to CR-NL translation,
    // no NL to CR translation, no column 0 CR suppression,
    // no Ctrl-D suppression, no fill characters, no case mapping,
    // no local output processing
    //
    // port_options.c_oflag &= ~(OCRNL | ONLCR | ONLRET |
    //                     ONOCR | ONOEOT| OFILL | OLCUC | OPOST);
    port_options.c_oflag = 0;

    // Turn off line processing
    //
    // echo off, echo newline off, canonical mode on,
    // extended input processing off, signal chars off
    // port_options.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG);
    //
    // In non-blocking mode, the infinite loop in the serial parser causes a
    // large CPU load which significantly increases the power consumption of
    // the BBB.
    port_options.c_lflag = 0;
    port_options.c_lflag &= ~ICANON; // Set to non-canonical (blocking) mode.
    port_options.c_cc[VMIN] = 0;     // Minimum number of bytes read.
    port_options.c_cc[VTIME] = 10;   // timeout in deci-seconds

    // Set the input and output baud rates
    cfsetispeed(&port_options, B1000000);
    cfsetospeed(&port_options, B1000000);
    //cfsetispeed(&port_options, B500000);
    //cfsetospeed(&port_options, B500000);
    //cfsetispeed(&port_options, B1152000);
    //cfsetospeed(&port_options, B1152000);

    // c_cflag contains a few important things- CLOCAL and CREAD, to prevent
    //   this program from "owning" the port and to enable receipt of data.
    //   Also, it holds the settings for number of data bits, parity, stop bits,
    //   and hardware flow control. 
    port_options.c_cflag |= CLOCAL;
    port_options.c_cflag |= CREAD;
    // Set up the frame information.
    port_options.c_cflag &= ~CSIZE; // clear frame size info
    port_options.c_cflag |= CS8;    // 8 bit frames
    port_options.c_cflag &= ~PARENB;// no parity
    port_options.c_cflag &= ~CSTOPB;// one stop bit

    // Now that we've populated our options structure, let's push it back to the
    //   system.
    tcsetattr(serial_port, TCSANOW, &port_options);

    syslog(LOG_DEBUG, "Configured the serial port:");
    syslog(LOG_DEBUG, "port_options.c_cflag: %d - %s", port_options.c_cflag, std::bitset<16>(port_options.c_cflag).to_string().c_str());
    syslog(LOG_DEBUG, "port_options.c_iflag: %d - %s", port_options.c_iflag, std::bitset<16>(port_options.c_iflag).to_string().c_str());
    syslog(LOG_DEBUG, "port_options.c_oflag: %d - %s", port_options.c_oflag, std::bitset<16>(port_options.c_oflag).to_string().c_str());
    syslog(LOG_DEBUG, "port_options.c_lflag: %d - %s", port_options.c_lflag, std::bitset<16>(port_options.c_lflag).to_string().c_str());

    // Flush the buffer one more time.
    tcflush(serial_port, TCIOFLUSH);

    //fcntl(serial_port, F_SETFL, 0);
}


void
Root::clear_tmp_dir(void)
{
    // Check for forgotten files in the tmp directory.
    syslog(LOG_NOTICE, "[root] Cleaning the tmp directory.");

    std::vector<std::string> file_list;
    boost::filesystem::path search_dir(tmp_dir);
    boost::filesystem::directory_iterator end_itr;

    if (boost::filesystem::exists(search_dir) && boost::filesystem::is_directory(search_dir))
    {
        for (boost::filesystem::directory_iterator dir_itr(search_dir); dir_itr != end_itr; dir_itr++)
        {
            //syslog(LOG_NOTICE, "Moving file %s from tmp directory to mseed_dir.", dir_itr->path().filename().string().c_str());
            file_list.push_back(dir_itr->path().filename().string());
        }
    }

    if (file_list.size() == 0)
    {
        syslog(LOG_NOTICE, "[root] No data files found in the tmp directory: %s.", search_dir.c_str());
    }
    else
    {
        recorder->copy_files(file_list);
    }
}


/*
pga_gain_t
Root::translate_pga_gain(double gain)
{
    pga_gain_t ret = PGA_GAIN_UNKNOWN;
    if (gain == 0.125 || gain == 0.172 || gain == 0.25 || gain == 0.344 || \
        gain == 0.5 || gain == 0.688 || gain == 1 || gain == 1.375 || \
        gain == 2 || gain == 2.75 || gain == 4 || gain == 5.5 || gain == 8 || \
        gain == 11 || gain == 16 || gain == 22 || gain == 32 || gain == 44 || \
        gain == 64 || gain == 88 || gain == 128 || gain == 176)
    {
        ret = (pga_gain_t) gain;
    }
    return ret;
}
*/


pga_gain_t
Root::translate_pga_gain(double gain)
{
pga_gain_t ret = PGA_GAIN_UNKNOWN;
    if (gain == 0.125)
        ret = PGA_GAIN_0_125;
    else if (gain == 0.172)
        ret = PGA_GAIN_0_172;
    else if (gain == 0.25)
        ret = PGA_GAIN_0_25;
    else if (gain == 0.344)
        ret = PGA_GAIN_0_344;
    else if (gain == 0.5)
        ret = PGA_GAIN_0_5;
    else if (gain == 0.688)
        ret = PGA_GAIN_0_688;
    else if (gain == 1)
        ret = PGA_GAIN_1;
    else if (gain == 1.375)
        ret = PGA_GAIN_1_375;
    else if (gain == 2)
        ret = PGA_GAIN_2;
    else if (gain == 2.75)
        ret = PGA_GAIN_2_75;
    else if (gain == 4)
        ret = PGA_GAIN_4;
    else if (gain == 5.5)
        ret = PGA_GAIN_5_5;
    else if (gain == 8)
        ret = PGA_GAIN_8;
    else if (gain == 11)
        ret = PGA_GAIN_11;
    else if (gain == 16)
        ret = PGA_GAIN_16;
    else if (gain == 22)
        ret = PGA_GAIN_22;
    else if (gain == 32)
        ret = PGA_GAIN_32;
    else if (gain == 44)
        ret = PGA_GAIN_44;
    else if (gain == 64)
        ret = PGA_GAIN_64;
    else if (gain == 88)
        ret = PGA_GAIN_88;
    else if (gain == 128)
        ret = PGA_GAIN_128;
    else if (gain == 176)
        ret = PGA_GAIN_176;

    return ret;
}



adc_sps_t
Root::translate_adc_sps(uint16_t sps)
{
    adc_sps_t ret = ADC_SPS_UNKNOWN;
    if (sps == 100 || sps == 200 || sps == 400 || sps == 500 || \
        sps == 800 )
    {
        ret = (adc_sps_t) sps;
    }
    return ret;
}


void
Root::lcd_status(void)
{
    char c_time_string[27];
    std::string time_string;
    timestamp_t last_timestamp;

    while (true)
    {
        std::stringstream stream;
        std::string msg1;
        std::string msg2;
        
        last_timestamp = recorder->get_last_timestamp();
        ms_hptime2isotimestr(last_timestamp.time, c_time_string, true);
        time_string = std::string(c_time_string);
        display->print(time_string.substr(0, 10), time_string.substr(11));
        std::this_thread::sleep_for(std::chrono::seconds(5));


        stream << "GPS F:" << last_timestamp.gps_fix << " OK:" << last_timestamp.gps_fix_ok;
        msg1 = stream.str();
        stream.str("");
        stream << "SLTS:" << last_timestamp.sec_slts;
        msg2 = stream.str();
        display->print(msg1, msg2);
        std::this_thread::sleep_for(std::chrono::seconds(5));


        std::vector<int> tmp_file_size(recorder->tmp_file_size);
        stream.str("");
        stream << "Recording";
        msg1 = stream.str();
        
        stream.str("");
        stream << tmp_file_size.size() << " files";
        msg2 = stream.str();
        display->print(msg1, msg2);
        std::this_thread::sleep_for(std::chrono::seconds(2));
      

        for (unsigned int k = 0; k < tmp_file_size.size(); k++)
        {
            stream.str("");
            stream << "Size file " << k + 1;
            msg1 = stream.str();

            stream.str("");
            stream << recorder->tmp_file_size[k] << " kB";
            msg2 = stream.str();
            display->print(msg1, msg2);
            std::this_thread::sleep_for(std::chrono::seconds(2));
        }
        
        /* TODO: Getting the file size is blocking the program somehow.
        std::string tmp_dir = recorder->get_tmp_dir();
        std::vector<std::string> rec_files = get_file_list(tmp_dir);
        std::sort(rec_files.begin(), rec_files.end());
        stream.str("");
        stream << rec_files.size() << " files";
        msg1 = stream.str();

        stream.str("");
        for (std::vector<std::string>::iterator it = rec_files.begin(); it != rec_files.end(); it++)
        {
            std::string file_path = tmp_dir + "/" + *it;
            stream << boost::filesystem::file_size(file_path) / 1024 << "; ";
        }
        msg2 = stream.str();
        stream.str("");
        */
    }
}

std::vector<std::string>
Root::get_file_list(const std::string path)
{
    std::vector<std::string> m_file_list;
    if (!path.empty())
    {
        boost::filesystem::path apk_path(path);

        for (auto& entry : boost::make_iterator_range(boost::filesystem::directory_iterator(apk_path), {}))
        {
            m_file_list.push_back(entry.path().string());
        }
    }
    return m_file_list;
}
