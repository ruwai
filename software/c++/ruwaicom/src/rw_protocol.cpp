
#include <rw_protocol.h>

void update_fletcher16_checksum(uint8_t* data, uint8_t length, uint8_t &ck0, uint8_t &ck1)
{
  for (uint8_t k = 0; k < length; k++) 
  {
    ck0 = ck0 + *data;
    ck1 = ck1 + ck0;
    data++;
  }
}

