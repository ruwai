/*--------------------------------------------------------------------------*/
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai_parser in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// ruwai is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*--------------------------------------------------------------------------*/

#include "lcddisplay.h"
#include <unistd.h>
#include <iostream>

LCDDisplay::LCDDisplay(int rs_gpio, int e_gpio, int db0_gpio, int db1_gpio, int db2_gpio, int db3_gpio, int db4_gpio, int db5_gpio, int db6_gpio, int db7_gpio)
{
    this->mode = 8;
    this->rs_gpio = new GPIO(rs_gpio);
    this->e_gpio = new GPIO(e_gpio);
    this->db0_gpio = new GPIO(db0_gpio);
    this->db1_gpio = new GPIO(db1_gpio);
    this->db2_gpio = new GPIO(db2_gpio);
    this->db3_gpio = new GPIO(db3_gpio);
    this->db4_gpio = new GPIO(db4_gpio);
    this->db5_gpio = new GPIO(db5_gpio);
    this->db6_gpio = new GPIO(db6_gpio);
    this->db7_gpio = new GPIO(db7_gpio);

    // Initialize the gpios.
    this->rs_gpio->set_direction(OUTPUT);
    this->e_gpio->set_direction(OUTPUT);
    this->db0_gpio->set_direction(OUTPUT);
    this->db1_gpio->set_direction(OUTPUT);
    this->db2_gpio->set_direction(OUTPUT);
    this->db3_gpio->set_direction(OUTPUT);
    this->db4_gpio->set_direction(OUTPUT);
    this->db5_gpio->set_direction(OUTPUT);
    this->db6_gpio->set_direction(OUTPUT);
    this->db7_gpio->set_direction(OUTPUT);
}


LCDDisplay::LCDDisplay(int rs_gpio, int e_gpio, int db4_gpio, int db5_gpio, int db6_gpio, int db7_gpio)
{
    this->mode = 4;
    this->rs_gpio = new GPIO(rs_gpio);
    this->e_gpio = new GPIO(e_gpio);
    this->db4_gpio = new GPIO(db4_gpio);
    this->db5_gpio = new GPIO(db5_gpio);
    this->db6_gpio = new GPIO(db6_gpio);
    this->db7_gpio = new GPIO(db7_gpio);

    // Initialize the gpios.
    this->rs_gpio->set_direction(OUTPUT);
    this->e_gpio->set_direction(OUTPUT);
    this->db4_gpio->set_direction(OUTPUT);
    this->db5_gpio->set_direction(OUTPUT);
    this->db6_gpio->set_direction(OUTPUT);
    this->db7_gpio->set_direction(OUTPUT);
}


void
LCDDisplay::init(void)
{
    switch(mode)
    {
        case 4:
	    init_4bit();
	    break;
	case 8:
	    init_8bit();
	    break;
    }

}

void
LCDDisplay::init_4bit(void)
{
    usleep(100000);

    write_4bit_high_only(0x30, true);
    usleep(15000);

    write_4bit_high_only(0x30, true);
    usleep(2000);

    write_4bit_high_only(0x30, true);
    //write_4bit(LCD_CMD_FUNCTION_SET | LCD_FUNCTION_TWO_LINES, true);
    usleep(2000);

    write_4bit_high_only(0x20, true);
    //write_4bit(LCD_CMD_FUNCTION_SET | LCD_FUNCTION_TWO_LINES, true);
    usleep(2000);

    write_4bit(LCD_CMD_FUNCTION_SET | LCD_FUNCTION_TWO_LINES, true);
    usleep(LCD_SHORT_DELAY);

    // display on
    //write_command(0b00001110);
    write_command(LCD_CMD_DISPLAY | LCD_DISPLAY_DISPLAY_ON |LCD_DISPLAY_CURSOR_ON);
    usleep(LCD_SHORT_DELAY);
}


void
LCDDisplay::init_8bit(void)
{
    usleep(100000);

    write_8bit(LCD_CMD_FUNCTION_SET | LCD_FUNCTION_8BIT_MODE | LCD_FUNCTION_TWO_LINES, true);
    usleep(15000);

    write_8bit(LCD_CMD_FUNCTION_SET | LCD_FUNCTION_8BIT_MODE | LCD_FUNCTION_TWO_LINES, true);
    usleep(2000);

    switch (mode)
    {
        case 4:
	    // Select 4 bit mode.
	    write_8bit(LCD_CMD_FUNCTION_SET | LCD_FUNCTION_TWO_LINES, true);
	    break;
        case 8:
            write_8bit(LCD_CMD_FUNCTION_SET | LCD_FUNCTION_8BIT_MODE | LCD_FUNCTION_TWO_LINES, true);
	    break;
    }
    usleep(LCD_SHORT_DELAY);

    // display on
    //write_command(0b00001110);
    write_command(LCD_CMD_DISPLAY | LCD_DISPLAY_DISPLAY_ON |LCD_DISPLAY_CURSOR_ON);
    usleep(LCD_SHORT_DELAY);
}

void
LCDDisplay::write_8bit(std::uint8_t data, bool command)
{
    if(command)
        rs_gpio->set_value(LOW);
    else
        rs_gpio->set_value(HIGH);

    // Write the high nibble.
    db7_gpio->set_value((gpio_value_t)((data & 0x80) >> 7));
    db6_gpio->set_value((gpio_value_t)((data & 0x40) >> 6));
    db5_gpio->set_value((gpio_value_t)((data & 0x20) >> 5));
    db4_gpio->set_value((gpio_value_t)((data & 0x10) >> 4));
    db3_gpio->set_value((gpio_value_t)((data & 0x08) >> 3));
    db2_gpio->set_value((gpio_value_t)((data & 0x04) >> 2));
    db1_gpio->set_value((gpio_value_t)((data & 0x02) >> 1));
    db0_gpio->set_value((gpio_value_t)(data & 0x01));
    toggle_e();
}


void
LCDDisplay::write_4bit(std::uint8_t data, bool command)
{
    if(command)
        rs_gpio->set_value(LOW);
    else
        rs_gpio->set_value(HIGH);

    // Write the high nibble.
    db7_gpio->set_value((gpio_value_t)((data & 0x80) >> 7));
    db6_gpio->set_value((gpio_value_t)((data & 0x40) >> 6));
    db5_gpio->set_value((gpio_value_t)((data & 0x20) >> 5));
    db4_gpio->set_value((gpio_value_t)((data & 0x10) >> 4));
    toggle_e();

    // Write the low nibble.
    db7_gpio->set_value((gpio_value_t)((data & 0x08) >> 3));
    db6_gpio->set_value((gpio_value_t)((data & 0x04) >> 2));
    db5_gpio->set_value((gpio_value_t)((data & 0x02) >> 1));
    db4_gpio->set_value((gpio_value_t)(data & 0x01));
    toggle_e();
}


void
LCDDisplay::write_4bit_high_only(std::uint8_t data, bool command)
{
    if(command)
        rs_gpio->set_value(LOW);
    else
        rs_gpio->set_value(HIGH);

    // Write the high nibble.
    db7_gpio->set_value((gpio_value_t)((data & 0x80) >> 7));
    db6_gpio->set_value((gpio_value_t)((data & 0x40) >> 6));
    db5_gpio->set_value((gpio_value_t)((data & 0x20) >> 5));
    db4_gpio->set_value((gpio_value_t)((data & 0x10) >> 4));
    toggle_e();
}

void
LCDDisplay::write_command(std::uint8_t data)
{
    switch(mode)
    {
        case 4:
	    write_4bit(data, true);
	    break;
	case 8:
	    write_8bit(data, true);
	    break;
    }
}

void
LCDDisplay::write_data(std::uint8_t data)
{
    switch(mode)
    {
        case 4:
	    write_4bit(data, false);
	    break;
	case 8:
	    write_8bit(data, false);
	    break;
    }
}


void
LCDDisplay::toggle_e(void)
{
    //e_gpio->set_value(LOW);
    //usleep(10);
    e_gpio->set_value(HIGH);
    usleep(1000);
    e_gpio->set_value(LOW);
}

void
LCDDisplay::clear(void)
{
    write_command(0x01);
    usleep(LCD_LONG_DELAY);
}

void
LCDDisplay::home(void)
{
    write_command(0x02);
    usleep(LCD_LONG_DELAY);
}


void
LCDDisplay::print(std::string msg_line1, std::string msg_line2)
{
    home();
    clear();

    for (int k = 0; k < msg_line1.length(); k++)
    {
        write_data(msg_line1[k]);
    }

    // Go to the beginning of line 2.
    write_command(LCD_CMD_SET_DDRAM_ADDR | 0x40);
    
    for (int k = 0; k < msg_line2.length(); k++)
    {
        write_data(msg_line2[k]);
    }
}
