/*--------------------------------------------------------------------------*/
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai_parser in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// ruwai is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*--------------------------------------------------------------------------*/

#include "gpio.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <unistd.h>

GPIO::GPIO(int gpio_number)
{
    number = gpio_number;

    std::ostringstream s;
    s << "gpio" << number;
    name = std::string(s.str());
    path = GPIO_PATH + name + "/";
    export_gpio();
    usleep(250000);
}


GPIO::~GPIO()
{
    unexport_gpio();
}


int
GPIO::write(std::string path, std::string filename, std::string value)
{
    std::ofstream fs;
    //std::cout << "Writing value " << value <<  " to file " << (path + filename).c_str() << std::endl;
    fs.open((path + filename).c_str());
    if (!fs.is_open()){
        perror("GPIO: write failed to open file ");
        return -1;
    }
    fs << value;
    fs.close();
    return 0;
}


int
GPIO::write(std::string path, std::string filename, int value)
{
    std::stringstream s;
    s << value;
    return write(path, filename, s.str());
}


int
GPIO::export_gpio(void)
{
    //std::cout << "number: " << number << std::endl;
    return write(GPIO_PATH, "export", number);
}


int
GPIO::unexport_gpio(void)
{
    return write(GPIO_PATH, "unexport", number);
}


int GPIO::set_direction(gpio_direction_t dir)
{
    switch(dir)
    {
        case INPUT:
            return write(path, "direction", "in");
            break;
        case OUTPUT:
            return write(path, "direction", "out");
            break;
    }
    return -1;
}


int GPIO::set_value(gpio_value_t val)
{
    switch (val)
    {
        case HIGH:
            return write(path, "value", "1");
            break;
        case LOW:
            return write(path, "value", "0");
            break;
    }
    return -1;
}



