/*--------------------------------------------------------------------------*/
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai_parser in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// ruwai is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*--------------------------------------------------------------------------*/

#include "rt_server.h"

RealTimeServer::RealTimeServer(unsigned int port, unsigned int max_clients)
{
    this->port = port;
    this->max_clients = max_clients;
}

// get sockaddr, IPv4 or IPv6:
void* 
RealTimeServer::get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET)
    {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }
    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}


int
RealTimeServer::start_listen(void)
{
    struct addrinfo hints, *serv_info, *p;
    struct sockaddr_storage their_addr;
    socklen_t addr_size;
    int sock_fd, new_fd;
    int rv;
    char ipstr[INET6_ADDRSTRLEN];

    serv_info = NULL;
    p = NULL;

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    //hints.ai_flags = AI_PASSIVE;

    if ((rv = getaddrinfo("127.0.0.1", std::to_string(port).c_str(), &hints, &serv_info)) != 0)
    {
        std::cerr << "getaddrinfo: " << gai_strerror(rv) << std::endl;
        return EXIT_FAILURE;
    }

    // Print the available address information.
    std::cout << "Opening socket for available address information:" << std::endl;
    for (p = serv_info; p != NULL; p = p->ai_next)
    {
        void *addr;
        std::string ipver;
        int yes = 1;

        if (p->ai_family == AF_INET)
        {
            struct sockaddr_in *ipv4 = (struct sockaddr_in *)p->ai_addr;
            addr = &(ipv4->sin_addr);
            ipver = "IPv4";
        }
        else
        {
            struct sockaddr_in6 *ipv6 = (struct sockaddr_in6 *)p->ai_addr;
            addr = &(ipv6->sin6_addr);
            ipver = "IPv6";
        }

        inet_ntop(p->ai_family, addr, ipstr, sizeof ipstr);
        std::cout << ipver << ": " << ipstr << std::endl;

        if ((sock_fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1)
        {
            std::cerr << strerror(errno) << std::endl;
            continue;
        }

        if (setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
        {
            std::cerr << strerror(errno) << std::endl;
            exit(EXIT_FAILURE);
        }

        if (bind(sock_fd, p->ai_addr, p->ai_addrlen) == -1)
        {
            close(sock_fd);
            std::cerr << strerror(errno) << std::endl;
            continue;
        }

        std::cout << "Success binding the socket." << std::endl;
        break;
    }

    freeaddrinfo(serv_info);

    if (p == NULL)
    {
        std::cerr << "Failed to bind to socket." << std::endl;
        return EXIT_FAILURE;
    }

    if (listen(sock_fd, RTS_LISTEN_BACKLOG) == -1)
    {
        std::cerr << strerror(errno) << std::endl;
        return EXIT_FAILURE;
    }

    std::cout << "Waiting for connections...." << std::endl;

    while(1)
    {
        addr_size = sizeof their_addr;
        new_fd = accept(sock_fd, (struct sockaddr *)&their_addr, &addr_size);

        if (new_fd == -1)
        {
            std::cerr << strerror(errno) << std::endl;
            continue;
        }

        inet_ntop(their_addr.ss_family,
                  get_in_addr((struct sockaddr *)&their_addr),
                  ipstr, sizeof ipstr);
        std::cout << "Got connection from " << ipstr << std::endl;

        // Start the client serve thread.
        std::thread t_client([&] (RealTimeServer* rt_server) {this->serve_client(new_fd);}, this);
        t_client.detach();
    }
}



void
RealTimeServer::serve_client(int client_fd)
{
    if (send(client_fd, "Ruwai Real Time Server - Hello.", 31, 0) == -1)
    {
        std::cerr << strerror(errno) << std::endl;
    }
    std::cout << "Sent message to client." << std::endl;
    close(client_fd);
}
