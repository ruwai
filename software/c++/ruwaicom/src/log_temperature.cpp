#include <log_temperature.h>
/*--------------------------------------------------------------------------*/
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// ruwai is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*--------------------------------------------------------------------------*/

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <string.h>
#include <dirent.h>
#include <syslog.h>
#include <unistd.h>

void
log_temperature(void)
{
    DIR *dir;
    struct dirent *dirent;
    std::string dev;

    std::string w1_path = "/sys/bus/w1/devices";

    // Search for the thermometer device in the OneWire folder.
    dir = opendir(w1_path.c_str());
    if (dir != NULL)
    {
        while ((dirent = readdir(dir)))
        {
            // 1-wire devices are links beginning with 28-
            if (dirent->d_type == DT_LNK && strstr(dirent->d_name, "10-") != NULL) 
            { 
                //strcpy(dev, dirent->d_name);
                dev = dirent->d_name;
                syslog(LOG_NOTICE, "[log_temperature]Found thermometer device: %s.", dev.c_str());
            }
        }
        (void) closedir(dir);
    }
    else
    {
        syslog(LOG_ERR, "[ERROR][log_temperature] Couldn't open the w1 devices directory: %s.", w1_path.c_str());
	return;
    }

    std::stringstream name_stream;
    name_stream << w1_path << "/" << dev << "/w1_slave";
    std::string dev_path(name_stream.str());

    std::string line;
    std::ifstream ds_file;

    while (true)
    {
        ds_file.open(dev_path);
        if (ds_file.is_open())
	{
	    // Read the two lines of the file.
	    // The second line contains the temperature value.
	    std::getline(ds_file, line);
	    std::getline(ds_file, line);
	    ds_file.close();

	    // Search for the temperature value and log it.
	    int found = line.find("t=");
	    if (found != std::string::npos)
	    {
	        std::string temp_str = line.substr(found + 2, 5);
		float temp_c = std::stof(temp_str);
		temp_c = temp_c / 1000;
		syslog(LOG_NOTICE, "[soh][temperature_bbb] %f", temp_c);
	    }
	}
	else
	{
	    syslog(LOG_ERR, "[ERROR][log_temperature] Couldn't open the thermometer file %s.", dev_path.c_str());
	}
	sleep(300);
    }

}
