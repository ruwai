/*--------------------------------------------------------------------------*/
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai_parser in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// ruwai is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*--------------------------------------------------------------------------*/
#include <signal.h>
#include "ruwaicom.h"
#include "root.h"


void
handle_sig_term(int signum)
{
    if ((signum == SIGTERM) || (signum == SIGINT))
    {
        LCDDisplay display = LCDDisplay(66, 67, 45, 23, 47, 27);
        display.init();
        display.clear();
        display.home();
        display.print("STOPPED", "RUWAICOM");
        syslog(LOG_WARNING, "Stopped ruwaicom (SIGTERM, SIGINT).");
    }

    std::exit(signum);
}


int main(int argc, char* argv[])
{
    signal(SIGTERM, handle_sig_term); 
    signal(SIGINT, handle_sig_term); 
    std::string config_file = "";
    std::string debug_level = "";
    if (argc != 2)
    {
        std::cout << "usage: " << argv[0] << " CONFIG_FILENAME" << std::endl;
        return(EXIT_SUCCESS);
    }

    if (strcmp(argv[1], "--version") == 0)
    {
        std::cout << "version " << RUWAI_RECORD_VERSION << std::endl;
        std::cout << "git revision " << GIT_VERSION << std::endl;
        return(EXIT_SUCCESS);
    }

    // TODO: Add an exception handling if the config file can't be read.
    config_file = (std::string)argv[1];

    boost::property_tree::ptree pt;
    boost::property_tree::ini_parser::read_ini(config_file, pt);
    debug_level = pt.get<std::string>("log.level");

    int log_level = LOG_NOTICE;
    if(debug_level == "emerg")
    {
        log_level = LOG_EMERG; 
    }
    else if(debug_level == "alert")
    {
        log_level = LOG_ALERT;
    }
    else if(debug_level == "critical")
    {
        log_level = LOG_CRIT;
    }
    else if(debug_level == "error")
    {
        log_level = LOG_ERR;
    }
    else if(debug_level == "warning")
    {
        log_level = LOG_WARNING;
    }
    else if(debug_level == "notice")
    {
        log_level = LOG_NOTICE;
    }
    else if(debug_level == "info")
    {
        log_level = LOG_INFO;
    }
    else if(debug_level == "debug")
    {
        log_level = LOG_DEBUG;
    }

    setlogmask(LOG_UPTO(log_level));
    openlog("ruwaicom", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL1);
    syslog(LOG_NOTICE, "Starting ruwaicom version %s.", RUWAI_RECORD_VERSION);
    syslog(LOG_NOTICE, "Git revision: %s.", GIT_VERSION);

    Root root = Root(config_file);
    syslog(LOG_NOTICE, "Starting the root...");

    if (root.storage_mode == "sd")
    {
        syslog(LOG_NOTICE, "Initial check for the SD card.");
        if (root.is_sd_dev_available())
        {
            if (!root.is_sd_mounted())
            {
                syslog(LOG_NOTICE, "Mounting the SD card....");
                // Try to mount the SD card.
                if(!root.mount_sd())
                {
                    syslog(LOG_ERR, "Couldn't mount the SD card. No place to write the data to.");
                }
            }
        }
    }

    if (root.ready())
    {
        root.clear_tmp_dir();
        root.run();
    }
    else
    {
        syslog(LOG_ERR, "The root is not ready. Can't start it. Good bye.");
        root.display->print("Had a problem.", "Check log.");
    }

    syslog(LOG_NOTICE, "Exiting ruwaicom.");
    closelog();
    return(EXIT_SUCCESS);
}
