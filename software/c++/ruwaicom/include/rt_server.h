/*--------------------------------------------------------------------------*/
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai_parser in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// ruwai is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*--------------------------------------------------------------------------*/

#ifndef RT_SERVER_H
#define RT_SERVER_H

#include <iostream>

#include <cstring>
#include <cstdlib>
#include <thread>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <errno.h>

#define RTS_LISTEN_BACKLOG 5

class RealTimeServer{
    public:
        RealTimeServer(unsigned int port, unsigned int max_clients);
        int start_listen(void);
        void* get_in_addr(struct sockaddr *sa);

    private:
        unsigned int port;
        unsigned int max_clients;

        void serve_client(int client_fd);

};

#endif /* RT_SERVER_H */
