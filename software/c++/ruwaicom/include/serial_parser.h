/*--------------------------------------------------------------------------*/
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai_parser in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// ruwai is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*--------------------------------------------------------------------------*/

#ifndef SERIAL_PARSER_H
#define SERIAL_PARSER_H

#include <sstream>
#include <iostream>
#include <cstdint>
#include <vector>
#include <algorithm>
#include <chrono>

#include <unistd.h>
#include <termios.h>

#include "rw_protocol.h"
#include "recorder.h"

// The states of the serial parsing.
typedef enum rw_read_state_e {
    RW_WAITING_FOR_SYNC1 = 0,
    RW_SYNC1_FOUND,
    RW_SYNC2_FOUND,
    RW_CLASS_READ,
    RW_MSGID_READ,
    RW_LENGTH_BYTE1_READ,
    RW_LENGTH_BYTE2_READ,
    RW_PAYLOAD_READ,
    RW_CKA_OK
} rw_read_state_t;

// The message receive buffer.
/*
typedef union PACKED rw_msg_buffer_u {
    rw_smp_24bit_t rw_smp_24bit;
    rw_smp_24bit_tsp_t rw_smp_24bit_tsp;
    rw_ack_sync_t rw_ack_sync;
    rw_ack_cfg_t rw_ack_cfg;
    std::uint8_t bytes[];
} rw_msg_buffer_t;
*/

//! The supported gain types.
typedef enum pga_gain_e {
    PGA_GAIN_0_125 = 0,
    PGA_GAIN_0_25,
    PGA_GAIN_0_5,
    PGA_GAIN_1,
    PGA_GAIN_2,
    PGA_GAIN_4,
    PGA_GAIN_8,
    PGA_GAIN_16,
    PGA_GAIN_32,
    PGA_GAIN_64,
    PGA_GAIN_128,
    PGA_GAIN_0_172 = 16,
    PGA_GAIN_0_344,
    PGA_GAIN_0_688,
    PGA_GAIN_1_375,
    PGA_GAIN_2_75,
    PGA_GAIN_5_5,
    PGA_GAIN_11,
    PGA_GAIN_22,
    PGA_GAIN_44,
    PGA_GAIN_88,
    PGA_GAIN_176,
    PGA_GAIN_UNKNOWN = 99
} pga_gain_t;

//! The supported sampling rates.
typedef enum adc_sps_e {
    ADC_SPS_UNKNOWN = 0,
    ADC_SPS_100 = 100,
    ADC_SPS_200 = 200,
    ADC_SPS_400 = 400,
    ADC_SPS_500 = 500,
    ADC_SPS_800 = 800,
    ADC_SPS_1000 = 1000,
    ADC_SPS_1600 = 1600,
    ADC_SPS_2000 = 2000
} adc_sps_t;

void serial_watchdog(int signum);

class SerialParser {
    public:
        SerialParser(int serial_port, Recorder* recorder);
        void read_and_parse(void);
        void sync(std::vector<uint8_t> payload);
        bool is_synced(void);
        void start_handshake(void);
        void send_ack_sync(uint8_t sync, uint8_t ack, uint8_t seq_num, uint8_t ack_num);
        bool set_sps(uint16_t sps);
        bool set_channels(bool c1_active, bool c2_active, bool c3_active, bool c4_active, pga_gain_t c1_gain, pga_gain_t c2_gain, pga_gain_t c3_gain, pga_gain_t c4_gain);
        bool set_gps(bool usb_active);
        bool set_control_mode(void);
        bool set_record_mode(void);
        bool request_version(void);


    private:
        // The serial port of the arduino stack.
        int serial_port;

        // The arduino handshake state.
        bool synced;
        uint8_t seq_num;

        // The last acknowledged configuration message id. (-1 if no message
        // was acknowledged.
        uint8_t last_ack_msg_class;
        uint8_t last_ack_msg_id;

        // The number of bytes to store in the serial buffer before parsing the
        // buffer.
        uint16_t parser_size;
        bool control_mode;
        bool record_mode;

        std::vector<std::uint8_t> serial_buffer;

        Recorder* recorder;

        bool set_mode(uint8_t mode);

        void send_ruwai_message(uint8_t msg_class, uint8_t msg_id, void* msg, uint8_t length);
        void parse_serial_buffer(void);
        bool parse_smp_24bit(std::vector<uint8_t> payload);
        bool parse_smp_24bit_tsp(std::vector<uint8_t> payload);
        bool wait_for_cfg_ack(uint8_t msg_class, uint8_t msg_id);

};

#endif /* SERIAL_PARSER_H */
