/*--------------------------------------------------------------------------*/
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai_parser in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// ruwai is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*--------------------------------------------------------------------------*/

#ifndef RECORDER_H
#define RECORDER_H

#include <iostream>
#include <vector>
#include <map>
#include <mutex>
#include <thread>
#include <chrono>

#include <cmath>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <errno.h>
#include <syslog.h>

#define BOOST_NO_CXX11_SCOPED_ENUMS     // Bug fix for an linking error caused by boost::filesystem::copy_file
#define BOOST_NO_SCOPED_ENUMS           // This define has to be used for boost versions below 1.51. BBB has 1.49 
#include <boost/format.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#undef BOOST_NO_CXX11_SCOPED_ENUMS     
#undef BOOST_NO_SCOPED_ENUMS          

//#include <sys/types.h>
//#include <sys/stat.h>
//#include <unistd.h>
//#include <stdio.h>

#include <libmseed/libmseed.h>

#include "rw_protocol.h"

//! The supported GPSfix types.
typedef enum gpsfix_e {
    FIX_NO = 0,
    FIX_DEAD_RECKONING_ONLY = 1,
    FIX_2D = 2,
    FIX_3D = 3,
    FIX_GPS_AND_DEAD_RECKONING = 4,
    FIX_TIME_ONLY = 5
} gpsfix_t;


typedef struct timestamp_s {
    bool utc_available;
    bool utc_timebase;
    bool gps_fix_ok;
    gpsfix_t gps_fix;
    uint32_t sec_slts;
    hptime_t time;
} timestamp_t;

void record_handler (char *record, int reclen, void *ptr);

class Recorder {
    public:
        Recorder(std::string serial_number, unsigned int total_channels, std::vector<unsigned int> record_channels, unsigned int sps, std::string sd_mnt, std::string tmp_dir, unsigned int file_length);
        void add_sample(rw_smp_24bit_t paket);
        void add_timestamp_sample(rw_smp_24bit_tsp_t paket);
        void handle_timestamp_sample(rw_smp_24bit_tsp_t paket);
        void pack_samplebuffer(void);
        bool check_output_dir_structure(void);
        void copy_files(std::vector<std::string> files);
        timestamp_t get_last_timestamp(void);
        std::string get_tmp_dir(void);

        std::vector<int> tmp_file_size;

    private:
        std::string serial_number;                                      // The serial number of the ruwai Arduino stack.
        unsigned int sps;                                               // The sampling rate of the data.
        unsigned int n_channels;                                        // The number of channels to record.
        std::vector<unsigned int> record_channels;                               // The channel number to record to file.
        unsigned int write_limit;                                       // The size of the sample_buffer when to copy the samples to the ms_trace.

        std::mutex mut_sample_buffer;
        std::mutex mut_timestamps;
        std::mutex mut_last_timestamp;



        std::string sd_mnt_point;                                       // The name of the SD card mount point.
        std::string data_dir;                                           // The name of the folder where to store the miniseed files.
        std::string tmp_dir;                                            // The name of the folder where to store the data files which are currently used for recording.

        std::vector <std::vector <std::int32_t> > sample_buffer;        // The buffers holding the samples.
        std::vector <std::vector <int> > timestamp_pos;                 // The timestamp positions in the sample vector.
        std::vector <timestamp_t> timestamps;                           // The timestamp values.
        std::vector <MSTrace *> traces;                                 // The miniseed traces. One for each channel.

        //MSTraceGroup *mstg;

        //hptime_t span_start;            
        //hptime_t span_end;
        unsigned int file_length;       // The lenght of the data files in seconds.
        double tow_sub_ms_scale;        // The scaling factor of the tow_sub_ms value of the GPS timing paket. 

        // The state of the recorder.
        bool state_first_timestamp_received;
        bool state_gps_fix_init;
        bool state_system_time_set;
        //gpsfix_t state_gps_fix;
        //bool state_gps_fix_ok;
        int sec_slts_cnt;
        timestamp_t last_timestamp;


        void init_msr(MSRecord* msr, unsigned int channel_number);
        void set_msr_timeflags(MSRecord* msr, std::vector <timestamp_t>& timestamps);
        bool msr_fits_to_end(MSTrace* mst, MSRecord* msr);
        std::string get_filename(BTime start_time, char* channel);
        std::string write_trace_to_file(MSTrace* mst, bool flush, MSRecord* msr_template);
};

#endif /* RECORDER_H */
