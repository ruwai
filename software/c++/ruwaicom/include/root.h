/*--------------------------------------------------------------------------*/
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai_parser in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// ruwai is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*--------------------------------------------------------------------------*/

#ifndef ROOT_H
#define ROOT_H
#include <iostream>
#include <bitset>
#include <string>
#include <chrono>
#include <thread>

#include <cerrno>

#include <unistd.h>
#include <termios.h>
#include <fcntl.h>
#include <syslog.h>
#include <mntent.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/mount.h>

#define BOOST_NO_CXX11_SCOPED_ENUMS     // Bug fix for an linking error caused by boost::filesystem::copy_file
#define BOOST_NO_SCOPED_ENUMS           // This define has to be used for boost versions below 1.51. BBB has 1.49 
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/range/iterator_range.hpp>
#undef BOOST_NO_CXX11_SCOPED_ENUMS     
#undef BOOST_NO_SCOPED_ENUMS          

#include "serial_parser.h"
#include "recorder.h"
#include "rt_server.h"
#include "lcddisplay.h"

class Root {
    public:
        Root(std::string config_file);
        void run(void);
        bool open_port(void);
        void configure_port(void);
        void close_port(void);
        bool ready(void);
        bool is_sd_dev_available(void);
        bool is_sd_mounted(void);
        bool mount_sd(void);
        void clear_tmp_dir(void);

        int serial_port; 
        std::string storage_mode;
        LCDDisplay* display;

    private:
        std::string dev_name;
        std::string serial_number;
        std::string sd_dev_label;
        std::string sd_dev_1;
        std::string sd_dev_2;
        std::string sd_mnt_point;
        std::string hdd_mseed_dir;
        std::string tmp_dir;
        std::string ruwai_serial_file;
        std::vector<unsigned int> record_channels;
        bool rts_start_server;
        unsigned int rts_port;
        unsigned int rts_max_clients;

        bool arduino_stack_valid;

        unsigned int n_channels;
        adc_sps_t sps;
        bool channel1_active;
        bool channel2_active;
        bool channel3_active;
        bool channel4_active;
        pga_gain_t channel1_gain;
        pga_gain_t channel2_gain;
        pga_gain_t channel3_gain;
        pga_gain_t channel4_gain;
        bool gps_usb_active;


        unsigned int file_length;

        SerialParser* parser;
        Recorder* recorder;
        RealTimeServer* rt_server;

        bool check_arduino_stack(void);
        pga_gain_t translate_pga_gain(double gain);
        adc_sps_t translate_adc_sps(uint16_t sps);
        void lcd_status(void);
        std::vector<std::string> get_file_list(const std::string path);
};

template<typename T>
std::vector<T> to_array(const std::string& s)
{
  std::vector<T> result;
  std::stringstream ss(s);
  std::string item;
  while(std::getline(ss, item, ',')) result.push_back(boost::lexical_cast<T>(item));
  return result;
}

#endif /* ROOT_H */

