/*--------------------------------------------------------------------------*/
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai_parser in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// ruwai is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*--------------------------------------------------------------------------*/

#ifndef LCDDISPLAY_H
#define LCDDISPLAY_H

#include "libgpio/gpio.h"
#include <string>


#define LCD_DELAY_ENABLE_PULSE 2
#define LCD_LONG_DELAY 1600
#define LCD_SHORT_DELAY 50


#define LCD_CMD_CLEAR_DISPLAY 0x01
#define LCD_CMD_RETURN_HOME 0x02
#define LCD_CMD_ENTRY_MODE_SET 0x04
#define LCD_CMD_DISPLAY 0x08
#define LCD_CMD_SHIFT 0x10
#define LCD_CMD_FUNCTION_SET 0x20
#define LCD_CMD_SET_CGRAM_ADDR 0x40
#define LCD_CMD_SET_DDRAM_ADDR 0x80

#define LCD_ENTRY_DISPLAY_SHIFT 0x01 
#define LCD_ENTRY_CURSOR_INCREMENT 0x02
#define LCD_DISPLAY_CURSOR_BLINKS 0x01
#define LCD_DISPLAY_CURSOR_ON 0x02
#define LCD_DISPLAY_DISPLAY_ON 0X04
#define LCD_SHIFT_RIGHT 0x04
#define LCD_SHIFT_DISPLAY 0x08
#define LCD_FUNCTION_5X10_DOTS 0X04
#define LCD_FUNCTION_TWO_LINES 0X08
#define LCD_FUNCTION_8BIT_MODE 0X10



class LCDDisplay {

    private:
        GPIO* rs_gpio;
        GPIO* e_gpio;
        GPIO* db0_gpio;
        GPIO* db1_gpio;
        GPIO* db2_gpio;
        GPIO* db3_gpio;
        GPIO* db4_gpio;
        GPIO* db5_gpio;
        GPIO* db6_gpio;
        GPIO* db7_gpio;

	int mode;

    public:
        LCDDisplay(int rs_gpio, int e_gpio, int db0_gpio, int db1_gpio, int db2_gpio, int db3_gpio, int db4_gpio, int db5_gpio, int db6_gpio, int db7_gpio);
        LCDDisplay(int rs_gpio, int e_gpio, int db4_gpio, int db5_gpio, int db6_gpio, int db7_gpio);
        void init(void);
        void clear(void);
        void home(void);
        void write_data(std::uint8_t data);
        void print(std::string msg_line1, std::string msg_line2);

    private:
        void init_4bit(void);
        void init_8bit(void);
        void write_4bit(std::uint8_t data, bool command);
        void write_4bit_high_only(std::uint8_t data, bool command);
        void write_8bit(std::uint8_t data, bool command);
        void write_command(std::uint8_t data);
        void toggle_e(void);
};


#endif /* LCDDISPLAY_H */
