/*--------------------------------------------------------------------------*/
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai_parser in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// ruwai is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*--------------------------------------------------------------------------*/

#ifndef GPIO_H
#define GPIO_H

#define GPIO_PATH "/sys/class/gpio/"

#include <string>

typedef enum gpio_direction_e {
    INPUT = 0,
    OUTPUT = 1
} gpio_direction_t;


typedef enum gpio_value_e {
    LOW = 0,
    HIGH = 1
}gpio_value_t;


class GPIO {

    private:
        int number;         // The GPIO number of the object. 
        std::string name;        // The name of the GPIO.
        std::string path;        // The full path to the GPIO.


    public:
        GPIO(int gpio_number);
        virtual ~GPIO();

        virtual int set_direction(gpio_direction_t);
        virtual int set_value(gpio_value_t);

    private:
        int export_gpio(void);
        int unexport_gpio(void);
        int write(std::string path, std::string filename, std::string value);
        int write(std::string path, std::string filename, int value);
};


#endif /* GPIO_H */
