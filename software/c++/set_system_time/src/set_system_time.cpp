/*--------------------------------------------------------------------------*/
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// ruwai is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*--------------------------------------------------------------------------*/
#include <iostream>

#include <ctime>
#include <cstdlib>
#include <cstring>
#include <errno.h>
#include <syslog.h>

int main(int argc, char* argv[])
{
    if (argc != 2)
    {
        std::cout << "Usage: " << argv[0] << " TIMESTAMP" << std::endl;
        return EXIT_SUCCESS;
    }

    time_t cur_time;
    long int timestamp;
    int ret_val;

    timestamp = atol(argv[1]);
    cur_time = (time_t) timestamp;     

    syslog(LOG_NOTICE, "Set system time to %ld", (long int) cur_time);
    ret_val = stime(&cur_time);

    if (ret_val != 0)
    {
        syslog(LOG_ERR, "[ERROR][set_system_time] Error setting the time: %s.", strerror(errno));
    }

    return ret_val;
}
