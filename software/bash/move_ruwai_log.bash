#!/bin/bash

echo "Checking for the ruwai_sd block device."
if [ -b /dev/disk/by-label/ruwai_sd ]
then
	echo "Ruwai SD card block device found."
else
	echo "ERROR: The SD card block device was not found. The SD card seems not to be inserted in the slot. Exiting."
	exit 1
fi

# Check for the mounted SD card using the mount command.
echo "Checking the correct mounting of the SD card using the mount command."
if mount | grep -q "/dev/mmcblk[01]p1 on /media/sd"
then
	echo "The SD card is correctly mounted."
else
	echo "The SD card is not mounted."
	exit 1
fi

# Check for the existing log directory.
if [ ! -d /media/sd/log ]; then
	mkdir /media/sd/log
	chown ruwai:adm /media/sd/log
fi
echo "Moving the compressed log files."
mv /var/log/ruwai.*.gz /media/sd/log

