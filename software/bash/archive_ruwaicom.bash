NAME=ruwaicom_`git symbolic-ref --short HEAD`_`git describe`
git archive --format=tar HEAD ruwaicom > tmp.tar
tar -rf tmp.tar ruwaicom/include/gitrevision.h
gzip -c tmp.tar > ${NAME}.tar.gz
rm tmp.tar

