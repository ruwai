# -*- coding: utf-8 -*-
# LICENSE
#
# This file is part of the Ruwai project.
#
# If you use this code in any program or publication, please inform and
# acknowledge its author Stefan Mertl (stefan@mertl-research.at).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
This script creates a bill-of-material from a KiCad netlist XML file.
The script can be used from the KiCad GUI as a BOM plugin.

This script requires the kicad_netlist_reader modified for the Ruwai
project.
'''

from __future__ import print_function

# Import the KiCad python helper module and the csv formatter
import kicad_netlist_reader
import natsort
import csv
import sys
import os

#print(sys.argv[1])
#print(sys.argv[2])

# Generate an instance of a generic netlist, and load the netlist tree from
# the command line option. If the file doesn't exist, execution will stop
netlist_xml_file = sys.argv[1]
net = kicad_netlist_reader.netlist(netlist_xml_file)

if len(sys.argv) >= 4:
    sub_path = sys.argv[3]
else:
    sub_path = 'bom'

# Open a file to write to, if the file cannot be opened output to stdout
# instead
output_file = sys.argv[2]
orig_output_dir = os.path.dirname(output_file)
[basename, ext] = os.path.splitext(os.path.basename(output_file))
output_file = basename + '_BOM_grouped.csv'
output_dir = os.path.join(orig_output_dir, sub_path)

if not os.path.exists(output_dir):
    os.mkdir(output_dir)
output_file = os.path.join(output_dir, output_file)
print(output_file)
try:
    f = open(output_file, 'w')
except IOError as e:
    print(__file__, ":", e, file=sys.stderr)
    f = sys.stdout

# Create a new csv writer object to use as the output formatter, although we
# are created a tab delimited list instead!
out = csv.writer(f, lineterminator='\n', delimiter=',', quoting=csv.QUOTE_MINIMAL)

# override csv.writer's writerow() to support utf8 encoding:
def writerow( acsvwriter, columns ):
    utf8row = []
    for col in columns:
        try:
            utf8row.append( str(col).encode('utf8') )
        except:
            try:
                utf8row.append(col.encode('utf8'))
            except:
                utf8row.append(''.join([x if ord(x) <= 127 else '?' for x in col]))
    acsvwriter.writerow( utf8row )

#components = net.getInterestingComponents()
groups = net.groupComponents()

# Compute the total amount of components.
n_comp = 0
sort_key = []
for cur_group in groups:
    group_ref = natsort.natsorted([x.getRef() for x in cur_group])
    sort_key.append(group_ref[0])
    n_comp += len(cur_group)

groups = [groups[sort_key.index(x)] for x in natsort.natsorted(sort_key)]

# Output a field delimited header line
writerow( out, ['Source:', net.getSource()] )
writerow( out, ['Date:', net.getDate()] )
writerow( out, ['Tool:', net.getTool()] )
writerow( out, ['Number of components:', n_comp] )
writerow( out, ['ref', 'value', 'quantity', 'manufacturer', 'manufacturer part number', 'package', 'description', 'footprint', 'library', 'part name',] )

# Output all of the component information
for cur_group in groups:
    group_ref = ','.join(natsort.natsorted([x.getRef() for x in cur_group]))
    group_value = ','.join(list(set([x.getValue() for x in cur_group])))
    group_manu = net.getGroupField(cur_group, 'Manu').strip()
    group_manu_num = net.getGroupField(cur_group, 'Manu#').strip()
    group_package = net.getGroupField(cur_group, 'Package').strip()
    group_desc = net.getGroupField(cur_group, 'Desc').strip()
    group_footprint = ','.join(list(set([x.getFootprint() for x in cur_group])))
    group_libname = ','.join(list(set([x.getLibName() for x in cur_group])))
    group_partname = ','.join(list(set([x.getPartName() for x in cur_group])))

    writerow( out, [group_ref, group_value, len(cur_group), group_manu,
        group_manu_num, group_package, group_desc,
        group_footprint, group_libname, group_partname])

print("Wrote file {filename:s}.\n".format(filename=output_file))

# Clean up the files.
# Move the netlist xml to the bom directory.
src_file = netlist_xml_file
dst_file = os.path.join(output_dir, os.path.basename(src_file))
os.rename(src_file, dst_file)

# Kicad seems to create an empty file with no extension during the BOM
# creation. Check for this file and delete it.
src_dir = os.path.dirname(netlist_xml_file)
[clean_name, ext] = os.path.splitext(os.path.basename(src_file))
src_file = os.path.join(src_dir, clean_name)
if os.path.exists(src_file):
    src_size = os.stat(src_file).st_size
    if src_size == 0:
        os.remove(src_file)

