#! /usr/bin/env python
# -*- coding: utf-8 -*-
# LICENSE
#
# This file is part of the Ruwai project.
#
# If you use this code in any program or publication, please inform and
# acknowledge its author Stefan Mertl (stefan@mertl-research.at).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
This script creates orderlist for a BOM created with bom_csv_grouped_components.py
using an CSV inventory file. The BOM file hast to be a csv formatted file with the
mandatory columns ref, value, manufacturer, manufacturer part number, quantity.

The orderlists can be used to easily place an order at a distributor for
electronic components.

The inventory file has to be in the following format:
number,type,manufacturer,manufacturer part number,mount,package,description,distributor,distributor part number,minimum order qty,packaging,cost per unit,stock
1,battery,Keystone,500,THM,THM,"Batteriehalter PCB-Montage, für 1 Knopfzelle Ø 12mm, mit Federarm-Kontakt",RS-Components,430653,1,RS-standard,2.65,0
2,capacitor,AVX,08051A100JAT2A,SMD,805,"AVX Vielschicht Keramikkondensator 10pF 100 V dc, C0G 805 SMD ±5%",RS-Components,4646559,50,RS-standard,0.076,0
3,capacitor,AVX,08051A330JAT2A,SMD,805,"AVX Vielschicht Keramikkondensator 33pF 100 V dc, C0G 805 SMD ±5%",RS-Components,4646600P,50,RS-standard,0.061,0

The Ruwai git repository contains an inventory file for all components used in the Ruwai project.
The Ruwai inventory file is located in the subfolder hardware/bom

The script searches the inventory for the components listed in the BOM, extracts
the according information, computes the quantity to order based on the stock
column and creates one orderlist for each distributor as well as one combined
orderlist as an overview.
'''
from __future__ import print_function

import csv
import sys
import os
import copy
import datetime
import argparse
import logging


def create_orderlist(args):
    # Open a file to write to, if the file cannot be opened output to stdout
    # instead
    #output_dir = os.path.dirname(bom_filename)

    bom_filename = args.bom
    inv_filename = args.inv
    n_boards = args.nboards
    ignore_lines = args.ignore_lines
    ignore_stock = args.ignore_stock

    cur_time = datetime.datetime.now()
    timestamp = cur_time.strftime('%Y%m%d-%H%M%S')
    subdir = 'orderlist_' + timestamp
    output_dir = os.path.join('.', subdir)
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    [bom_basename, bom_ext] = os.path.splitext(os.path.basename(bom_filename))


    # Read the inventory information.
    inventory = {}
    with open(inv_filename, 'rb') as inv_fid:
        inv_reader = csv.DictReader(inv_fid, delimiter=',')
        for row in inv_reader:
            cur_manufacturer = row['manufacturer'].lower().strip()
            cur_part_num = row['manufacturer part number'].lower().strip()
            part_attr = copy.copy(inv_reader.fieldnames)
            part_attr.remove('manufacturer')
            part_attr.remove('manufacturer part number')
            cur_part = {}
            for cur_attr in part_attr:
                cur_part[cur_attr] = row[cur_attr]

            if cur_manufacturer not in inventory.keys():
                inventory[cur_manufacturer] = {}

            inventory[cur_manufacturer][cur_part_num] = cur_part


    # Read the BOM, assign the inventory information and write it to output file.
    with open(bom_filename, 'r') as bom_fid:
        header = []
        for k in range(ignore_lines):
            header.append(bom_fid.readline())
        bom_reader = csv.DictReader(bom_fid, delimiter=',')

        # Select the fields of the inventory to export to the order list.
        export_attr = ['number', 'package', 'distributor', 'distributor part number', 'description', 'cost per unit', 'stock', '# boards', 'rem. stock', 'quantity order']
        export_inv_header = ['number', 'manufacturer package', 'distributor', 'distributor part number', 'distributor description', 'cost per unit', 'stock', '# boards', 'rem. stock', 'quantity order']

        # Select the fields of the bom to export to the order list.
        export_bom_attr = ['ref', 'value', 'quantity', 'manufacturer', 'manufacturer part number']
        #output_fieldnames = [x for x in bom_reader.fieldnames if x in export_bom_attr]
        #output_fieldnames.extend(export_inv_header)

        order_list = {}
        order_list['missing_distributor'] = []
        for row in bom_reader:
            try:
                cur_manu = row['manufacturer'].lower().strip()
                cur_part_num = row['manufacturer part number'].lower().strip()
                inv_data = inventory[cur_manu][cur_part_num]

                # Compute the quantity to order.
                qty_needed = float(row['quantity']) * n_boards
                qty_stock = float(inv_data['stock'])
                if ignore_stock:
                    qty_order = qty_needed
                    rem_stock = qty_stock
                else:
                    if qty_needed < qty_stock:
                        qty_order = 0
                    else:
                        qty_order = qty_needed - qty_stock
                    rem_stock = qty_stock - qty_needed

                    if rem_stock < 0:
                        rem_stock = 0


                inv_data['# boards'] = n_boards
                inv_data['quantity order'] = str(qty_order)
                inv_data['rem. stock'] = rem_stock
            except:
                inv_data = None

            if inv_data is not None:
                inv_row = [inv_data[x] for x in export_attr]
            else:
                inv_row = ['' for x in export_attr]

            output_row = [row[x] for x in export_bom_attr]
            output_row.extend(inv_row)

            if inv_data is not None:
                if inv_data['distributor'] not in order_list.keys():
                    order_list[inv_data['distributor']] = []

                order_list[inv_data['distributor']].append(output_row)
            else:
                order_list['missing_distributor'].append(output_row)


        overview_filename = os.path.join(output_dir, bom_basename + '_orderlist_' + timestamp + '.csv')
        overview_fid = open(overview_filename, 'w')
        overview_writer = csv.writer(overview_fid, lineterminator='\n', delimiter=',', quoting=csv.QUOTE_MINIMAL)
        export_header = []
        export_header.extend(export_bom_attr)
        export_header.extend(export_inv_header)
        overview_writer.writerow(export_header)

        for cur_key, cur_order_list in order_list.iteritems():

            output_filename = os.path.join(output_dir, bom_basename + '_orderlist_' + timestamp + '_' + cur_key + '.csv')
            try:
                out_fid = open(output_filename, 'w')
            except IOError as e:
                print(__file__, ":", e, file=sys.stderr)
                out_fid = sys.stdout

            # Create a new csv writer object to use as the output formatter, although we
            # are created a tab delimited list instead!
            out_writer = csv.writer(out_fid, lineterminator='\n', delimiter=',', quoting=csv.QUOTE_MINIMAL)

            #for cur_header in header:
            #    out_writer.writerow(cur_header.strip().split(','))
            export_header = []
            export_header.extend(export_bom_attr)
            export_header.extend(export_inv_header)
            out_writer.writerow(export_header)

            out_writer.writerows(cur_order_list)
            overview_writer.writerows(cur_order_list)

            logging.info('Wrote file %s for distributor %s.', output_filename, cur_key)

        logging.info('Wrote overview file %s.', overview_filename)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Create orderlists in CSV format from bill-of-materials and inventory CSV files.')
    parser.set_defaults(func = create_orderlist)
    parser.add_argument('bom', help = 'The bill-of-material csv file.',
                        type = str, metavar = 'BOM_FILENAME')
    parser.add_argument('inv', help = 'The inventory csv file.',
                        type = str, metavar = 'INV_FILENAME')
    parser.add_argument('--nboards', help = 'The number of pcb boards for which the orderlist is created.',
                        type = int, metavar = 'N_BOARDS', default = 1)
    parser.add_argument('--ignore-lines', help = 'The number of lines to ignore at the beginning of the file.',
                        type = int, metavar = 'ignore_lines', default = 4)
    parser.add_argument('--loglevel', help = 'Specify the log level.',
                        type = str, choices = ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL', 'NOTSET'],
                        default = 'INFO')
    parser.add_argument('--ignore-stock', help = 'Ignore the parts available in stock when computing the quantity to order.',
                        action = 'store_true', dest = 'ignore_stock')

    args = parser.parse_args()

    logging.basicConfig(level = args.loglevel,
                        format = "#LOG# - %(asctime)s - %(process)d - %(levelname)s: %(message)s")

    args.func(args)


