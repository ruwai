#! /usr/bin/env python
# -*- coding: utf-8 -*-
# LICENSE
#
# This file is part of the Ruwai project.
#
# If you use this code in any program or publication, please inform and
# acknowledge its author Stefan Mertl (stefan@mertl-research.at).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from __future__ import print_function

# Import the KiCad python helper module and the csv formatter
import csv
import sys
import os
import copy
import argparse
import logging
import datetime


def combine_orderlist(args):
    orderlist_files = args.orderlists
    inv_filename = args.inventory
    ignore_stock = args.ignore_stock
    ignore_available = args.ignore_available

    if not orderlist_files:
        return;

    if not inv_filename:
        return;

    # Create a timestamp.
    cur_time = datetime.datetime.now()
    timestamp = cur_time.strftime('%Y%m%d-%H%M%S')

    # Open a file to write to, if the file cannot be opened output to stdout
    # instead
    output_dir = './'

    # Read the orderlist files.
    orderlist = {}
    for cur_file in orderlist_files:
        with open(cur_file, 'rb') as ol_fid:
            ol_reader = csv.DictReader(ol_fid, delimiter=',')
            for row in ol_reader:
                cur_distributor = row['distributor'].lower().strip()
                cur_manufacturer = row['manufacturer'].lower().strip()
                cur_part_num = row['manufacturer part number'].lower().strip()
                part_attr = copy.copy(ol_reader.fieldnames)
                part_attr.remove('manufacturer')
                part_attr.remove('manufacturer part number')
                cur_part = {}
                for cur_attr in part_attr:
                    cur_part[cur_attr] = row[cur_attr]

                if cur_distributor == '':
                    cur_distributor = 'missing distributor'

                if cur_manufacturer == '':
                    cur_manufacturer = 'missing manufacturer'

                if cur_distributor not in orderlist.keys():
                    orderlist[cur_distributor] = {}

                if cur_manufacturer not in orderlist[cur_distributor].keys():
                    orderlist[cur_distributor][cur_manufacturer] = {}

                if cur_part['quantity'] == '':
                    part_quantity = 0
                else:
                    part_quantity = float(cur_part['quantity'])

                if cur_part['# boards'] == '':
                    part_nboards = 0
                else:
                    part_nboards = int(cur_part['# boards'])

                if cur_part_num not in orderlist[cur_distributor][cur_manufacturer].keys():
                    cur_part['quantity'] = part_quantity * part_nboards
                    orderlist[cur_distributor][cur_manufacturer][cur_part_num] = cur_part
                else:
                    cur_quantity = part_quantity * part_nboards
                    ol_quantity = orderlist[cur_distributor][cur_manufacturer][cur_part_num]['quantity'];
                    orderlist[cur_distributor][cur_manufacturer][cur_part_num]['quantity'] = ol_quantity + cur_quantity
                    #if cur_part['quantity order'] == '':
                    #    cur_quantity = 0
                    #    ol_quantity = 0
                    #else:
                    #    cur_quantity = int(cur_part['quantity order'])
                    #    ol_quantity = int(orderlist[cur_distributor][cur_manufacturer][cur_part_num]['quantity order']);
                    #orderlist[cur_distributor][cur_manufacturer][cur_part_num]['quantity order'] = ol_quantity + cur_quantity


    # Read the inventory information.
    inventory = {}
    with open(inv_filename, 'rb') as inv_fid:
        inv_reader = csv.DictReader(inv_fid, delimiter=',')
        for row in inv_reader:
            cur_manufacturer = row['manufacturer'].lower().strip()
            cur_part_num = row['manufacturer part number'].lower().strip()
            part_attr = copy.copy(inv_reader.fieldnames)
            part_attr.remove('manufacturer')
            part_attr.remove('manufacturer part number')
            cur_part = {}
            for cur_attr in part_attr:
                cur_part[cur_attr] = row[cur_attr]

            if cur_manufacturer not in inventory.keys():
                inventory[cur_manufacturer] = {}

            inventory[cur_manufacturer][cur_part_num] = cur_part


    output_filename = os.path.join(output_dir, 'orderlist_combined_' + timestamp + '.csv')
    try:
        out_fid = open(output_filename, 'w')
    except IOError as e:
        print(__file__, ":", e, file=sys.stderr)
        out_fid = sys.stdout

    # Create a new csv writer object to use as the output formatter, although we
    # are created a tab delimited list instead!
    out_writer = csv.writer(out_fid, lineterminator='\n', delimiter=',', quoting=csv.QUOTE_MINIMAL)

    export_header = ['value', 'manufacturer', 'manufacturer part number', 'number', 'manufacturer package', 'distributor', 'distributor part number', 'distributor description', 'cost per unit', 'quantity', 'stock', 'rem. stock', 'quantity order']
    out_writer.writerow(export_header)

    for cur_distributor in orderlist.keys():
        dist_filename = os.path.join(output_dir, 'orderlist_combined_' + cur_distributor + '_' + timestamp + '.csv')
        dist_fid = open(dist_filename, 'w')
        dist_writer = csv.writer(dist_fid, lineterminator='\n', delimiter=',', quoting=csv.QUOTE_MINIMAL)
        dist_writer.writerow(export_header)
        for cur_manufacturer in orderlist[cur_distributor].keys():
            for cur_part_number in orderlist[cur_distributor][cur_manufacturer].keys():
                cur_part = orderlist[cur_distributor][cur_manufacturer][cur_part_number]
                # Compute the quantity to order regarding the available stock.
                try:
                    inv_part = inventory[cur_manufacturer][cur_part_number]
                    if ignore_stock:
                        rem_stock = inv_part['stock']
                        quantity_order = cur_part['quantity']
                    else:
                        rem_stock = int(inv_part['stock']) - cur_part['quantity']
                        if rem_stock < 0:
                            quantity_order = rem_stock * -1
                            rem_stock = 0
                        else:
                            quantity_order = 0
                except:
                    logging.exception('Part %s - %s not found in the inventory.', cur_manufacturer, cur_part_number)
                    continue

                if ignore_available and quantity_order == 0:
                    logging.info('Part %s - %s is available in stock. It is not included in the orderlist.', cur_manufacturer, cur_part_number)
                    continue

                row = [cur_part['value'], cur_manufacturer, cur_part_number, cur_part['number'],
                        cur_part['manufacturer package'], cur_part['distributor'], cur_part['distributor part number'],
                        cur_part['distributor description'], cur_part['cost per unit'], cur_part['quantity'], inv_part['stock'], rem_stock,
                        quantity_order]
                out_writer.writerow(row)
                dist_writer.writerow(row)
        dist_fid.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'Combine multiple order lists to one common oder list.')
    parser.set_defaults(func = combine_orderlist)
    parser.add_argument('inventory', help = 'The inventory csv file.',
                        type = str, metavar = 'INV_FILENAME')
    parser.add_argument('--orderlists', help = 'One or more orderlists to combine.',
                        type = str, nargs = '+')
    parser.add_argument('--loglevel', help = 'Specify the log level.',
                        type = str, choices = ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL', 'NOTSET'],
                        default = 'INFO')
    parser.add_argument('--ignore-stock', help = 'Ignore the parts available in stock when computing the quantity to order.',
                        action = 'store_true', dest = 'ignore_stock')
    parser.add_argument('--ignore-available', help = 'Only output those parts, that are not available in stock.',
                        action = 'store_true', dest = 'ignore_available')

    args = parser.parse_args()

    logging.basicConfig(level = args.loglevel,
                        format = "#LOG# - %(asctime)s - %(process)d - %(levelname)s: %(message)s")

    args.func(args)



