#!/usr/bin/env python

import ipdb

import argparse
import logging
import sys
import os
import re


def replace_part(args):
    logging.debug("args: %s", args)

    schematic_files = args.schematic

    if not schematic_files:
        return

    is_comp = False
    manu_match = False
    mpn_match = False
    package_match = False
    comp_cache = []
    mod_cache = []
    for cur_file in schematic_files:
        logging.info("Processing schematic file %s.", cur_file)
        with open(cur_file, 'r') as s_fid:
            schematic_lines = s_fid.readlines()

        with open(cur_file, 'w') as s_fid:
            for cur_line in schematic_lines:
                tmp = re.split('\s', cur_line)

                if tmp[0] == '$Comp':
                    logging.debug("Part section start found.")
                    is_comp = True
                    manu_match = False
                    mpn_match = False
                    package_match = False
                    comp_cache = [cur_line,]
                    mod_cache = []
                elif is_comp and tmp[0] == '$EndComp':
                    logging.debug("Part section end found.")
                    comp_cache.append(cur_line)

                    for cur_comp_line in comp_cache:
                        tmp = re.split('\s', cur_comp_line)
                        if tmp[1] == '4':
                            line_parts = re.match(ur'(\w+)\s(\w+)\s("[\w\s!@#$%^&*()_+=\[{\]};:<>|./?,-]+")\s(.*)', cur_comp_line, re.UNICODE).groups()
                            if line_parts[2][1:-1].strip() == args.search_manu:
                                manu_match = True
                                logging.debug("Found a matching manufacturer: %s.", line_parts[2][1:-1])
                                cur_comp_line = line_parts[0] + ' ' + line_parts[1] + ' "' + args.replace_manu + '" ' + line_parts[3] + '\n'
                        elif tmp[1] == '5':
                            line_parts = re.match(ur'(\w+)\s(\w+)\s("[\w\s!@#$%^&*()_+=\[{\]};:<>|./?,-]+")\s(.*)', cur_comp_line, re.UNICODE).groups()
                            if line_parts[2][1:-1].strip() == args.search_mpn:
                                mpn_match = True
                                logging.debug("Found a matching mpn: %s.", line_parts[2][1:-1])
                                cur_comp_line = line_parts[0] + ' ' + line_parts[1] + ' "' + args.replace_mpn + '" ' + line_parts[3] + '\n'
                        elif tmp[1] == '6':
                            line_parts = re.match(ur'(\w+)\s(\w+)\s("[\w\s!@#$%^&*()_+=\[{\]};:<>|./?,-]+")\s(.*)', cur_comp_line, re.UNICODE).groups()
                            if line_parts[2][1:-1].strip() == args.search_package:
                                package_match = True
                                logging.debug("Found a matching package: %s.", line_parts[2][1:-1])
                                cur_comp_line = line_parts[0] + ' ' + line_parts[1] + ' "' + args.replace_package + '" ' + line_parts[3] + '\n'
                        elif tmp[1] == '7':
                            try:
                                line_parts = re.match(ur'(\w+)\s(\w+)\s("[\w\s!@#$%^&*()_+=\[{\]};:<>|./?,-]+")\s(.*)', cur_comp_line, re.UNICODE).groups()
                                if manu_match and mpn_match and package_match:
                                    cur_comp_line = line_parts[0] + ' ' + line_parts[1] + ' " " ' + line_parts[3] + '\n'
                            except:
                                logging.error("Couldn't handle the description line. There might be some special characters, that I can't handle. I'm keeping the original line: %s", cur_comp_line)


                        mod_cache.append(cur_comp_line)

                    if manu_match and mpn_match and package_match:
                        logging.info("Found a matching component.")
                        logging.info('%s', mod_cache)
                        for cur_mod_line in mod_cache:
                            s_fid.write(cur_mod_line)
                    else:
                        for cur_comp_line in comp_cache:
                            s_fid.write(cur_comp_line)
                    is_comp = False
                elif is_comp:
                    comp_cache.append(cur_line)
                else:
                    s_fid.write(cur_line)




if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Replace a part manufacturer, manufacturer number and manufacturer package.')
    parser.set_defaults(func = replace_part)

    parser.add_argument('search_manu', help = 'The manufacturer of the part to replace.',
                        type = str)
    parser.add_argument('search_mpn', help = 'The manufacturer part number of the part to replace.',
                        type = str)
    parser.add_argument('search_package', help = 'The manufacturer package of the part to replace.',
                        type = str)
    parser.add_argument('replace_manu', help = 'The manufacturer of the new part.',
                        type = str)
    parser.add_argument('replace_mpn', help = 'The manufacturer part number of the new part.',
                        type = str)
    parser.add_argument('replace_package', help = 'The manufacturer package of the new part.',
                        type = str)
    parser.add_argument('--schematic', help = 'One or more KiCad schematic files to process.',
                        type = str, nargs = '+')
    parser.add_argument('--loglevel', help = 'Specify the log level.',
                        type = str, choices = ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL', 'NOTSET'],
                        default = 'INFO')

    args = parser.parse_args()

    logging.basicConfig(level = args.loglevel,
                        format = "#LOG# - %(asctime)s - %(process)d - %(levelname)s: %(message)s")

    args.func(args)



#path, filename = os.path.split(sys.argv[1])
#name, ext = os.path.splitext(filename)
#new_filename = os.path.join(path, name + '_mod' + ext)
#fid_new = open(new_filename, 'w')

#try:
#    fields = get_fields_from_lib(fid_lib)
#    update_fields(fid_schematic, fields, fid_new)
#finally:
#    fid_schematic.close()
#    fid_lib.close()


