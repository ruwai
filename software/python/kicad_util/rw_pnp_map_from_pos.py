#! /usr/bin/env python
# -*- coding: utf-8 -*-
# LICENSE
#
# This file is part of the Ruwai project.
#
# If you use this code in any program or publication, please inform and
# acknowledge its author Stefan Mertl (stefan@mertl-research.at).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Create pick-and-place maps from KiCad position files and a Gerber
silkscreen layer using the gerbv program.


'''

from __future__ import print_function

import csv
import sys
import os
import argparse
import logging
import time


def create_pnp_map(args):
    bom_filename = args.bom
    pos_filename = args.pos
    silk_filename = args.silk

    subdir = 'pnp_map_' + time.strftime('%Y%m%d-%H%M%S')
    output_dir = os.path.join('.', subdir)
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    [bom_basename, bom_ext] = os.path.splitext(os.path.basename(bom_filename))

    csv_output_dir = os.path.join(output_dir, 'csv')
    png_output_dir = os.path.join(output_dir, 'png')

    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    if not os.path.exists(csv_output_dir):
        os.mkdir(csv_output_dir)

    if silk_filename and not os.path.exists(png_output_dir):
        os.mkdir(png_output_dir)

    # Read the Kicad position file.
    comp_pos = {}
    n_header_lines = 5
    header_lines = []
    field_names = ['ref', 'val', 'package', 'pos_x', 'pos_y', 'rot', 'layer']
    with open(pos_filename, 'rb') as pos_fid:
        for n in range(n_header_lines):
            header_lines.append(pos_fid.readline())

        # Determine the units.
        if header_lines[2].find('inches') >= 0:
            unit_conv = 1000.
        else:
            unit_conv = 1/25.4 * 1000.

        pos_reader = csv.DictReader(pos_fid, fieldnames = field_names, delimiter = ' ', skipinitialspace = True)
        for row in pos_reader:
            if row['ref'] == '##':
                continue

            tmp = row.copy()
            tmp['pos_x'] = float(tmp['pos_x']) * unit_conv
            tmp['pos_y'] = float(tmp['pos_y']) * unit_conv

            # The rotation of gerbv is clockwise. KiCad seems to rotate
            # counterclockwise.
            tmp['rot'] = float(tmp['rot']) * -1

            # Older KiCad versions wrote F.Cu and B.Cu instead of top and
            # bot. gerbv needs either top or bot(tom).
            if tmp['layer'].startswith('F.'):
                tmp['layer'] = 'top'
            elif tmp['layer'].startswith('B.'):
                tmp['layer'] = 'bot'

            comp_pos[row['ref']] = tmp


    # Read the BOM and create one pick-and-place file in gerbv readable format for
    # each component group.
    num_headerlines = 4
    with open(bom_filename, 'r') as bom_fid:

        header = []
        for k in range(num_headerlines):
            header.append(bom_fid.readline())
        bom_reader = csv.DictReader(bom_fid, delimiter=',')


        order_list = {}
        order_list['missing_distributor'] = []
        for row in bom_reader:

            comp_refs = row['ref'].split(',')
            output_rows = []
            for cur_ref in comp_refs:
                if cur_ref in comp_pos.keys():
                    cur_pos = comp_pos[cur_ref]
                    output_rows.append([cur_pos['ref'], cur_pos['package'], cur_pos['val'],
                                        cur_pos['pos_x'], cur_pos['pos_y'], cur_pos['rot'],
                                        cur_pos['layer']])

            if output_rows:
                # Add a dummy entry at the end to ensure, that gerbv recognizes the
                # file as a pick and place file, even if no R, C or U components
                # are present int the component refs.
                output_rows.append(['C0', None, None, None, None, None, None])
                comps = row['ref'].split(',')
                n_comps = len(comps)
                if n_comps > 3:
                    n_comps = 3
                comp_filename = '_'.join(comps[:n_comps])
                csv_output_filename = os.path.join(csv_output_dir, 'pnp_' + comp_filename + '.csv')
                try:
                    out_fid = open(csv_output_filename, 'w')
                except IOError as e:
                    print(__file__, ":", e, file=sys.stderr)
                    out_fid = sys.stdout

                # Create a new csv writer object to use as the output formatter, although we
                # are created a tab delimited list instead!
                out_writer = csv.writer(out_fid, lineterminator='\n', delimiter=',', quoting=csv.QUOTE_MINIMAL)
                out_writer.writerow(field_names)
                out_writer.writerows(output_rows)
                out_fid.close()

                # Create the placemap images using gerbv.
                if silk_filename:
                    # TODO: Check for existing gerbv and the correct version.
                    png_output_filename = os.path.join(png_output_dir, 'pnp_' + comp_filename + '.png')
                    cmd = 'gerbv --background=#FFFFFF --foreground=#FF0000FF --foreground=#000000FF -x png -D 300 -o ' + png_output_filename + ' ' + os.path.abspath(csv_output_filename) + ' ' +  os.path.abspath(silk_filename)
                    os.system(cmd)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'Create pick-and-place maps from KiCad position files and a Gerber silkscreen layer using the gerbv program.')

    parser.set_defaults(func = create_pnp_map)
    parser.add_argument('bom', help = 'The bill-of-material csv file.',
                        type = str, metavar = 'BOM_FILENAME')
    parser.add_argument('pos', help = 'The KiCad component position file.',
                        type = str, metavar = 'POS_FILENAME')
    parser.add_argument('--silk', help = 'The Gerber silkscreen layer.',
                        type = str, metavar = 'SILK_FILENAME')
    parser.add_argument('--loglevel', help = 'Specify the log level.',
                        type = str, choices = ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL', 'NOTSET'],
                        default = 'INFO')

    args = parser.parse_args()

    logging.basicConfig(level = args.loglevel,
                        format = "#LOG# - %(asctime)s - %(process)d - %(levelname)s: %(message)s")

    args.func(args)
