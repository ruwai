#!/usr/bin/env python
import ipdb

import sys
import os
import re

def update_fields(fid_schematic, fields, fid_new):
    for cur_line in fid_schematic:
        tmp = re.split('\s', cur_line)

        new_line = cur_line
        if len(tmp) >= 2:
            if tmp[0] == 'L':
                cur_part = tmp[1]
            elif tmp[0] == 'F':
                if cur_part in fields.keys():
                    if tmp[1] == '4':
                        line_parts = re.match('(\w+)\s(\w+)\s("[\w\s!@#$%^&*()_+=\[{\]};:<>|./?,-]+")\s(.*)', cur_line).groups()
                        new_line = line_parts[0] + ' ' + line_parts[1] + ' ' + fields[cur_part]['Manu'] + ' ' + line_parts[3] + '\n'
                    elif tmp[1] == '5':
                        line_parts = re.match('(\w+)\s(\w+)\s("[\w\s!@#$%^&*()_+=\[{\]};:<>|./?,-]+")\s(.*)', cur_line).groups()
                        new_line = line_parts[0] + ' ' + line_parts[1] + ' ' + fields[cur_part]['Manu#'] + ' ' + line_parts[3] + '\n'
                    elif tmp[1] == '6':
                        line_parts = re.match('(\w+)\s(\w+)\s("[\w\s!@#$%^&*()_+=\[{\]};:<>|./?,-]+")\s(.*)', cur_line).groups()
                        new_line = line_parts[0] + ' ' + line_parts[1] + ' ' + fields[cur_part]['Package'] + ' ' + line_parts[3] + '\n'
                    elif tmp[1] == '7':
                        line_parts = re.match('(\w+)\s(\w+)\s("[\w\s!@#$%^&*()_+=\[{\]};:<>|./?,-]+")\s(.*)', cur_line).groups()
                        new_line = line_parts[0] + ' ' + line_parts[1] + ' ' + fields[cur_part]['Desc'] + ' ' + line_parts[3] + '\n'

        fid_new.write(new_line)


def get_fields_from_lib(fid_lib):
    fields = {}


    for cur_line in fid_lib:
        tmp = re.split('\s', cur_line)

        if len(tmp) > 2:
            cmd = tmp[0]

            if cmd == 'DEF':
                part_name = tmp[1]
                fields[part_name] = {}

            if cmd == 'F4':
                match_obj = re.match('(\w+)\s("[\w\s!@#$%^&*()_+=\[{\]};:<>|./?,-]+")', cur_line)
                if match_obj is not None:
                    fields[part_name]['Manu'] = match_obj.group(2)
                else:
                    fields[part_name]['Manu'] = 'unknown'
            elif cmd == 'F5':
                match_obj = re.match('(\w+)\s("[\w\s!@#$%^&*()_+=\[{\]};:<>|./?,-]+")', cur_line)
                if match_obj is not None:
                    fields[part_name]['Manu#'] = match_obj.group(2)
                else:
                    fields[part_name]['Manu#'] = 'unknown'
            elif cmd == 'F6':
                match_obj = re.match('(\w+)\s("[\w\s!@#$%^&*()_+=\[{\]};:<>|./?,-]+")', cur_line)
                if match_obj is not None:
                    fields[part_name]['Package'] = match_obj.group(2)
                else:
                    fields[part_name]['Package'] = 'unknown'
            elif cmd == 'F7':
                match_obj = re.match('(\w+)\s("[\w\s!@#$%^&*()_+=\[{\]};:<>|./?,-]+")', cur_line)
                if match_obj is not None:
                    fields[part_name]['Desc'] = match_obj.group(2)
                else:
                    fields[part_name]['Desc'] = 'unknown'


    return fields



fid_schematic = open(sys.argv[1], 'r')
fid_lib = open(sys.argv[2], 'r')


path, filename = os.path.split(sys.argv[1])
name, ext = os.path.splitext(filename)
new_filename = os.path.join(path, name + '_mod' + ext)
fid_new = open(new_filename, 'w')

try:
    fields = get_fields_from_lib(fid_lib)
    update_fields(fid_schematic, fields, fid_new)
finally:
    fid_schematic.close()
    fid_lib.close()


