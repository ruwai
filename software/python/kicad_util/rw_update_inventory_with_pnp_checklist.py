#! /usr/bin/env python
# -*- coding: utf-8 -*-
# LICENSE
#
# This file is part of the Ruwai project.
#
# If you use this code in any program or publication, please inform and
# acknowledge its author Stefan Mertl (stefan@mertl-research.at).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
This script updates the Ruwai inventory with the used components
listed in a pnp-checklist.
'''

from __future__ import print_function

# Import the KiCad python helper module and the csv formatter
import csv
import sys
import os
import shutil
import datetime
import argparse
import logging



def update_inventory(args):
    inv_filename = args.inv
    checklist_filename = args.pnp_checklist

    # Read the inventory information.
    checklist = {}
    with open(checklist_filename, 'r') as chk_fid:
        chk_reader = csv.DictReader(chk_fid, delimiter=',')
        for row in chk_reader:
            if row['number'].strip() == '':
                continue
            cur_number = int(row['number'].strip())
            cur_part_num = row['manufacturer part number'].lower().strip()
            cur_parts_used = row['parts used'].strip()
            if cur_parts_used != '':
                cur_parts_used = int(cur_parts_used)
            else:
                cur_parts_used = 0
            cur_part = {}
            cur_part['manufacturer part number'] = cur_part_num
            cur_part['parts used'] = cur_parts_used
            checklist[cur_number] = cur_part



    # Read the inventory information.
    invmod_filename = inv_filename + '.tmp'
    out_fid = open(invmod_filename, 'w')
    out_writer = csv.writer(out_fid, lineterminator='\n', delimiter=',', quoting=csv.QUOTE_MINIMAL)
    with open(inv_filename, 'rb') as inv_fid:
        inv_reader = csv.DictReader(inv_fid, delimiter=',')
        out_writer.writerow(inv_reader.fieldnames)
        for row in inv_reader:
            cur_number = int(row['number'].strip())
            cur_part_num = row['manufacturer part number'].lower().strip()

            if cur_number in checklist.keys():
                row['stock'] = int(row['stock']) - checklist[cur_number]['parts used']

            out_writer.writerow([row[x] for x in inv_reader.fieldnames])
    out_fid.close()


    # Create a backup of the inventory file.
    [cur_path, cur_name] = os.path.split(inv_filename)
    cur_time = datetime.datetime.now()
    cur_name = cur_name + '.backup_' + cur_time.strftime('%Y%m%d-%H%M%S')
    shutil.copy(inv_filename, os.path.join(cur_path, cur_name))
    logging.info('Created the inventory backup file %s.', os.path.join(cur_path, cur_name))

    # Rename the temporary file.
    shutil.move(invmod_filename, inv_filename)

    # Update the history file.
    history_file = os.path.join(cur_path, 'inventory_history.txt')
    hist_fid = open(history_file, 'a')
    hist_fid.write("{0:s}: Updated inventory with file {1:s}.\n".format(cur_time.strftime('%Y%m%d-%H%M%S'), checklist_filename))
    hist_fid.close()
    logging.info('Updated the history file %s.', history_file)



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'Update the stock of a Ruwai inventory file with the used components of a pnp-checklist.')

    parser.set_defaults(func = update_inventory)
    parser.add_argument('inv', help = 'The inventory csv file.',
                        type = str, metavar = 'INV_FILENAME')
    parser.add_argument('pnp_checklist', help = 'The pick-and-place checklist csv file.',
                        type = str, metavar = 'PNP_CHECKLIST_FILENAME')
    parser.add_argument('--loglevel', help = 'Specify the log level.',
                        type = str, choices = ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL', 'NOTSET'],
                        default = 'INFO')

    args = parser.parse_args()

    logging.basicConfig(level = args.loglevel,
                        format = "#LOG# - %(asctime)s - %(process)d - %(levelname)s: %(message)s")

    args.func(args)


