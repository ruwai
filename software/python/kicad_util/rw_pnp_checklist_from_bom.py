#! /usr/bin/env python
# -*- coding: utf-8 -*-
# LICENSE
#
# This file is part of the Ruwai project.
#
# If you use this code in any program or publication, please inform and
# acknowledge its author Stefan Mertl (stefan@mertl-research.at).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
This script creates a pick-and-place checklist from a bill-of-materials and an inventory
CSV file.

The pick-and-place checklist is helpful when preparing the components
for the assembly of the PCBs. It can also be used to update the inventory
stock in the inventory CSV file after the PCB was assembled and the
checklist was updated with the parts used for the assembly.

To update the inventory CSV file use the script rw_update_inventory.py.
'''

from __future__ import print_function
import ipdb

import csv
import sys
import os
import copy
import argparse
import logging
import time


def create_pnp_checklist(args):
    bom_filename = args.bom
    inv_filename = args.inv
    n_boards = args.nboards

    # Open a file to write to, if the file cannot be opened output to stdout
    # instead
    output_dir = '.'
    [bom_basename, bom_ext] = os.path.splitext(os.path.basename(bom_filename))

    # Read the inventory information.
    inventory = {}
    inventory_numbered = {}
    with open(inv_filename, 'rb') as inv_fid:
        inv_reader = csv.DictReader(inv_fid, delimiter=',')
        for row in inv_reader:
            cur_manufacturer = row['manufacturer'].lower().strip()
            cur_part_num = row['manufacturer part number'].lower().strip()
            part_attr = copy.copy(inv_reader.fieldnames)
            #part_attr.remove('manufacturer')
            #part_attr.remove('manufacturer part number')
            cur_part = {}
            for cur_attr in part_attr:
                cur_part[cur_attr] = row[cur_attr]

            if cur_manufacturer not in inventory.keys():
                inventory[cur_manufacturer] = {}

            inventory[cur_manufacturer][cur_part_num] = cur_part
            inventory_numbered[cur_part['number']] = cur_part

    # Read the BOM, assign the inventory information and write it to output file.
    num_headerlines = 4
    with open(bom_filename, 'r') as bom_fid:
        output_filename = os.path.join(output_dir, bom_basename + '_placement_checklist_' + time.strftime("%Y%m%d_%H%M") + '.csv')
        try:
            out_fid = open(output_filename, 'w')
        except IOError as e:
            print(__file__, ":", e, file=sys.stderr)
            out_fid = sys.stdout

        # Create a new csv writer object to use as the output formatter, although we
        # are created a tab delimited list instead!
        out_writer = csv.writer(out_fid, lineterminator='\n', delimiter=',', quoting=csv.QUOTE_MINIMAL)

        #for cur_header in header:
        #    out_writer.writerow(cur_header.strip().split(','))
        export_header = ['ref', 'number', 'parts needed', 'n boards', 'total needed', 'parts used', 'value', 'package', 'manufacturer', 'manufacturer part number', 'mount', 'stock', 'comment',
                         'alternative', 'alternative manufacturer', 'alternative mpn', 'alternative stock', 'alternative comment']
        out_writer.writerow(export_header)

        header = []
        for k in range(4):
            header.append(bom_fid.readline())
        bom_reader = csv.DictReader(bom_fid, delimiter=',')

        # Select the fields of the inventory to export to the order list.
        #export_attr = ['number', 'package', 'distributor', 'distributor part number', 'description', 'cost per unit', 'stock', 'quantity order']
        #export_inv_header = ['number', 'manufacturer package', 'distributor', 'distributor part number', 'distributor description', 'cost per unit', 'stock', 'quantity order']

        # Select the fields of the bom to export to the order list.
        #export_bom_attr = ['ref', 'value', 'quantity', 'manufacturer', 'manufacturer part number']
        #output_fieldnames = [x for x in bom_reader.fieldnames if x in export_bom_attr]
        #output_fieldnames.extend(export_inv_header)

        order_list = {}
        order_list['missing_distributor'] = []
        for row in bom_reader:
            try:
                cur_manu = row['manufacturer'].lower().strip()
                cur_part_num = row['manufacturer part number'].lower().strip()
                inv_data = inventory[cur_manu][cur_part_num]
            except:
                inv_data = None


            if inv_data is not None:
                if inv_data['alternative'] != '':
                    alternative_part = inventory_numbered[inv_data['alternative']]
                else:
                    alternative_part = None

                total_needed =  int(row['quantity']) * n_boards
                if total_needed > int(inv_data['stock']):
                    comment = "not enough parts in stock - REORDER"
                else:
                    comment = 'stock ok'

                if alternative_part:
                    if total_needed > int(alternative_part['stock']):
                        alt_comment = "not enough parts in alt. stock - REORDER"
                    else:
                        alt_comment = 'alt. stock ok'
                    output_row = [row['ref'], inv_data['number'], row['quantity'], str(n_boards), str(total_needed), '', row['value'], inv_data['package'], row['manufacturer'], row['manufacturer part number'],
                                  inv_data['mount'], inv_data['stock'], comment, inv_data['alternative'], alternative_part['manufacturer'], alternative_part['manufacturer part number'], alternative_part['stock'], alt_comment]
                else:
                    output_row = [row['ref'], inv_data['number'], row['quantity'], str(n_boards), str(total_needed), '', row['value'], inv_data['package'], row['manufacturer'], row['manufacturer part number'], inv_data['mount'], inv_data['stock'], comment, inv_data['alternative'], '', '', '', '']
            else:
                output_row = [row['ref'], '', '', '', '', '', row['value'], '', row['manufacturer'], row['manufacturer part number'], '',  '', 'part not found in inventory', '', '', '', '', '']

            out_writer.writerow(output_row)

        logging.info('Wrote file %s', output_filename)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'Create a pick-and-place checklist to track the electronic components when assembling the PCB.')

    parser.set_defaults(func = create_pnp_checklist)
    parser.add_argument('bom', help = 'The bill-of-material csv file.',
                        type = str, metavar = 'BOM_FILENAME')
    parser.add_argument('inv', help = 'The inventory csv file.',
                        type = str, metavar = 'INV_FILENAME')
    parser.add_argument('--nboards', help = 'The number of pcb boards for which the orderlist is created.',
                        type = int, metavar = 'N_BOARDS', default = 1)    
    parser.add_argument('--loglevel', help = 'Specify the log level.',
                        type = str, choices = ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL', 'NOTSET'],
                        default = 'INFO')

    args = parser.parse_args()

    logging.basicConfig(level = args.loglevel,
                        format = "#LOG# - %(asctime)s - %(process)d - %(levelname)s: %(message)s")

    args.func(args)
