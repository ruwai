# -*- coding: utf-8 -*-
# LICENSE
#
# This file is part of ruwai_parser.
#
# If you use ruwai_parser in any program or publication, please inform and
# acknowledge its author Stefan Mertl (stefan@mertl-research.at).
#
# ruwai_parser is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from construct import Container
from construct import Struct
from construct import Array
from construct import ULInt8, ULInt16, ULInt32
from construct import SLInt32
from construct import BitStruct
from construct import Flag
from construct import Bits

# The RUWAI protocal paket indicators.
RUWAI_SYNC_CHAR1 = 0xD8
RUWAI_SYNC_CHAR2 = 0xBF

# The RUWAI class IDs.
rw_msg_class = {}
rw_msg_class['CFG'] = 0x00
rw_msg_class['SOH'] = 0x01
rw_msg_class['SMP'] = 0x02


# The RUWAI SMP class message IDs.
rw_msg_id = {}
tmp = {}
tmp['24BIT'] = 0x00
tmp['24BIT_TSP'] = 0x01
tmp['10BIT'] = 0x02
tmp['DIGITAL'] = 0x03
tmp['TIMESTAMP_MARKER'] = 0x04
tmp['TIMESTAMP'] = 0x05
tmp['UTC_TIME'] = 0x06
rw_msg_id['SMP'] = tmp

tmp = {}
tmp['SPS'] = 0x00
tmp['CHANNELS'] = 0x01
rw_msg_id['CFG'] = tmp



# The RUWAI paket header.
header = Struct('header',
        ULInt8('sync_char1'),
        ULInt8('sync_char2'),
        ULInt8('msg_class'),
        ULInt8('msg_id'),
        ULInt16('payload_length')
        )

# The Ruwai paket checksum.
checksum = Struct('checksum',
                ULInt8('ck0'),
                ULInt8('ck1')
                )


# The RUWAI SMP-24BIT message.
smp_24bit = Struct('24bit',
                   Array(4, SLInt32('samples'))
                  )

# The RUWAI SMP-24BIT_TSP message.
smp_24bit_tsp = Struct('24bit_tsp',
                   ULInt16('week'),
                   ULInt32('tow_ms'),
                   ULInt32('tow_sub_ms'),
                   ULInt32('sec_slts'),
                   BitStruct('flags',
                        Flag('reserved', 1),
                        Flag('gps_fix_ok', 1),
                        Bits('gps_fix', 4),
                        Flag('timebase', 1),
                        Flag('utc', 1)),
                   Array(4, SLInt32('samples'))
                  )

# The RUWAI CFG-SPS message.
cfg_sps = Struct('cfg:sps',
        ULInt16('sps')
        )

cfg_channels = Struct('cfg:channels',
        BitStruct('channel_flags',
            Bits('reserved', 4),
            Flag('channel4'),
            Flag('channel3'),
            Flag('channel2'),
            Flag('channel1')),
        ULInt8('gain_channel1'),
        ULInt8('gain_channel2'),
        ULInt8('gain_channel3'),
        ULInt8('gain_channel4')
        )



def compute_fletcher16_checksum(header, payload, modulus = 256):
    ''' Compute the Fletcher16 checksum
    '''
    ck0 = 0
    ck1 = 0

    data = []
    data = map(ord, header[2:])
    data.extend(map(ord, payload))

    for cur_data in data:
        ck0 = ck0 + cur_data
        ck1 = ck1 + ck0

    ck0 = ck0 % modulus
    ck1 = ck1 % modulus
    return ck0, ck1



def build_paket(class_id, msg_id, payload_length, payload):
    ''' Build the Ruwai paket using the passed message.
    '''
    header_cont = Container(sync_char1 = RUWAI_SYNC_CHAR1,
                            sync_char2 = RUWAI_SYNC_CHAR2,
                            msg_class = class_id,
                            msg_id = msg_id,
                            payload_length = payload_length)

    header_data = header.build(header_cont)

    ck0, ck1 = compute_fletcher16_checksum(header_data, payload)
    checksum_data = checksum.build(Container(ck0 = ck0, ck1 = ck1))

    return ''.join([header_data, payload, checksum_data])

