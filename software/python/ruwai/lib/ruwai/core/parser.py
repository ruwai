# -*- coding: utf-8 -*-
# LICENSE
#
# This file is part of ruwai.
#
# If you use ruwai_parser in any program or publication, please inform and
# acknowledge its author Stefan Mertl (stefan@mertl-research.at).
#
# ruwai is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import serial
import threading
import Queue
import time
import ruwai_protocol as rw_prot


class RuwaiProtocolParser:

    def __init__(self):

        self.msg_parser = {}
        tmp = {}
        tmp[rw_prot.rw_msg_id['SMP']['24BIT']] = self.parse_smp_24bit
        tmp[rw_prot.rw_msg_id['SMP']['24BIT_TSP']] = self.parse_smp_24bit_tsp
        self.msg_parser[rw_prot.rw_msg_class['SMP']] = tmp


    def parse(self, data):

        paket_pointer = 0
        parsed_pakets = 0;
        msg = []
        continue_search = True

        while continue_search:
            paket_pointer = data.find(chr(rw_prot.RUWAI_SYNC_CHAR1) + chr(rw_prot.RUWAI_SYNC_CHAR2));
            if paket_pointer is not -1:
                data = data[paket_pointer:]
                if len(data) <= 6:
                    continue_search = False
                else:
                    header = rw_prot.header.parse(data[:6])
                    if header.msg_class in self.msg_parser.keys():
                        if header.msg_id in self.msg_parser[header.msg_class].keys():
                            payload_offset = 6;
                            checksum_offset = payload_offset + header.payload_length
                            if checksum_offset + 2 > len(data):
                                continue_search = False
                            else:
                                valid = self.is_valid_checksum(data[:6],
                                                               data[payload_offset:payload_offset + header.payload_length],
                                                               checksum = data[checksum_offset : checksum_offset + 2])
                                if (valid):
                                    # If a valid paket is found, parse it.
                                    cur_msg = self.msg_parser[header.msg_class][header.msg_id](header = header,
                                                                                 payload = data[payload_offset:payload_offset + header.payload_length],
                                                                                 checksum = data[checksum_offset : checksum_offset + 2])
                                    msg.append(cur_msg)
                                    parsed_pakets += 1
                                    data = data[6 + header.payload_length + 2:]
                                else:
                                    # The paket is not valid, skip it.
                                    print "Wrong checksum."
                                    data = data[6 + header.payload_length + 2:]

            else:
                continue_search = False

        return parsed_pakets, msg, data


    def parse_smp_24bit(self, header, payload, checksum):
        ''' Parse a SMP 24BIT paket.
        '''
        paket = {}
        paket['header'] = header
        paket['msg'] = rw_prot.smp_24bit.parse(payload)
        return paket


    def parse_smp_24bit_tsp(self, header, payload, checksum):
        ''' Parse a SMP 24BIT TSP paket.
        '''
        paket = {}
        paket['header'] = header
        paket['msg'] = rw_prot.smp_24bit_tsp.parse(payload)
        return paket


    def is_valid_checksum(self, header, payload, checksum):
        ''' Test the checksum of the paket.
        '''
        ck0, ck1 = self.compute_fletcher16_checksum(header, payload)

        if (ck0 == ord(checksum[0])) and (ck1 == ord(checksum[1])):
            return True
        else:
            return False


    def compute_fletcher16_checksum(self, header, payload, modulus = 256):
        ''' Compute the Fletcher16 checksum
        '''
        ck0 = 0
        ck1 = 0

        data = []
        data = map(ord, header[2:])
        data.extend(map(ord, payload))

        for cur_data in data:
            ck0 = ck0 + cur_data
            ck1 = ck1 + ck0

        ck0 = ck0 % modulus
        ck1 = ck1 % modulus
        return ck0, ck1




class SerialParser:

    def __init__(self, ard_stack):
        ''' Initialize the instance.
        '''
        self.ard_stack = ard_stack

        # The Ruwai protocol parser.
        self.parser = RuwaiProtocolParser()

        # The pakets read from the serial port.
        self.parsed_pakets = Queue.Queue()

        # The event to signal the capture thread to stop.
        self.stop_capture_event = threading.Event()


    def start_capture(self):
        self.capture_thread = threading.Thread(target = self.capture)
        self.capture_thread.setDaemon(True)
        self.capture_thread.start()


    def stop_capture(self):
        self.stop_capture_event.set()


    def capture(self):

        if not self.ard_stack.isOpen():
            self.ard_stack.open()

        time.sleep(0.1)
        self.ard_stack.flushInput()
        self.ard_stack.flushOutput()
        time.sleep(0.1)

        data = '';
        count = 0;
        while (not self.stop_capture_event.is_set()):
            bytes_waiting = self.ard_stack.inWaiting()
            if bytes_waiting > 0:
                data += (self.ard_stack.read(bytes_waiting))
                n_parsed_pakets, msg, data = self.parser.parse(data);
                if n_parsed_pakets > 0:
                    for cur_msg in msg:
                        #print cur_msg
                        self.parsed_pakets.put(cur_msg)
                    count += n_parsed_pakets;
                    #print "Parsed %d pakets (total: %d).\n" % (n_parsed_pakets, count)

        print "LEAVING THE CAPTURE THREAD"

