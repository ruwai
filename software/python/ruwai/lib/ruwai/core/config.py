# -*- coding: utf-8 -*-
# LICENSE
#
# This file is part of ruwai.
#
# If you use ruwai_parser in any program or publication, please inform and
# acknowledge its author Stefan Mertl (stefan@mertl-research.at).
#
# ruwai is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.




class RuwaiConfig:

    def __init__(self, filename = None):
        '''
        '''
        self.ard_stack = ArduinoStackConfig()


class ArduinoStackConfig:

    def __init__(self, serial_port = '/dev/ttyACM0',
                       baud_rate = 230400,
                       sps = 200):
        ''' The initialization of the instance.
        '''
        # The serial port to which the Arduino stack is connected.
        self.serial_port = serial_port

        # The baud rate of the serial port.
        self.baud_rate = baud_rate

        # The sampling rate.
        self.sps = sps


