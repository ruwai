# -*- coding: utf-8 -*-
# LICENSE
#
# This file is part of ruwai.
#
# If you use ruwai_parser in any program or publication, please inform and
# acknowledge its author Stefan Mertl (stefan@mertl-research.at).
#
# ruwai is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import ruwai_protocol as rw_prot
import construct


class RuwaiControl:

    def __init__(self, parent):
        '''
        '''
        self.parent = parent


    def send_ruwai_msg(self, class_id, msg_id, payload_length, payload):
        ''' Send a Ruwai message via the serial port.
        '''
        paket = rw_prot.build_paket(class_id = class_id,
                                    msg_id = msg_id,
                                    payload_length = payload_length,
                                    payload = payload)
        self.self.parent.ard_stack.write(paket)


    def set_sps(self, sps = 100):
        ''' Set the sampling rate of the data recorder.
        '''
        class_id = rw_prot.rw_msg_class['CFG']
        msg_id = rw_prot.rw_msg_id['CFG']['SPS']
        payload = rw_prot.cfg_sps.build(construct.Container(sps = sps))
        self.send_ruwai_msg(class_id = class_id,
                            msg_id = msg_id,
                            payload_length = rw_prot.cfg_sps.sizeof(),
                            payload = payload)


    def set_channels(self, c1_act, c2_act, c3_act, c4_act,
                     c1_gain, c2_gain, c3_gain, c4_gain):
        ''' Set the channel configuration.
        '''
        class_id = rw_prot.rw_msg_class['CFG']
        msg_id = rw_prot.rw_msg_id['CFG']['CHANNELS']

        channel_flags_cont = construct.Container(channel1 = c1_act,
                                channel2 = c2_act,
                                channel3 = c3_act,
                                channel4 = c4_act,
                                reserved = 0)

        chan_cont = construct.Container(
                                channel_flags = channel_flags_cont,
                                gain_channel1 = c1_gain,
                                gain_channel2 = c2_gain,
                                gain_channel3 = c3_gain,
                                gain_channel4 = c4_gain
                )

        payload = rw_prot.cfg_channels.build(chan_cont)
        self.send_ruwai_msg(class_id = class_id,
                            msg_id = msg_id,
                            payload_length = rw_prot.cfg_channels.sizeof(),
                            payload = payload)

