import unittest
import construct
import ruwai_parser.core.ruwai_protocol as rw_prot
import ruwai_parser.core.parser as rw_parser

class ParserTestCase(unittest.TestCase):
    """
    """

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        pass

    def tearDown(self):
        pass


    def test_compute_fletcher16_checksum(self):
        ''' Test the computation of the fletcher16 checksum.
        '''
        parser = rw_parser.RuwaiProtocolParser()
        header_cont = construct.Container(sync_char1 = rw_prot.RUWAI_SYNC_CHAR1,
                                     sync_char2 = rw_prot.RUWAI_SYNC_CHAR2,
                                     msg_class = rw_prot.rw_msg_class['SMP'],
                                     msg_id = rw_prot.rw_msg_id['SMP']['24BIT'],
                                     payload_length = rw_prot.smp_24bit.sizeof()
                                     )

        payload_cont = construct.Container(n_channels = 4,
                                      channel = [construct.Container(channel_id = 1, sample = 1),
                                                 construct.Container(channel_id = 2, sample = 2),
                                                 construct.Container(channel_id = 3, sample = 3),
                                                 construct.Container(channel_id = 4, sample = 4)]
                                      )

        header_bytes = rw_prot.header.build(header_cont)
        payload_bytes = []
        ck0, ck1 = parser.compute_fletcher16_checksum(header_bytes, payload_bytes)
        self.assertEqual(ck0, 23)
        self.assertEqual(ck1, 50)

        payload_bytes = rw_prot.smp_24bit.build(payload_cont)
        ck0, ck1 = parser.compute_fletcher16_checksum(header_bytes, payload_bytes)

        self.assertEqual(ck0, 47)
        self.assertEqual(ck1, 39)

        header_string = "d8bf02001500"
        payload_string = "040100000000020000000003000000000400000000"
        checksum_string = "25cd"
        header_bytes = header_string.decode('hex')
        payload_bytes = payload_string.decode('hex')
        checksum_bytes = checksum_string.decode('hex')
        ck0, ck1 = parser.compute_fletcher16_checksum(header_bytes, payload_bytes)

        self.assertEqual(ck0, ord(checksum_bytes[0]))
        self.assertEqual(ck1, ord(checksum_bytes[1]))






def suite():
    return unittest.makeSuite(ParserTestCase, 'test')


if __name__ == '__main__':
    unittest.main(defaultTest='suite')

