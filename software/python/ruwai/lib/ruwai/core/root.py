# -*- coding: utf-8 -*-
# LICENSE
#
# This file is part of ruwai.
#
# If you use ruwai_parser in any program or publication, please inform and
# acknowledge its author Stefan Mertl (stefan@mertl-research.at).
#
# ruwai is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import serial
import time
import threading
import zmq

import ruwai
import ruwai.core.control
import ruwai.core.record
import ruwai.core.parser as rw_parser

class RuwaiRoot:
    ''' The Ruwai root class.

    Manage the Recorder Configuration.
    Manager the serial communication with the Arduino stack.
    Distribute the captured Ruwai pakets to the according instances.
    '''
    def __init__(self, config):
        '''
        '''
        # The Ruwai configuration instance.
        self.config = config


        # The serial port connected with the Arduino stack.
        self.ard_stack = serial.Serial(port = self.config.ard_stack.serial_port,
                                       baudrate = self.config.ard_stack.baud_rate,
                                       bytesize = serial.EIGHTBITS,
                                       parity = serial.PARITY_NONE,
                                       stopbits = serial.STOPBITS_ONE)


        # The Ruwai control instance.
        self.control = ruwai.core.control.RuwaiControl(parent = self)

        # The Ruwai recording instance.
        self.recorder = ruwai.core.record.RuwaiRecorder(parent = self)

        # The serial parser for the Arduino stack pakets.
        self.parser = rw_parser.SerialParser(self.ard_stack)


    def run(self):
        ''' Start the Processing loop.
        '''
        stop_event = threading.Event()
        self.open_arduino_stack()

        # Create the threads.
        self.control_server_thread = threading.Thread(target = self.control_server, args = (stop_event, ))
        self.handle_pakets_thread = threading.Thread(target = self.handle_pakets, args = (stop_event, ))


        # Start the threads.
        self.control_server_thread.start()
        self.handle_pakets_thread.start()
        self.parser.start_capture()

        # Wait for the threads to end.
        self.control_server_thread.join()
        self.handle_pakets_thread.join()

        # Stop the serial parser thread.
        self.parser.stop_capture()
        self.parser.capture_thread.join()


    def control_server(self, stop_event, port = 5000):
        '''
        '''
        context = zmq.Context()
        socket = context.socket(zmq.REP)
        socket.bind("tcp://127.0.0.1:%d" % port)
        print "Running server on port: %d." % port

        while not stop_event.is_set():
            msg = socket.recv()
            print "Received message: %s." % msg
            if msg == 'stop':
                socket.send("Stopping the thread. Good-bye.")
                stop_event.set()
            else:
                socket.send("Ja, hier Server.")


        print "LEAVING THE CONTROL_SERVER THREAD"


    def handle_pakets(self, stop_event):
        ''' Save the parsed pakets to data arrays.
        '''
        print "Starting the handle pakets thread."
        while (not stop_event.is_set()):
            if not self.parser.parsed_pakets.empty():
                paket = self.parser.parsed_pakets.get()
                if paket['header']['msg_id'] == 1:
                    print paket

        print "LEAVING THE HANDLE_PAKETS THREAD"


    def open_arduino_stack(self):
        ''' Open the serial port of the arduino stack.
        '''
        # Open the serial device.
        if not self.ard_stack.isOpen():
            self.ard_stack.open()

        time.sleep(0.1)
        self.ard_stack.flushInput()
        self.ard_stack.flushOutput()
        time.sleep(0.1)


