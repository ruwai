import ruwai.core.config
import ruwai.core.root


if __name__ == '__main__':
    config = ruwai.core.config.RuwaiConfig()
    config.ard_stack.serial_port = '/dev/ttyACM1'
    root = ruwai.core.root.RuwaiRoot(config)
    root.run()

