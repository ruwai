import threading
import ruwai_parser.core.parser as rw_parser
import matplotlib.pyplot as plt
import numpy as np


def handle_capture(parser, stop_event, data):
    counter = 0
    while (not stop_event.is_set()):
        if not parser.parsed_pakets.empty():
            paket = parser.parsed_pakets.get()
            data[0].append(paket.channel[0].sample)
            data[1].append(paket.channel[1].sample)
            data[2].append(paket.channel[2].sample)
            data[3].append(paket.channel[3].sample)
            counter += 1

    print "LEAVING THE HANDLE_CAPTURE THREAD"


data = [[],[],[],[]]

parser = rw_parser.SerialParser('/dev/ttyACM0', 576000)
stop_event = threading.Event()

handle_capture_thread = threading.Thread(target = handle_capture, args = (parser, stop_event, data))

handle_capture_thread.start()
parser.start_capture()

raw_input('Press any key to stop capturing...')
stop_event.set()

parser.stop_capture()
parser.capture_thread.join()
handle_capture_thread.join()


plot_mode = 'counts'

v_ref = 2.5
bitweight = v_ref / (2**23 - 1)
data = [np.array(x) for x in data]
data = np.vstack(data).transpose()

if data.shape[0] > 200:
    data = data[200:, :]


if plot_mode == 'volt':
    data = data * bitweight
offset = np.median(data, 0)
data = data - offset
data_range = np.max(data, 0) - np.min(data, 0)
data_rms = np.sqrt(np.sum(data**2, 0) / data.shape[0])

# Add the offset again for plotting.
#data = data + offset

if plot_mode == 'volt':
    print "Data offset\nch1: %f uV\nch2: %f uV\nch3: %f uV\nch4: %f uV\n" % (offset[0] * 1e6, offset[1] * 1e6, offset[2] * 1e6, offset[3] * 1e6)
    print "Data range\nch1: %f uV\nch2: %f uV\nch3: %f uV\nch4: %f uV\n" % (data_range[0] * 1e6, data_range[1] * 1e6,data_range[2] * 1e6,data_range[3] * 1e6)
    print "Data RMS:\nch1: %f uV\nch2: %f uV\nch3: %f uV\nch4: %f uV" % (data_rms[0] * 1e6, data_rms[1] * 1e6, data_rms[2] * 1e6, data_rms[3] * 1e6)
else:
    print "Data offset\nch1: %f counts\nch2: %f counts\nch3: %f counts\nch4: %f counts\n" % (offset[0], offset[1], offset[2], offset[3])
    print "Data range\nch1: %f counts\nch2: %f counts\nch3: %f counts\nch4: %f counts\n" % (data_range[0], data_range[1], data_range[2], data_range[3])
    print "Data RMS:\nch1: %f counts\nch2: %f counts\nch3: %f counts\nch4: %f counts" % (data_rms[0], data_rms[1], data_rms[2], data_rms[3])

fig = plt.figure(facecolor = 'white')

#plot_channels = [1, 2, 3, 4]
plot_channels = [4, ]
for k, cur_channel in enumerate(plot_channels):
    ax = fig.add_subplot(len(plot_channels), 1, k)
    plt.plot(data[:, cur_channel - 1])
    if plot_mode == 'volt':
        plt.ylabel('Amplitude [V]')
    else:
        plt.ylabel('counts')
    ax.text(0.95, 0.95, 'channel %d' % (cur_channel), transform = ax.transAxes, fontsize = 12, fontweight = 'bold', va = 'top', ha = 'right')
    if k == len(plot_channels) - 1:
        plt.xlabel('samples')

plt.show()

#parser.ruwai_device.close()





