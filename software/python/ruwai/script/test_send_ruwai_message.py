import ruwai.core.control as rw_control
import threading
import time

def listen_to_serial(stop_event):
    while not stop_event.is_set():
        bytes_waiting = control.ruwai_device.inWaiting()
        if bytes_waiting > 0:
            print control.ruwai_device.read(bytes_waiting)

def keyboard_input(stop_event):
    ''' Wait for a keyboard input to stop the program.
    '''
    while not stop_event.is_set():
        char = raw_input('Enter q and carriage return to stop the program...')
        if char == 'q':
            stop_event.set()


# Create the Ruwai parser instance.
control = rw_control.RuwaiControl('/dev/ttyACM0', 576000)

stop_event = threading.Event()

listen_thread = threading.Thread(target = listen_to_serial, args = (stop_event,))
keyboard_thread = threading.Thread(target = keyboard_input, args = (stop_event,))
listen_thread.start()
keyboard_thread.start()


while not stop_event.is_set():
    control.set_sps()
    time.sleep(1)
    control.set_channels(c1_act = True, c2_act= True, c3_act = False, c4_act = False, 
                        c1_gain = 1, c2_gain = 2, c3_gain = 3, c4_gain = 4)
    time.sleep(1)



