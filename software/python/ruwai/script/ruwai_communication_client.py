import zmq
import time

def control_client(port = 5000):
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.connect("tcp://127.0.0.1:%d" % port)

    msg = 'Hallo Stefan.'
    for request in range(5):
        print "Sending request %d with message: %s" % (request, msg)
        socket.send(msg)
        reply = socket.recv()
        print "Received reply: %s" % reply
        time.sleep(1)

    msg = 'stop'
    print "Stopping the server with message: %s" % msg
    socket.send(msg)
    reply = socket.recv()
    print "Received reply: %s" % reply

if __name__ == "__main__":
    control_client()
