import threading
import ruwai.core.parser as rw_parser


def handle_capture(parser, stop_event, data):
    ''' Save the parsed pakets to a data array.
    '''
    counter = 0
    while (not stop_event.is_set()):
        if not parser.parsed_pakets.empty():
            paket = parser.parsed_pakets.get()
            print paket
            counter += 1

    print "LEAVING THE HANDLE_CAPTURE THREAD"


def print_capture(parser, stop_event):
    ''' Print the captured pakets.
    '''
    while (not stop_event.is_set()):
        if not parser.parsed_pakets.empty():
            paket = parser.parsed_pakets.get()
            if paket['header']['msg_id'] == 1:
                print paket
                print paket['msg'].samples[0]

    print "LEAVING THE HANDLE_CAPTURE THREAD"


def keyboard_input(stop_event):
    ''' Wait for a keyboard input to stop the program.
    '''
    while not stop_event.is_set():
        char = raw_input('Enter q and carriage return to stop the program...')
        if char == 'q':
            stop_event.set()

    print "LEAVING THE KEYBOARD INPUT THREAD"


# Create the data array.
data = [[],[],[],[]]

# Create the Ruwai parser instance.
parser = rw_parser.SerialParser('/dev/ttyACM0', 230400)

# Create the thread events.
stop_event = threading.Event()

# Create the threads.
print_capture_thread = threading.Thread(target = print_capture, args = (parser, stop_event,))
keyboard_interrupt_thread = threading.Thread(target = keyboard_input, args = (stop_event,))

# Start the threads
print_capture_thread.start()
keyboard_interrupt_thread.start()
parser.start_capture()

# Wait for the threads to end.
print_capture_thread.join()
keyboard_interrupt_thread.join()

# Stop the capturing of the parser.
parser.stop_capture()
parser.capture_thread.join()

