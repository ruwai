import threading
import ruwai.core.parser as rw_parser
import matplotlib.pyplot as plt
import matplotlib.animation as mpl_anim
import numpy as np
import collections
import serial
import time


def handle_capture(parser, stop_event, data):
    ''' Save the parsed pakets to data arrays.
    '''
    counter = 0
    while (not stop_event.is_set()):
        if not parser.parsed_pakets.empty():
            paket = parser.parsed_pakets.get()
            if paket['header']['msg_id'] == 0:
                data[0].append(paket['msg'].samples[0])
                data[1].append(paket['msg'].samples[1])
                data[2].append(paket['msg'].samples[2])
                data[3].append(paket['msg'].samples[3])
                counter += 1
            if paket['header']['msg_id'] == 1:
                data[0].append(paket['msg'].samples[0])
                data[1].append(paket['msg'].samples[1])
                data[2].append(paket['msg'].samples[2])
                data[3].append(paket['msg'].samples[3])
                counter += 1
                print counter
                counter = 0


    print "LEAVING THE HANDLE_CAPTURE THREAD"



def keyboard_input(stop_event):
    ''' Wait for a keyboard input to stop the program.
    '''
    while not stop_event.is_set():
        char = raw_input('Press q to stop the program...')
        print char
        if char == 'q':
            stop_event.set()


    print "LEAVING THE KEYBOARD_INPUT THREAD"




def seismogram_display(data, sps):
    plot_mode = 'volt'                # counts, volt
    v_ref = 2.5
    bitweight = v_ref / (2**23 - 1)

    fig = plt.figure(facecolor = 'white')
    fig.canvas.manager.set_window_title('RUWAI display')
    plot_channels = [1,2,3,4]
    #plot_channels = [1,]
    axes = []
    lines = []
    for k, cur_channel in enumerate(plot_channels):
        cur_ax = fig.add_subplot(len(plot_channels), 1, k+1)
        axes.append(cur_ax)
        cur_ax.text(0.95, 0.95, 'channel %d' % (cur_channel), transform = axes[k].transAxes, fontsize = 12, fontweight = 'bold', va = 'top', ha = 'right')
        cur_line, = cur_ax.plot([], [])
        lines.append(cur_line)

        if plot_mode == 'volt':
            plt.ylabel('Amplitude [V]')
        else:
            plt.ylabel('counts')
        if k == len(plot_channels) - 1:
            plt.xlabel('time [s]')


    def display_init():
        for k, cur_channel in enumerate(plot_channels):
            ind = cur_channel - 1
            #lines[k].set_data(np.arange(0, len(data[ind])), data[ind])
            ylim = np.max(np.abs(data[ind]))
            axes[k].set_ylim(-ylim, ylim)
            axes[k].set_xlim(0, len(data[ind]) * 1./sps)

        return lines

    def animate(frame):
        for k, cur_channel in enumerate(plot_channels):
            ind = cur_channel - 1
            offset = np.median(data[ind])
            lines[k].set_data(np.arange(0, len(data[ind])) * 1/sps, data[ind] - offset)
            ylim = np.max(np.abs(data[ind] - offset))
            axes[k].set_ylim(-ylim, ylim)
            rms = np.sqrt(np.mean((data[ind] - offset)**2))
            if plot_mode == 'volt':
                rms = rms * bitweight
                print "%f [V]" % rms
            else:
                print "%f [cnt]" % rms
        print "\n"

        return lines

    anim = mpl_anim.FuncAnimation(fig, animate, init_func = display_init, blit = False, interval = 10)
    plt.show()


sps = 800.                   # the sampling rate of the ruwai device
display_length = 20.          # display length [s]

buffer_size = sps * display_length
data = [collections.deque(np.zeros(buffer_size), maxlen = buffer_size),
        collections.deque(np.zeros(buffer_size), maxlen = buffer_size),
        collections.deque(np.zeros(buffer_size), maxlen = buffer_size),
        collections.deque(np.zeros(buffer_size), maxlen = buffer_size)]

# Configure the serial device.
ard_stack = serial.Serial(port = '/dev/ttyACM0',
                          baudrate = 576000,
                          bytesize = serial.EIGHTBITS,
                          parity = serial.PARITY_NONE,
                          stopbits = serial.STOPBITS_ONE)
# Open the serial device.
if not ard_stack.isOpen():
    ard_stack.open()

time.sleep(0.1)
ard_stack.flushInput()
ard_stack.flushOutput()
time.sleep(0.1)


parser = rw_parser.SerialParser(ard_stack)
stop_event = threading.Event()


# Create the threads.
handle_capture_thread = threading.Thread(target = handle_capture, args = (parser, stop_event, data))
keyboard_interrupt_thread = threading.Thread(target = keyboard_input, args = (stop_event,))
thread_animation = threading.Thread(target = seismogram_display, args = (data, sps))

# Start the threads.
handle_capture_thread.start()
keyboard_interrupt_thread.start()
parser.start_capture()
thread_animation.start()

# Wait for the threads to end.
handle_capture_thread.join()
keyboard_interrupt_thread.join()
thread_animation.join()

# Stop the capturing of the parser.
parser.stop_capture()
parser.capture_thread.join()








