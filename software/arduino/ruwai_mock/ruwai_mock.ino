// -*- coding: utf-8 -*-
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// pSysmon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <ruwai.h>
#include <gps_ublox.h>
#include <ubx_protocol.h>
#include <rw_protocol.h>
#include <timer16.h>

void send_samples(void);
void send_ruwai_message(uint8_t msg_class, uint8_t msg_id, void* msg, uint8_t length);

HardwareSerial & usb_serial = Serial;
Timer16 &timer1 = Timer1;
uint8_t led = 13;
uint8_t samples_ready = 0;
uint8_t race_condition = 0;
uint8_t run_loop = 1;
uint16_t count = 0;
uint16_t interval = 10000;
uint16_t int_counter = 0;
uint16_t sps = 1000000 / interval;

void setup(void) {
    // Initialize the output pins.
    pinMode(led, OUTPUT);

    // Initialize the serial ports.
    usb_serial.begin(USB_SERIAL_BAUDRATE);

    // Initialize the timer.
    timer1.begin(interval);
    timer1.start();
}


void loop() 
{
    if (run_loop == 1) {
        if (race_condition == 1) {
            usb_serial.println("Race Condition.");
            run_loop = 0;
        }
        else if (samples_ready == 1) {
            send_samples();
            //usb_serial.println("012345678901234567890123456789");
            //usb_serial.println(count);
            //count++;
            samples_ready = 0;
        }
    }

    /*
    // Toggle the LED pin.
    // Use Arduino macros to get the correct port register.
    uint8_t port = digitalPinToPort(led);
    uint8_t bitmask = digitalPinToBitMask(led);
    volatile uint8_t *out;

    out = portOutputRegister(port);
    *out ^= bitmask;

    // Wait for some time.
    delay(1);
    */
}

void
send_samples()
{
    //static uint8_t counter = 0;
    rw_smp_24bit_t msg;
    msg.n_channels = 4;
    msg.channel[0].channel_id = 1;
    msg.channel[0].sample = int32_t(random(100)) - 50;
    msg.channel[1].channel_id = 2;
    msg.channel[1].sample = int32_t(random(100)) - 50;
    msg.channel[2].channel_id = 3;
    msg.channel[2].sample = int32_t(random(100)) - 50;
    msg.channel[3].channel_id = 4;
    msg.channel[3].sample = int32_t(random(100)) - 50;
    send_ruwai_message(RW_CLASS_SMP, RW_SMP_ID_24BIT, &msg, sizeof(msg));

    /*
    counter += 1;
    if (counter >= 200)
    {
        counter = 0;
    }
    */

}


void 
send_ruwai_message(uint8_t msg_class, uint8_t msg_id, void* msg, uint8_t length)
{
    rw_header_t header;
    uint8_t ck0 = 0, ck1 = 0;
    header.sync_char1 = RUWAI_SYNC_CHAR1;
    header.sync_char2 = RUWAI_SYNC_CHAR2;
    header.msg_class = msg_class;
    header.msg_id = msg_id;
    header.payload_length = length;

    // Update the checksum using the relevant header part.
    update_fletcher16_checksum((uint8_t *)&header.msg_class, sizeof(header) - 2, ck0, ck1);

    // Update the checksum using the payload.
    update_fletcher16_checksum((uint8_t *)msg, length, ck0, ck1);

    // Send the paket to the serial port.
    usb_serial.write((const uint8_t *)&header, sizeof(header));
    usb_serial.write((const uint8_t *)msg, length);
    usb_serial.write(ck0);
    usb_serial.write(ck1);
}

// The timer1 interrupt service routine.
ISR(TIMER1_OVF_vect)
{
    // Toggle the LED pin.
    // Use Arduino macros to get the correct port register.
    uint8_t port = digitalPinToPort(led);
    uint8_t bitmask = digitalPinToBitMask(led);
    volatile uint8_t *out;


    if (int_counter == sps) {
        out = portOutputRegister(port);
        *out ^= bitmask;
        int_counter = 0;
    }
    else {
        int_counter++;
    }


    if (samples_ready == 1) {
        race_condition = 1;
    }
    samples_ready = 1;

    // Directly access the LED pin of the mega2560.
    //PORTB ^= _BV(PB7);
}

