// -*- coding: utf-8 -*-
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// pSysmon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef ruwai_h
#define ruwai_h

#include <Arduino.h>

#define DEBUG

#ifdef DEBUG
    #define debug_print(x) Serial.println(x)
    #define debug_print_hex(x) Serial.println(x, HEX)
#else
    #define debug_print(x)
    #define debug_print_hex(x)
#endif



/***********************************************************/
// The Ruwai PCF8574 and PCF8574A port expander addresses.
// This are the 7-bit addresses.
#define S1_PCF_PGA1 0x20
#define S1_PCF_PGA2 0x21
#define S1_PCF_PGA3 0x22
#define S1_PCF_PGA4 0x23
#define S1_PCF_ADC 0x38


/************************************************************/
// The settings for the UART communication.
#define GPS_DEFAULT_BAUDRATE 9600
#define GPS_BAUDRATE 38400  // TODO: Check why a baudrate > 38400 is not working with the GPS module.
#define USB_SERIAL_BAUDRATE 230400


#endif
