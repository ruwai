// -*- coding: utf-8 -*-
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// pSysmon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef rw_control_h
#define rw_control_h

#include <Arduino.h>
#include <rw_protocol.h>
#include <ruwai.h>
#include <rw_serial.h>

// Used to pack the structures to remove eventual byte overhead.
#ifndef PACKED
    #define PACKED __attribute__((packed))
#endif

typedef enum rw_read_state_e {
    RW_WAITING_FOR_SYNC1 = 0,
    RW_SYNC1_FOUND,
    RW_SYNC2_FOUND,
    RW_CLASS_READ,
    RW_MSGID_READ,
    RW_LENGTH_BYTE1_READ,
    RW_LENGTH_BYTE2_READ,
    RW_PAYLOAD_READ,
    RW_CKA_OK
} rw_read_state_t;

//! The RUWAI message receive buffer.
typedef union PACKED rw_msg_buffer_u {
    rw_cfg_sps_t rw_cfg_sps;
    rw_cfg_channels_t rw_cfg_channels;
    rw_cfg_mode_t rw_cfg_mode;
    rw_cfg_gps_t rw_cfg_gps;
    rw_ack_sync_t rw_ack_sync;
    rw_soh_request_t rw_soh_request;
    uint8_t bytes[];
} rw_msg_buffer_t;


//! The Ruwai control class.
/*!
 * Detailed description.
 */
class RuwaiControl {
    private:
        /*! The step of the message read flow. */
        rw_read_state_t read_step;

        /* The message class of the read flow. */
        uint8_t read_msg_class;

        /* The message id of the read flow. */
        uint8_t read_msg_id;

        /* The payload length of the message in the read flow. */
        uint16_t read_payload_length;

        /* The checksum of the read flow. */
        uint8_t read_cka;
        uint8_t read_ckb;

        /* The counter of the payload in the read flow. */
        uint16_t read_payload_counter;

        /* The buffer of the received message in the read flow. */
        rw_msg_buffer_t read_buffer;


        /*! The recieved acknowledgement packets. */
        rw_ack_sync_t rcv_ack_sync;


        /*! The state of the controler. */
        volatile bool synced;

        /*! The serial port used for BBB communication. */
        RuwaiSerial &serial_port;



        /*! Send a ublox message. */
        //void send_ublox_message(uint8_t msg_class, uint8_t msg_id, void *msg, uint8_t length);

        /*! Parse a Ruwai RW message. */
        bool parse_ruwai();

        /*! Handle an unexpected RW message. */
        void handle_unexpected_msg();

        void send_ack_sync(uint8_t sync, uint8_t ack, uint8_t seq_num, uint8_t ack_num);

    public:
        /*! The constructor. */
        RuwaiControl(RuwaiSerial &serial_stream);

        /*! Start the serial port. */
        void start_serial(uint32_t baud_rate);


        /*! The state of received configuration and SOH requests. */
        volatile bool rcv_ack_sync_changed;
        volatile bool rcv_soh_request_changed;
        volatile bool rcv_cfg_sps_changed;
        volatile bool rcv_cfg_channels_changed;
        volatile bool rcv_cfg_gps_changed;
        volatile bool rcv_cfg_mode_changed;

        /*! The received configuration settings. */
        rw_cfg_sps_t rcv_cfg_sps;
        rw_cfg_channels_t rcv_cfg_channels;
        rw_cfg_mode_t rcv_cfg_mode;
        rw_cfg_gps_t rcv_cfg_gps;

        /*! The received SOH requests. */
        rw_soh_request_t rcv_soh_request;

        /*! Read data from the serial port. */
        bool read();

        /*! Handshake with ruwaicom. !*/
        bool handshake();

        /*! Reset the ruwai_control state. !*/
        void reset();

        /*! Acknowledge a received configuration message. !*/
        void ack_msg(rw_msg_class_t msg_class, rw_msg_cfg_id_t msg_id);
        
        /*! Send a ruwai message to the serial port. */
        void send_message(uint8_t msg_class, uint8_t msg_id, void* msg, uint8_t length);

        /*! Send a ruwai status message to the serial port. */
        void send_status(rw_soh_status_type_t type, String status);
};

#endif
