// -*- coding: utf-8 -*-
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// pSysmon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// Parts of this code were adapted from AP_GPS_UBLOX code from ArduPilot. The
// origin of the used code was by Michael Smith, Jordi Munoz and Jose Julio,
// DIYDrones.com and partly rewritten by Andrew Tridgell. 


#include <rw_control.h>
#include <avr/wdt.h>

extern RuwaiSerial & usb_serial;

RuwaiControl::RuwaiControl(RuwaiSerial &serial_stream) :
    serial_port(serial_stream)
{
    reset();
}

void
RuwaiControl::reset()
{
    // Set the read flow state.
    read_step = RW_WAITING_FOR_SYNC1;
    read_msg_class = 0;
    read_msg_id = 0;
    read_cka = 0;
    read_ckb = 0;
    read_payload_length = 0;
    read_payload_counter = 0;

    // Initialize the state of the Ruwai control.
    synced = false;
    rcv_ack_sync_changed = false;
    rcv_cfg_sps_changed = false;
    rcv_cfg_channels_changed = false;
    rcv_cfg_gps_changed = false;
    rcv_soh_request_changed = false;
}


void
RuwaiControl::start_serial(uint32_t baud_rate)
{
    serial_port.begin(baud_rate);
}


bool
RuwaiControl::handshake()
{
    static uint8_t seq_num = 1;
    bool parsed = false;

    while (!synced)
    {
        wdt_reset();
        //serial_port.write((uint8_t)85);
        //delay(100);
        if (serial_port.available() > 0)
        {
            parsed = read();
        }

        if (parsed)
        {
            if (rcv_ack_sync_changed)
            {
                if ((rcv_ack_sync.sync == 1) && (rcv_ack_sync.ack == 0))
                {
                    // Answer the sync request.
                    usb_serial.println("Answer request.");
                    send_ack_sync(1, 1, seq_num++, rcv_ack_sync.seq_num + 1);
                }
                else if ((rcv_ack_sync.sync == 0) && (rcv_ack_sync.ack == 1)) 
                {
                    // Received the confirmation of the answer.
                    send_ack_sync(0, 1, seq_num++, rcv_ack_sync.seq_num + 1);
                    synced = true;
                    usb_serial.println("Got confirmation answer.");
                }
                else
                {
                    usb_serial.println("???");
                }
                rcv_ack_sync_changed = false;
            }
            parsed = false;
        }
    }
    return true;
}

void
RuwaiControl::send_message(uint8_t msg_class, uint8_t msg_id, void* msg, uint8_t length)
{
    rw_header_t header;
    uint8_t ck0 = 0, ck1 = 0;
    header.sync_char1 = RUWAI_SYNC_CHAR1;
    header.sync_char2 = RUWAI_SYNC_CHAR2;
    header.msg_class = msg_class;
    header.msg_id = msg_id;
    header.payload_length = length;

    // Update the checksum using the relevant header part.
    update_fletcher16_checksum((uint8_t *)&header.msg_class, sizeof(header) - 2, ck0, ck1);

    // Update the checksum using the payload.
    update_fletcher16_checksum((uint8_t *)msg, length, ck0, ck1);

    // Send the paket to the serial port.
    serial_port.write((const uint8_t *)&header, sizeof(header));
    serial_port.write((const uint8_t *)msg, length);
    serial_port.write(ck0);
    serial_port.write(ck1);
}


bool
RuwaiControl::read()
{
    uint8_t data = 0;
    uint8_t bytes_available = 0;
    bool parsed = false;

    bytes_available = serial_port.available();
    for (uint16_t k = 0; k < bytes_available; k++)
    {
        //usb_serial.print("Read step:");
        //usb_serial.println(read_step);
        data = serial_port.read();
        //usb_serial.print(data, HEX);
        //usb_serial.print(" ");

        reset:
        switch(read_step)
        {
            case RW_SYNC1_FOUND:
                //usb_serial.print("RW_SYNC1_FOUND");
                if (data == RUWAI_SYNC_CHAR2)
                {
                    read_step = RW_SYNC2_FOUND;
                    //usb_serial.print("RW_SYNC2_FOUND");
                    break;
                }
                // If the second sync char is wrong, reset to initial mode and
                // continue the processing of the current data.
                read_step = RW_WAITING_FOR_SYNC1;
                //usb_serial.print("SYNC2 doesn't match.");
            case RW_WAITING_FOR_SYNC1:
                if (data == RUWAI_SYNC_CHAR1)
                {
                    read_step = RW_SYNC1_FOUND;
                }
                break;
            case RW_SYNC2_FOUND:
                read_msg_class = data;
                read_cka = data;
                read_ckb = data;
                read_step = RW_CLASS_READ;
                //usb_serial.print("RW_CLASS_READ");
                break;
            case RW_CLASS_READ:
                read_msg_id = data;
                read_cka += data;
                read_ckb += read_cka;
                read_step = RW_MSGID_READ;
                //usb_serial.print("RW_MSGID_READ");
                break;
            case RW_MSGID_READ:
                read_payload_length = data;
                read_cka += data;
                read_ckb += read_cka;
                read_step = RW_LENGTH_BYTE1_READ;
                //usb_serial.print("RW_LENGTH_BYTE1_READ");
                break;
            case RW_LENGTH_BYTE1_READ:
                read_payload_length += (uint16_t)(data << 8);
                read_cka += data;
                read_ckb += read_cka;
                read_payload_counter = 0;
                read_step = RW_LENGTH_BYTE2_READ;
                //usb_serial.print("RW_LENGTH_BYTE2_READ");
                if(read_payload_length > sizeof(read_buffer)) {
                    //usb_serial.print("Too large payload.");
                    read_payload_length = 0;
                    read_step = RW_WAITING_FOR_SYNC1;
                    goto reset;
                }
                break;
            case RW_LENGTH_BYTE2_READ:
                //usb_serial.println("++++++ in RW_LENGTH_BYTE2_READ");
                //usb_serial.print("payload_counter: ");
                //usb_serial.println(read_payload_counter);
                //usb_serial.print("read_payload_length");
                //usb_serial.println(read_payload_length);
                //usb_serial.print("sizeof(read_buffer)");
                //usb_serial.println(sizeof(read_buffer));

                if (read_payload_counter < sizeof(read_buffer))
                {
                    read_buffer.bytes[read_payload_counter] = data;
                }
                else if (read_payload_counter >= sizeof(read_buffer))
                {
                    //usb_serial.println("payload_counter larger than read buffer");
                    read_payload_length = 0;
                    read_payload_counter = 0;
                    read_step = RW_WAITING_FOR_SYNC1;
                    goto reset;
                }

                read_payload_counter++;
                if (read_payload_counter == read_payload_length)
                {
                    read_step = RW_PAYLOAD_READ;
                    //usb_serial.print("RW_PAYLOAD_READ");
                }
                read_cka += data;
                read_ckb += read_cka;
                break;
            case RW_PAYLOAD_READ:
                if (read_cka != data)
                {
                    //usb_serial.print("Bad cka.");
                    read_step = RW_WAITING_FOR_SYNC1;
                    goto reset;
                }
                read_step = RW_CKA_OK;
                //usb_serial.print("RW_CKA_OK");
                break;
            case RW_CKA_OK:
                read_step = RW_WAITING_FOR_SYNC1;
                if(read_ckb != data)
                {
                    //usb_serial.print("Bad ckb.");
                    break;
                }
                //usb_serial.print("RW_CKB_OK");
                parsed = parse_ruwai();
        }
    }

    return parsed;
}


bool
RuwaiControl::parse_ruwai()
{
    bool parsed = false;
    if (read_msg_class == RW_CLASS_ACK)
    {
        switch (read_msg_id)
        {
            case RW_ACK_ID_SYNC:
                rcv_ack_sync = read_buffer.rw_ack_sync;
                rcv_ack_sync_changed = true;
                parsed = true;
                break;
        }
    }
    else if (read_msg_class == RW_CLASS_CFG)
    {
        switch (read_msg_id)
        {
            case RW_CFG_ID_SPS:
                rcv_cfg_sps = read_buffer.rw_cfg_sps;
                rcv_cfg_sps_changed = true;
                parsed = true;
                break;
            case RW_CFG_ID_CHANNELS:
                rcv_cfg_channels = read_buffer.rw_cfg_channels;
                debug_print(rcv_cfg_channels.active.channel1);
                debug_print(rcv_cfg_channels.gain_channel3);
                rcv_cfg_channels_changed = true;
                parsed = true;
                break;
            case RW_CFG_ID_MODE:
                rcv_cfg_mode = read_buffer.rw_cfg_mode;
                rcv_cfg_mode_changed = true;
                parsed = true;
                break;
            case RW_CFG_ID_GPS:
                rcv_cfg_gps = read_buffer.rw_cfg_gps;
                rcv_cfg_gps_changed = true;
                parsed = true;
                break;
            default:
                handle_unexpected_msg();
        }
    }
    else if (read_msg_class == RW_CLASS_SOH)
    {
        switch (read_msg_id)
        {
            case RW_SOH_ID_REQUEST:
                rcv_soh_request = read_buffer.rw_soh_request;
                rcv_soh_request_changed = true;
                parsed = true;
                break;
            default:
                handle_unexpected_msg();
        }
    }
    else
    {
        handle_unexpected_msg();
    }

    return parsed;
}

void
RuwaiControl::send_ack_sync(uint8_t sync, uint8_t ack, uint8_t seq_num, uint8_t ack_num)
{
    rw_ack_sync_t msg;
    msg.sync = sync;
    msg.ack = ack;
    msg.seq_num = seq_num;
    msg.ack_num = ack_num;
    send_message(RW_CLASS_ACK, RW_ACK_ID_SYNC, &msg, sizeof(msg));
}

void
RuwaiControl::ack_msg(rw_msg_class_t msg_class, rw_msg_cfg_id_t msg_id)
{
    rw_ack_cfg_t msg;
    msg.msg_class = msg_class;
    msg.msg_id = msg_id; 
    send_message(RW_CLASS_ACK, RW_ACK_ID_CFG, &msg, sizeof(msg));
}

void
RuwaiControl::handle_unexpected_msg()
{
    debug_print("Handling an unexpected RUWAI message.");
}

void
RuwaiControl::send_status(rw_soh_status_type_t type, String status)
{
    rw_soh_status_t msg;
    msg.type = type;
    //strlcpy(msg.status, status.c_str(), status.length() + 1);
    status.toCharArray(msg.status, sizeof(msg.status));
    send_message(RW_CLASS_SOH, RW_SOH_ID_STATUS, &msg, sizeof(msg));
}
