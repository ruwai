// -*- coding: utf-8 -*-
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// pSysmon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef rw_pga281_h
#define rw_pga281_h

#include <Arduino.h>

#define PCF_PORT_G0 0
#define PCF_PORT_G1 1
#define PCF_PORT_G2 2
#define PCF_PORT_G3 3
#define PCF_PORT_G4 4
#define PCF_PORT_EF 5

//! The supported gain types.
typedef enum pga_gain_e {
    PGA_GAIN_0_125 = 0,
    PGA_GAIN_0_25,
    PGA_GAIN_0_5,
    PGA_GAIN_1,
    PGA_GAIN_2,
    PGA_GAIN_4,
    PGA_GAIN_8,
    PGA_GAIN_16,
    PGA_GAIN_32,
    PGA_GAIN_64,
    PGA_GAIN_128,
    PGA_GAIN_0_172 = 16,
    PGA_GAIN_0_344,
    PGA_GAIN_0_688,
    PGA_GAIN_1_375,
    PGA_GAIN_2_75,
    PGA_GAIN_5_5,
    PGA_GAIN_11,
    PGA_GAIN_22,
    PGA_GAIN_44,
    PGA_GAIN_88,
    PGA_GAIN_176
} pga_gain_t;

//! The Ruwai PGA281 class.
/*!
 * Detailed description.
 */
class RwPga281
{
    public:
        /*! The constructor. */
        RwPga281(uint8_t port_expander_address);

        /*! Set the gain of the PGA. */
        void set_gain(pga_gain_t pga_gain);


    private:
        uint8_t port_expander;
        pga_gain_t gain;
};

#endif 
