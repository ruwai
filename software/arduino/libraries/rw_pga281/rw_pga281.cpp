// -*- coding: utf-8 -*-
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// pSysmon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <rw_pga281.h>
#include <Wire.h>


RwPga281::RwPga281(uint8_t port_expander_address)
{
    port_expander = port_expander_address;
}


void
RwPga281::set_gain(pga_gain_t pga_gain)
{
    gain = pga_gain;
    Wire.beginTransmission(port_expander);
    Wire.write((gain << PCF_PORT_G0) | (0x01 << PCF_PORT_EF));
    Wire.endTransmission(port_expander);
}
