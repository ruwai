// -*- coding: utf-8 -*-
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// pSysmon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef gpslea6t_h
#define gpslea6t_h

#include <Arduino.h>
#include <ubx_protocol.h>
#include <ruwai.h>
#include <rw_serial.h>

// Used to pack the structures to remove eventual byte overhead.
#ifndef PACKED
    #define PACKED __attribute__((packed))
#endif

// The mapping of the GPS pins to the Arduino pins.
#define GPS_ENABLE_USB 34
#define GPS_TP_INTERRUPT 1

//! The supported GPSfix types.
typedef enum gpsfix_e {
    FIX_NO = 0,
    FIX_DEAD_RECKONING_ONLY = 1,
    FIX_2D = 2,
    FIX_3D = 3,
    FIX_GPS_AND_DEAD_RECKONING = 4,
    FIX_TIME_ONLY = 5
} gpsfix_t;

typedef enum read_state_e {
    WAITING_FOR_SYNC1 = 0,
    SYNC1_FOUND,
    SYNC2_FOUND,
    CLASS_READ,
    MSGID_READ,
    LENGTH_BYTE1_READ,
    LENGTH_BYTE2_READ,
    PAYLOAD_READ,
    CKA_OK
} read_state_t;


typedef enum target_e {
    T_DDC = 0,
    T_UART1,
    T_UART2,
    T_USB,
    T_SPI,
    T_RESERVED
} target_t;



//! The UBX message receive buffer.
typedef union PACKED ubx_msg_buffer_u {
    ubx_cfg_tp5_t cfg_tp5;
    ubx_cfg_prt_uart_t cfg_prt_uart;
    ubx_ack_ack_t ack_ack;
    ubx_nav_status_t nav_status;
    ubx_nav_timeutc_t nav_timeutc;
    ubx_nav_posllh_t nav_posllh;
    ubx_tim_tp_t tim_tp;
    uint8_t bytes[];
} ubx_msg_buffer_t;

//! The uBlox GPS class.
/*!
 * Detailed description.
 */
class GpsLea6T {
    private:
        /*! The step of the message read flow. */
        read_state_t read_step;

        /* The message class of the read flow. */
        uint8_t read_msg_class;

        /* The message id of the read flow. */
        uint8_t read_msg_id;

        /* The payload length of the message in the read flow. */
        uint16_t read_payload_length;

        /* The checksum of the read flow. */
        uint8_t read_cka;
        uint8_t read_ckb;

        /* The counter of the payload in the read flow. */
        uint16_t read_payload_counter;

        /* The state of received ACK messages. */
        volatile bool msg_ack_changed;

        /* The buffer of the received message in the read flow. */
        ubx_msg_buffer_t read_buffer;

        /*! The type of the GPSfix. */
        gpsfix_t gps_fix;

        /*! The timepulse configuration. */
        ubx_cfg_tp5_t timepulse_cfg;

        /*! The timepulse1 configuration. */
        ubx_cfg_tp5_t timepulse1_cfg;

        /*! The timepulse2 configuration. */
        ubx_cfg_tp5_t timepulse2_cfg;

        /*! The navigation status configuration. */
        ubx_nav_status_t navigation_status;

        /*! The cfg message acknowledge information. */
        ubx_ack_ack_t msg_ack;



        /*! The validity of the GPSfix. */
        bool gps_fix_ok;

        /*! The Arduino Serial Stream object used for communication. */
        RuwaiSerial &serial_port; 

        /*! Send a ublox message. */
        void send_ublox_message(uint8_t msg_class, uint8_t msg_id, void *msg, uint8_t length);

        /*! Parse a ublox UBX message. */
        bool parse_ubx();

        /*! Handle an unexpected UBX message. */
        void handle_unexpected_msg();

        /*! Send the timepulse2 configuration to the GPS module. */
        bool send_cfg_tp5(uint8_t tp_idx);

        /*! Wait for the acknowledgement message for a sent config message. */
        bool wait_for_cfg_ack(uint8_t msg_class, uint8_t msg_id);

        /*! Test the serial connection with a given baud rate. */
        bool test_baud_rate(uint32_t baud_rate);

        /*! Poll the baud rate and wait for an answer for a given baud rate. */
        bool poll_baud_rate_blocking(uint32_t baud_rate);

    public:
        /*! The constructor. */
        GpsLea6T(RuwaiSerial &serial);


        /* The state of the polled GPS status fields. */ 
        /*! The flag to track changes of the NAV settings. */
        volatile bool nav_status_changed;
        volatile bool nav_timeutc_changed;
        volatile bool nav_position_changed;

        /*! The flag to track changes of the CFG settings. */
        volatile bool cfg_timepulse1_changed;
        volatile bool cfg_timepulse2_changed;
        volatile bool cfg_prt_uart_changed;

        /*! The flags used to track the timestamp state. */
        volatile bool timestamp_init;
        volatile bool timestamp_empty;
        volatile bool timestamp_buf_empty;
        volatile bool timestamp_race_error;

        /*! The UTC time information. */
        ubx_nav_timeutc_t timeutc;

        /*! The navigation geodetic position solution. */
        ubx_nav_posllh_t nav_position;

        /*! The time of the next timepulse. */
        ubx_tim_tp_t next_timepulse;
        ubx_tim_tp_t next_timepulse_buf;

        ubx_cfg_prt_uart_t cfg_prt_uart;

        /*! Initialize the state. */
        void init_state(void);

        /*! Start the serial port. */
        bool start_serial(uint32_t baud_rate);

        void stop_serial(void);

        /*! Set the timepulse1 PPS. */
        bool set_timepulse1_pps();

        /*! Set the timepulse2 frequency. */
        bool set_timepulse2_frequency(uint32_t freq, uint32_t freq_lock);

        /*! Disable the timepulse2 frequency. */
        bool disable_timepulse2_frequency(void);

        /* Get the timepulse2 frequency. */
        uint32_t get_timepulse2_frequency();

        /*! Get the timepulse2 locked frequency. */
        uint32_t get_timepulse2_frequency_locked();

        /*! The the serial port baud rate. */
        void set_uart1_baudrate(uint32_t baudrate);

        /*! The the usb port baud rate. */
        void set_usb_baudrate(uint32_t baudrate);

        /*! Get the GPS fix type. */
        gpsfix_t get_gps_fix();

        /*! Get the GPS fix_ok flag. */
        bool get_gps_fix_ok();


        /*! Poll the configuration settings of the UART port. */
        void poll_cfg_prt_uart();    

        /*! Poll a message configuration. */
        void poll_cfg_msg(uint8_t msg_class, uint8_t msg_id);

        /*! Send the message rate configuration to the current target. */
        bool send_cfg_msg(uint8_t msg_class, uint8_t msg_id, uint8_t rate);
        bool send_cfg_msg(uint8_t msg_class, uint8_t msg_id, uint8_t rate, uint8_t* targets, uint8_t n_targets);

        /*! Poll the timepulse configuration. */
        void poll_cfg_tp5(uint8_t timepulse_id);

        /*! Poll the navigation status information. */
        void poll_nav_status();

        /*! Poll the geodetic position solution. */
        void poll_nav_posllh();

        /*! Test for a working baud rate. */
        uint32_t detect_baud_rate(uint32_t baud_1, uint32_t baud_2);


        /*! Disable the default messages on the current target port. */
        uint8_t disable_default_messages();

        /*! Enable a message on the current target port. */
        bool enable_message(uint8_t msg_class, uint8_t msg_id, uint8_t rate);
        bool enable_message(uint8_t msg_class, uint8_t msg_id, uint8_t rate, uint8_t target);
        bool enable_message(uint8_t msg_class, uint8_t msg_id, uint8_t rate, uint8_t* targets, uint8_t n_targets);

        /*! Enable the usb port and the raw messages. */
        void enable_usb();

        /*! Disable the usb port. */
        void disable_usb();


        /*! Read data from the serial port. */
        bool read();
};

#endif
