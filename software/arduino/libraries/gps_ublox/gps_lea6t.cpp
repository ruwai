// -*- coding: utf-8 -*-
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// pSysmon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// Parts of this code were adapted from AP_GPS_UBLOX code from ArduPilot. The
// origin of the used code was by Michael Smith, Jordi Munoz and Jose Julio,
// DIYDrones.com and partly rewritten by Andrew Tridgell. 


#include <gps_lea6t.h>

extern RuwaiSerial & usb_serial;

GpsLea6T::GpsLea6T(RuwaiSerial &serial_stream) : 
    serial_port(serial_stream)
{
    // Initialize the Arduino pins.
    pinMode(GPS_ENABLE_USB, OUTPUT);

    // Disable the usb port power supply by default.
    digitalWrite(GPS_ENABLE_USB, 1);

    init_state();
}

void
GpsLea6T::init_state(void)
{
    // Set the read flow state.
    read_step = WAITING_FOR_SYNC1;
    read_msg_class = 0;
    read_msg_id = 0;
    read_cka = 0;
    read_ckb = 0;
    read_payload_length = 0;
    read_payload_counter = 0;

    // Initialize the state of the GPS module.
    timepulse1_cfg.tp_idx = 0;
    timepulse2_cfg.tp_idx = 1;
    gps_fix = FIX_NO;
    gps_fix_ok = false;
    nav_status_changed = false;
    nav_timeutc_changed = false;
    nav_position_changed = false;
    cfg_timepulse1_changed = false;
    cfg_timepulse2_changed = false;
    cfg_prt_uart_changed = false;
    msg_ack_changed = false;
    timestamp_init = false;
    timestamp_empty = true;
    timestamp_buf_empty = true;
    timestamp_race_error = false;

    next_timepulse.tow_ms = 0;
    next_timepulse.tow_sub_ms = 0;
    next_timepulse.q_err = 0;
    next_timepulse.week = 0;
    next_timepulse.flags.time_base = 1;
    next_timepulse.flags.utc = 0;
    next_timepulse.reserved1 = 0;

}

bool
GpsLea6T::start_serial(uint32_t baud_rate)
{
    serial_port.begin(baud_rate);
    delay(100);
    // Test the connection by polling the baud rate.
    bool is_working = poll_baud_rate_blocking(baud_rate);
    return is_working;
}

void
GpsLea6T::stop_serial()
{
    serial_port.end();
    delay(100);
}


bool
GpsLea6T::disable_timepulse2_frequency(void)
{
    timepulse2_cfg.ant_cable_delay = 0;
    timepulse2_cfg.freq_period = 0;
    timepulse2_cfg.freq_period_lock = 0;
    timepulse2_cfg.pulse_len_ratio = 2147483648;            //Be aware of the scaling factor 0.5^-32: 50% duty cycle = 0.5 * 0.5^-32
    timepulse2_cfg.pulse_len_ratio_lock = 2147483648;
    timepulse2_cfg.user_config_delay = 0;
    timepulse2_cfg.flags.active = 1;
    timepulse2_cfg.flags.lock_gps_freq = 1;
    timepulse2_cfg.flags.locked_other_set = 0;
    timepulse2_cfg.flags.is_freq = 1;
    timepulse2_cfg.flags.is_length = 0;
    timepulse2_cfg.flags.align_to_tow = 1;
    timepulse2_cfg.flags.polarity = 1;
    timepulse2_cfg.flags.grid_utc_gps = 0;

    return send_cfg_tp5(1);
}

bool
GpsLea6T::set_timepulse2_frequency(uint32_t freq, uint32_t freq_lock)
{
    timepulse2_cfg.ant_cable_delay = 0;
    timepulse2_cfg.freq_period = freq;
    timepulse2_cfg.freq_period_lock = freq_lock;
    timepulse2_cfg.pulse_len_ratio = 2147483648;            //Be aware of the scaling factor 0.5^-32: 50% duty cycle = 0.5 * 0.5^-32
    timepulse2_cfg.pulse_len_ratio_lock = 2147483648;
    timepulse2_cfg.user_config_delay = 0;
    timepulse2_cfg.flags.active = 1;
    timepulse2_cfg.flags.lock_gps_freq = 1;
    timepulse2_cfg.flags.locked_other_set = 0;
    timepulse2_cfg.flags.is_freq = 1;
    timepulse2_cfg.flags.is_length = 0;
    timepulse2_cfg.flags.align_to_tow = 1;
    timepulse2_cfg.flags.polarity = 1;
    timepulse2_cfg.flags.grid_utc_gps = 0;

    return send_cfg_tp5(1);
}


bool
GpsLea6T::set_timepulse1_pps()
{
    timepulse1_cfg.ant_cable_delay = 0;
    timepulse1_cfg.freq_period = 1;
    timepulse1_cfg.freq_period_lock = 1;
    timepulse1_cfg.pulse_len_ratio = 500000;            // The scaling only applies to the frequency.
    timepulse1_cfg.pulse_len_ratio_lock = 100000;
    timepulse1_cfg.user_config_delay = 0;
    timepulse1_cfg.flags.active = 1;
    timepulse1_cfg.flags.lock_gps_freq = 1;
    timepulse1_cfg.flags.locked_other_set = 1;
    timepulse1_cfg.flags.is_freq = 1;
    timepulse1_cfg.flags.is_length = 1;
    timepulse1_cfg.flags.align_to_tow = 1;
    timepulse1_cfg.flags.polarity = 1;
    timepulse1_cfg.flags.grid_utc_gps = 0;

    return send_cfg_tp5(0);
}

void
GpsLea6T::set_uart1_baudrate(uint32_t baudrate)
{
    ubx_cfg_prt_uart_t msg;
    msg.prt_id = 1;
    msg.reserved0 = 0;
    msg.tx_ready.en = 0;
    msg.tx_ready.pol = 0;
    msg.tx_ready.pin = 0;
    msg.tx_ready.thres = 0;
    msg.mode.reserved1 = 1;
    msg.mode.charlen = 3;
    msg.mode.parity = 4;
    msg.mode.nstopbits = 0;
    msg.mode.unused0 = 0;
    msg.mode.unused1 = 0;
    msg.mode.unused2 = 0;
    msg.mode.unused3 = 0;
    msg.baud_rate = baudrate;
    msg.in_proto_mask.ubx = 1;
    msg.in_proto_mask.nmea = 1;
    msg.in_proto_mask.rtcm = 1;
    msg.in_proto_mask.unused0 = 0;
    msg.out_proto_mask.ubx = 1;
    msg.out_proto_mask.nmea = 1;
    msg.out_proto_mask.rtcm = 1;
    msg.out_proto_mask.unused0 = 0;
    msg.reserved4 = 0;
    msg.reserved5 = 0;

    send_ublox_message(UBX_CLASS_CFG, UBX_CFG_ID_PRT, &msg, sizeof(msg));
    //confirmed = wait_for_cfg_ack(UBX_CLASS_CFG, UBX_CFG_ID_PRT);
}


void
GpsLea6T::set_usb_baudrate(uint32_t baudrate)
{
    ubx_cfg_prt_uart_t msg;
    msg.prt_id = 3;
    msg.reserved0 = 0;
    msg.tx_ready.en = 0;
    msg.tx_ready.pol = 0;
    msg.tx_ready.pin = 0;
    msg.tx_ready.thres = 0;
    msg.mode.reserved1 = 1;
    msg.mode.charlen = 3;
    msg.mode.parity = 4;
    msg.mode.nstopbits = 0;
    msg.mode.unused0 = 0;
    msg.mode.unused1 = 0;
    msg.mode.unused2 = 0;
    msg.mode.unused3 = 0;
    msg.baud_rate = baudrate;
    msg.in_proto_mask.ubx = 1;
    msg.in_proto_mask.nmea = 1;
    msg.in_proto_mask.rtcm = 1;
    msg.in_proto_mask.unused0 = 0;
    msg.out_proto_mask.ubx = 1;
    msg.out_proto_mask.nmea = 1;
    msg.out_proto_mask.rtcm = 1;
    msg.out_proto_mask.unused0 = 0;
    msg.reserved4 = 0;
    msg.reserved5 = 0;

    send_ublox_message(UBX_CLASS_CFG, UBX_CFG_ID_PRT, &msg, sizeof(msg));
    //confirmed = wait_for_cfg_ack(UBX_CLASS_CFG, UBX_CFG_ID_PRT);
}


uint32_t
GpsLea6T::get_timepulse2_frequency()
{
    return timepulse2_cfg.pulse_len_ratio;
}

uint32_t
GpsLea6T::get_timepulse2_frequency_locked()
{
    return timepulse2_cfg.freq_period_lock;
}

gpsfix_t
GpsLea6T::get_gps_fix()
{
    return (gpsfix_t)navigation_status.gps_fix;
}

bool
GpsLea6T::get_gps_fix_ok()
{
    if (navigation_status.flags.gps_fix_ok == 1)
    {
        return true;
    }
    else
    {
        return false;
    }
}


bool
GpsLea6T::read()
{
    uint8_t data = 0;
    uint8_t bytes_available = 0;
    bool parsed = false;

    bytes_available = serial_port.available();
    for (uint16_t k = 0; k < bytes_available; k++)
    {
        data = serial_port.read();

        reset:
        switch(read_step)
        {
            case SYNC1_FOUND:
                //debug_print("SYNC1_FOUND");
                if (data == SYNC_CHAR_2)
                {
                    read_step = SYNC2_FOUND;
                    //debug_print("SYNC2_FOUND");
                    break;
                }
                // If the second sync char is wrong, reset to initial mode and
                // continue the processing of the current data.
                read_step = WAITING_FOR_SYNC1;
                //debug_print("SYNC2 doesn't match.");
            case WAITING_FOR_SYNC1:
                if (data == SYNC_CHAR_1)
                {
                    read_step = SYNC1_FOUND;
                }
                break;
            case SYNC2_FOUND:
                read_msg_class = data;
                read_cka = data;
                read_ckb = data;
                read_step = CLASS_READ;
                //debug_print("CLASS_READ");
                break;
            case CLASS_READ:
                read_msg_id = data;
                read_cka += data;
                read_ckb += read_cka;
                read_step = MSGID_READ;
                //debug_print("MSGID_READ");
                break;
            case MSGID_READ:
                read_payload_length = data;
                read_cka += data;
                read_ckb += read_cka;
                read_step = LENGTH_BYTE1_READ;
                //debug_print("LENGTH_BYTE1_READ");
                break;
            case LENGTH_BYTE1_READ:
                read_payload_length += (uint16_t)(data << 8);
                read_cka += data;
                read_ckb += read_cka;
                read_payload_counter = 0;
                read_step = LENGTH_BYTE2_READ;
                //debug_print("LENGTH_BYTE2_READ");
                if(read_payload_length > 256) {
                    //debug_print("Too large payload");
                    read_payload_length = 0;
                    read_step = WAITING_FOR_SYNC1;
                    goto reset;
                }
                break;
            case LENGTH_BYTE2_READ:
                //debug_print("LENGTH_BYTE2_READ");
                if (read_payload_counter < sizeof(read_buffer))
                {
                    read_buffer.bytes[read_payload_counter] = data;
                }
                if (++read_payload_counter == read_payload_length)
                {
                    read_step = PAYLOAD_READ;
                    //debug_print("PAYLOAD_READ");
                }
                read_cka += data;
                read_ckb += read_cka;
                break;
            case PAYLOAD_READ:
                if (read_cka != data)
                {
                    //debug_print("Bad cka.");
                    read_step = WAITING_FOR_SYNC1;
                    goto reset;
                }
                read_step = CKA_OK;
                //debug_print("CKA_OK");
                break;
            case CKA_OK:
                read_step = WAITING_FOR_SYNC1;
                if(read_ckb != data)
                {
                    //debug_print("Bad ckb.");
                    break;
                }
                //debug_print("CKB_OK");
                parsed = parse_ubx();
        }
    }
    return parsed;
}


bool
GpsLea6T::parse_ubx()
{
    bool parsed = false;
    if (read_msg_class == UBX_CLASS_ACK)
    {
        debug_print("ACK ");
        switch (read_msg_id)
        {
            case UBX_ACK_ID_ACK:
                msg_ack = read_buffer.ack_ack;
                msg_ack_changed = true;
                /*
                usb_serial.println("Received UBX_ACK_ID_ACK.");
                usb_serial.println(msg_ack.cls_id);
                usb_serial.println(msg_ack.msg_id);
                usb_serial.println("----");
                */
                break;
            default:
                handle_unexpected_msg();
        }
    }
    else if (read_msg_class == UBX_CLASS_CFG)
    {
        switch (read_msg_id)
        {
            case UBX_CFG_ID_TP5:
                //debug_print("Parsing UBX_CFG_ID_TP5");
                //timepulse2_cfg.tp_idx = read_buffer.cfg_tp5.tp_idx;
                if (read_buffer.cfg_tp5.tp_idx == 0)
                {
                    timepulse_cfg = read_buffer.cfg_tp5;
                    parsed = true;
                    cfg_timepulse1_changed = true;
                }
                else if (read_buffer.cfg_tp5.tp_idx == 1)
                {
                    timepulse2_cfg = read_buffer.cfg_tp5;
                    parsed = true;
                    cfg_timepulse2_changed = true;
                }
                break;
            case UBX_CFG_ID_PRT:
                //usb_serial.println("Parsing UBX_CFG_ID_PRT");
                cfg_prt_uart = read_buffer.cfg_prt_uart;
                parsed = true;
                cfg_prt_uart_changed = true;
                break;
            default:
                handle_unexpected_msg();
        }
    }
    else if (read_msg_class == UBX_CLASS_NAV)
    {
        switch (read_msg_id)
        {
            case UBX_NAV_ID_STATUS:
                //debug_print("Parsing UBX_NAV_STATUS");
                navigation_status = read_buffer.nav_status;
                nav_status_changed = true;
                break;

            case UBX_NAV_ID_TIMEUTC:
                //debug_print("Parsing UBX_NAV_TIMEUTC");
                timeutc = read_buffer.nav_timeutc;
                nav_timeutc_changed = true;
                break;

            case UBX_NAV_ID_POSLLH:
                //debug_print("Parsing UBX_NAV_TIMEUTC");
                nav_position = read_buffer.nav_posllh;
                nav_position_changed = true; 
                break;

            default:
                handle_unexpected_msg();
        }

    }
    else if (read_msg_class == UBX_CLASS_MON)
    {

    }
    else if (read_msg_class == UBX_CLASS_TIM)
    {
        switch (read_msg_id)
        {
            case UBX_TIM_ID_TP:
                //debug_print("Parsing UBX_TIM_TP");
                if (!timestamp_buf_empty)
                {
                    timestamp_race_error = true;
                }
                next_timepulse_buf = read_buffer.tim_tp;
                timestamp_buf_empty = false;
                if (!timestamp_init)
                {
                    timestamp_init = true;
                }
                //usb_serial.println(read_buffer.tim_tp.week);
                //usb_serial.println(read_buffer.tim_tp.tow_ms);
                //usb_serial.println(read_buffer.tim_tp.tow_sub_ms);
                break;

            default:
                handle_unexpected_msg();
        }
    }
    else
    {
        handle_unexpected_msg();
    }

    return parsed;
}


void
GpsLea6T::handle_unexpected_msg()
{
    debug_print("Handling an unexpected UBX message.");
}


void
GpsLea6T::send_ublox_message(uint8_t msg_class, uint8_t msg_id, void *msg, uint8_t length)
{
    ubx_header_t header;
    uint8_t ck_a=0, ck_b=0;
    header.sync_char1 = SYNC_CHAR_1;
    header.sync_char2 = SYNC_CHAR_2;
    header.msg_class = msg_class;
    header.msg_id = msg_id;
    header.payload_length = length;

    // Update the checksum using the relevant header part.
    update_ubx_checksum((uint8_t *)&header.msg_class, sizeof(header) - 2, ck_a, ck_b);

    // Update the checksum using the payload.
    update_ubx_checksum((uint8_t *)msg, length, ck_a, ck_b);

    // Send the paket to the serial port.
    serial_port.write((const uint8_t *)&header, sizeof(header));
    serial_port.write((const uint8_t *)msg, length);
    serial_port.write((const uint8_t *)&ck_a, 1);
    serial_port.write((const uint8_t *)&ck_b, 1);
}


uint8_t
GpsLea6T::disable_default_messages()
{
    uint8_t ack_status = 0;
    uint8_t targets[2] = {1, 3};
    uint8_t n_targets = 2;

    // Disable the default messages on the used targest.
    if (send_cfg_msg(NMEA_CLASS_STD, NMEA_STD_ID_GGA, 0, targets, n_targets))
        ack_status |= 1UL << 0;

    if (send_cfg_msg(NMEA_CLASS_STD, NMEA_STD_ID_GLL, 0, targets, n_targets))
        ack_status |= 1UL << 1;

    if (send_cfg_msg(NMEA_CLASS_STD, NMEA_STD_ID_GSA, 0, targets, n_targets))
        ack_status |= 1UL << 2;

    if (send_cfg_msg(NMEA_CLASS_STD, NMEA_STD_ID_GSV, 0, targets, n_targets))
        ack_status |= 1UL << 3;

    if (send_cfg_msg(NMEA_CLASS_STD, NMEA_STD_ID_RMC, 0, targets, n_targets))
        ack_status |= 1UL << 4;

    if (send_cfg_msg(NMEA_CLASS_STD, NMEA_STD_ID_VTG, 0, targets, n_targets))
        ack_status |= 1UL << 5;

    if (send_cfg_msg(NMEA_CLASS_STD, NMEA_STD_ID_ZDA, 0, targets, n_targets))
        ack_status |= 1UL << 6;

    // Disable the NMEA_STD_ID_TXT output using the CFG-INF message.
    // Disable the output for the USB port as well.
    ubx_cfg_inf_t msg;
    msg.protocol_id = 1;
    msg.reserved_0 = 0;
    msg.reserved_1 = 0;
    msg.inf_msg_mask.serial_1.error = 0;
    msg.inf_msg_mask.serial_1.warning = 0;
    msg.inf_msg_mask.serial_1.notice = 0;
    msg.inf_msg_mask.serial_1.debug = 0;
    msg.inf_msg_mask.serial_1.test = 0;
    msg.inf_msg_mask.serial_1.reserved = 0;
    msg.inf_msg_mask.usb.error = 0;
    msg.inf_msg_mask.usb.warning = 0;
    msg.inf_msg_mask.usb.notice = 0;
    msg.inf_msg_mask.usb.debug = 0;
    msg.inf_msg_mask.usb.test = 0;
    msg.inf_msg_mask.usb.reserved = 0;
    send_ublox_message(UBX_CLASS_CFG, UBX_CFG_ID_INF, &msg, sizeof(msg));
    if(wait_for_cfg_ack(UBX_CLASS_CFG, UBX_CFG_ID_INF))
        ack_status |= 1UL << 7;

    return ack_status;
}


bool GpsLea6T::enable_message(uint8_t msg_class, uint8_t msg_id, uint8_t rate)
{
    // Enable a message on the current target which is used to send this
    // message.
    bool confirmed = send_cfg_msg(msg_class, msg_id, rate);
    return confirmed;
}

bool GpsLea6T::enable_message(uint8_t msg_class, uint8_t msg_id, uint8_t rate, uint8_t target)
{
    // Enable a message on the specified target.
    uint8_t targets[1] = {target};
    uint8_t n_targets = 1;
    bool confirmed = send_cfg_msg(msg_class, msg_id, rate, targets, n_targets);
    return confirmed;
}

bool GpsLea6T::enable_message(uint8_t msg_class, uint8_t msg_id, uint8_t rate, uint8_t* targets, uint8_t n_targets)
{
    // Enable a message on the specified target.
    bool confirmed = send_cfg_msg(msg_class, msg_id, rate, targets, n_targets);
    return confirmed;
}

void
GpsLea6T::poll_cfg_prt_uart()
{
    send_ublox_message(UBX_CLASS_CFG, UBX_CFG_ID_PRT, NULL, 0);
}

void
GpsLea6T::poll_cfg_msg(uint8_t msg_class, uint8_t msg_id)
{
    ubx_cfg_msg_rate_poll_t msg;
    msg.msg_class = msg_class;
    msg.msg_id = msg_id;
    send_ublox_message(UBX_CLASS_CFG, UBX_CFG_ID_MSG, &msg, sizeof(msg)); 
}

bool
GpsLea6T::send_cfg_msg(uint8_t msg_class, uint8_t msg_id, uint8_t rate)
{
    bool confirmed = false;
    ubx_cfg_msg_rate_t msg;
    msg.msg_class = msg_class;
    msg.msg_id = msg_id;
    msg.rate = rate;
    send_ublox_message(UBX_CLASS_CFG, UBX_CFG_ID_MSG, &msg, sizeof(msg));
    confirmed = wait_for_cfg_ack(UBX_CLASS_CFG, UBX_CFG_ID_MSG);
    return confirmed;
}

bool
GpsLea6T::send_cfg_msg(uint8_t msg_class, uint8_t msg_id, uint8_t rate, uint8_t* targets, uint8_t n_targets)
{
    bool confirmed = false;
    ubx_cfg_msg_rate_all_t msg;
    msg.msg_class = msg_class;
    msg.msg_id = msg_id;
    msg.rate_ddc = 0;
    msg.rate_uart1 = 0;
    msg.rate_uart2 = 0;
    msg.rate_usb = 0;
    msg.rate_spi = 0;
    msg.rate_reserved = 0;

    for (int k = 0; k < n_targets; k++)
    {
        switch(targets[k])
        {
            case 1:
                msg.rate_uart1 = rate;
                break;
            case 3:
                msg.rate_usb = rate;
                break;
        }
    }
    send_ublox_message(UBX_CLASS_CFG, UBX_CFG_ID_MSG, &msg, sizeof(msg));
    confirmed = wait_for_cfg_ack(UBX_CLASS_CFG, UBX_CFG_ID_MSG);
    return confirmed;
}

void
GpsLea6T::poll_cfg_tp5(uint8_t timepulse_id)
{
    ubx_cfg_tp5_poll_t msg;
    msg.tp_idx = timepulse_id;
    send_ublox_message(UBX_CLASS_CFG, UBX_CFG_ID_TP5, &msg, sizeof(msg));
}

bool
GpsLea6T::send_cfg_tp5(uint8_t tp_idx)
{
    if (tp_idx == 0)
    {
        send_ublox_message(UBX_CLASS_CFG, UBX_CFG_ID_TP5, &timepulse1_cfg, sizeof(timepulse1_cfg));
    }
    else if (tp_idx == 1)
    {
        send_ublox_message(UBX_CLASS_CFG, UBX_CFG_ID_TP5, &timepulse2_cfg, sizeof(timepulse2_cfg));
    }

    return wait_for_cfg_ack(UBX_CLASS_CFG, UBX_CFG_ID_TP5);
}


void
GpsLea6T::poll_nav_status()
{
    send_ublox_message(UBX_CLASS_NAV, UBX_NAV_ID_STATUS, NULL, 0);
}


void
GpsLea6T::poll_nav_posllh()
{
    send_ublox_message(UBX_CLASS_NAV, UBX_NAV_ID_POSLLH, NULL, 0);
}


bool
GpsLea6T::wait_for_cfg_ack(uint8_t msg_class, uint8_t msg_id)
{
    unsigned timeout = 1000;          // The timeout in milliseconds.
    unsigned long start = 0;
    unsigned long current = 0;
    unsigned long elapsed = 0;
    uint8_t gps_byte_available = 0;


    start = millis();
    while (elapsed < timeout)
    {
        gps_byte_available = serial_port.available();
        if (gps_byte_available > 0)
        {
            read();
        }

        if (msg_ack_changed)
        {
            if ((msg_ack.cls_id == msg_class) && (msg_ack.msg_id == msg_id))
            {
                usb_serial.println("Message acknowledged.");
                msg_ack_changed = false;
                return true;
            }
            else
            {
                // TODO: Handle the reception of ACK messages not fitting the
                // required msg_class and msg_id.
                msg_ack_changed = false;
            }
        }
        current = millis();
        elapsed = current - start;
    }
    usb_serial.println("GPS ACK Timeout");
    usb_serial.println(msg_class);
    usb_serial.println(msg_id);
    usb_serial.println("#####");
    return false;
}

uint32_t
GpsLea6T::detect_baud_rate(uint32_t baud_1, uint32_t baud_2)
{
    uint32_t active_baud = 0;

    // Try the first baud rate.
    if (test_baud_rate(baud_1))
        active_baud = cfg_prt_uart.baud_rate;
    else if (test_baud_rate(baud_2))
        active_baud = cfg_prt_uart.baud_rate;

    return active_baud;
}

bool
GpsLea6T::test_baud_rate(uint32_t baud_rate)
{
    bool confirmed = false;
    serial_port.end();
    delay(100);
    serial_port.begin(baud_rate);
    delay(100);
    confirmed = poll_baud_rate_blocking(baud_rate);
    serial_port.end();
    delay(100);
    return confirmed;
}

bool
GpsLea6T::poll_baud_rate_blocking(uint32_t baud_rate)
{
    unsigned timeout = 3000;          // The timeout in milliseconds.
    unsigned long start = 0;
    unsigned long current = 0;
    unsigned long elapsed = 0;
    uint8_t gps_byte_available = 0;

    poll_cfg_prt_uart();
    start = millis();
    while (elapsed < timeout)
    {
        gps_byte_available = serial_port.available();
        if (gps_byte_available > 0)
        {
            read();
        }
        if (cfg_prt_uart_changed) 
        {
            cfg_prt_uart_changed = false;
            usb_serial.println("Working baud rate found.");
            usb_serial.println(baud_rate);
            return true;
        }
        current = millis();
        elapsed = current - start;
    }
    usb_serial.println("Baud Timeout");
    usb_serial.println(baud_rate);
    usb_serial.println("#####");
    return false;
}


void
GpsLea6T::enable_usb()
{
    // Enable the usb power supply. The pin is active low.
    digitalWrite(GPS_ENABLE_USB, 0);

    // Configure the port baudrate.
    set_usb_baudrate(GPS_USB_BAUDRATE);

    // Enable the raw messages needed for post processing with rtklib.
    enable_message(UBX_CLASS_RXM, UBX_RXM_ID_RAW, 1, 3);
    enable_message(UBX_CLASS_RXM, UBX_RXM_ID_SFRB, 1, 3);
    
}


void
GpsLea6T::disable_usb()
{
    // Disable the usb power supply. The pin is active low.
    digitalWrite(GPS_ENABLE_USB, 1);

    // Setting the rate to 0 disables the message.
    enable_message(UBX_CLASS_RXM, UBX_RXM_ID_RAW, 0, 3);
    enable_message(UBX_CLASS_RXM, UBX_RXM_ID_SFRB, 0, 3);
    
}
