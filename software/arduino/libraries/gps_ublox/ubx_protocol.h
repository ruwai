// -*- coding: utf-8 -*-
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// pSysmon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef ubloxprotocol_h
#define ubloxprotocol_h

#include <Arduino.h>

// Used to pack the structures to remove eventual byte overhead.
#ifndef PACKED
    #define PACKED __attribute__((packed))
#endif


#define SYNC_CHAR_1 0xB5
#define SYNC_CHAR_2 0x62

#define TIMEPULSE  0
#define TIMEPULSE2 1

#define UBX_CFG_TP5_SCALING 4294967296


/*! Compute the checksum values of a UBX paket. */
void update_ubx_checksum(uint8_t* payload, uint8_t length, uint8_t &ck_a, uint8_t &ck_b);


//! The UBX class IDs.
typedef enum ubx_msg_class_e {
    UBX_CLASS_NAV = 0x01,
    UBX_CLASS_RXM = 0x02,
    UBX_CLASS_INF = 0x04,
    UBX_CLASS_ACK = 0x05,
    UBX_CLASS_CFG = 0x06,
    UBX_CLASS_MON = 0x0A,
    UBX_CLASSS_AID = 0x0B,
    UBX_CLASS_TIM = 0x0D,
    UBX_CLASS_ESF = 0x10
} ubx_msg_class_t;

//! The UBX CFG class message IDs.
typedef enum cfg_msg_id_e {
    UBX_CFG_ID_PRT = 0x00,
    UBX_CFG_ID_MSG = 0x01,
    UBX_CFG_ID_INF = 0x02,
    UBX_CFG_ID_TP5 = 0x31
} cfg_msg_id_t;

//! The UBX NAV class message IDs.
typedef enum nav_msg_id_e {
    UBX_NAV_ID_POSLLH = 0x02,
    UBX_NAV_ID_STATUS = 0x03,
    UBX_NAV_ID_TIMEUTC = 0x21
} nav_msg_id_t;

//! The UBX ACK class message IDs.
typedef enum ack_msg_id_e {
    UBX_ACK_ID_NAK = 0x00,
    UBX_ACK_ID_ACK = 0x01
} ack_msg_id_t;

//! The UBX TIM class message IDs.
typedef enum tim_msg_id_e {
    UBX_TIM_ID_TP = 0x01
} tim_msg_id_t;

//! The UBX RXM class message IDs.
typedef enum rxm_msg_id_e {
    UBX_RXM_ID_RAW = 0x10,
    UBX_RXM_ID_SFRB = 0x11,
    UBX_RXM_ID_SVSI = 0x20,
    UBX_RXM_ID_ALM = 0x30,
    UBX_RXM_ID_EPH = 0x31,
    UBX_RXM_ID_PMREQ = 0x41
} rxm_msg_id_t;


//! The NMEA class IDs.
typedef enum nmea_msg_class_e {
    NMEA_CLASS_STD = 0xF0,
    NMEA_CLASS_UBX = 0xF1
} nmea_msg_class_t;

//! The NMEA message IDs.
typedef enum nmea_std_msg_id_e {
    NMEA_STD_ID_DTM = 0x0A,
    NMEA_STD_ID_GBS = 0x09,
    NMEA_STD_ID_GGA = 0x00,
    NMEA_STD_ID_GLL = 0x01,
    NMEA_STD_ID_GPQ = 0x40,
    NMEA_STD_ID_GRS = 0x06,
    NMEA_STD_ID_GSA = 0x02,
    NMEA_STD_ID_GST = 0x07,
    NMEA_STD_ID_GSV = 0x03,
    NMEA_STD_ID_RMC = 0x04,
    NMEA_STD_ID_THS = 0x0E,
    NMEA_STD_ID_TXT = 0x41,
    NMEA_STD_ID_VTG = 0x05,
    NMEA_STD_ID_ZDA = 0x08
} nmea_std_msg_id_t;


//! The UBX paket header.
typedef struct PACKED ubx_header_s {
    uint8_t sync_char1;
    uint8_t sync_char2;
    uint8_t msg_class;
    uint8_t msg_id;
    uint16_t payload_length;
} ubx_header_t;

//! The UBX CFG-MSG poll message.
typedef struct PACKED ubx_cfg_msg_rate_poll_s {
    uint8_t msg_class;
    uint8_t msg_id;
} ubx_cfg_msg_rate_poll_t;

//! The UBX CFG-MSG set current target message.
typedef struct PACKED ubx_cfg_msg_rate_s {
    uint8_t msg_class;
    uint8_t msg_id;
    uint8_t rate;
} ubx_cfg_msg_rate_t;

//! The UBX CFG-MSG set all target message rates.
typedef struct PACKED ubx_cfg_msg_rate_all_s {
    uint8_t msg_class;
    uint8_t msg_id;
    uint8_t rate_ddc;
    uint8_t rate_uart1;
    uint8_t rate_uart2;
    uint8_t rate_usb;
    uint8_t rate_spi;
    uint8_t rate_reserved;
} ubx_cfg_msg_rate_all_t;

//! The UBX CFG-TP5 poll message.
typedef struct PACKED ubx_cfg_tp5_poll_s {
    uint8_t tp_idx;
} ubx_cfg_tp5_poll_t;


// The UBX CFG-TP5 flags bitfield.
typedef struct PACKED ubx_cfg_tp5_flags_s {
    uint8_t active : 1;
    uint8_t lock_gps_freq : 1;
    uint8_t locked_other_set : 1;
    uint8_t is_freq : 1;
    uint8_t is_length : 1;
    uint8_t align_to_tow : 1;
    uint8_t polarity : 1;
    uint8_t grid_utc_gps : 1;
    uint32_t reserved : 24;
} ubx_cfg_tp5_flags_t;


//! The UBX CFG-TP5 message.
typedef struct PACKED ubx_cfg_tp5_s {
    uint8_t tp_idx;
    uint8_t reserved0;
    uint16_t reserved1;
    int16_t ant_cable_delay;
    int16_t rf_group_delay;
    uint32_t freq_period;
    uint32_t freq_period_lock;
    uint32_t pulse_len_ratio;
    uint32_t pulse_len_ratio_lock;
    int32_t user_config_delay;
    ubx_cfg_tp5_flags_t flags;
} ubx_cfg_tp5_t;


//! The UBX CFG-PRT txready bitfield.
typedef struct PACKED ubx_cfg_prt_txready_s {
    uint8_t en      : 1;
    uint8_t pol     : 1;
    uint8_t pin     : 5;
    uint16_t thres   : 9;
} ubx_cfg_prt_txready_t;

//! The UBX CFG-PRT mode bitfield.
typedef struct PACKED ubx_cfg_prt_mode_s {
    uint8_t unused0     : 4;
    uint8_t reserved1   : 1;
    uint8_t unused1     : 1;
    uint8_t charlen     : 2;
    uint8_t unused2     : 1;
    uint8_t parity      : 3;
    uint8_t nstopbits   : 2;
    uint32_t unused3    : 18;
} ubx_cfg_prt_mode_t;

//! The UBX CFG-PRT inProtoMask bitfield.
typedef struct PACKED ubx_cfg_prt_inprotomask_s {
    uint8_t ubx         : 1;
    uint8_t nmea        : 1;
    uint8_t rtcm        : 1;
    uint16_t unused0    : 13;
} ubx_cfg_prt_inprotomask_t;

//! The UBX CFG-PRT outProtoMask bitfield.
typedef struct PACKED ubx_cfg_prt_outprotomask_s {
    uint8_t ubx         : 1;
    uint8_t nmea        : 1;
    uint8_t rtcm        : 1;
    uint16_t unused0    : 13;
} ubx_cfg_prt_outprotomask_t;

//! The UBX CFG-PRT message.
typedef struct PACKED ubx_cfg_prt_uart_s {
    uint8_t prt_id;
    uint8_t reserved0;
    ubx_cfg_prt_txready_t tx_ready;
    ubx_cfg_prt_mode_t mode;
    uint32_t baud_rate;
    ubx_cfg_prt_inprotomask_t in_proto_mask;
    ubx_cfg_prt_outprotomask_t out_proto_mask;
    uint16_t reserved4;
    uint16_t reserved5;
} ubx_cfg_prt_uart_t;


//! The UBX NAV-STATUS flags bitfield.
typedef struct PACKED ubx_nav_status_flags_s {
    uint8_t gps_fix_ok : 1;
    uint8_t diff_so_in : 1;
    uint8_t wkn_set : 1;
    uint8_t tow_set : 1;
    uint8_t reserved : 4;
} ubx_nav_status_flags_t;

//! THE UBX NAV-STATUS fixStat bitfield.
typedef struct PACKED ubx_nav_status_fixstat_s {
    uint8_t dgps_i_stat : 1;
    uint8_t reserved : 5;
    uint8_t map_matching : 2;
} ubx_nav_status_fixstat_t;

//! The UBX NAV-STATUS flags2 bitfield.
typedef struct PACKED ubx_nav_status_flags2_s {
    uint8_t psm_state : 2;
    uint8_t reserved : 6;
} ubx_nav_status_flags2_t;

//! The UBX NAV-STATUS message.
typedef struct PACKED ubx_nav_status_s {
    uint32_t i_tow;
    uint8_t gps_fix;
    ubx_nav_status_flags_t flags;
    ubx_nav_status_fixstat_t fix_stat;
    ubx_nav_status_flags2_t flags2;
    uint32_t ttff;
    uint32_t msss;
} ubx_nav_status_t;

//! The UBX NAV-TIMEUTC valid bitfield.
typedef struct PACKED ubx_nav_timeutc_valid_s {
    uint8_t validtow : 1;
    uint8_t validwkn : 1;
    uint8_t validutc : 1;
} ubx_nav_timeutc_valid_t;


//! The UBX NAV-TIMEUTC message.
typedef struct PACKED ubx_nav_timeutc_s {
    uint32_t i_tow;
    uint32_t t_acc;
    int32_t nano;
    uint16_t year;
    uint8_t month;
    uint8_t day;
    uint8_t hour;
    uint8_t min;
    uint8_t sec;
    ubx_nav_timeutc_valid_t valid;
} ubx_nav_timeutc_t;


//! The UBX NAV-POSLLH message.
typedef struct PACKED ubx_nav_posllh_s {
    uint32_t i_tow;
    int32_t lon;
    int32_t lat;
    int32_t height;
    int32_t h_msl;
    uint32_t h_acc;
    uint32_t v_acc;
} ubx_nav_posllh_t;


//! The UBX TIM-TP flags bitfield.
typedef struct PACKED ubx_tim_tp_flags_s {
    uint8_t time_base : 1;
    uint8_t utc : 1;
    uint8_t reserved : 6;
} ubx_tim_tp_flags_t;

//! The UBX TIM-TP message.
typedef struct PACKED ubx_tim_tp_s {
    uint32_t tow_ms;
    uint32_t tow_sub_ms;
    int32_t q_err;
    uint16_t week;
    ubx_tim_tp_flags_t flags;
    uint8_t reserved1;
} ubx_tim_tp_t;


//! The UBX MON-HW flags bitfield.
typedef struct PACKED ubx_mon_hw_flags_s {
    uint8_t rtccalib : 1;
    uint8_t safeboot : 1;
    uint8_t jamming_state: 2;
    uint8_t reserved : 4;
} ubx_mon_hw_flags_t;


//! The UBX MON-HW message.
typedef struct PACKED ubx_mon_hw_s {
    uint32_t pin_sel;
    uint32_t pin_bank;
    uint32_t pin_dir;
    uint32_t pin_val;
    uint16_t noise_per_ms;
    uint16_t agc_cnt;
    uint8_t a_status;
    uint8_t a_power;
    ubx_mon_hw_flags_t flags;
    uint8_t reserved1;
    uint32_t used_mask;
    uint8_t vp[25];
    uint8_t jam_ind;
    uint16_t reserved3;
    uint32_t pin_irq;
    uint32_t pull_h;
    uint32_t pull_l;
} ubx_mon_hw_t;


//! The UBX ACK-ACK message.
typedef struct PACKED ubx_ack_ack_s {
    uint8_t cls_id;
    uint8_t msg_id;
} ubx_ack_ack_t;


// The UBX CFG-INF target_mask bitfiedl.
typedef struct PACKED ubx_cfg_inf_target_mask_s {
    uint8_t error    : 1;
    uint8_t warning  : 1;
    uint8_t notice   : 1;
    uint8_t debug    : 1;
    uint8_t test     : 1;
    uint8_t reserved : 3;
} ubx_cfg_inf_target_mask_t;

// The UBX CFG-INF inf_msg_mask bitfield.
typedef struct PACKED ubx_cfg_inf_inf_msg_mask_s {
    ubx_cfg_inf_target_mask_t ddc;
    ubx_cfg_inf_target_mask_t serial_1;
    ubx_cfg_inf_target_mask_t serial_2;
    ubx_cfg_inf_target_mask_t usb;
    ubx_cfg_inf_target_mask_t spi;
    ubx_cfg_inf_target_mask_t reserved;
} ubx_cfg_inf_inf_msg_mask_t;

//! The UBX CFG-INF message.
typedef struct PACKED ubx_cfg_inf_s {
    uint8_t protocol_id;
    uint8_t reserved_0;
    uint8_t reserved_1;
    ubx_cfg_inf_inf_msg_mask_t inf_msg_mask;

} ubx_cfg_inf_t;
#endif
