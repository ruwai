// -*- coding: utf-8 -*-
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// pSysmon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <timer16.h>

Timer16::Timer16(volatile uint8_t* tccra_, volatile uint8_t* tccrb_,
                 volatile uint8_t* tccrc_, volatile uint16_t* tcnt_,
                 volatile uint16_t* ocra_, volatile uint16_t* ocrb_,
                 volatile uint16_t* icr_, volatile uint8_t* timsk_,
                 volatile uint8_t* tifr_)
{
    tccra = tccra_;
    tccrb = tccrb_;
    tccrc = tccrc_;
    tcnt = tcnt_;
    ocra = ocra_;
    ocrb = ocrb_;
    icr = icr_;
    timsk = timsk_;
    tifr = tifr_;

    clockselect = 0;
}


void Timer16::begin(uint32_t microseconds)
{
    *tccra = 0;
    *tccrb = 0;

    *tccrb |= _BV(WGM13);
    set_period(microseconds);

}


void Timer16::start()
{
    *tcnt = 0;
    *timsk |= _BV(TOIE1);
    *tccrb |= clockselect;
}


void Timer16::stop()
{
    *timsk &= ~_BV(TOIE1);
    *tccrb &= ~(_BV(CS12) | _BV(CS11) | _BV(CS10));
}


void Timer16::set_period(uint32_t microseconds)
{
    uint32_t top;
    clockselect = 0;

    top = (F_CPU / 2000000) * microseconds;

    if (top < TIMER16_RESOLUTION)
    {
        clockselect = _BV(CS10);
    }
    else if (top < TIMER16_RESOLUTION * 8)
    {
        clockselect = _BV(CS11);
        top = top / 8;
    }
    else if (top < TIMER16_RESOLUTION * 64)
    {
        clockselect = _BV(CS11) | _BV(CS10);
        top = top / 64;
    }
    else if (top < TIMER16_RESOLUTION * 256)
    {
        clockselect = _BV(CS12);
        top = top / 256;
    }
    else if (top < TIMER16_RESOLUTION * 1024)
    {
        clockselect = _BV(CS12) | _BV(CS10);
        top = top / 1024;
    }

    *icr = (uint16_t) top;
    //*tccrb |= clockselect;
}


// Preinstantiate objects.
#if defined(TCCR1A)
    Timer16 Timer1(&TCCR1A, &TCCR1B, &TCCR1C, &TCNT1, &OCR1A, &OCR1B, &ICR1, &TIMSK1, &TIFR1);
#endif
