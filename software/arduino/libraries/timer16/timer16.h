// -*- coding: utf-8 -*-
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// pSysmon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef timer16_h
#define timer16_h

#include <Arduino.h>

#define TIMER16_RESOLUTION 65536

class Timer16
{
    public:
        Timer16(volatile uint8_t* tccra_, volatile uint8_t* tccrb_,
                volatile uint8_t* tccrc_, volatile uint16_t* tcnt,
                volatile uint16_t* ocra, volatile uint16_t* ocrb,
                volatile uint16_t* icr, volatile uint8_t* timsk,
                volatile uint8_t* tifr);

        /*! Initialize the timer. */
        void begin(uint32_t microseconds);

        /*! Start the timer. */
        void start(void);

        /*! Stop the timer. */
        void stop(void);

    private:
        volatile uint8_t *tccra;
        volatile uint8_t *tccrb;
        volatile uint8_t *tccrc;
        volatile uint16_t *tcnt;
        volatile uint16_t *ocra;
        volatile uint16_t *ocrb;
        volatile uint16_t *icr;
        volatile uint8_t *timsk;
        volatile uint8_t *tifr;

        uint8_t clockselect;

        /*! Set the period of the timer. */
        void set_period(uint32_t microseconds);
};


#if defined(TCCR1A)
    extern Timer16 Timer1;
#endif

#endif
