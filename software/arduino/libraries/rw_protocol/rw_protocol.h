// -*- coding: utf-8 -*-
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// ruwai is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef rw_protocol_h
#define rw_protocol_h

#include <Arduino.h>

// Used to pack the structures to remove eventual byte overhead.
#define PACKED __attribute__((__packed__))

#define RUWAI_SYNC_CHAR1 0xD8
#define RUWAI_SYNC_CHAR2 0xBF

/*! Update the fletcher16 checksum of a RUWAI paket. */
void update_fletcher16_checksum(uint8_t* data, uint8_t length, uint8_t &ck0, uint8_t &ck1);


//! The RUWAI class IDs.
typedef enum rw_msg_class_e {
    RW_CLASS_CFG = 0x00,
    RW_CLASS_SOH = 0x01,
    RW_CLASS_SMP = 0x02,
    RW_CLASS_ACK = 0x03
} rw_msg_class_t;


//! The RUWAI CFG class message IDs.
typedef enum rw_msg_cfg_id_e {
    RW_CFG_ID_SPS = 0x00,
    RW_CFG_ID_CHANNELS = 0x01,
    RW_CFG_ID_MODE = 0x02,
    RW_CFG_ID_GPS = 0x03
} rw_msg_cfg_id_t;

//! The RUWAI SOH class message IDs.
typedef enum rw_msg_soh_id_e {
    RW_SOH_ID_REQUEST = 0x00,
    RW_SOH_ID_STATUS = 0x01,
    RW_SOH_ID_VERSION = 0x02,
    RW_SOH_ID_GPSPOS = 0x03,
    RW_SOH_ID_ENVDATA = 0x04
} rw_msg_soh_id_t;


//! The RUWAI SMP class message IDs.
typedef enum rw_msg_smp_id_e {
    RW_SMP_ID_24BIT = 0x00,
    RW_SMP_ID_24BIT_TSP = 0x01,
    RW_SMP_ID_10BIT = 0x02,
    RW_SMP_ID_DIGITAL = 0x03,
    RW_SMP_ID_TIMESTAMP_MARKER = 0x04,
    RW_SMP_ID_TIMESTAMP = 0x05,
    RW_SMP_ID_UTC_TIME = 0x06
} rw_msg_smp_id_t;

//! The RUWAI ACK class message IDs.
typedef enum rw_msg_ack_id_e {
    RW_ACK_ID_SYNC = 0x00,
    RW_ACK_ID_CFG = 0x01
} rw_msg_ack_id_t;

//! The RUWAI Status types.
typedef enum rw_soh_status_type_e {
    RW_STATUS_ERROR = 0x00,
    RW_STATUS_WARNING = 0x01,
    RW_STATUS_NOTICE = 0x02,
    RW_STATUS_INFO = 0x03,
    RW_STATUS_DEBUG = 0x04
} rw_soh_status_type_t;


//! The RUWAI paket header.
typedef struct PACKED rw_header_s {
    uint8_t sync_char1;
    uint8_t sync_char2;
    uint8_t msg_class;
    uint8_t msg_id;
    uint16_t payload_length;
} rw_header_t;


//! The RUWAI SMP-24BIT message.
typedef struct PACKED rw_smp_24bit_s {
    int32_t samples[4];
} rw_smp_24bit_t;

//! The RUWAI SMP-24BIT_TSP message.
typedef struct PACKED rw_smp_24bit_tsp_flags_s {
    uint8_t utc : 1;
    uint8_t timebase : 1;
    uint8_t gps_fix : 4;
    uint8_t gps_fix_ok : 1;
    uint8_t reserved : 1;
} rw_smp_24bit_tsp_flags_t;

typedef struct PACKED rw_smp_24bit_tsp_s {
    uint16_t week;
    uint32_t tow_ms;
    uint32_t tow_sub_ms;
    uint32_t sec_slts;
    rw_smp_24bit_tsp_flags_t flags;
    int32_t samples[4];
} rw_smp_24bit_tsp_t;


//! The Ruwai SMP-TIMESTAMP-MARKER message.
typedef struct PACKED rw_smp_timestamp_marker_s {
    uint16_t id;
} rw_smp_timestamp_marker_t;


//! The Ruwai SMP-UTC-TIME message.
typedef struct PACKED rw_smp_utc_time_e {
    uint8_t id;
    uint16_t year;
    uint8_t month;
    uint8_t day;
    uint8_t hour;
    uint8_t min;
    uint8_t sec;
    uint8_t validutc;
    uint8_t gps_fix;
    uint8_t gps_fix_ok;
} rw_smp_utc_time_t;


//! The Ruwai SMP-TIMESTAMP message.
typedef struct PACKED rw_smp_timestamp_s {
    uint16_t week;
    uint32_t tow_ms;
    uint32_t tow_sub_ms;
    uint8_t timebase;
    uint8_t utc;
} rw_smp_timestamp_t;


//! The Ruwai CFG-SPS message.
typedef struct PACKED rw_cfg_sps_s {
    uint16_t sps;
} rw_cfg_sps_t;


//! The Ruwai CFG-CHANNELS message.
typedef struct rw_cfg_channels_active_flags_s {
    uint8_t channel1: 1;
    uint8_t channel2: 1;
    uint8_t channel3: 1;
    uint8_t channel4: 1;
    uint8_t reserved: 4;
} rw_cfg_channels_active_flags_t;


typedef struct rw_cfg_channels_s {
    rw_cfg_channels_active_flags_t active;
    uint8_t gain_channel1;
    uint8_t gain_channel2;
    uint8_t gain_channel3;
    uint8_t gain_channel4;
} rw_cfg_channels_t;

//! The Ruwai CFG-MODE message.
typedef struct rw_cfg_mode_s {
    uint8_t mode;       // 0: control; 1: record
} rw_cfg_mode_t;


typedef struct rw_cfg_gps_port_flags_s {
    uint8_t usb: 1;
    uint8_t reserved: 7;
} rw_cfg_gps_port_flags_t;


//! The Ruwai CFG-GPS message.
typedef struct rw_cfg_gps_s {
    rw_cfg_gps_port_flags_t ports;
} rw_cfg_gps_t;


//! The Ruwai ACK-SYNC message.
typedef struct rw_ack_sync_s {
    uint8_t sync;
    uint8_t ack;
    uint8_t seq_num;
    uint8_t ack_num;
} rw_ack_sync_t;

//! The Ruwai ACK-CFG message.
typedef struct rw_ack_cfg_s {
    uint8_t msg_class;
    uint8_t msg_id;
} rw_ack_cfg_t;


//! The Ruwai SOH-REQUEST message.
typedef struct rw_soh_request_s {
    uint8_t msg_id;
} rw_soh_request_t;


// TODO: Make the status a variable length field.
//! The Ruwai SOH-STATUS message.
typedef struct rw_soh_status_s {
    uint8_t type;
    char status[100];
} rw_soh_status_t;


//! The Ruwai SOH-GPSPOS message.
typedef struct rw_soh_gpspos_s {
    uint32_t i_tow;
    int32_t lon;
    int32_t lat;
    int32_t height;
} rw_soh_gpspos_t;

//! The Ruwai SOH-ENVDATA message.
typedef struct rw_soh_envdata_s {
    int32_t temp_gts;
} rw_soh_envdata_t;


//! The Ruwai SOH-VERSION message.
typedef struct rw_soh_version_s {
    char version[20];
} rw_soh_version_t;

#endif  // rw_protocol_h
