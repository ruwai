// -*- coding: utf-8 -*-
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// pSysmon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef rw_adc_h
#define rw_adc_h

#include <ruwai.h>
#include <rw_pga281.h>

#define ADC_OVERSAMPLING_HIGH_RES 128
#define ADC_OVERSAMPLING 64

// The CLKDIV values for the individual modes.
#define ADC_CLKDIV_HIGHSPEED_1 4
#define ADC_CLKDIV_HIGHRES_1 4
#define ADC_CLKDIV_LOWPOW_0 4
#define ADC_CLKDIV_LOWPOW_1 8
#define ADC_CLKDIV_LOWSPEED_0 8
#define ADC_CLKDIV_LOWSPEED_1 40

// The PWDN port mapping on the port expander.
#define PCF_PORT_PWDN1 0
#define PCF_PORT_PWDN2 1
#define PCF_PORT_PWDN3 2
#define PCF_PORT_PWDN4 3

// The mapping of the ADC pins to the Arduino pins.
#define ADC_CLKDIV 41
#define ADC_DRDY 19
#define ADC_FORMAT0 25
#define ADC_MODE0 23
#define ADC_MODE1 24
#define ADC_SYNC 22
#define ADC_DRDY_INTERRUPT 4


typedef enum adc_mode_e
{
    ADC_MODE_HIGHSPEED = 0,
    ADC_MODE_HIGHRES,
    ADC_MODE_LOWPOW_CLKDIV_0,
    ADC_MODE_LOWPOW_CLKDIV_1,
    ADC_MODE_LOWSPEED_CLKDIV_0,
    ADC_MODE_LOWSPEED_CLKDIV_1
} adc_mode_t;

typedef enum adc_output_format_e
{
    ADC_FORMAT_SPI_TDM_DYNAMIC = 0,
    ADC_FORMAT_SPI_TDM_FIXED
} adc_output_format_t;

typedef struct adc_channel_pwdn_s {
    uint8_t pwdn1 : 1;
    uint8_t pwdn2 : 1;
    uint8_t pwdn3 : 1;
    uint8_t pwdn4 : 1;
    uint8_t reserved : 4;
} adc_channel_pwdn_t;

class RwAdc
{
    public:
        /*! The constructor. */
        RwAdc(uint8_t port_expander_address, uint8_t pga1_address, uint8_t pga2_address, uint8_t pga3_address, uint8_t pga4_address);

        /*! Initialize the ADC. */
        void begin(adc_mode_t adc_mode, adc_output_format_t adc_output_format);

        /*! Stop the ADC SPI. */
        void end(void);

        /*! Enable a channel. */
        void enable_channel(uint8_t channel_number);

        /*! Disable a channel. */
        void disable_channel(uint8_t channel_number);

        /*! Set the gain of a channel. */
        void set_gain(uint8_t channel_number, pga_gain_t gain);

        /*! Set the mode. */
        void set_mode(adc_mode_t adc_mode);

        /*! Set the output format. */
        void set_output_format(adc_output_format_t adc_output_format);

        /*! Get the clk frequency needed for the given sampling rate. */
        uint32_t get_clk_freq(uint16_t sampling_rate);

        /*! Request the digitized data from the ADC. */
        void request_data(void);

        /*! The digitized samples */
        //uint8_t samples[4][3];
        int32_t samples[4];

    private:
        uint16_t sampling_rate;
        uint8_t pwdn_port_expander;
        adc_output_format_t output_format;
        adc_mode_t mode;
        RwPga281 pga1;
        RwPga281 pga2;
        RwPga281 pga3;
        RwPga281 pga4;
        uint16_t clkdiv;
        uint8_t oversampling;
        uint8_t channel1_enable;
        uint8_t channel2_enable;
        uint8_t channel3_enable;
        uint8_t channel4_enable;

        int32_t convert_24bit_to_int(uint32_t);

};

#endif
