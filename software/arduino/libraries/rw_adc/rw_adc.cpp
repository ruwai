// -*- coding: utf-8 -*-
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// pSysmon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <rw_adc.h>
#include <ruwai.h>
#include <SPI.h>
#include <Wire.h>

extern volatile bool samples_ready;

RwAdc::RwAdc(uint8_t port_expander_address, uint8_t pga1_address, uint8_t pga2_address, uint8_t pga3_address, uint8_t pga4_address):pga1(pga1_address), pga2(pga2_address), pga3(pga3_address), pga4(pga4_address)
{
    pwdn_port_expander = port_expander_address;
}

void
RwAdc::begin(adc_mode_t adc_mode, adc_output_format_t adc_output_format)
{
    // Initialize the Arduino pins.
    //pinMode(ADC_DRDY, INPUT);
    pinMode(ADC_SYNC, OUTPUT);
    pinMode(ADC_MODE0, OUTPUT);
    pinMode(ADC_MODE1, OUTPUT);
    pinMode(ADC_FORMAT0, OUTPUT);
    pinMode(ADC_CLKDIV, OUTPUT);

    // Initialize the SPI interface.
    SPI.setDataMode(SPI_MODE1);
    SPI.setBitOrder(MSBFIRST);
    // TODO: Check which clock divider fits for which sampling rate.
    SPI.setClockDivider(SPI_CLOCK_DIV4);
    SPI.begin();
    delay(200);

    // Initialize the channels.
    channel1_enable = 1;
    channel2_enable = 1;
    channel3_enable = 1;
    channel4_enable = 1;

    set_mode(adc_mode);
    set_output_format(adc_output_format);

    digitalWrite(ADC_SYNC, LOW);
}

void
RwAdc::end(void)
{
    SPI.end();
    delay(200);
}


void
RwAdc::request_data(void)
{
    uint32_t tmp;
    static uint32_t counter = 65536;

    switch (output_format)
    {
        case ADC_FORMAT_SPI_TDM_DYNAMIC:
            /*
            if (channel1_enable)
            {
                // Request data of channel 1.
                samples[0] = SPI.transfer(0);
                samples[0] = (samples[0] << 8) + SPI.transfer(0);
                samples[0] = (samples[0] << 8) + SPI.transfer(0);
            }
            else
            {
                samples[0] = 0;
            }

            if (channel2_enable)
            {
                // Request data of channel 2.
                samples[1] = SPI.transfer(0);
                samples[1] = (samples[1] << 8) + SPI.transfer(0);
                samples[1] = (samples[1] << 8) + SPI.transfer(0);
            }
            else
            {
                samples[1] = 0;
            }

            if (channel3_enable)
            {
                // Request data of channel 3.
                samples[2] = SPI.transfer(0);
                samples[2] = (samples[2] << 8) + SPI.transfer(0);
                samples[2] = (samples[2] << 8) + SPI.transfer(0);
            }
            else
            {
                samples[2] = 0;
            }

            if (channel4_enable)
            {
                // Request data of channel 4.
                samples[3] = SPI.transfer(0);
                samples[3] = (samples[3] << 8) + SPI.transfer(0);
                samples[3] = (samples[3] << 8) + SPI.transfer(0);
            }
            else
            {
                samples[3] = 0;
            }
            samples_ready = true;
            */
            break;
        case ADC_FORMAT_SPI_TDM_FIXED:
            // Request data of channel 1.
            tmp = SPI.transfer(0);
            tmp = (tmp << 8) + SPI.transfer(0);
            tmp = (tmp << 8) + SPI.transfer(0);
            samples[0] = convert_24bit_to_int(tmp);

            // Request data of channel 2.
            tmp = SPI.transfer(0);
            tmp = (tmp << 8) + SPI.transfer(0);
            tmp = (tmp << 8) + SPI.transfer(0); 
            samples[1] = convert_24bit_to_int(tmp);

            // Request data of channel 3.
            tmp = SPI.transfer(0);
            tmp = (tmp << 8) + SPI.transfer(0);
            tmp = (tmp << 8) + SPI.transfer(0); 
            samples[2] = convert_24bit_to_int(tmp);

            // Request data of channel 4.
            tmp = SPI.transfer(0);
            tmp = (tmp << 8) + SPI.transfer(0);
            tmp = (tmp << 8) + SPI.transfer(0);
            //tmp = (uint32_t) counter;
            samples[3] = convert_24bit_to_int(tmp);
            counter += 10;

            if (counter == 8388606)
            {
                counter = 65536;
            }


            digitalWrite(49, HIGH);
            samples_ready = true;
            break;
    }
}



void
RwAdc::enable_channel(uint8_t channel_number)
{
    uint8_t pwdn = 0;
    if(channel_number == 1)
    {
        channel1_enable = 1;
    }
    else if(channel_number == 2)
    {
        channel2_enable = 1;
    }
    else if(channel_number == 3)
    {
        channel3_enable = 1;
    }
    else if(channel_number == 4)
    {
        channel4_enable = 1;
    }
    pwdn = ((channel1_enable << PCF_PORT_PWDN1) | (channel2_enable << PCF_PORT_PWDN2) | (channel3_enable << PCF_PORT_PWDN3) | (channel4_enable << PCF_PORT_PWDN4));
    Wire.beginTransmission(pwdn_port_expander);
    Wire.write(pwdn);
    Wire.endTransmission(pwdn_port_expander);
}


void
RwAdc::disable_channel(uint8_t channel_number)
{
    uint8_t pwdn = 0;
    if(channel_number == 1)
    {
        channel1_enable = 0;
    }
    else if(channel_number == 2)
    {
        channel2_enable = 0;
    }
    else if(channel_number == 3)
    {
        channel3_enable = 0;
    }
    else if(channel_number == 4)
    {
        channel4_enable = 0;
    }
    pwdn = ((channel1_enable << PCF_PORT_PWDN1) | (channel2_enable << PCF_PORT_PWDN2) | (channel3_enable << PCF_PORT_PWDN3) | (channel4_enable << PCF_PORT_PWDN4));
    Wire.beginTransmission(pwdn_port_expander);
    Wire.write(pwdn);
    Wire.endTransmission(pwdn_port_expander);
}


void
RwAdc::set_gain(uint8_t channel_number, pga_gain_t gain)
{
    switch (channel_number)
    {
        case 1:
            pga1.set_gain(gain);
            break;
        case 2:
            pga2.set_gain(gain);
            break;
        case 3:
            pga3.set_gain(gain);
            break;
        case 4:
            pga4.set_gain(gain);
            break;
    }
}

void
RwAdc::set_mode(adc_mode_t adc_mode)
{
    uint8_t clkdiv_pin_val;
    uint8_t mode0_pin_val;
    uint8_t mode1_pin_val;
    mode = adc_mode;
    switch (adc_mode)
    {
        case ADC_MODE_HIGHSPEED:
            clkdiv_pin_val = 1;
            mode0_pin_val = 0;
            mode1_pin_val = 0;
            clkdiv = ADC_CLKDIV_HIGHSPEED_1;
            oversampling = ADC_OVERSAMPLING;
            break;
        case ADC_MODE_HIGHRES:
            clkdiv_pin_val = 1;
            mode0_pin_val = 1;
            mode1_pin_val = 0;
            clkdiv = ADC_CLKDIV_HIGHRES_1;
            oversampling = ADC_OVERSAMPLING_HIGH_RES;
            break;
        case ADC_MODE_LOWPOW_CLKDIV_0:
            clkdiv_pin_val = 0;
            mode0_pin_val = 0;
            mode1_pin_val = 1;
            clkdiv = ADC_CLKDIV_LOWPOW_0;
            oversampling = ADC_OVERSAMPLING;
            break;
        case ADC_MODE_LOWPOW_CLKDIV_1:
            clkdiv_pin_val = 1;
            mode0_pin_val = 0;
            mode1_pin_val = 1;
            clkdiv = ADC_CLKDIV_LOWPOW_1;
            oversampling = ADC_OVERSAMPLING;
            break;
        case ADC_MODE_LOWSPEED_CLKDIV_0:
            clkdiv_pin_val = 0;
            mode0_pin_val = 1;
            mode1_pin_val = 1;
            clkdiv = ADC_CLKDIV_LOWSPEED_0;
            oversampling = ADC_OVERSAMPLING;
            break;
        case ADC_MODE_LOWSPEED_CLKDIV_1:
            clkdiv_pin_val = 1;
            mode0_pin_val = 1;
            mode1_pin_val = 1;
            clkdiv = ADC_CLKDIV_LOWSPEED_1;
            oversampling = ADC_OVERSAMPLING;
            break;
    }

    digitalWrite(ADC_CLKDIV, clkdiv_pin_val);
    digitalWrite(ADC_MODE0, mode0_pin_val);
    digitalWrite(ADC_MODE1, mode1_pin_val);

}


void
RwAdc::set_output_format(adc_output_format_t adc_output_format)
{
    uint8_t format_pin_val;
    output_format = adc_output_format;

    switch (output_format)
    {
        case ADC_FORMAT_SPI_TDM_DYNAMIC:
            format_pin_val = 0;
            break;
        case ADC_FORMAT_SPI_TDM_FIXED:
            format_pin_val = 1;
            break;
    }

    digitalWrite(ADC_FORMAT0, format_pin_val);
}


uint32_t
RwAdc::get_clk_freq(uint16_t adc_sampling_rate)
{
    uint32_t clk;
    sampling_rate = adc_sampling_rate;
    clk = (uint32_t)sampling_rate * (uint32_t)clkdiv * (uint32_t)oversampling;
    return clk;
}


int32_t
RwAdc::convert_24bit_to_int(uint32_t value)
{
    uint8_t sign_bit;

    sign_bit = (value >> 23) & 1;

    if (sign_bit == 1)
    {
        // Handle negative values.
        //value = value - 1;
        //value = (~value) & (uint32_t) 0xFFFFFF;
        //return (int32_t) value * (int32_t) -1;
        return (int32_t) (value | (uint32_t) 0xFF000000);
    }
    else
    {
        // Handle positive values.
        return (int32_t) value;
    }

}



