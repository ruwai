// -*- coding: utf-8 -*-
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// pSysmon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef ruwai_h
#define ruwai_h

#include <Arduino.h>

//#define DEBUG

#ifdef DEBUG
    #define debug_print(x) Serial.println(x)
    #define debug_print_hex(x) Serial.println(x, HEX)
#else
    #define debug_print(x)
    #define debug_print_hex(x)
#endif

/***********************************************************/
// The software version.
#define VERSION "0.1.2"


/***********************************************************/
// The Ruwai PCF8574 and PCF8574A port expander addresses.
// This are the 7-bit addresses.
#define S1_PCF_PGA1 0x20
#define S1_PCF_PGA2 0x21
#define S1_PCF_PGA3 0x22
#define S1_PCF_PGA4 0x23
#define S1_PCF_ADC 0x38


/***********************************************************/
// Define some pin mappings.
#define ONEWIREBUS 36

/***********************************************************/
// The allowed sampling rates.
#define RW_SPS_100 100
#define RW_SPS_200 200
#define RW_SPS_300 300
#define RW_SPS_400 400
#define RW_SPS_500 500
#define RW_SPS_600 600
#define RW_SPS_700 700
#define RW_SPS_800 800
#define RW_SPS_900 900
#define RW_SPS_1000 1000
#define RW_SPS_2000 2000


/************************************************************/
// The settings for the UART communication.
#define GPS_DEFAULT_BAUDRATE 9600
// According to the baud rate calculator http://wormfood.net/avrbaudcalc.php,
// the following baud rates accepted by the ublox should work: 4800, 9600,
// 19200 and 38400. The 57600 and 115200 rates are above the recommended error
// but not above the maximum error.
#define GPS_BAUDRATE 38400  
#define GPS_USB_BAUDRATE 115200  
#define USB_SERIAL_BAUDRATE 576000
//#define RUWAI_SERIAL_BAUDRATE 500000
#define RUWAI_SERIAL_BAUDRATE 1000000
//#define USB_SERIAL_BAUDRATE 1152000


#endif
