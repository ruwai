// -*- coding: utf-8 -*-
// LICENSE
//
// This file is part of ruwai.
//
// If you use ruwai in any program or publication, please inform and
// acknowledge its author Stefan Mertl (stefan@mertl-research.at).
//
// pSysmon is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <ruwai.h>
#include <rw_control.h>
#include <gps_ublox.h>
#include <ubx_protocol.h>
#include <rw_protocol.h>
#include <rw_pga281.h>
#include <timer16.h>
#include <rw_adc.h>
#include <SPI.h>
#include <Wire.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <avr/wdt.h>
#include <avr/sleep.h>
#include <avr/power.h>
#include <rw_serial.h>

void data_ready_isr(void);
void gps_pps_isr(void);
void send_samples(void);
void send_samples_with_timestamp(void);
void record_loop(void);
void control_loop(void);
void set_control_mode(void);
void set_record_mode(void);
void arduino_reconnect(void);
void connect_gps(void);
void init_state(void);
void init_thermometer(void);
void sleep_now(void);
void sync_adc(void);
//void send_ruwai_message(uint8_t msg_class, uint8_t msg_id, void* msg, uint8_t length);

RuwaiSerial &usb_serial = RwSerial0;
RuwaiSerial &gps_serial = RwSerial2;
RuwaiSerial &ruwai_serial = RwSerial3;

// Setup a oneWire bus.
OneWire one_wire(ONEWIREBUS);
DallasTemperature ds_sensors(&one_wire);
DeviceAddress ds_gts;


uint8_t led = 13;
uint8_t count = 0;
uint8_t gps_byte_available = 0;
uint8_t ruwai_byte_available = 0;
volatile bool record_mode = false;
volatile bool control_mode = false;
volatile bool adc_data_ready = false;
volatile bool samples_ready = false;
volatile bool pps_set = false;
volatile bool timestamp_set = false;
volatile uint8_t ts_marker_id = 0;
volatile uint32_t sec_slts = 0;             // Seconds since last timestamp.

// Debug output pins.
bool pin_state_46 = false;
bool pin_state_47 = false;
bool pin_state_48 = false;
bool pin_state_49 = false;

RuwaiControl ruwai_control = RuwaiControl(ruwai_serial);
GpsLea6T gps = GpsLea6T(gps_serial);
//GpsLea6T gps = GpsLea6T(usb_serial);
RwAdc adc = RwAdc(S1_PCF_ADC, S1_PCF_PGA1, S1_PCF_PGA2, S1_PCF_PGA3, S1_PCF_PGA4);
//Timer16 &timer1 = Timer1;

uint8_t test_counter = 0;
uint16_t dbg_sample_counter = 0;

void setup(void) {
    // Clear the watchdog reset flag and enable the watchdog.
    MCUSR = 0;
    wdt_enable(WDTO_8S);

    // Clear the interrupt service routines.
    detachInterrupt(ADC_DRDY_INTERRUPT);
    detachInterrupt(GPS_TP_INTERRUPT);

    String status_msg;

    // Activate the debug serial output on Serial0.
    usb_serial.begin(USB_SERIAL_BAUDRATE);
    usb_serial.println("SETUP");

    // Initialize the output pins.
    pinMode(led, OUTPUT);
    digitalWrite(led, LOW);
    pinMode(46, OUTPUT);
    digitalWrite(46, pin_state_46);
    pinMode(47, OUTPUT);
    digitalWrite(47, pin_state_47);
    pinMode(48, OUTPUT);
    digitalWrite(48, pin_state_48);
    pinMode(49, OUTPUT);
    digitalWrite(49, pin_state_48);
    wdt_reset();


    // Initialize serial port used for communication with the BBB.
    ruwai_control.start_serial(RUWAI_SERIAL_BAUDRATE);
    wdt_reset();

    // Handshake with ruwaicom running on Beaglebone.
    usb_serial.println("Waiting for handshake...");
    bool handshake_success = ruwai_control.handshake();
    if (handshake_success)
    {
        usb_serial.println("handshake success.");
        ruwai_control.send_status(RW_STATUS_NOTICE, "Handshake with BBB established.");
    }


    status_msg = "Hello, Arduino Stack is speaking. Running on version ";
    status_msg += VERSION;
    ruwai_control.send_status(RW_STATUS_NOTICE, status_msg);
    wdt_reset();

    // Initialize the I2C communication.
    Wire.begin();

    // Initialize ADC. By default all channels are enabled.
    usb_serial.println("Init ADC");
    ruwai_control.send_status(RW_STATUS_NOTICE, "Init ADC.");
    adc.begin(ADC_MODE_LOWSPEED_CLKDIV_1, ADC_FORMAT_SPI_TDM_FIXED);
    wdt_reset();


    usb_serial.println("Connect to GPS");
    ruwai_control.send_status(RW_STATUS_NOTICE, "Connect to GPS.");
    connect_gps();
    wdt_reset();

    usb_serial.println("Initializing the thermometer.");
    init_thermometer();
    wdt_reset();

    /* 
    gps.set_timepulse2_frequency(2048000, 2048000);
    set_record_mode();
    */

    // Activate the control mode.
    set_control_mode();
    wdt_reset();

    // Syncing the ADC might block because of a missing PPS interrupt. Call it
    // after the watchdog has been enabled.
    sync_adc();
    wdt_reset();

    // TODO: Get the ADC configuration from ruwaicom.
    //adc.begin(ADC_MODE_LOWSPEED_CLKDIV_0, ADC_FORMAT_SPI_TDM_FIXED);
    //adc.begin(ADC_MODE_HIGHRES, ADC_FORMAT_SPI_TDM_FIXED);
    //adc.begin(ADC_MODE_LOWPOW_CLKDIV_1, ADC_FORMAT_SPI_TDM_FIXED);
    //adc.disable_channel(1);
    //adc.disable_channel(2);
    //adc.disable_channel(3);
    //adc.disable_channel(4);

    //clk_freq = adc.get_clk_freq(800);

}

void init_state(void)
{
    record_mode = false;
    control_mode = false;
    adc_data_ready = false;
    samples_ready = false;
    pps_set = false;
    timestamp_set = false;
}

void connect_gps(void)
{
    bool success = false;
    String status_msg;

    // Check for correct baud rate.
    usb_serial.println("Start testing the baud rates:");
    ruwai_control.send_status(RW_STATUS_NOTICE, "Start testing the baud rates.");
    uint32_t detected_baudrate = 0;
    uint32_t working_baudrate = 0;

    while (detected_baudrate == 0)
    {
        detected_baudrate = gps.detect_baud_rate(GPS_DEFAULT_BAUDRATE, GPS_BAUDRATE);
        usb_serial.println("Detected baudrate:");
        usb_serial.println(detected_baudrate);
        status_msg = "Detected baud rate: ";
        status_msg += detected_baudrate;
        ruwai_control.send_status(RW_STATUS_NOTICE, status_msg);
    }

    usb_serial.println("Starting GPS serial with detected baud rate.");
    ruwai_control.send_status(RW_STATUS_NOTICE, "Starting GPS serial with detected baud rate.");
    bool gps_serial_up = gps.start_serial(detected_baudrate);
    if (gps_serial_up)
    {
        usb_serial.println("Established serial connection with GPS.");
        ruwai_control.send_status(RW_STATUS_NOTICE, "Established serial connection with GPS.");
    }
    else
    {
        usb_serial.println("The serial connection to the GPS is not working.");
        ruwai_control.send_status(RW_STATUS_NOTICE, "The serial connection to the GPS is not working.");
    }

    usb_serial.println("Disabling the default messages.");
    ruwai_control.send_status(RW_STATUS_NOTICE, "Disabling the default messages.");
    uint8_t ack_status = gps.disable_default_messages();
    usb_serial.println("ack_status");
    usb_serial.println(ack_status, BIN);
    usb_serial.println("-------");
    status_msg = "Message disable ack status: ";
    status_msg += String(ack_status, BIN);
    ruwai_control.send_status(RW_STATUS_NOTICE, status_msg);

    usb_serial.println("Disabling the timepulse2 frequency.");
    ruwai_control.send_status(RW_STATUS_NOTICE, "Disabling the timepulse2 frequency.");
    success = gps.disable_timepulse2_frequency();
    if (success)
    {
        usb_serial.println("success");
        ruwai_control.send_status(RW_STATUS_NOTICE, "Succesfully disabled the timepulse 2 frequency.");
    }
    else
    {
        usb_serial.println("no confirmation");
        ruwai_control.send_status(RW_STATUS_NOTICE, "No confirmation for disabling the timepulse 2 frequency.");
    }

    // Configure the baudrate of the GPS module.
    if (detected_baudrate != GPS_BAUDRATE)
    {
        working_baudrate = GPS_BAUDRATE;

        usb_serial.println("Setting the new GPS baudrate.");
        status_msg = "Setting the GPS baud rate to the desired value of ";
        status_msg += working_baudrate;
        ruwai_control.send_status(RW_STATUS_NOTICE, status_msg);
        gps.set_uart1_baudrate(GPS_BAUDRATE);

        usb_serial.println("Stopping the gps serial.");
        ruwai_control.send_status(RW_STATUS_NOTICE, "Stopping the GPS serial.");
        gps.stop_serial();

        usb_serial.println("Reconnect using the new baud rate.");
        ruwai_control.send_status(RW_STATUS_NOTICE, "Reconnect using the new baud rate.");

        while (!gps.start_serial(GPS_BAUDRATE))
        {
            gps.stop_serial();
            usb_serial.println("Stopped the serial.");
        }

        usb_serial.println("GPS connection with new baud rate established.");
        ruwai_control.send_status(RW_STATUS_NOTICE, "GPS connection with the new baud rate established.");
    }
    else
    {
        working_baudrate = detected_baudrate;
        status_msg = "The detected baud rate is already the desired baud rate of ";
        status_msg += GPS_BAUDRATE;
        ruwai_control.send_status(RW_STATUS_NOTICE, status_msg);
    }

    status_msg = "Communicating with the GPS using baud rate ";
    status_msg += working_baudrate;
    ruwai_control.send_status(RW_STATUS_NOTICE, status_msg);


    // Enable the needed UBX messages.
    // TODO: Save the succes in the bitfield.
    // TODO: Make the POSLLH interval user selectable.
    ruwai_control.send_status(RW_STATUS_NOTICE, "Enabling the GPS UBX messages.");
    gps.enable_message(UBX_CLASS_NAV, UBX_NAV_ID_STATUS, 1);
    gps.enable_message(UBX_CLASS_NAV, UBX_NAV_ID_TIMEUTC, 1);
    gps.enable_message(UBX_CLASS_NAV, UBX_NAV_ID_POSLLH, 240);
    gps.enable_message(UBX_CLASS_TIM, UBX_TIM_ID_TP, 1);

    // Configure the timepulse 1.
    ruwai_control.send_status(RW_STATUS_NOTICE, "Setting the timepulse 1.");
    gps.set_timepulse1_pps();

    // Initialize the PPS interrupt.
    attachInterrupt(GPS_TP_INTERRUPT, gps_pps_isr, RISING);


    // Configure the USB Port.
    ruwai_control.send_status(RW_STATUS_NOTICE, "Disabling the GPS USB port.");
    gps.disable_usb();

    ruwai_control.send_status(RW_STATUS_NOTICE, "Finished configuration of the GPS.");
}

void
init_thermometer(void)
{
    // Connect to the DS18B20 thermometer.
    usb_serial.println("Locating oneWire devices...");
    ds_sensors.begin();
    usb_serial.print("Found ");
    usb_serial.print(ds_sensors.getDeviceCount(), DEC);
    usb_serial.println(" devices.");
    if (!ds_sensors.getAddress(ds_gts, 0))
    {
        usb_serial.println("Unable to find address for OneWire sensor 0.");
        ruwai_control.send_status(RW_STATUS_ERROR, "Unable to find address for OneWire sensor 0.");
    }
    // Set the sensor resolution to 10 bit (0.25 degree).
    ds_sensors.setResolution(ds_gts, 10);
    int resolution = ds_sensors.getResolution(ds_gts);
    usb_serial.print("Device 0 Resolution: ");
    usb_serial.println(resolution, DEC); 
    usb_serial.print("Requesting temperature...");
    ds_sensors.requestTemperaturesByAddress(ds_gts);
    usb_serial.println("...done.");
    float temp_c = ds_sensors.getTempC(ds_gts);
    usb_serial.print("Temperature: ");
    usb_serial.println(temp_c);

    rw_soh_envdata_t msg;
    msg.temp_gts = (int32_t) (temp_c * 100);
    ruwai_control.send_message(RW_CLASS_SOH, RW_SOH_ID_ENVDATA, &msg, sizeof(msg));

}


void arduino_reconnect(void)
{
    String status_msg;

    wdt_reset();
    ruwai_control.send_status(RW_STATUS_NOTICE, "Starting reconnection....");

    // Clear the interrupt service routines.
    detachInterrupt(ADC_DRDY_INTERRUPT);
    detachInterrupt(GPS_TP_INTERRUPT);

    /*
    usb_serial.println("Disable the Watchdog Timer.");
    ruwai_control.send_status(RW_STATUS_NOTICE, "Disabling the watchdog timer.");
    wdt_disable();
    */
    usb_serial.println("Initialize the state.");
    ruwai_control.send_status(RW_STATUS_NOTICE, "Initialize the ruwai state.");
    init_state();
    wdt_reset();

    usb_serial.println("Initialize ADC.");
    ruwai_control.send_status(RW_STATUS_NOTICE, "Initialize the ADC.");
    //adc.end();
    adc.begin(ADC_MODE_LOWSPEED_CLKDIV_1, ADC_FORMAT_SPI_TDM_FIXED);
    wdt_reset();

    usb_serial.println("RECONNECT");
    ruwai_control.send_status(RW_STATUS_NOTICE, "Reconnect to GPS.");
    gps.init_state();
    gps.stop_serial();
    connect_gps(); 
    wdt_reset();

    usb_serial.println("Reset the Ruwai Control.");
    ruwai_control.send_status(RW_STATUS_NOTICE, "Reset the ruwai control.");
    ruwai_control.reset();
    wdt_reset();

    usb_serial.println("Waiting for handshake.");
    bool handshake_success = ruwai_control.handshake();
    if (handshake_success)
    {
        usb_serial.println("Handshake success."); 
        ruwai_control.send_status(RW_STATUS_NOTICE, "Handshake with BBB established.");
    }

    status_msg = "Hello, Arduino Stack is back. Running on version ";
    status_msg += VERSION;
    ruwai_control.send_status(RW_STATUS_NOTICE, status_msg);
    wdt_reset();

    init_thermometer();
    wdt_reset();

    set_control_mode();
    wdt_reset();

    // Syncing the ADC might block because of a missing PPS interrupt. Call it
    // after the watchdog has been enabled.
    sync_adc();
    wdt_reset();

}


void set_control_mode(void)
{
    usb_serial.println("ENTER CONTROL MODE");
    control_mode = true;
    record_mode = false;

    detachInterrupt(ADC_DRDY_INTERRUPT);
    ruwai_control.send_status(RW_STATUS_NOTICE, "Control mode activated.");
}


void set_record_mode(void)
{
    usb_serial.println("ENTER RECORD MODE");
    control_mode = false;
    record_mode = true;

    //digitalWrite(47, LOW);

    attachInterrupt(ADC_DRDY_INTERRUPT, data_ready_isr, FALLING);
    ruwai_control.send_status(RW_STATUS_NOTICE, "Record mode activated.");
}


void sleep_now(void)
{
    //set_sleep_mode(SLEEP_MODE_STANDBY);
    set_sleep_mode(SLEEP_MODE_IDLE);

    // Disable peripherals that are not needed.
    //power_spi_disable();
    power_adc_disable();
    power_timer0_disable();
    power_timer1_disable();
    power_twi_disable();

    cli();
    if (!adc_data_ready)
    {
        sleep_enable();
        sei(); 
        sleep_cpu();
        sleep_disable();
    }
    sei();

    // Upon waking up.
    power_all_enable();
}

void sync_adc(void)
{
    uint32_t clk_freq;
    float t_syn;
    uint32_t t_syn_u;

    // Start the ADC. Sync to PPS.
    clk_freq = adc.get_clk_freq(ruwai_control.rcv_cfg_sps.sps);
    t_syn = 1 / clk_freq;
    t_syn_u = (uint32_t) ceil(t_syn * 1e6);
    if (t_syn_u == 0)
    {
        t_syn_u = 1;
    }

    // Wait for a PPS interrupt.
    while(!pps_set)
    {
    }
    digitalWrite(ADC_SYNC, LOW);
    delayMicroseconds(2 * t_syn_u);
    pps_set = false;
    digitalWrite(47, LOW);

    // Sync the ADC after the next received PPS.
    while(!pps_set)
    {
    }    
    digitalWrite(ADC_SYNC, HIGH);
    pps_set = false;

}


void loop(void)
{
    // Reset the watchdog.
    wdt_reset();

    if (control_mode)
    {
        control_loop();
    }
    else if (record_mode)
    {
        record_loop();
    }
}




void control_loop(void)
{
    uint32_t clk_freq;
    bool ruwai_parsed = false;
    String status_msg;

    ruwai_byte_available = RwSerial3.available();
    if (ruwai_byte_available > 0)
    {
        ruwai_parsed = ruwai_control.read();
    }

    if (ruwai_parsed)
    {
        if (ruwai_control.rcv_cfg_sps_changed)
        {
            // Set the new frequency and wait some time for the frequency to
            // settle.
            clk_freq = adc.get_clk_freq(ruwai_control.rcv_cfg_sps.sps);
            gps.set_timepulse2_frequency(clk_freq, clk_freq);
            delay(1000);

            ruwai_control.rcv_cfg_sps_changed = false;
            status_msg = "Set clock frequency to ";
            status_msg += clk_freq;
            status_msg += ".";
            ruwai_control.send_status(RW_STATUS_NOTICE, status_msg);
            ruwai_control.ack_msg(RW_CLASS_CFG, RW_CFG_ID_SPS);
        }

        if (ruwai_control.rcv_cfg_channels_changed)
        {
            if (ruwai_control.rcv_cfg_channels.active.channel1)
                adc.enable_channel(1);
            else
                adc.disable_channel(1);

            if (ruwai_control.rcv_cfg_channels.active.channel2)
                adc.enable_channel(2);
            else
                adc.disable_channel(2);

            if (ruwai_control.rcv_cfg_channels.active.channel3)
                adc.enable_channel(3);
            else
                adc.disable_channel(3);

            if (ruwai_control.rcv_cfg_channels.active.channel4)
                adc.enable_channel(4);
            else
                adc.disable_channel(4);

            adc.set_gain(1, (pga_gain_t)ruwai_control.rcv_cfg_channels.gain_channel1);
            adc.set_gain(2, (pga_gain_t)ruwai_control.rcv_cfg_channels.gain_channel2);
            adc.set_gain(3, (pga_gain_t)ruwai_control.rcv_cfg_channels.gain_channel3);
            adc.set_gain(4, (pga_gain_t)ruwai_control.rcv_cfg_channels.gain_channel4);
            ruwai_control.rcv_cfg_channels_changed = false;
            ruwai_control.ack_msg(RW_CLASS_CFG, RW_CFG_ID_CHANNELS);
        }

        if (ruwai_control.rcv_cfg_mode_changed)
        {
            if (ruwai_control.rcv_cfg_mode.mode == 0)
            {
                set_control_mode();
            }
            else if (ruwai_control.rcv_cfg_mode.mode == 1)
            {
                set_record_mode();
            }

            ruwai_control.rcv_cfg_mode_changed = false;
            ruwai_control.ack_msg(RW_CLASS_CFG, RW_CFG_ID_MODE);
        }

        if (ruwai_control.rcv_cfg_gps_changed)
        {
            if (ruwai_control.rcv_cfg_gps.ports.usb == 0)
            {
                gps.disable_usb();
            }
            else if (ruwai_control.rcv_cfg_gps.ports.usb == 1)
            {
                gps.enable_usb();
            }

            ruwai_control.rcv_cfg_gps_changed = false;
            ruwai_control.ack_msg(RW_CLASS_CFG, RW_CFG_ID_GPS);
        }

        if (ruwai_control.rcv_ack_sync_changed)
        {
            usb_serial.println("sync in control mode");
            ruwai_control.send_status(RW_STATUS_NOTICE, "Received sync request in control mode.");
            arduino_reconnect();
        }

        if (ruwai_control.rcv_soh_request_changed)
        {
            usb_serial.print("Received a SOH request for msg_id ");
            usb_serial.println(ruwai_control.rcv_soh_request.msg_id);
            ruwai_control.rcv_soh_request_changed = false;
        }
    }
}


void record_loop(void)
{
    bool gps_parsed = false;
    bool ruwai_parsed = false;
    bool bbb_serial_free = false;
    bool wait_for_samples = false;
    String status_msg;

    // Enter sleep mode if no adc_data_ready interrupt has occured.
    if (!adc_data_ready)
    {
        //usb_serial.println("sleep");
        sleep_now();
        /*
        usb_serial.print(adc_data_ready);
        usb_serial.print(";");
        usb_serial.print(samples_ready);
        usb_serial.print(";");
        usb_serial.println(pps_set);
        */
    }

    /***************************************************
    // Get the data from the ADC.
    ***************************************************/
    if (adc_data_ready)
    {
        if (samples_ready)
        {
            usb_serial.println("race condition sr");
        }
        adc.request_data();
        adc_data_ready = false;
        wait_for_samples = true;
        digitalWrite(48, LOW);
    }

    /***************************************************
    // Send the samples to the serial port.
    ***************************************************/
    if (wait_for_samples && !samples_ready)
    {
        usb_serial.println("samples_ready not set");
        wait_for_samples = false;
    }

    if (samples_ready)
    {
        dbg_sample_counter++;

        if (pps_set && timestamp_set) 
        {
            send_samples_with_timestamp();
            timestamp_set = false;
            pps_set = false;
            digitalWrite(46, LOW);
            digitalWrite(47, LOW);
            sec_slts++;
        }
        // TODO: Handle the situation where the PPS is set, but no timestamp
        // was issued by the GPS. The PPS of the GPS has to be set to active
        // also if no GPS-Lock is available. Try to figure out how to get a
        // valid timestamp within the limits of the GPS-clock drift using
        // either the GPS time information itself (e.g. internal RTC) or the
        // RTC of the GPS Timing Shield. It would be great to get the time from
        // the GPS. This would enable the removal of the RTC components from
        // the GPS timing shield.
        else if (pps_set)
        {
            send_samples_with_timestamp();
            pps_set = false;
            digitalWrite(47, LOW);
            digitalWrite(47, HIGH);
            digitalWrite(47, LOW);
            sec_slts++;
        }
        else 
        {        
            send_samples();
        }
        samples_ready = false;
        bbb_serial_free = true;
        /*
        // Testing the ADC group delay.
        if (adc.samples[0] >= 0) {
            PORTB |= _BV(PB7);
        }
        else {
            PORTB &= ~_BV(PB7);
        }
        // End testing the ADC group delay.
        */
        digitalWrite(49, LOW);
    }


    /***************************************************
    // Check and parse bytes from the GPS.
    ***************************************************/
    gps_byte_available = gps_serial.available();
    if (gps_byte_available > 0)
    {
        gps_parsed = gps.read();
    }


    /***************************************************
    // Copy the timestamp from the GPS buffer to the 
    // timestamp variable.
    ***************************************************/
    if (gps.timestamp_init)
    {
        if (!gps.timestamp_buf_empty && gps.timestamp_empty)
        {
            gps.next_timepulse = gps.next_timepulse_buf;
            gps.timestamp_buf_empty = true;
            gps.timestamp_empty = false;
            timestamp_set = true;
            digitalWrite(46, HIGH);
            sec_slts = 0;
        }
    }
    else
    {
        //usb_serial.println("Timestamp not init.");
    }

    if (gps.timestamp_race_error)
    {
        usb_serial.println("ERROR - timestamp race condition.");
        gps.timestamp_race_error = false;
    }




    /***************************************************
    // React on the updated GPS navigation status.
    ***************************************************/
    if (gps.nav_status_changed)
    {
        gps.nav_status_changed = false;
    }


    /***************************************************
    // React on the updated GPS UTC time.
    ***************************************************/
    if (gps.nav_timeutc_changed)
    {
        // Send the timestamp paket using the timestamp marker id.
        /*
        rw_smp_utc_time_t msg;
        msg.id = ts_marker_id - 1;
        msg.year = gps.timeutc.year;
        msg.month = gps.timeutc.month;
        msg.day = gps.timeutc.day;
        msg.hour = gps.timeutc.hour;
        msg.min = gps.timeutc.min;
        msg.sec = gps.timeutc.sec;
        msg.validutc = gps.timeutc.valid.validutc;
        msg.gps_fix = gps.get_gps_fix();
        msg.gps_fix_ok = gps.get_gps_fix_ok();
        send_ruwai_message(RW_CLASS_SMP, RW_SMP_ID_UTC_TIME, &msg, sizeof(msg));
        */
        gps.nav_timeutc_changed = false;
    }


    /***************************************************
    // React on the updated GPS position.
    ***************************************************/
    if (gps.nav_position_changed && bbb_serial_free)
    {
        //status_msg = "[gps_pos] ";
        //status_msg = status_msg + gps.nav_position.lon + "," + gps.nav_position.lat + "," + gps.nav_position.height + "," + gps.nav_position.h_msl + "," + gps.nav_position.h_acc + "," + gps.nav_position.v_acc;
        rw_soh_gpspos_t msg;
        msg.i_tow = gps.nav_position.i_tow;
        msg.lon = gps.nav_position.lon;
        msg.lat = gps.nav_position.lat;
        msg.height = gps.nav_position.height;
        ruwai_control.send_message(RW_CLASS_SOH, RW_SOH_ID_GPSPOS, &msg, sizeof(msg));
        gps.nav_position_changed = false;
    }


    /***************************************************
    // Handle the bytes available at the ruwai control
    // port.
    ***************************************************/
    ruwai_byte_available = RwSerial3.available();
    if (ruwai_byte_available > 0)
    {
        ruwai_parsed = ruwai_control.read();
    }

    if (ruwai_parsed)
    {
        if (ruwai_control.rcv_ack_sync_changed)
        {
            debug_print("sync in record mode");
            ruwai_control.send_status(RW_STATUS_NOTICE, "Received sync request in record mode.");
            arduino_reconnect();
        }
        /*
        String msg = "timepulse2 frequency: ";
        msg.concat(gps.get_timepulse2_frequency());
        usb_serial.println(msg);
        msg = "timepulse2 frequency locked: ";
        msg.concat(gps.get_timepulse2_frequency_locked());
        usb_serial.println(msg);
        parsed = false;
        */
    }
}


void
data_ready_isr()
{
    //pin_state_48 = !pin_state_48;
    if (adc_data_ready)
    {
        digitalWrite(48, HIGH);
        /*
        usb_serial.println("adr race");
        usb_serial.print(adc_data_ready);
        usb_serial.print(";");
        usb_serial.print(samples_ready);
        usb_serial.print(";");
        usb_serial.println(pps_set);
        */
    }
    adc_data_ready = true;
    /*
    // Check the isr using the Arduino builtin LED.
    if (count == 10)
    {
        PORTB ^= _BV(PB7);
        count = 0;
    }
    else
    {
        count++;
    }
    */
    //usb_serial.println("DRDY");
}

void
gps_pps_isr()
{
    digitalWrite(47, HIGH);
    pps_set = true;
    //usb_serial.println("PPS");
}


void
send_samples()
{
    rw_smp_24bit_t msg;
    /*
    msg.samples[0] = (int32_t) test_counter;
    msg.samples[1] = (int32_t) test_counter;
    msg.samples[2] = (int32_t) test_counter;
    msg.samples[3] = (int32_t) test_counter;
    test_counter++;
    if(test_counter == 100)
    {
        test_counter = 0;
    }
    */
    msg.samples[0] = adc.samples[0];
    msg.samples[1] = adc.samples[1];
    msg.samples[2] = adc.samples[2];
    msg.samples[3] = adc.samples[3];

    ruwai_control.send_message(RW_CLASS_SMP, RW_SMP_ID_24BIT, &msg, sizeof(msg));
}

void
send_samples_with_timestamp()
{
    rw_smp_24bit_tsp_t msg;
    msg.week = gps.next_timepulse.week;
    msg.tow_ms = gps.next_timepulse.tow_ms;
    msg.tow_sub_ms = gps.next_timepulse.tow_sub_ms;
    msg.sec_slts = sec_slts;
    msg.flags.utc = gps.next_timepulse.flags.utc;
    msg.flags.timebase = gps.next_timepulse.flags.time_base;
    msg.flags.gps_fix = gps.get_gps_fix();
    msg.flags.gps_fix_ok = gps.get_gps_fix_ok();
    msg.samples[0] = adc.samples[0];
    msg.samples[1] = adc.samples[1];
    msg.samples[2] = adc.samples[2];
    msg.samples[3] = adc.samples[3];
    ruwai_control.send_message(RW_CLASS_SMP, RW_SMP_ID_24BIT_TSP, &msg, sizeof(msg));
    gps.timestamp_empty = true;

    dbg_sample_counter = 0;
}





// The timer1 interrupt service routine.
/*
ISR(TIMER1_OVF_vect)
{
    debug_print();
    debug_print("Polling GPS:");
    //gps.poll_cfg_prt_uart();
    //gps.poll_cfg_msg(NMEA_CLASS_STD, NMEA_STD_ID_GGA);
    gps.poll_cfg_tp5(TIMEPULSE2);
}
*/

// The timer1 interrupt service routine.
/*
ISR(TIMER1_OVF_vect)
{
    // Toggle the LED pin.
    // Use Arduino macros to get the correct port register.
    uint8_t port = digitalPinToPort(led);
    uint8_t bitmask = digitalPinToBitMask(led);
    volatile uint8_t *out;

    out = portOutputRegister(port);
    *out ^= bitmask;

    count++;

    // Directly access the LED pin of the mega2560.
    //PORTB ^= _BV(PB7);
}
*/
